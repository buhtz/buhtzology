# Buhtzology - My own toolbox

It's a prepared collection of modules and functions that are frequently used
in the projects and daily work of the project maintainer. This most often
relates to research projects and activities in Health and Nursing Science and
Data Science, but also beyond. In that understanding the package could be
described as the _personal toolbox_
of [Christian Buhtz](https://buhtz.codeberg.page/).

**Status of the project**: The package is constantly being developed and
improved as research activities progress. Development activity happens in the
`develop` branch.

**Branching model**:
[OneFlow](https://www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow)
(described by _Adam Ruka_) is used. The latest development state is reflected
by the primary branch named `develop`. The latest stable version (the
latest release) can be found in the branch `stable` or via the highest version
tag (e.g. `v0.1.2`) can be used.

# Table of contents
<!-- TOC start - via https://derlin.github.io/bitdowntoc/-->
- [Technologies](#technologies)
- [Documentation](#documentation)
- [Functionality at a glance](#functionality-at-a-glance)
    + [Reading and validation of data sources](#reading-and-validation-of-data-sources)
    + [Parallelizing work on pandas data frames](#parallizing-work-on-pandas-data-frames)
    + [And more...](#and-more)
- [Support & Feedback](#support-feedback)
- [Install](#install)
<!-- TOC end -->

# Technologies
 - [Python 3](https://python.org)
 - [`pandas`](https://pandas.pydata.org/)
 - [`seaborn`](https://seaborn.pydata.org/) and [`matplotlib`](https://matplotlib.org/)
 - [`python-docx`](https://python-docx.readthedocs.io)
 - Conventions & Standards
   - [Semantic Versioning](https://semver.org)
   - [Common Changelog](https://common-changelog.org),
   - [Conventional Commits](https://www.conventionalcommits.org)
   - [OneFlow](www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow)
     branching model

# Documentation
- [Release](https://buhtzology.readthedocs.io/en/stable) [![Documentation (release/stable)](https://readthedocs.org/projects/buhtzology/badge/?version=stable)](https://buhtzology.readthedocs.io/en/stable/)
- [Development](https://buhtzology.readthedocs.io/en/develop/) [![Documentation (latest/develop)](https://readthedocs.org/projects/buhtzology/badge/?version=develop)](https://buhtzology.readthedocs.io/en/develop)
- [Changelog](CHANGELOG.md)

The documentation is generated via
[`pydoctor`](https://pydoctor.readthedocs.io/en/latest/) and
hosted at [Read the Docs](https://readthedocs.io).

# Functionality at a glance
The following is a brief excerpt of the functionality. A complete description
can be found in the
[documentation](https://buhtzology.readthedocs.io/en/develop/).

### Reading and validation of data sources
While reading the `*.csv` or `*.xlsx` file some checks and validations can be
done; e.g. fields per row (for `*.csv` ), length of fields, valid and missing
values, etc.

Here is an example:

```python
from buhtzology import bandas
                         
specs_and_rules = {
    'ColumnA': 'str',
    'ColumnB': ('str', 'no answer'),
    'ColumnC': None,
    'ColumnD': (
        'Int16',
        -9, {
            'len': [1, 2, (4-8)],
            'val': [0, 1, (3-9)]
        }
    }
}

df = bandas.read_and_validate_csv('data.csv', specs_and_rules)
```

- ``ColumnA`` is of type `str`.
- ``ColumnB`` is of type `str` and the value ``no answer`` is treated as
  missing.
- ``ColumnC`` exist in the ``csv`` file but will be ignored while reading
  and won't be a part of the resulting data frame.
- ``ColumnD`` is of type `pandas.Int16Dtype`. The value ``-9`` is a
  missing. The field length can be ``1``, ``2`` or ``4`` to ``8``. Possible
  or valid values are ``0``, ``1``, ``3`` to ``9`` and the missing ``-9``.

### Parallelizing work on pandas data frames
Heavy work on one data frame can be parallelized
via `bandas.parallelize_job_on_dataframe()` very easy and without taking care
of process management. The data frame is automatically cut into pieces, send
to separate process and glued together when everything is done.

```python
from buhtzology import bandas

def the_worker(sub_dataframe):
    # Imagine something heavy here. ;)
    sub_dataframe.foo = 7

    return sub_dataframe

df = bandas.parallelize_job_on_dataframe(df, the_worker)
```

### And more...
Not everything can be described here (but in the
[documentation](https://buhtzology.readthedocs.io)).
And not everything is useful for others. That's why it is **my** toolbox.

# Support & Feedback
Everything can go to the
[Issues](https://codeberg.org/buhtz/buhtzology/issues) section. Beside
usual Bugs and Feature Requests all questions and discussions can go their. 

# Install
Please decide yourself if you want to install `buhtzology` system-wide or only
for the current user. I would recommend the latter.

You can use `pip` to install directly from upstream git repository without
explicit downloading or cloning.

```
$ pip3 install git+https://codeberg.org/buhtz/buhtzology.git
```
If `git` is not installed on your system use the archive.
```
$ pip3 install https://codeberg.org/buhtz/buhtzology/archive/main.zip
```
Depending on your operating system and your environment there are several
ways to invoke `pip`.
```
$ pip
$ pip3
$ python -m pip
$ python3 -m pip
$ py -3 -m pip
```

<sub>August 2023</sub>
