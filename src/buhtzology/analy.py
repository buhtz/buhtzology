"""Generate output for analysis in form of tables and figures.

For tables `pandas.DataFrame` is used. For figures ``seaborn`` or
other `matplotlib` based packages are used. The results are intended to be
used by `report`.
"""
# pylint: disable=too-many-lines
import logging
import uuid
from typing import Union, Callable, Tuple, Dict, Iterable
import copy
import statistics
import pandas


_log = logging.getLogger(__name__)
"""Handle to the logger."""

INDENTION_DEFAULT = 4
"""Spaces to indent, e.g. when indenting row labels (indexes)."""

LABEL_TOTAL = 'Total'
"""Default label used for total columns and rows."""

DEFAULT_AGGFUNC_LABELS = {
    statistics.mean: 'Mean',
    pandas.Series.mean: 'Mean',
    statistics.median: 'Median',
    pandas.Series.median: 'Median',
    statistics.stdev: 'SD',
    pandas.Series.std: 'SD',
    sum: 'Sum',
    pandas.Series.sum: 'Sum',
}
"""Labels used by default in `aggregate()`."""

# ----------
# -- Misc --
# ----------


def _total_label(total: Union[bool, str] = True) -> str:
    """Determine correct value for total column.

    Helper function to determine the correct value for the total column
    depending on the given argument value.
    Used by multiple functions in this module.

    Args:
        total: If ``True`` the value of `LABEL_TOTAL` is used and if it is of
            type `str` its own value is used. In all other cases ``None`` is
            returned.

    Returns:
        The total label as a string or ``None``.

    """
    if isinstance(total, str):
        return total

    if total is True:
        return LABEL_TOTAL

    return None


# -------------------------------
# -- Frequencies & percentages --
# -------------------------------

# pylint: disable-next=too-many-arguments,too-many-positional-arguments
def frequencies_and_percentages(data: pandas.Series,  # noqa: PLR0913
                                dropna: bool = False,
                                result_labels: Tuple[str] = ('n', '%'),
                                combine: str = '{} ({})',
                                reverse_columns: bool = False,
                                index: Union[bool, str] = False,
                                data_label: str = None,
                                sort_index: bool = None,
                                round_percentage_digits: int = 2,
                                ) -> pandas.DataFrame:
    """Frequencies and percentages for one variable only.

    It is a wrapper around `frequencies()` and `percentages()`.
    The index (labels of the row) by default contain the unique values
    specified by ``data`` (e.g. ``female`` and ``male``). Additionally an
    explanatory name (e.g. ``Gender``) can be added in to flavors to the
    index.  If ``index is True`` an *indented index* is created with row
    labels indented by four blank spaces (see `INDENTION_DEFAULT`). The same
    happens when ``index == 'indented'``.  If ``index == 'multi'`` a
    ``pandas.MultiIndex`` is created with the explanatory name in the first
    and the value labels in the second level.

    Output example from ``titanic`` dataset as markdown: ::

        |       |   n |       % |
        |:------|----:|--------:|
        | man   | 869 | 66.0334 |
        | women | 447 | 33.9666 |

    Example with ``index='multi'``: ::

        |                  |   n |       % |
        |:-----------------|----:|--------:|
        | ('sex', 'man')   | 869 | 66.0334 |
        | ('sex', 'women') | 447 | 33.9666 |

    Example with customized variable label (``data_label='Gender'``): ::

        |                     |   n |       % |
        |:--------------------|----:|--------:|
        | ('Gender', 'man')   | 869 | 66.0334 |
        | ('Gender', 'women') | 447 | 33.9666 |

    Example with customized variable label (``index=True,
    data_label='Gender'``): ::

        |            | n (%)         |
        |:-----------|:--------------|
        | Gender     |               |
        |     male   | 577.0 (64.76) |
        |     female | 314.0 (35.24) |

    Args:
        data: A series of values or dataframe column.
        dropna: Ignore missing values or not.
        result_labels: The labels used for the result columns.
        combine: Combine the two columns into one (e.g. ``"n (%)"``).
        reverse_columns: Order of columns in resulting table.
        index: Add ``data``'s name to the index and/or specify the index kind
            as ``MultiIndex`` (value: ``multi``) or indented (value:
            ``indented``). See details below.
        data_label: Replace ``data``'s name by this label.
        sort_index: Sorting the row index (e.g. by ordered categories).
        round_percentage_digits: Round percentage values to n digits.

    Returns:
        A table as a data frame.

    """
    freq = frequencies(data=data,
                       dropna=dropna,
                       result_label=result_labels[0],
                       index=index,
                       data_label=data_label)

    perc = percentages(data=data,
                       dropna=dropna,
                       result_label=result_labels[1],
                       index=index,
                       data_label=data_label)

    # ordering of "%" and "n"
    ff = [perc, freq] if reverse_columns else [freq, perc]

    ff = pandas.concat(ff, axis=1)

    if sort_index:
        ff = ff.sort_index()

    if combine:
        if not isinstance(combine, bool) and '{}' in combine:
            join_fstring = combine
        else:
            join_fstring = '{} ({})'

        # if index is not None:
        cidx = 1 if reverse_columns else 0
        if ff.iloc[:, cidx].hasnans:
            ff.iloc[:, cidx] = ff.iloc[:, cidx].astype('Int64')

        ff = _join_columns(
            data=ff,
            round_digits=round_percentage_digits,
            join_fstring=join_fstring,
        )

        # data name (index) values should be empty
        if index is not False:
            # ff.iloc[0, 0] = ff.iloc[0, 0].replace('nan (nan)', '')
            ff.iloc[0, 0] = ''

    return ff


def frequencies(data: pandas.Series,
                dropna: bool = False,
                result_label: str = 'n',
                index: Union[bool, str] = False,
                data_label: str = None,
                ) -> pandas.DataFrame:
    """Frequencies for one variable.

    Example output (markdown style): ::

        | Gender   |   n |
        |:---------|----:|
        | Female   | 101 |
        | Male     |  99 |


    See `frequencies_and_percentages()` for more examples and details.

    Args:
        data: A series of values or a data frame's column.
        dropna: Ignore missing values or not.
        result_label: The label used for the result column.
        index: Modify row labels. See `frequencies_and_percentages()` for
            details.
        data_label: Overwrite explanatory data label. See
            `frequencies_and_percentages()` for details.

    Returns:
        The frequencies table as a data frame.

    """
    return _generic_frequency_or_fraction(
        data=data,
        dropna=dropna,
        normalize=False,
        column_label=result_label,
        index=index,
        data_label=data_label
    )


def percentages(data: pandas.Series,
                dropna: bool = False,
                result_label: str = '%',
                index: Union[bool, str] = False,
                data_label: str = None,
                ) -> pandas.DataFrame:
    """Percentages for one variable.

    Example output: ::

        |       |       % |
        |:------|--------:|
        | man   | 66.0334 |
        | women | 33.9666 |

    See `frequencies_and_percentages()` for more examples and details.

    Args:
        data: A series of values or data frame's column.
        dropna: Ignore missing values or not.
        result_label: The label used for the result column.
        index: Modify row labels. See `frequencies_and_percentages()` for
            details.
        data_label: Overwrite explanatory data label. See
            `frequencies_and_percentages()` for details.

    Returns:
        The percentages table as a data frame.

    """
    result = _generic_frequency_or_fraction(
        data=data,
        dropna=dropna,
        normalize=True,
        column_label=result_label,
        index=index,
        data_label=data_label)

    result = result * 100

    return result


# pylint: disable-next=too-many-arguments,too-many-positional-arguments
def _generic_frequency_or_fraction(data: pandas.Series,  # noqa: PLR0913
                                   dropna: bool,
                                   normalize: bool,
                                   column_label: str,
                                   index: Union[bool, str] = False,
                                   data_label: str = None,
                                   ) -> pandas.DataFrame:
    """Frequencies or percentages for one variable.

    It is the core function used by `frequencies()`, `percentages()` and
    `frequencies_and_percentages()`.

    Args:
        data: A series of values or dataframe column.
        dropna: Ignore missing values or not.
        normalize: Do frequencies (``False``) or percentages (``True``).
        column_label: Label used for the column.
        index: Modify row labels. See `frequencies_and_percentages()` for
            details.
        data_label: Overwrite explanatory data label. See
            `frequencies_and_percentages()` for details.

    Returns:
        A table as a data frame.

    Example: .. python ::

        >>> tab = _generic_frequency_or_fraction(
        ...     df.sex, False, False, 'Count', False)
        >>> print(tab.to_markdown())
        |       |   Count |
        |:------|--------:|
        | man   |     869 |
        | women |     447 |

        >>> tab = _generic_frequency_or_fraction(
        ...     df.sex, False, True, 'perc', True)
        >>> print(tab.to_markdown())
        |                  |     perc |
        |:-----------------|---------:|
        | ('sex', 'man')   | 0.660334 |
        | ('sex', 'women') | 0.339666 |


    See `frequencies_and_percentages()` for more examples and details.

    """
    tab = data.value_counts(dropna=dropna, normalize=normalize)
    # if not normalize:
    #     tab = tab.astype('Int64')
    #     print(tab)

    # sort index if it is an ordered category
    if data.dtype.name == 'category':
        if data.cat.ordered:
            tab = tab.sort_index()

    # MultiIndex
    if index == 'multi':
        # Series to DataFrame
        tab = tab.to_frame()

        # two column MultiIndex
        a = str(uuid.uuid4()).replace('-', '')  # random label
        tab[a] = data_label if data_label else data.name
        tab = tab.reset_index()

        tab = tab.set_index([a, tab.columns[0]])
        # Credits: https://stackoverflow.com/a/75309326/4865723
        tab.index.names = (None, None)

    # indented index
    elif index == 'indented' or index is True:
        # index as string
        tab.index = tab.index.astype(str)

        label_row = pandas.Series(
            [None], index=[data_label if data_label else data.name])

        # indentation
        tab.index = [
            f'{INDENTION_DEFAULT * " "}{val}' for val in tab.index
        ]

        tab = pandas.concat([label_row, tab])

        tab = tab.to_frame()

    else:
        tab = tab.to_frame()

    tab.columns = [column_label]

    return tab

# ---------------
# -- Summarize --
# ---------------


# pylint: disable-next=too-many-statements,R0912,R0914,R0913,R0917
def summarize(data: pandas.DataFrame,  # noqa: PLR0913,PLR0912,PLR0915
              values_in: Union[str, Iterable[str], Dict[str, str]],
              frequency: Union[bool, Tuple[bool, bool]] = (True, False),
              combine: bool = True,
              group_by: str = None,
              total: Union[bool, str] = True,
              sort_columns: bool = True,
              ) -> pandas.DataFrame:
    """Summary table with frequency and percentage stratified.

    Create summary table with frequency and percentage for multiple
    variables using there name and values.

    Args:
        data: The dataframe.
        values_in: List of column names in the data to summarize.
        frequency: Control if frequencies and/or percentages used.
        combine: Combine frequencies and percentages into one column.
        group_by: A column in ``data`` which values based on new columns are
            added to the resulting table.
        total: Add total column (default: ``True``) with optionally customized
            label when using a string instead of ``True``.
        sort_columns: Order the columns but keep "Total" column at the end.
            Sorting is ignored if the columns build from an ordered
            categorical.

    Returns:
        Resulting table as a data frame.


    Simple output example using ``titanic`` dataset: ::

        >>> analy.summarize(df, 'sex')
                          n (%)
        Total       891 (100.0)
        sex
            male    577 (64.76)
            female  314 (35.24)

    Example with more variables: ::

        >>> analy.summarize(df, ['sex', 'class', 'alive'])
                          n (%)
        Total       891 (100.0)
        sex
            male    577 (64.76)
            female  314 (35.24)
        class
            Third   491 (55.11)
            First   216 (24.24)
            Second  184 (20.65)
        alive
            no      549 (61.62)
            yes     342 (38.38)

    Using argument ``group_by`` with multiple variables: ::

                          Betazed        Bajor        Trill        Total
                            n (%)        n (%)        n (%)        n (%)
        Total         117 (100.0)  127 (100.0)  105 (100.0)  349 (100.0)
        Person
            Sarek      29 (24.79)   14 (11.02)    19 (18.1)   62 (17.77)
            Diana      25 (21.37)   33 (25.98)   20 (19.05)   78 (22.35)
            Picard      22 (18.8)   26 (20.47)   22 (20.95)   70 (20.06)
            Worf        22 (18.8)   29 (22.83)   26 (24.76)   77 (22.06)
            Quark      19 (16.24)   25 (19.69)   18 (17.14)   62 (17.77)
        Gender
            diverse    29 (24.79)    32 (25.2)   24 (22.86)   85 (24.36)
            female     25 (21.37)   20 (15.75)   25 (23.81)   70 (20.06)
            androgyn   24 (20.51)   23 (18.11)   17 (16.19)   64 (18.34)
            unknown    20 (17.09)   25 (19.69)   17 (16.19)   62 (17.77)
            male       19 (16.24)   27 (21.26)   22 (20.95)   68 (19.48)

    Use argument ``frequency`` to control if frequencies and/or percentages
    used and in which order. ::

        # Default
        >>> analy.summarize(df, 'class')
                        n (%)
        Total       891 (100.0)
        class
            Third   491 (55.11)
            First   216 (24.24)
            Second  184 (20.65)

        # Percentage only
        >>> analy.summarize(df, 'class', frequency=False)
                            %
        Total           100.0
        class
            Third   55.106622
            First   24.242424
            Second  20.650954

        # Frequency only
        >>> analy.summarize(df, 'class', frequency=True)
                    n
        Total       891
        class
            Third   491
            First   216
            Second  184

        # Reverse order of
        >>> analy.summarize(df, 'class', frequency=(False, True))
                        % (n)
        Total       100.0 (891)
        class
            Third   55.11 (491)
            First   24.24 (216)
            Second  20.65 (184)

    """
    # Used for total column (can be disabled)
    total = _total_label(total)
    # Used for total row (currently not able to be disabled)
    label_total_row = total if isinstance(total, str) else LABEL_TOTAL

    # Make sure that "values_in" is a dict
    if isinstance(values_in, str):
        values_in = {values_in: None}
    elif isinstance(values_in, list):
        values_in = {c: None for c in values_in}

    # Frequency and/or percentage?
    if isinstance(frequency, bool):
        frequency = (frequency, )

    # Decide which columns to keep
    if frequency == (True, False):
        the_func = frequencies_and_percentages
        total_cols = ['n', '%']
    elif frequency == (False, True):
        the_func = frequencies_and_percentages
        total_cols = ['%', 'n']
    elif frequency == (False, ):
        the_func = percentages
        total_cols = ['%']
    elif frequency == (True, ):
        the_func = frequencies
        total_cols = ['n']
    else:
        raise ValueError(f'Unknown value for "frequency": {frequency}')

    if len(total_cols) == 1:
        combine = False

    # The 1st level column names (e.g. Year)
    if group_by:
        if isinstance(data[group_by].dtype, pandas.CategoricalDtype):
            group_values = data[group_by].cat.categories.tolist()

            # irgnore column sorting on ordered categories
            if data[group_by].cat.ordered:
                sort_columns = False
        else:
            group_values = data[group_by].unique().tolist()

        if sort_columns is True:
            group_values = sorted(group_values)

    else:
        group_values = None

    # total row
    if group_by:

        total_row = {}

        for gval in group_values:

            group_data = data.loc[data[group_by].eq(gval)]

            total_row[gval] = pandas.DataFrame(
                [[len(group_data), 100.0]],
                columns=['n', '%'],
                index=pandas.Index([label_total_row])
            )

        # Total column?
        if total:
            total_row[label_total_row] = pandas.DataFrame(
                [[len(data), 100.0]],
                columns=['n', '%'],
                index=pandas.Index([label_total_row])
            )

        if combine is not False \
           and next(iter(total_row.values())).shape[1] == 2:

            for key in total_row:
                total_row[key] = _join_columns(
                    total_row[key], total_cols,
                    f'{total_cols[0]} ({total_cols[1]})'
                )

        total_row = pandas.concat(total_row, axis=1)

    else:
        total_row = pandas.DataFrame(
            [[len(data), 100.0]],
            columns=['n', '%'],
            index=pandas.Index([label_total_row])
        )

        if combine is not False and total_row.shape[1] == 2:
            total_row = _join_columns(
                total_row, total_cols, f'{total_cols[0]} ({total_cols[1]})')

    value_tabs = [total_row]

    # Each value
    for one_value in values_in:

        args = {
            'index': True,
            'data_label': values_in.get(one_value, None),
            'combine': combine,
            'reverse_columns': frequency == (False, True),
        }

        if len(frequency) == 1:
            del args['combine']
            del args['reverse_columns']

        # each group
        if group_by:

            group_tabs = {}
            for gval in group_values:
                # data subset
                args['data'] = data.loc[
                    data[group_by].eq(gval)][one_value]

                # frequencies/percentages
                group_tabs[gval] = the_func(**args)

                one_tab = pandas.concat(group_tabs, axis='columns')

            # total column/group
            if total:
                args['data'] = data[one_value]
                total_group = the_func(**args)

                total_group.columns = pandas.MultiIndex.from_product(
                    [[total], total_group.columns.tolist()]
                )

                one_tab = pandas.concat([one_tab, total_group], axis='columns')

        else:
            args['data'] = data[one_value]
            # frequencies/percentages
            one_tab = the_func(**args)

        value_tabs.append(one_tab)

    value_tabs = pandas.concat(value_tabs)

    if combine is not False:
        value_tabs = value_tabs.fillna('0 (0)')

    else:
        # Select 'n' and/or '%' columns
        if group_by:

            if total:
                group_values = group_values + [total]

            total_cols = pandas.MultiIndex \
                .from_product((group_values, total_cols))

        value_tabs = value_tabs.loc[:, total_cols]

    return value_tabs


# pylint: disable=too-many-arguments, too-many-locals, too-many-branches
def aggregate(data: pandas.DataFrame,  # noqa: PLR0912, PLR0913
              values_in: Union[str, list, Dict[str, str]],
              aggfunc: Union[Callable,
                             Iterable[Callable],
                             Dict[Callable, str]],
              *,
              group_by: str = None,
              total: Union[bool, str] = True,
              round_digits: int = None) -> pandas.DataFrame:
    """Summarize data using aggregating functions.

    Output example from ``titanic`` dataset as markdown showing the sum and
    mean value of ``age`` : ::

        |     |     Sum |    Mean |
        |:----|--------:|--------:|
        | age | 21205.2 | 29.6991 |

    Adding fare costs: ::

        |      |     Sum |    Mean |
        |:-----|--------:|--------:|
        | age  | 21205.2 | 29.6991 |
        | fare | 28693.9 | 32.2042 |

    Mean (with customized label) for age and fare and grouped by gender with
    values rouned: ::

        |      | ('female', 'MD') | ('male', 'MD') | ('Total', 'MD') |
        |:-----|-----------------:|---------------:|----------------:|
        | age  |               28 |             31 |              30 |
        | fare |               44 |             26 |              32 |

    About ``values_in``: This argument can be a string, a list of strings or a
    dictionary. Use a string to specify one column in the data or use a list
    of strings to specify multiple columns. To customize labels in the
    resulting data frame use a dictionary indexed by the column names with the
    customized labels as values.


    About ``aggfunc``: Similar to ``values_in`` this argument can be a one
    function, a list of functions or a dictionary. The term function refers
    to a `Callable`. Labels in resulting table used for aggregating functions
    are read from `DEFAULT_AGGFUNC_LABELS` by default. To add more
    or customized labels use a `dict` indexed by functions with customized
    label as values.

    Args:
        data: Data frame with raw data.
        values_in: Specify columns in ``data`` to aggregate. See details
            below.
        aggfunc: Aggregate function(s) to use. See details below.
        group_by: Optional column in ``data`` to values.
        total: Add total column (default: ``True``) with optionally customized
            label when using a string instead of ``True``.
        round_digits: Number of digits to round values in all cells. Use ``0``
            to round to integers.

    Returns:
        Resulting table as `pandas.DataFrame`.

    """
    total = _total_label(total)

    # Manage the aggregate functions and (customized) labels (e.g. "Mean" for
    # `statistics.mean()`. The approach here is to keep a `list` of functions
    # and a separate `dict` (indexed by functions) with the labels.
    aggfunc_labels = copy.deepcopy(DEFAULT_AGGFUNC_LABELS)
    aggfunc_list = []

    if isinstance(aggfunc, dict):
        # We have a dict, so separate into function list and label dict.
        for key in aggfunc:

            # the aggregate function
            one_func = key

            # Keep the function in a list
            aggfunc_list.append(one_func)

            # Remember its label in a dict or overwrite its default label if
            # there is one.
            aggfunc_labels[one_func] = aggfunc[key]

    elif isinstance(aggfunc, list):
        # We have a list of functions only.
        aggfunc_list = aggfunc

    # Only one single function.
    if len(aggfunc_list) == 1:
        aggfunc = aggfunc_list[0]

    # Multiple functions.
    elif len(aggfunc_list) > 1:

        # result for each function
        tab = []

        for one_func in aggfunc_list:
            tab.append(aggregate(
                data=data,
                group_by=group_by,
                values_in=values_in,
                aggfunc={
                    one_func: aggfunc_labels.get(one_func,
                                                 one_func.__name__)
                },
                total=total,
                round_digits=round_digits,
            ))

        # concat the single results
        tab = pandas.concat(tab, axis=1)

        if group_by:
            # Values of group_by column
            lvl_one = data[group_by].unique().tolist()

            # Total column
            if total:
                lvl_one.append(total)

        else:
            # No multiindex columns because of no grouping
            lvl_one = None

        # Build column names with labels of aggregating functions
        lvl_two = [
            aggfunc_labels.get(func, func.__name__) for func in aggfunc_list]

        if lvl_one:
            new_ordered_columns = pandas.MultiIndex.from_product(
                [lvl_one, lvl_two])
        else:
            new_ordered_columns = lvl_two

        # Rearrange the columns
        tab = tab.loc[:, new_ordered_columns]

        return tab

    # TODO(buhtz): remove "the_aggregate_function" and replace with "aggfunc".
    # Can not remember why this exists.
    # the_aggregate_function = lambda val: aggfunc(val)
    the_aggregate_function = aggfunc

    # Make sure that "values_in" is a dict.
    # Note: Maybe use Python 3.11 match?
    if isinstance(values_in, str):
        values_in = {values_in: values_in}
    elif isinstance(values_in, list):
        values_in = {c: c for c in values_in}

    if group_by:
        # Each group expression in one column
        tab = data.loc[:, [group_by] + list(values_in)]
        tab = tab.groupby(group_by, dropna=False).agg(the_aggregate_function)
        tab = tab.T

        # Add "total" column
        if total:
            tab[total] = data.loc[:, list(values_in)] \
                .agg(the_aggregate_function)

        # Improve column labels
        tab.columns = pandas.MultiIndex.from_tuples(
            [(c, aggfunc_labels[aggfunc]) for c in tab.columns])

    # Only one column / no group_by column
    else:
        tab = data.loc[:, list(values_in)]
        tab = tab.agg(the_aggregate_function)

        label = aggfunc_labels[aggfunc]

        # Series to DataFrame
        tab = pandas.DataFrame({label: tab})

    # Label the rows
    tab = tab.rename(index=values_in)

    # Rounding
    if round_digits is not None:
        # To get zero decimal digits round() use `None` instead of 0 for its
        # 2nd argument.
        use_round_digits = None if round_digits == 0 else round_digits

        # Dev: keep "applymap" instead of "map" because we are stuck to pandas
        # 2.0.3 when using Python 3.8
        tab = tab.map(lambda val: round(val, use_round_digits)
                      if not pandas.isna(val) else val)

    return tab


# ------------------------------
# -- Morphing existing tables --
# ------------------------------


def _join_columns(data: pandas.DataFrame,
                  columns: Tuple[str] = None,
                  joined_column_name: str = None,
                  join_fstring: str = '{} ({})',
                  round_digits: Union[int, Tuple[int, int]] = None
                  ) -> pandas.DataFrame:
    """Join two columns into one.

    It is a helper function to morph existing tables. One use case would be
    to join frequencies and percentages into one column (e.g. "3 (4.5)").

    Input example: ::

        |                |   n |         % |
        |:---------------|----:|----------:|
        | Ja             | 403 | 62.0955   |
        | Nein           | 238 | 36.6718   |
        | Weiß ich nicht |   6 |  0.924499 |
        | (Fehlend)      |   2 |  0.308166 |

    Output example: ::

        |                | n (%)      |
        |:---------------|:-----------|
        | Ja             | 403 (62,1) |
        | Nein           | 238 (36,7) |
        | Weiß ich nicht | 6 (0,9)    |
        | (Fehlend)      | 2 (0,3)    |

    Args:
        data: The data frame to modify.
        columns: Name of two columns to join. Data frame column names are used
            as default if not present.
        joined_column_name: Name of the new column. If not present it is
            created based on ``join_fstring`` argument.
        join_fstring: Format string used to join values of the two columns.
        round_digits: Number of digits to round the two values. Use a tuple of
            two integers to have different number of digits.

    Returns:
        Resulting table as data frame.

    """
    # pylint: disable=consider-using-f-string

    # Use columns of data frame as default
    if columns is None:
        columns = data.columns.tolist()

    if len(columns) != 2:
        raise ValueError('Only two columns allowed.')

    # Create new column name based on "join_fstring" as default.
    if joined_column_name is None:
        joined_column_name = join_fstring.format(*columns)

    t = data.loc[:, columns].copy()

    if isinstance(round_digits, int):
        round_digits = (round_digits, round_digits)

    def _sub_delimiters_and_rounding(val):
        if pandas.isna(val):
            return str(val)

        if isinstance(val, float) and round_digits:
            return '{:n}'.format(round(val, round_digits[idx]))

        return '{:n}'.format(val)

    # Rounding & delimiters for decimals and thousands
    for temp_col, idx in (('a', 0), ('b', 1)):
        t[temp_col] = t.loc[:, columns[idx]].apply(
            _sub_delimiters_and_rounding)

    def _sub_format_join(row):
        # The astype() hack is needed because under specific circumstances
        # pandas convert integers to floats.

        try:
            a = row.a.astype(t.a.dtype)
        except AttributeError:  # "a" has no astype())
            a = row.a

        try:
            b = row.b.astype(t.b.dtype)
        except AttributeError:  # "b" has no astype())
            b = row.b

        return join_fstring.format(a, b)

    # Join a & b
    t['c'] = t.apply(_sub_format_join, axis=1)

    # Drop a & b
    t = t.loc[:, ['c']]

    # Rename c
    t.columns = [joined_column_name]

    return t
