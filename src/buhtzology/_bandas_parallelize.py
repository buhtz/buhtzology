# pylint: disable=too-many-lines
"""Helpers with `pandas`.

Some little helper's for and depending on the data science package
`pandas`.
"""
from __future__ import annotations
import logging
from collections.abc import Callable
from typing import Union
import concurrent.futures
import functools
import psutil
import pandas
import buhtzology
_log = logging.getLogger(__name__)

DECREASE_WORKERS_BY = buhtzology.config['bandas']['decrease_workers_by']
"""Number of workers using `parallelize_job_on_dataframe()` is decreased by
this number."""

SERVER_N_CORES = 16
"""Machines with this number of logical cores are treated as servers or
crunching machines used by multiple users. See `SERVER_USE_CORE_FX` for
details.
"""

SERVER_USE_CORE_FX = 0.25
"""When parallelizing tasks using `parallelize_job_on_dataframe()` on a
mutli-user server use only this fraction of available cores.

This is used to preventing to take all cors and block other users. See
`SERVER_N_CORES` for details.
"""


def cut_into_pieces(data: pandas.DataFrame,
                    n_pieces: int) -> list[pandas.DataFrame]:
    """Cut a dataframe horizontally into pieces of (nearly) the same size.

    This pieces can be used for parallelization. To keep groups of rows
    together there is the alternative `cut_by_row_keep_group()`. The number
    of pieces is garantueed.

    Args:
        data: The data frame that should be cut.
        n_pieces: Number of resulting pieces.

    Returns:
        A list of the data frame parts.

    """
    total = len(data)
    target_n = int(total / n_pieces)

    df_parts = []

    if n_pieces == 1:
        df_parts.append(data)
        data = data.iloc[0:0]  # no rest

    while len(data) > 0:  # while rest exists

        if len(df_parts) < (n_pieces - 1):
            # deep-copy the part
            df_parts.append(data.iloc[:target_n].copy())
            # remove the part from original
            data = data.iloc[target_n:]

        else:
            # Rest
            df_parts.append(data.copy())
            # clear
            data = data.iloc[0:0]

    return df_parts


# pylint: disable-next=too-many-locals
def cut_by_row_keep_group(data: pandas.DataFrame,
                          group_column: Union[str, int],
                          n_pieces: int,
                          sort_kind: str = 'quicksort'
                          ) -> list[pandas.DataFrame]:
    """Cut a dataframe into pieces row by row but keep groups together.

    Groups are specified by the column name in ``group_column``. The ``data``
    will by sorted by ``group_column`` using the sort algorithm specified by
    ``sort_kind`` which is used by pandas/numpy. The default ``quicksort`` is
    the fastest. For unittesting ``stable`` should be used. The number of
    resulting parts is not guaranteed to be ``n_pieces``.

    Args:
        data: The data frame that should be cut.
        group_column: Name or index of the group column.
        n_pieces: Number of resulting pieces.
        sort_kind: Used for unittesting.

    Returns:
        A list of the data frame parts.

    """
    # remember index name for restore at the end
    org_index_name = data.index.name

    # sort by the group column
    data = data.sort_values(group_column, kind=sort_kind)
    # and set a new integer index
    data = data.reset_index(drop=False)

    # number of rows one piece should have
    # pn = int(len(data) / n_pieces)
    target_n_per_piece = int(len(data) / n_pieces)

    # the resulting list
    df_parts = []

    def _cutted_into_the_group(maybe_after):
        """Tell's us if we have cut into the group."""
        group_value_a = data.loc[maybe_after + 1, group_column]

        try:
            group_value_b = data.loc[maybe_after, group_column]

            if group_value_a == group_value_b:
                return True

        except KeyError:
            # if maybe_after < 0
            pass

        return False

    idx = 0  # a index cursor
    while idx in data.index:
        # first cut
        before = data.loc[idx].name

        # second cut
        try:
            backward_offset = 0
            foreward_offset = 0

            # step backward if cut into the group
            while _cutted_into_the_group(
                    (idx + target_n_per_piece - 1 - backward_offset)):
                backward_offset = backward_offset + 1

            if backward_offset != 0:
                # step forward if cut into the group
                while _cutted_into_the_group(
                        (idx + target_n_per_piece + foreward_offset)):
                    foreward_offset = foreward_offset + 1

            # some way length for booth directions?
            if backward_offset == foreward_offset:
                # make forward shorter
                backward_offset = backward_offset * 2

            # pre calculate the indexes
            backward_loc_idx \
                = (idx - backward_offset) + target_n_per_piece - 1
            foreward_loc_idx \
                = (idx + foreward_offset) + target_n_per_piece

            # which direction is better and possible?
            if foreward_offset < backward_offset or backward_loc_idx < 0:
                after = data.loc[foreward_loc_idx].name
            else:
                after = data.loc[backward_loc_idx].name

        except KeyError:
            # last row
            after = data.index[-1]

        # cut that piece
        a_part = data.truncate(before=before, after=after)
        # restore original index
        a_part = a_part.set_index('index')
        a_part.index.name = org_index_name
        # remember the cut part
        df_parts.append(a_part)

        # next piece
        idx = after + 1

    # Plausi
    if len(data) != sum(len(dfp) for dfp in df_parts):
        raise TypeError('Number of rows not correct!')

    return df_parts


# pylint: disable-next=too-many-arguments
def parallelize_job_on_dataframe(*,  # noqa: PLR0913
                                 data: pandas.DataFrame,
                                 worker_func: Callable,
                                 group_column: str = None,
                                 worker_args: tuple = tuple(),
                                 n_pieces: int = None,
                                 decrease_workers_by: int = None,
                                 ) -> pandas.DataFrame:
    """Use multiprocessing to working on dataframe row by row.

    A dataframe is cut into multiple dataframes. Each of them is
    transferred to another process (not thread). This is fast because of
    using multiple CPU cores but costs a lot of RAM and some time for
    transffering the dataframe pieces (via `pickle`) to a process and back.

    There are two options to cut a dataframe. By default or when using
    ``n_pieces`` it is cut into pieces with nearly the same number of rows.
    The function `bandas.cut_into_pieces()` is used in that case.
    When using ``group_column`` the function `bandas.cut_by_row_keep_group()`
    will be used.

    Just ``return`` the result of the worker:

    .. python::
        def the_worker(sub_dataframe):
            sub_dataframe.foo = 7

            return sub_dataframe

    To add additional arguments to the worker use ``worker_args`` argument and
    the values in a tuple.

    .. python::
        def the_worker(columns, sub_dataframe):
            sub_dataframe['Extra'] = sub_dataframe.loc[:, cols].apply(
                lambda row: row * 7, axis=1)

            return sub_dataframe

        if __name__ == '__main__':
            result = bandas.parallelize_job_on_dataframe(
                data=df,
                worker_func=the_worker,
                group_column='group',
                worker_args=(['colA', 'colD', 'colT'], )
            )

    Args:
        data: The dataframe.
        worker_func: A function used in each process.
        group_column: That group is not cut into while cutting the
            dataframe.
        worker_args: Tuple of arguments used in the worker function.
        n_pieces: Number of pieces the dataframe should be cut into.
        decrease_workers_by: Reduce the number of cores to use by value
           (default: 0).

    Returns:
        The resulting dataframe.

    """
    n_cores = psutil.cpu_count()

    if decrease_workers_by is None:
        decrease_workers_by = DECREASE_WORKERS_BY

    if n_cores >= SERVER_N_CORES:
        n_cores = int(n_cores * SERVER_USE_CORE_FX)
        _log.info(f'The machine has {psutil.cpu_count()} cores hence is '
                  'assumed as a multi-user server. Because of that the '
                  'number of used CPU cores is reduiced by factor '
                  f'{SERVER_USE_CORE_FX} to {n_cores} cores.')

    if decrease_workers_by:
        n_cores = n_cores - decrease_workers_by
        logging.info(
            f'Decreased number of usable cores by {decrease_workers_by} '
            f'to {n_cores}.')

    with concurrent.futures.ProcessPoolExecutor(
            max_workers=n_cores) as executor:

        if group_column:
            # keep groups together when cutting in pieces
            df_parts = cut_by_row_keep_group(
                data=data,
                group_column=group_column,
                n_pieces=n_pieces if n_pieces else n_cores)
        else:
            # cut based on row count ignoring groups
            df_parts = cut_into_pieces(
                data=data,
                n_pieces=n_pieces if n_pieces else n_cores)

        _log.info(f'Start parallel processing with {n_cores} workers in '
                  f'the pool and a data frame with {data.shape[0]:n} lines '
                  f'cut into {len(df_parts)} pieces.')

        # execute
        results = executor.map(
            functools.partial(worker_func, *worker_args),
            df_parts)

    # clear the original data frame
    # and glue the results together
    data = pandas.concat(results)

    _log.info('Finished working parallel on data frame.')

    return data
