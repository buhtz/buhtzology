"""Create reports from several data objects like `pandas.DataFrame`.

That module can create reports from data objects like `pandas.DataFrame`,
images or figures (e.g. based on ``seaborn`` or `matplotlib`).
The reports are created with an instance of class `Report` independent from
the desired output format. After compose the report with its components it is
possible to generate an output document in the formats MS Word (``docx``), MS
Excel (``xlsx``) or Markdown (``md``). It must be easy to implement additional
formats. There are plans to implement *Open Document* (``odt`` and ``ods``) as
well, what depends on the activity and future in the ``odfpy`` package or
derivats.
"""
# pylint: disable=too-many-lines

# https://stackoverflow.com/a/33533514/4865723
from __future__ import annotations
import logging
import warnings
import re
from collections import defaultdict
from collections.abc import Hashable
from datetime import datetime
import pathlib
import enum
import io
import string
import random
import contextlib
import locale
from typing import NamedTuple, Union, Dict, List, Iterable
import subprocess
import platform
import textwrap
import lxml
import lxml.etree
import docx  # "python-docx"
import docx.table
import docx.shared
import docx.oxml
import docx.oxml.ns
import docx.enum.section
import docx.enum.style
import PIL
import PIL.Image
import matplotlib
import matplotlib.figure
import seaborn
import pandas
import webcolors

_log = logging.getLogger(__name__)

DEFAULT_DPI = 300
"""DPI used for creating image data from plotted figures."""


class _ControlType(enum.IntEnum):
    """An Enum to control page behavior and other parts of a document."""

    PAGE_BREAK = 1
    ORIENTATION_TOGGLE = 2
    ORIENTATION_LANDSCAPE = 3
    ORIENTATION_PORTRAIT = 4


class _ReferenceKind(enum.IntEnum):
    """Enum for references list types."""

    CONTENT = 1
    FIGURES = 2
    TABLES = 3


# pylint: disable-next=too-many-public-methods
class Report:
    """Content container for a report like document.

    The report container is independent from its output format. It can have
    text paragraphs, headings, tables (``pandas.DataFrame``), images and
    figures.

    """

    class Paragraph(NamedTuple):
        """Content element representing a text paragraph."""

        text: str
        style: str

    class Enumeration(NamedTuple):
        """Ordered or unordered enumeration.

        Nesting is not supported. See for details:
        <https://github.com/python-openxml/python-docx/issues/122>
        """

        items: tuple[str]
        ordered: bool
        style: str

    class Heading(NamedTuple):
        """Content element representing a heading element."""

        text: str
        level: int

    class Table(NamedTuple):
        """Content element representing a data table."""

        df: pandas.DataFrame
        autofit: bool
        caption: str
        note: str

    class Control(NamedTuple):
        """Control several page layout aspects.

        An element controlling page breaks, orientation and insertion of
        auto generated reference lists like table of contents.
        """

        control_type: _ControlType

    class References(NamedTuple):
        """Reference list, e.g. TOC, Figure, Tables."""

        kind: _ReferenceKind
        depth: int

    class Image(NamedTuple):
        """An image content element."""

        image_bytes: bytes
        scale_factor: float
        caption: str

    def __init__(self,
                 title: str = None,
                 subtitle: str = None,
                 heading_level_offset: int = 0):
        """Initiate a report instance.

        Args:
            title: The title element.
            subtitle: The subtitle.
            heading_level_offset: Heading level to start with.

        """
        self._elements = []
        """List of all elements of that report."""

        self._element_tags = []
        """Tags for each element in `_elements` with the same index."""

        self._current_heading_level = 1
        """The current activate heading level."""

        self._heading_level_offset = heading_level_offset
        """Internally used with child-reports."""

        self._current_auto_tags = None
        """These tags are added by default to new elements."""

        self._xlsx_writer = None

        if title:
            self.title(title)

        if subtitle:
            self.subtitle(subtitle)

    def __getitem__(self, idx: int):
        """Use the report with a ``[]`` operator like a list."""
        return self._elements[idx]

    def get_filtered_elements(self,
                              tags_allowed: Iterable[str] = None,
                              tags_excluded: Iterable[str] = None
                              ) -> NamedTuple:
        """Return only elements that fit the tag rules.

        It is a generator. Exclusion tags have higher priority then allowed
        tags. In practice it means that if an element have tag from
        ``tag_allowed`` and ``tags_excluded`` it is excluded from that
        generator.

        Args:
            tags_allowed: Allowed tags.
            tags_excluded: Tags to exclude.

        Yields:
            Elements that fitting the tag rules.

        """
        # Each element and the corresponding tag.
        for element, element_tags in zip(self._elements, self._element_tags):

            # check for exclusive inclusion tags?
            if tags_allowed:
                if not element_tags:
                    continue

                # None of the element_tags in allowed tags
                if not any(et in tags_allowed for et in element_tags):
                    # ignore that element
                    continue

            # check for exclusion tags?
            if tags_excluded and element_tags:
                if any(et in tags_excluded for et in element_tags):
                    # ignore that element
                    continue

            # Is it a child-report?
            if isinstance(element, Report):
                yield from element.get_filtered_elements(
                    tags_allowed=tags_allowed,
                    tags_excluded=tags_excluded)

                continue

            yield element

    def __len__(self):
        """Return the number of elements."""
        return len(self._elements)

    @contextlib.contextmanager
    def auto_tags(self, tags: Union[list, str]) -> Report:
        """Use temporary default tags for new elements.

        Args:
            tags: One tag or a list of tags.

        Returns:
            The instance itself.

        Example:

            .. python::
                import report

                r = report.Report()
                r.paragraph('Paragraph without a tag')

                with r.auto_tags(['short', 'boss']):
                    r.paragraph('Paragraph with tags "short" and "boss".')
                    r.table(pandas.DataFrame())  # tagged table

                r.paragraph('Another paragraph without a tag.')

        """
        if isinstance(tags, str):
            tags = [tags]

        old_auto_tags = self._current_auto_tags
        self._current_auto_tags = tags

        yield self

        self._current_auto_tags = old_auto_tags

    def _add_element(self,
                     element: Union[NamedTuple, Report],
                     tags: Union[list, str] = None):
        """Add a report element to the list and taking care of (auto) tags.

        The instance attribute `_current_auto_tags` is taken into account.

        Args:
            element: The element.
            tags: One or more tags used for that element.

        """
        if isinstance(tags, str):
            tags = [tags]

        # add element
        self._elements.append(element)

        # is there an auto tag?
        if self._current_auto_tags:
            if not tags:
                tags = self._current_auto_tags

        # add tag for that element
        self._element_tags.append(tags)

    def _page_break_with_orientation(self,
                                     orientation: _ControlType,
                                     tags: Union[list, str] = None):
        """Page break including specific page orientation."""
        self.page_break(tags=tags)

        e = self.__class__.Control(control_type=orientation)
        self._add_element(element=e, tags=tags)

    def page_break_portrait(self, tags: Union[list, str] = None):
        """Page break with portrait orientation."""
        self._page_break_with_orientation(
            _ControlType.ORIENTATION_PORTRAIT, tags)

    def page_break_landscape(self, tags: Union[list, str] = None):
        """Page break with landscape orientation."""
        self._page_break_with_orientation(
            _ControlType.ORIENTATION_LANDSCAPE, tags)

    def page_break(self,
                   toggle_orientation: bool = False,
                   tags: Union[list, str] = None):
        """Just a page break."""
        e = self.__class__.Control(control_type=_ControlType.PAGE_BREAK)
        self._add_element(element=e, tags=tags)

        if toggle_orientation:
            self.toggle_orientation(tags=tags)

    def toggle_orientation(self, tags: Union[list, str] = None):
        """Toggle the current existing page orientation.

        This will effect all past elements until the last page break.
        """
        e = self.__class__.Control(
            control_type=_ControlType.ORIENTATION_TOGGLE)
        self._add_element(element=e, tags=tags)

    def title(self, text: str, tags: Union[list, str] = None):
        """Title of the report.

        The title is a heading of level ``0``.

        Args:
            text: Title string.
            tags: One or more tags for that title element.

        """
        curr_level = self.heading_level()

        self.heading(text=text, level=0, tags=tags)

        self.heading_level(level=curr_level)

    def subtitle(self, text: str, tags: Union[list, str] = None):
        """Subtitle for the document.

        The subtitle is realized via a paragraph of style _Subtitle_.

        Args:
            text: The subtitle string.
            tags: One or more tags for that subtitle element.

        """
        self.paragraph(text, style='Subtitle', tags=tags)

    def heading_level(self,
                      up: bool = None,
                      down: bool = None,
                      level: int = None) -> int:
        """Modify the current heading level.

        The arguments are not mutually exclusive.

        Args:
            up: One level up (decrement).
            down: One level down (increment).
            level: Explicit value.

        Returns:
            Current heading level.

        """
        if level is not None:
            self._current_heading_level = level

        if up:
            self._current_heading_level -= 1

        if down:
            self._current_heading_level += 1

        return self._current_heading_level

    # pylint: disable-next=too-many-arguments
    def heading(self,  # noqa: PLR0913
                text: str,
                *,
                level: int = None,
                up: bool = None,
                down: bool = None,
                persistent: bool = True,
                tags: Union[list, str] = None):
        """Add a heading element.

        Args:
            text: The heading string.
            level: The level (starting with 1).
            up: Decrease current level and use that for this heading.
            down: Increase current level and use that for this heading.
            persistent: Stay on the level if ``True`` (default).
            tags: One or more tags for that element.

        """
        former_level = self._current_heading_level

        heading_level = self.heading_level(
            up=up, down=down, level=level)
        heading_level = heading_level + self._heading_level_offset

        e = self.__class__.Heading(text=text, level=heading_level)

        self._add_element(element=e, tags=tags)

        if not persistent:
            self._current_heading_level = former_level

    # pylint: disable-next=too-many-arguments
    def heading_temporary(self,  # noqa: PLR0913
                          text: str,
                          *,
                          level: int = None,
                          up: bool = None,
                          down: bool = None,
                          tags: Union[list, str] = None):
        """Create headings on a none persistent level.

        It is a wrapper for `heading()`. None persistent means that the used
        ``level`` is set back to the previous level before that method was
        called.
        """
        self.heading(
            text=text,
            level=level,
            up=up,
            down=down,
            persistent=False,
            tags=tags
        )

    def image_dict(self,
                   image_dict: dict,
                   page_break: bool = False,
                   tags: Union[list, str] = None):
        """Add a bunch of images at once via (nested) dictionary.

        The keys in that dictionary are used as heading or caption.

        Args:
            image_dict: (Nested) dictionary of image like instances.
            page_break: New page after each image?
            tags: Tag that element.

        """
        for key in image_dict:

            # also a dict?
            if isinstance(image_dict[key], dict):

                # heading one level down
                self.heading(f'{key}', down=True, tags=tags)

                # add that dict also
                self.image_dict(
                    image_dict=image_dict[key],
                    page_break=page_break,
                    tags=tags
                )

                self.heading_level(up=True)

            else:

                self.image(image_dict[key], caption=f'{key}', tags=tags)

                if page_break:
                    self.page_break(tags=tags)

    def table_dict(self,
                   dataframe_dict: dict,
                   page_break: bool = False,
                   autofit: bool = True,
                   tags: Union[list, str] = None):
        """Add a bunch of tables at once via (nested) dictionary.

        The keys in that dictionary are used as heading or caption.

        Args:
            dataframe_dict: (Nested) dictionary of `pandas.DataFrame`'s.
            page_break: New page after each table?
            autofit: Set autofit to each the table.
            tags: Tag that element.

        """
        for key in dataframe_dict:

            if isinstance(dataframe_dict[key], dict):

                self.heading(f'{key}', down=True)
                self.table_dict(
                    dataframe_dict=dataframe_dict[key],
                    page_break=page_break,
                    autofit=autofit,
                    tags=tags
                )
                self.heading_level(up=True)

            else:

                self.table(dataframe_dict[key], caption=f'{key}', tags=tags)

                if page_break:
                    self.page_break()

    # pylint: disable-next=too-many-arguments
    def table(self,  # noqa: PLR0913
              dataframe: pandas.DataFrame,
              caption: str = None,
              *,
              note: str = None,
              autofit: bool = True,
              tags: Union[list, str] = None):
        """Table element as `pandas.DataFrame`.

        Args:
            dataframe: The data object.
            caption: String used as table caption.
            note: String used as (foot)note.
            autofit: Adjust table width via autofit?
            tags: One or more tag for that element.

        """
        e = self.__class__.Table(df=dataframe,
                                 autofit=autofit,
                                 caption=caption,
                                 note=note)

        self._add_element(element=e, tags=tags)

    def table_of_contents(self, depth: int = 6, tags: Union[list, str] = None):
        """Generate a table of content element."""
        self.paragraph('Table of Contents', style='TOC Heading', tags=tags)

        e = self.__class__.References(_ReferenceKind.CONTENT, depth)
        self._add_element(element=e, tags=tags)

    @staticmethod
    def image_to_bytes(
            file_path_or_figure: Union[pathlib.Path,
                                       matplotlib.figure.Figure,
                                       matplotlib.axes.Axes,
                                       seaborn.axisgrid._BaseGrid]) -> bytes:
        """Create a bytes object from an image file or a plotted figure."""
        # Get figure from plotted object
        if isinstance(file_path_or_figure, (matplotlib.axes.Axes,
                                            seaborn.axisgrid.Grid)):
            file_path_or_figure = file_path_or_figure.figure

        # Extern file
        if isinstance(file_path_or_figure, pathlib.Path):

            with file_path_or_figure.open('rb') as handle:
                image_bytes = handle.read()

        # or a figure
        elif isinstance(file_path_or_figure, matplotlib.figure.Figure):

            file_path_or_figure.tight_layout()

            image_bytes = io.BytesIO()
            file_path_or_figure.savefig(
                image_bytes,
                dpi=DEFAULT_DPI,
                format='png')
            file_path_or_figure.clf()

            image_bytes.seek(0)
            image_bytes = image_bytes.getvalue()

        return image_bytes

    def image(self,
              file_path_or_figure: Union[pathlib.Path,
                                         matplotlib.figure.Figure,
                                         matplotlib.axes.Axes,
                                         seaborn.axisgrid._BaseGrid,
                                         bytes],
              scale_factor: float = None,
              caption: str = None,
              tags: Union[list, str] = None):
        """Image or plotted figure as report element.

        Args:
            file_path_or_figure: File or plot instance.
            scale_factor: Float scaling factor from 0.0 to 1.0.
            caption: String used as image caption.
            tags: One or more tags for that element.

        """
        # pylint: disable-next=fixme
        # TODO: replace with "Report.figure_to_image_bytes()"

        if isinstance(file_path_or_figure, (matplotlib.axes.Axes,
                                            seaborn.axisgrid.Grid)):
            file_path_or_figure = file_path_or_figure.figure

        if isinstance(file_path_or_figure, pathlib.Path):

            with file_path_or_figure.open('rb') as handle:
                image_bytes = handle.read()

        elif isinstance(file_path_or_figure, matplotlib.figure.Figure):

            # pylint: disable-next=fixme
            # TODO(buhtz): replace with "Report.figure_to_image_bytes()"

            file_path_or_figure.tight_layout()

            image_bytes = io.BytesIO()
            file_path_or_figure.savefig(
                image_bytes,
                dpi=DEFAULT_DPI,
                format='png')
            file_path_or_figure.clf()

            image_bytes.seek(0)
            image_bytes = image_bytes.getvalue()

        elif isinstance(file_path_or_figure, bytes):
            image_bytes = file_path_or_figure

        else:
            raise TypeError(
                f'{file_path_or_figure=} {type(file_path_or_figure)=}')

        e = self.__class__.Image(image_bytes=image_bytes,
                                 scale_factor=scale_factor,
                                 caption=caption)

        self._add_element(element=e, tags=tags)

    def list_of_tables(self,
                       heading_text: str = 'List of Tables',
                       tags: Union[list, str] = None):
        """Add list of tables element."""
        self.paragraph(heading_text, style='TOC Heading', tags=tags)

        e = self.__class__.References(_ReferenceKind.TABLES, 0)
        self._add_element(element=e, tags=tags)

    def list_of_figures(self,
                        heading_text: str = 'List of Figures',
                        tags: Union[list, str] = None):
        """Add list of figures element."""
        self.paragraph(heading_text, style='TOC Heading', tags=tags)

        e = self.__class__.References(_ReferenceKind.FIGURES, 0)
        self._add_element(element=e, tags=tags)

    def paragraph(self,
                  text: str = '',
                  style: str = None,
                  tags: Union[list, str] = None):
        """Add a paragraph element.

        Args:
            text: The paragraph content.
            style: Style name (relevant for ``docx`` output).
            tags: One or more tags for that element.

        """
        e = self.__class__.Paragraph(text=text, style=style)
        self._add_element(element=e, tags=tags)

    def enumeration(self,
                    items: Iterable[str],
                    ordered: bool,
                    style: str = None,
                    tags: Union[list, str] = None) -> None:
        """Enumeration element.

        When used with ``DocxDocument`` the visual result is sligthely
        different from the regular MS Words enumeration. For example the
        idention is missing. Another limitation is that nested enumerations are
        not supported.

        Args:
            items: List of enumeration items as strings.
            ordered: Use bullet points (``False``) or numbers (``True``).
            style: If used this overwrites `ordered` argument.
            tags: One or more tags for that element.

        """
        e = self.__class__.Enumeration(
            items=items, ordered=ordered, style=style)
        self._add_element(element=e, tags=tags)

    def section(self, title: str = None, tags: Union[list, str] = None
                ) -> Report:
        """Open a section and return it as a child report object.

        Args:
            title: Heading of the section,
            tags: On eor more tags for that section.

        Returns:
            A report object as a child of the current report.

        """
        e = Report(title, heading_level_offset=self._heading_level_offset + 1)

        self._add_element(element=e, tags=tags)

        return e


# pylint: disable-next=too-few-public-methods
class _GenericDocumentOutput:
    """Base class for the different output file types."""

    def __init__(self, file_path: pathlib.Path,
                 tags_allowed: Union[List[str], str] = None,
                 tags_excluded: Union[List[str], str] = None):

        if not isinstance(tags_allowed, list):
            tags_allowed = [tags_allowed] if tags_allowed else None

        if not isinstance(tags_excluded, list):
            tags_excluded = [tags_excluded] if tags_excluded else None

        self.file_path = file_path
        """Path to the file."""

        self.tags_allowed = tags_allowed
        """List of tags for elements that are allowed."""

        self.tags_excluded = tags_excluded
        """List of tags for elements that are excluded."""

    def show(self):
        """Open the document with the associated application.

        On Linux ``xdg-open`` and on Windows ``start`` is used.
        """
        cmd = {
            'Linux': ['xdg-open'],
            # Credits:
            # https://superuser.com/a/1178490/486099
            # /C is used to call the "start" command on CMD.EXE
            # The "" is an empty window title
            'Windows': ['CMD.EXE', '/C', 'start', '']
        }[platform.system()]

        cmd = cmd + [self.file_path]
        _log.info(f'Try to open with {cmd}.')
        subprocess.run(cmd, check=False)

    def _not_implemented(self, *args, **kwargs):
        """Handle not implemented elements and throws a warning."""
        warnings.warn(
            f'Not implemented yet. kwargs={kwargs} args={args}', stacklevel=2)


class MarkdownDocument(_GenericDocumentOutput):
    """Export a `Report` into a markdown document.

    In some special situations HTML tags are used for some elements.
    """

    def __init__(self, file_path: pathlib.Path):
        """Initiate the document.

        Args:
            file_path: Path to the output file.

        """
        super().__init__(file_path)

        self._mdfile = None

    def save(self, report: Report) -> _GenericDocumentOutput:
        """Start the conversion of the `Report` into a markdown file.

        Args:
            report: The report object.

        Returns:
            Itself.

        """
        with self.file_path.open('w', encoding='utf-8') as mdfile:
            self._mdfile = mdfile

            for element in report:
                if isinstance(element, Report):
                    self._not_implemented(element)
                    continue

                {
                    Report.Table: self.dataframe_to_table,
                    Report.Heading: self.string_to_heading,
                    Report.Paragraph: self.string_to_paragraph,
                    Report.Image: self.image,
                    Report.Control: self._not_implemented,
                    Report.References: self._not_implemented,
                }[type(element)](**(element._asdict()))

        return self

    def image(self,
              image_bytes: bytes,
              scale_factor: float,
              caption: str):
        """Convert `Report.Image` element into extern file.

        All images are stored in a sub-folder. It's name is build from the
        output filename base and the suffix ``_images``. The filename of an
        image is build from its ``caption`` or if not present with random
        characters.

        The original image data is not scaled. Scaling is realized via the
        ``width`` attribute in an ``<img />`` tag.

        Args:
            image_bytes: The image as bytes.
            scale_factor: The factor to scale that image.
            caption: The caption string.

        """
        # image folder
        image_folder = self.file_path.with_suffix('')
        image_folder = image_folder.parent / (image_folder.name + '_images')
        image_folder.mkdir(exist_ok=True)

        # image filename
        allowed = string.ascii_letters + string.digits

        if caption:
            file_name = filter(lambda c: c in allowed, caption)
        else:
            file_name = random.choices(allowed, k=35)

        file_name = ''.join(file_name)
        image_file_path = image_folder / (file_name + '.png')

        image = PIL.Image.open(io.BytesIO(image_bytes))
        image.save(image_file_path, format='png')

        # make real file path relative to markdown file
        image_file_path = image_file_path.relative_to(self.file_path.parent)

        link_label = caption if caption else image_file_path

        if scale_factor:
            # relative scale via hTML "<img>"
            scale_percent = round(scale_factor * 100)
            self._mdfile.write(
                f'<a href="{image_file_path}">'
                f'<img alt="{link_label}" src="{image_file_path}" '
                f'width="{scale_percent}%" /></a>\n'
            )
        else:
            # markdown image link
            self._mdfile.write(f'![{link_label}]({image_file_path})\n')

        self._mdfile.write('\n')

        # Add the image caption as a "<sub>" paragraph
        if caption:
            self.string_to_paragraph(text=caption, style='Subtitle')

    def string_to_heading(self, text: str, level: int):
        """Convert a string to a markdown."""
        prefix = '#' * (level + 1)
        self._mdfile.write(f'{prefix} {text}\n')

    def string_to_paragraph(self, text: str, style: str):
        """Add paragraph.

        If ``style`` is ``Subtitle`` then the paragraph is encapsulated by
        ``<sub>`` tag.
        """
        subtitle = ('<sub>', '</sub>') if style == 'Subtitle' else ('', '')

        self._mdfile.write('{}{}{}'.format(
            subtitle[0],
            '\n'.join(textwrap.wrap(text, width=80)),
            subtitle[1]
        ))

        self._mdfile.write('\n\n')

    # pylint: disable-next=too-many-arguments
    def dataframe_to_table(self,  # noqa: PLR0913
                           df: pandas.DataFrame,
                           *,
                           caption: str,
                           note: str,
                           autofit: bool,
                           decimal_places: int = 2):
        """Markdown output of a table element.

        Args:
            df: The table in form of a `pandas.DataFrame`.
            caption: Caption string realized as ``Subtitle`` paragraph.
            note: (Not implemented yet).
            autofit: Ignored because not relevant for markdown.
            decimal_places: Ignored in current implementation.

        """
        # pylint: disable=unused-argument

        df.to_markdown(self._mdfile)
        self._mdfile.write('\n')

        if caption:
            self._mdfile.write('\n')
            self.string_to_paragraph(text=caption, style='Subtitle')


class XlsxDocument(_GenericDocumentOutput):
    """Export a `Report` into an ``xlsx`` (MS Excel) file.

    It use `pandas.ExcelWriter` with `openpyxl` as engine.

    Development notes: Improve it based on Moffitt, Chris (2015): Improving
    Pandas Excel Output. Hg. v. Practical Business Python. Online available
    at https://pbpython.com/improve-pandas-excel-output.html
    """

    def __init__(self, file_path: pathlib.Path):
        """Initiate the document.

        Args:
            file_path: The output file path.

        """
        super().__init__(file_path)

        self._current_heading_level = 1
        """The current active heading level."""

        self._xlsx_writer = None

    # pylint: disable-next=too-many-arguments
    def dataframe_to_table(self,  # noqa: PLR0913
                           df: pandas.DataFrame,
                           *,
                           caption: str,
                           note: str,
                           autofit: bool,
                           decimal_places: int = 2):
        """Convert table element into an excel table.

        The currently active sheet is used. If there is no active sheet the
        default name ``Sheet 1`` is used. The table is indented by one column
        per heading level.

        Args:
            df: The table as `pandas.DataFrame`.
            caption: Caption string currently ignored.
            note: (Not implemented yet).
            autofit: Currently ignored.
            decimal_places: Currently ignored.

        """
        # pylint: disable=unused-argument
        try:
            sheet_name = self._xlsx_writer.book.active.title

        except AttributeError:
            sheet_name = 'Sheet 1'
            max_row = -1
            # max_col = 1
        else:
            # will return (1, 1) on an empty sheet
            max_row = self._xlsx_writer.book.active.max_row
            # max_col = self._xlsx_writer.book.active.max_column

        # print(f'max_row={max_row} max_col={max_col}')

        start_row = max_row + 1 if max_row > 1 else 0
        start_col = 0 + (self._current_heading_level - 1)

        df.to_excel(
            excel_writer=self._xlsx_writer,
            sheet_name=sheet_name,
            startrow=start_row,
            startcol=start_col
        )

    def string_to_heading(self, text: str, level: int):
        """Add heading string into an excel file."""
        # remember that level
        self._current_heading_level = level

        book = self._xlsx_writer.book

        if level == 0:
            book.create_sheet('Cover', 0)
            book.active = book['Cover']

            book.active['A1'] = f'TITLE - {text}'
            return

        if level == 1:
            book.create_sheet(text)
            book.active = book[text]
            return

        # create default sheet if there isn't one
        if not book.active:
            sheet = book.create_sheet()
            book.active = sheet

        # add heading as text line in the second free row
        start_row = book.active.max_row + 2
        # level 2 headings starting in the first column
        start_col = self._current_heading_level - 1
        book.active.cell(row=start_row, column=start_col).value = text

    def save(self, report: Report) -> _GenericDocumentOutput:
        """Start converting all report elements to an Excel file.

        The `pandas.ExcelWriter` with engine `openpyxl` is used.

        Returns:
            Itself.

        """
        # pylint: disable-next=abstract-class-instantiated
        with pandas.ExcelWriter(path=self.file_path,
                                engine='openpyxl') as writer:
            self._xlsx_writer = writer

            for element in report:

                if isinstance(element, Report):
                    self._not_implemented(element)
                    continue

                {
                    Report.Table: self.dataframe_to_table,
                    Report.Heading: self.string_to_heading,
                    Report.Paragraph: self._not_implemented,
                    Report.Control: self._not_implemented,
                    Report.References: self._not_implemented,
                    Report.Image: self._not_implemented,
                }[type(element)](**(element._asdict()))

        return self


# pylint: disable-next=too-many-instance-attributes
class DocxDocument(_GenericDocumentOutput):
    """Export a `Report` as MS Word ``docx`` file.

    The ``docx`` (known as ``python-docx``) package is used for that.
    """

    @staticmethod
    def _element_with_attribute(element_name: str,
                                attribute_name: str,
                                attribute_value: str) -> docx.oxml.OxmlElement:
        """Create an XML element with attribute and value."""
        element = docx.oxml.OxmlElement(element_name)
        element.set(docx.oxml.ns.qn(attribute_name), attribute_value)

        return element

    @staticmethod
    def _add_field(run, field):
        """Add a _field_ to an existing _run_.

        A _run_ is a part of a paragraph. Fields are visible in MS Word
        as elements with gray background (e.g. for page numbers or citation
        references).

        Credits: https://github.com/python-openxml/python-docx\
                 /issues/498#issuecomment-394143566

        Args:
            run: The run.
            field: The field.

        """
        # pylint: disable=protected-access

        fld_char1 = DocxDocument._element_with_attribute(
            'w:fldChar', 'w:fldCharType', 'begin')

        instr_txt = DocxDocument._element_with_attribute(
            'w:instrText', 'xml:space', 'preserve')

        instr_txt.text = field

        fld_char4 = DocxDocument._element_with_attribute(
            'w:fldChar', 'w:fldCharType', 'end')

        r_element = run._r
        r_element.append(fld_char1)
        r_element.append(instr_txt)
        r_element.append(fld_char4)

    @staticmethod
    def _add_page_number(paragraph: docx.text.paragraph.Paragraph,
                         with_pagenum: bool,
                         prefix: str,
                         infix: str,
                         suffix: str):
        """Add a page number field to an existing paragraph.

        Credits:  https://stackoverflow.com/a/62534711/4865723
        """
        # pylint: disable=protected-access

        # prefix "Page "
        t1 = DocxDocument._element_with_attribute(
            'w:t', 'xml:space', 'preserve')
        t1.text = prefix
        prefix_run = paragraph.add_run()
        prefix_run._r.append(t1)

        # field "PAGE"
        page_run = paragraph.add_run()
        DocxDocument._add_field(page_run, 'PAGE')

        if with_pagenum:
            # " of "
            infix_run = paragraph.add_run()
            t2 = DocxDocument._element_with_attribute(
                'w:t', 'xml:space', 'preserve')
            t2.text = infix
            infix_run._r.append(t2)

            # field "NUMPAGES"
            numpages_run = paragraph.add_run()
            DocxDocument._add_field(numpages_run, 'NUMPAGES')

        # suffix
        if suffix:
            t3 = DocxDocument._element_with_attribute(
                'w:t', 'xml:space', 'preserve')
            t3.text = suffix
            suffix_run = paragraph.add_run()
            suffix_run._r.append(t3)

    # pylint: disable-next=too-many-arguments
    def __init__(self,  # noqa: PLR0913
                 file_path: pathlib.Path,
                 *,
                 margins: Union[str, tuple] = 'narrow',
                 autoupdate_fields: bool = True,
                 tags_allowed: Union[List[str], str] = None,
                 tags_excluded: Union[List[str], str] = None,
                 text_header: str = None,
                 text_footer: str = None,
                 spell_and_grammar_checking: bool = False,
                 language: str = None,
                 captions_position_table: int = 1,
                 captions_position_figure: int = 1):
        """Ctor.

        Args:
            file_path: Path to the ``docx`` file to generate.
            margins: Names of predefined margins (``narrow`` or ``moderate``),
                a 4 item tuple with millimeter values clockwise starting with
                top, or a 2 item tuple with millimeter values for top-buttom
                and left-right.
            autoupdate_fields: Activate auto-updated all fields in the docx.
            tags_allowed: See `_GenericDocumentOutput`.
            tags_excluded: See `_GenericDocumentOutput`.
            text_header: Text to put in document header.
            text_footer: Text to put in document footer.
            spell_and_grammar_checking: Activate checking of spelling and
                grammar.
            language: Specify language used.
            captions_position_table: Captions before (0) or
                after (1, default) a table.
            captions_position_figure: Captions before (0) or
                after (1, default) a figure.

        """
        super().__init__(file_path=file_path,
                         tags_allowed=tags_allowed,
                         tags_excluded=tags_excluded)

        self._doc = None
        """Instance of ``docx.document.Document``."""

        self._margins = self._set_margins(margins)
        """A 4 item tuple with millimeter values clockwise starting with
        top.
        """

        self._autoupdate_fields = autoupdate_fields
        """Activate the auto update field feature."""

        self._header_text = text_header
        """String for the document header."""

        self._footer_text = text_footer
        """String for the document footer."""

        self._page_numbering = None
        """A `dict` with settings about page numbering."""

        self._spell_and_grammar_checking = spell_and_grammar_checking
        """Document global checking of spelling and grammar."""

        self._language = language
        """The language relevant for spell checking."""

        self._captions_position_table = captions_position_table
        """Position of table captions on top (0) or bottom of a table.
        Bottom is default."""

        self._captions_position_figure = captions_position_figure
        """Position of figure captions on top (0) or bottom of a figure.
        Bottom is default."""

        self._additional_styles = []
        """List of user defined styles add via `add_paragraph_style()`."""

    def _set_margins(self,
                     margins: Union[str, tuple]) -> tuple[int, int, int, int]:
        """See `__init__()` for details."""
        if isinstance(margins, str):
            try:
                margins = {
                    # margins "Narrow" (ger: "Normal")
                    'narrow': (25, 25, 20, 25),
                    # margins "Moderate" (ger: "Schmal")
                    'moderate': (12.7, 12.7, 12.7, 12.7),
                }[margins]

            except KeyError as exc:
                raise ValueError(
                    f'Unknown margins identifier "{margins}".') from exc

            return margins

        if len(margins) not in (2, 4):
            raise TypeError(f'Invalid margins: {margins}')

        if len(margins) == 2:
            return (margins[0], margins[1], margins[0], margins[1])

        return margins

    # pylint: disable-next=too-many-arguments
    def page_numbering(self,  # noqa: PLR0913
                       pos: Union[int, str],
                       *,
                       prefix: str = '\tPage ',
                       with_pagenum: bool = True,
                       infix: str = ' of ',
                       suffix: str = None,
                       ):
        """Set the page numbering behavior.

        Args:
            pos: Indicates header (``0``/``header``) or footer
                 (``1``/``footer``).
            prefix: String in front of the page number.
            with_pagenum: Add count of all pages.
            infix: String between current page number and all pages count.
            suffix: String at the end.

        """
        self._page_numbering = {
            'pos': pos,
            'prefix': prefix,
            'with_pagenum': with_pagenum,
            'infix': infix,
            'suffix': suffix
        }

    def _init_docx_document_instance(self):
        """Initiate and setup the document instance ``self._doc``.

        The method is not called by `__init__()` but `save()`.
        """
        self._doc = docx.Document()

        section = self._doc.sections[0]

        # page size A4
        section.page_height = docx.shared.Mm(297)
        section.page_width = docx.shared.Mm(210)

        # set margins
        section.top_margin = docx.shared.Mm(self._margins[0])
        section.right_margin = docx.shared.Mm(self._margins[1])
        section.bottom_margin = docx.shared.Mm(self._margins[2])
        section.left_margin = docx.shared.Mm(self._margins[3])

        # Footer/Header distance
        section.header_distance = docx.shared.Mm(12.5)
        section.footer_distance = docx.shared.Mm(12.5)

        # no space before or after paragraph
        self._doc.styles['Normal'].paragraph_format.space_before \
            = docx.shared.Pt(0)
        self._doc.styles['Normal'].paragraph_format.space_after \
            = docx.shared.Pt(0)

        # for TOC and caption numbering
        if self._autoupdate_fields:
            self._activate_auto_update_fields()

        # init styles
        self._init_styles()

        self._init_header_and_footer()

    @staticmethod
    def _color_to_rgbcolor(color: Union[str, tuple[int, int, int]]
                           ) -> docx.shared.RGBColor:
        """Convert "color" value into ``docx`` (``python-docx``) format.

        The package `webcolors` is used to do the conversion.

        Args:
            color: Name of a color, Hex Color code as string or an integer
                tuple with RGB values for red, green and blue.

        """
        rex_hexstring = r'^#[a-fA-F0-9]{6}$'

        # Rgb integers values
        if isinstance(color, (tuple, list)):
            return docx.shared.RGBColor(r=color[0], g=color[1], b=color[2])

        if not isinstance(color, str):
            raise TypeError(
                'Expect color as name, hex-string or tuple with three rgb '
                f'integer values. But it is {color=}.')

        # Hex value
        if re.search(rex_hexstring, color):
            rgb = webcolors.hex_to_rgb(color)
        else:
            # Assume color name
            rgb = webcolors.name_to_rgb(color)

        return docx.shared.RGBColor(r=rgb.red, g=rgb.green, b=rgb.blue)

    def _init_styles(self):
        """Create some additional styles."""
        # See https://stackoverflow.com/q/75601802/4865723
        # about how to get default font size of an empty document.

        # -- Buhtzology internal used styles --

        # Table (foot)note
        self.add_paragraph_style(name='BuhtzologyTableNote', font_pt=10)

        # Add all styles to the current document instance
        for style in self._additional_styles:
            curr = self._doc.styles.add_style(style['name'], style['type'])

            if 'font_name' in style:
                curr.font.name = style['font_name']

            if 'font_pt' in style:
                curr.font.size = docx.shared.Pt(style['font_pt'])

            if 'font_color' in style:
                curr.font.color.rgb \
                    = self._color_to_rgbcolor(style['font_color'])

            if 'font_bgcolor' in style:
                curr.font.highlight_color.rgb \
                    = self._color_to_rgbcolor(style['font_bgcolor'])

    def _init_header_and_footer(self):
        """Set up the header and footer elements of the document.

        The behavior is based on the instance attributes `_page_numbering`,
        `_header_text` and `_footer_text`.
        """
        # header
        if self._page_numbering:

            prefix = self._page_numbering['prefix']

            # header
            if self._page_numbering['pos'] in [0, 'header']:
                paragraph = self._doc.sections[0].header.paragraphs[0]

                # add header text as prefix
                if self._header_text:
                    prefix = self._header_text + ' ' + prefix
                    self._header_text = None

            # footer
            elif self._page_numbering['pos'] in [1, 'footer']:
                paragraph = self._doc.sections[0].footer.paragraphs[0]

                # add footer text as prefix
                if self._footer_text:
                    prefix = self._footer_text + ' ' + prefix
                    self._footer_text = None

            # don't know
            else:
                raise ValueError('Unknown position for page number. '
                                 f'{self._page_numbering}')

            DocxDocument._add_page_number(
                paragraph=paragraph,
                with_pagenum=self._page_numbering['with_pagenum'],
                prefix=prefix,
                infix=self._page_numbering['infix'],
                suffix=self._page_numbering['suffix']
            )

        # header
        if self._header_text:
            self._doc.sections[0].header \
                .paragraphs[0].add_run(self._header_text)

        # footer
        if self._footer_text:
            self._doc.sections[0].footer \
                .paragraphs[0].add_run(self._footer_text)

    def _activate_auto_update_fields(self):
        """All fields will semi-automatic updated.

        This includes table of content and numbering in table and figure
        captions. In practice when opening such a docx file MS Word will ask
        if all fields should be updated.

        Credits: https://stackoverflow.com/a/63799828/4865723
        """
        namespace = '{http://schemas.openxmlformats.org/' \
                    'wordprocessingml/2006/main}'

        # add child to doc.settings element
        element_updatefields = lxml.etree.SubElement(
            self._doc.settings.element,
            f'{namespace}updateFields')

        element_updatefields.set(f'{namespace}val', 'true')

    def available_styles(self) -> Dict[Hashable, list]:
        """Return a `dict` of available styles.

        The dict keys are the categories of styles. The values are the style
        names as list.

        .. note ::

            Seems to be unused. Also makes not much sense because
            `_doc` is `None` except while calling `save()`.
        """
        available_styles = defaultdict(list)

        for s in self._doc.styles:
            available_styles[str(s.__class__)].append(f'{s.name}')

        return available_styles

    def _table_of_something(self, kind: _ReferenceKind, depth: int):
        """Generic method to create a reference list (e.g. TOC).

        It is used for `table_of_contents()`.

        Credits ::

            The code is based on the following sources

            - ttps://stackoverflow.com/a/59170642/4865723
            - https://github.com/python-openxml/python-docx/issues/36
            - https://github.com/xiaominzhaoparadigm/python-docx-add-list-of-tables-figures/blob/master/LOT.py
        """  # noqa  # pylint: disable=line-too-long
        # pylint: disable=protected-access)

        run = self._doc.add_paragraph().add_run()

        fld_char = docx.oxml.OxmlElement('w:fldChar')
        fld_char.set(docx.oxml.ns.qn('w:fldCharType'), 'begin')

        fld_char.set(docx.oxml.ns.qn('w:dirty'), 'true')

        instr_txt = docx.oxml.OxmlElement('w:instrText')
        instr_txt.set(docx.oxml.ns.qn('xml:space'), 'preserve')

        instr_txt.text = {
            _ReferenceKind.CONTENT: f'TOC \\o "1-{depth}" \\h \\z \\u',
            _ReferenceKind.TABLES: 'TOC \\h \\z \\c "Table"',
            _ReferenceKind.FIGURES: 'TOC \\h \\z \\c "Figure"'
        }[kind]

        fld_char2 = docx.oxml.OxmlElement('w:fldChar')
        fld_char2.set(docx.oxml.ns.qn('w:fldCharType'), 'separate')

        # This was a workaround before we knew about semi-auto-update all
        # fields.
        # fldChar3 = docx.oxml.OxmlElement('w:t')
        # fldChar3.text = "Right-click to update field."

        fld_char3 = docx.oxml.OxmlElement('w:updateFields')
        fld_char3.set(docx.oxml.ns.qn('w:val'), 'true')
        fld_char2.append(fld_char3)

        fld_char4 = docx.oxml.OxmlElement('w:fldChar')
        fld_char4.set(docx.oxml.ns.qn('w:fldCharType'), 'end')

        run._r.append(fld_char)
        run._r.append(instr_txt)
        run._r.append(fld_char2)
        run._r.append(fld_char4)

    # pylint: disable-next=too-many-arguments
    def add_paragraph_style(self,  # noqa: PLR0913
                            name: str,
                            *,
                            based_on: str = None,
                            font_name: str = None,
                            font_pt: int = None,
                            font_color:
                                Union[tuple[int, int, int], str] = None,
                            font_bgcolor:
                                Union[tuple[int, int, int], str] = None):
        """Add a user-definied paragraph style to the document.

        Args:
            name: Name of the new style.
            based_on: Parent style that style should inherit from.
            font_name: Font name.
            font_pt: Font size in points.
            font_color: Foreground color as name, Hex-value or RGB value
                tuple.
            font_bgcolor: Background color of the font.

        A style can then be used via its name when adding new paragraphs:

        .. python::

            doc = DocxDocument(fp)
            report = Report()

            # Create the style
            doc.add_paragraph_style(name='MyStyle', font_name='Fira Code')

            # Use the style
            report.paragraph('lore ipsum', style='MyStlye')

        """
        # pylint: disable=unused-argument
        new_style = {
            'type': docx.enum.style.WD_STYLE_TYPE.PARAGRAPH,
            'name': name
        }

        arg_names = [
            'based_on', 'font_name', 'font_pt', 'font_color', 'font_bgcolor'
        ]

        for argn in arg_names:
            val = locals()[argn]

            if val:
                new_style[argn] = val

        self._additional_styles.append(new_style)

    def image(self,
              image_bytes: bytes,
              scale_factor: float = None,
              caption: str = None):
        """Embed  an image into the document.

        The original picture source is not scaled, resized or converted.
        Scaling is done "dynamic" via MS Word specifying the view-size in
        Millimeters. That value is calculated based on original pixel size,
        the DPI and the scaling factor ``scale_factor``.

        Plots and figures from `matplotlib` or derivates are saved in
        PNG format with `DEFAULT_DPI` dpi.

        Args:
            image_bytes: The image as bytes.
            scale_factor: Factor to scale.
            caption: Image caption string.

        """
        image_bytes = io.BytesIO(image_bytes)

        # image object
        image = PIL.Image.open(image_bytes)

        # # remember it's format because it is lost while resize()
        # image_format = image.format

        try:
            dpi = image.info['dpi']
        except KeyError:
            dpi = (DEFAULT_DPI, DEFAULT_DPI)

        # Original size in Millimeters
        width_inch = docx.shared.Inches(image.size[0] / dpi[0])
        height_inch = docx.shared.Inches(image.size[1] / dpi[1])

        # resize
        if scale_factor:
            width_inch = width_inch * scale_factor
            height_inch = height_inch * scale_factor

        if caption and self._captions_position_figure == 0:
            self._caption(caption=caption, target='Figure')

        # add image bytes to docx
        self._doc.add_picture(image_bytes,
                              width=width_inch,
                              height=height_inch)

        if caption and self._captions_position_figure == 1:
            self._caption(caption=caption, target='Figure')

    def save(self, report: Report) -> _GenericDocumentOutput:
        """Start the export of each element into the ``docx`` file.

        Args:
            report: The report instance with all elements.

        Returns:
            Itself.

        """
        logging.debug(f'Generate report document with {__class__}...')
        self._init_docx_document_instance()

        for element in report.get_filtered_elements(
                tags_allowed=self.tags_allowed,
                tags_excluded=self.tags_excluded):

            # call element method
            {
                Report.Table: self.dataframe_to_table,
                Report.Heading: self.string_to_heading,
                Report.Paragraph: self.string_to_paragraph,
                Report.Enumeration: self.strings_to_enumeration,
                Report.Image: self.image,
                Report.Control: self.control_to,
                Report.References: self._table_of_something,
            }[type(element)](**(element._asdict()))

        self._setup_spell_and_grammar_checking()

        self._setup_language()

        logging.debug(f'Try to save report document {self.file_path}...')
        try:
            self._doc.save(str(self.file_path))

        except PermissionError:
            # This can happen when the file is exclusively open (e.g. by MS
            # Word).
            timestamp = datetime.now().strftime('%Y%m%d%H%M%S')
            self.file_path = self.file_path.with_suffix(
                f'.{timestamp}{self.file_path.suffix}')

            logging.error('Unable to save because of PermissionError. '
                          f'Using an alternative filename {self.file_path}.')

            return self.save(report)

        return self

    def _setup_language(self):
        """Documents language.

        The language is set based on `_language` for all existing styles.
        """
        if not self._language:
            return

        # Credits: https://stackoverflow.com/a/63343007/4865723
        for my_style in self._doc.styles:

            style = self._doc.styles[my_style.name]

            rpr = style.element.get_or_add_rPr()

            if not rpr.xpath('w:lang'):

                element = DocxDocument._element_with_attribute(
                    'w:lang', 'w:val', self._language)
                # element.set(docx.oxml.shared.qn('w:eastAsia'),'en-US')
                # element.set(docx.oxml.shared.qn('w:bidi'),'ar-SA')
                rpr.append(element)

    def _setup_spell_and_grammar_checking(self):
        if self._spell_and_grammar_checking is False:
            for tag in ['w:hideSpellingErrors', 'w:hideGrammaticalErrors']:
                element = docx.oxml.OxmlElement(tag)
                self._doc.settings.element.append(element)

    def _set_page_orientation(self, orientation):
        """Set page orientation if different."""
        if self._doc.sections[-1].orientation != orientation:
            self.toggle_page_orientation()

    def set_page_orientation_portrait(self):
        """Page/section in portrait orientation."""
        self._set_page_orientation(docx.enum.section.WD_ORIENT.PORTRAIT)

    def set_page_orientation_landscape(self):
        """Page/section in landscape orientation."""
        self._set_page_orientation(docx.enum.section.WD_ORIENT.LANDSCAPE)

    def toggle_page_orientation(self, section: docx.section.Section = None):
        """Toggle the current page/section's orientation."""
        # Use current section by default
        if not section:
            section = self._doc.sections[-1]

        h = section.page_height
        w = section.page_width

        if section.orientation == docx.enum.section.WD_ORIENT.LANDSCAPE:
            section.orientation = docx.enum.section.WD_ORIENT.PORTRAIT
        else:
            section.orientation = docx.enum.section.WD_ORIENT.LANDSCAPE

        section.page_width = h
        section.page_height = w

    def control_to(self, control_type: _ControlType):
        """Convert `_ControlType` elements into MS Word document components.

        Such components are page breaks, page orientation, reference lists for
        headings, figures and tables.
        """
        # page break
        if control_type is _ControlType.PAGE_BREAK:
            self._doc.add_section()
            return

        # orientation landscape/portrait
        if control_type is _ControlType.ORIENTATION_PORTRAIT:
            self.set_page_orientation_portrait()
            return
        if control_type is _ControlType.ORIENTATION_LANDSCAPE:
            self.set_page_orientation_landscape()
            return

        # toggle orientation
        if control_type is _ControlType.ORIENTATION_TOGGLE:
            self.toggle_page_orientation()
            return

        raise ValueError(f'Unknown control type "{control_type}".')

    def string_to_heading(self, text: str, level: int):
        """Add heading."""
        self._doc.add_heading(text, level=level)

    def string_to_paragraph(self, text: str, style: str):
        """Add paragraph."""
        self._doc.add_paragraph(text=text, style=style)

    def strings_to_enumeration(self,
                               items: tuple[str],
                               ordered: bool,
                               style: str) -> None:
        """Add a paragraph as enumeration item.

        The argument ``ordered`` is ignored if argument ``style`` is used.

        Args:
            items: List of strings as enumeration items.
            ordered: If ``True`` style 'List Number' is used otherwise 'List
                Bullet'.
            style: Explicit naming the style.
        """
        if style is None:
            style = 'List Number' if ordered else 'List Bullet'

        for list_item in items:
            self.string_to_paragraph(text=list_item, style=style)

    @staticmethod
    def _table_autofit(tab: docx.table.Table):
        """Autofit objects in table.

        Credits: https://github.com/python-openxml/python-docx\
                 /issues/209#issuecomment-566128709
        """
        # pylint: disable=protected-access)

        for row_idx, _ in enumerate(tab.rows):
            for cell_idx, _ in enumerate(tab.rows[row_idx].cells):
                tab.rows[row_idx].cells[cell_idx]._tc.tcPr.tcW.type = 'auto'
                tab.rows[row_idx].cells[cell_idx]._tc.tcPr.tcW.w = 0

    def _caption(self, caption: str, target: str):
        """Add caption paragraph including name and numbering.

        Args:
            caption: The content of the caption.
            target: The prefix name (e.g. _Table_ or _Figure_)

        Based on: https://github.com/python-openxml/python-docx/issues/359

        """
        # pylint: disable=protected-access

        # caption type
        paragraph = self._doc.add_paragraph(f'{target} ', style='Caption')

        # numbering field
        run = paragraph.add_run()

        fld_char = docx.oxml.OxmlElement('w:fldChar')
        fld_char.set(docx.oxml.ns.qn('w:fldCharType'), 'begin')
        run._r.append(fld_char)

        instr_txt = docx.oxml.OxmlElement('w:instrText')
        instr_txt.text = f' SEQ {target} \\* ARABIC'
        run._r.append(instr_txt)

        fld_char = docx.oxml.OxmlElement('w:fldChar')
        fld_char.set(docx.oxml.ns.qn('w:fldCharType'), 'end')
        run._r.append(fld_char)

        # caption text
        paragraph.add_run(f' {caption}')

    @staticmethod
    def _cell_value_to_formated_string(cell_value, decimal_places) -> str:
        """Convert `pandas.DataFrame` cell values to a MS Word table.

        The original ``cell_value`` is converted into a string. Decimal point
        and Thousand delimiter used based on the current `locale.LC_NUMERIC`. A
        float is rounded based on ``decimal_places`` using `round()`. Items of
        an iterable are treated the same way.
        """
        # Integer with thousands delimiter
        if pandas.api.types.is_integer_dtype(type(cell_value)):
            return locale.format_string('%d', cell_value, grouping=True)

        # Float with thousands delimiter and float with correct rounding
        if pandas.api.types.is_float_dtype(type(cell_value)):
            return locale.format_string(
                f'%.{decimal_places}f',
                cell_value,
                grouping=True)

        # List, tuple, ...
        if pandas.api.types.is_list_like(cell_value):
            return type(cell_value)(
                [DocxDocument._cell_value_to_formated_string(
                    item_val, decimal_places) for item_val in cell_value]
            )

        # default as string
        return cell_value if isinstance(cell_value, str) else str(cell_value)

    @staticmethod
    def _multiindex_to_dict(idx: pandas.MultiIndex) -> dict:
        """Convert a MultiIndex to dictionary.

        Helper function used by `_add_row_labels_two_levels()` and
        `_add_column_labels_two_levels()`.

        Args:
            idx: Index object (columns or rows of a pandas dataframe)

        Returns:
            The dictionary indexed by the first level of the multi index.

        """
        result = defaultdict(list)

        for a, b in idx:
            if not isinstance(a, str):
                a = str(a)
            if not isinstance(b, str):
                b = str(b)
            result[a].append(b)

        return result

    @staticmethod
    def _add_row_labels_one_level(df, tab):
        """Use in `_add_row_and_column_labels()`."""
        # row names
        for idx, row_name in enumerate(df.index.astype(str),
                                       start=df.columns.nlevels):
            # label
            tab.cell(idx, 0).text = row_name
            # bold
            tab.cell(idx, 0).paragraphs[0].runs[0].bold = True

    @staticmethod
    def _add_row_labels_two_levels(df, tab):
        """Use in `_add_row_and_column_labels()`."""
        # convert row multiindex into dict
        row_header = DocxDocument._multiindex_to_dict(df.index)

        # merge cells
        b_idx = df.columns.nlevels
        for a_val in row_header:

            # a value
            tab.cell(b_idx, 0).text = a_val
            tab.cell(b_idx, 0).paragraphs[0].runs[0].bold = True

            # merge
            for m_offset in range(1, len(row_header[a_val])):
                tab.cell(b_idx, 0).merge(
                    tab.cell(b_idx + m_offset, 0)
                )

            # b value
            for b_offset, b_val in enumerate(row_header[a_val]):
                tab.cell(b_idx + b_offset, 1).text = b_val
                tab.cell(b_idx + b_offset, 1).paragraphs[0].runs[0].bold = True

            # next
            b_idx = b_idx + len(row_header[a_val])

    @staticmethod
    def _add_column_labels_one_level(df, tab):
        """Use in `_add_row_and_column_labels()`."""
        for idx, col_name in enumerate(df.columns.astype(str),
                                       start=df.index.nlevels):
            tab.cell(0, idx).text = col_name
            # centered
            tab.cell(0, idx).paragraphs[0].alignment \
                = docx.enum.text.WD_ALIGN_PARAGRAPH.CENTER
            # bold
            tab.cell(0, idx).paragraphs[0].runs[0].bold = True

    @staticmethod
    def _add_column_labels_two_levels(df, tab):
        """Use in `_add_row_and_column_labels()`."""
        # convert column multiindex into dict
        column_header = DocxDocument._multiindex_to_dict(df.columns)

        # merge cells
        b_idx = df.index.nlevels
        for a_val in column_header:

            # a value
            tab.cell(0, b_idx).text = a_val
            tab.cell(0, b_idx).paragraphs[0].runs[0].bold = True
            tab.cell(0, b_idx).paragraphs[0].alignment \
                = docx.enum.text.WD_ALIGN_PARAGRAPH.CENTER

            # If the current column is MultiIndex but has only one entry
            m_offset = 0

            # merge
            for m_offset in range(1, len(column_header[a_val])):

                tab.cell(0, b_idx).merge(
                    tab.cell(0, b_idx + m_offset)
                )

            # b value
            for b_offset, b_val in enumerate(column_header[a_val]):
                tab.cell(1, b_idx + b_offset).text = b_val
                tab.cell(1, b_idx + b_offset).paragraphs[0].runs[0].bold = True
                tab.cell(1, b_idx + b_offset).paragraphs[0].alignment \
                    = docx.enum.text.WD_ALIGN_PARAGRAPH.CENTER

            # next
            b_idx = b_idx + m_offset + 1

    @staticmethod
    def _add_row_and_column_labels(df, tab):
        """Format the labels of rows and columns."""
        # rows
        try:
            func_add_row_labels = {
                1: DocxDocument._add_row_labels_one_level,
                2: DocxDocument._add_row_labels_two_levels,
            }[df.index.nlevels]
        except KeyError as err:
            raise ValueError(
                'Only one- or two-level indexes allowed.') from err

        func_add_row_labels(df, tab)

        # columns
        try:
            func_add_column_labels = {
                1: DocxDocument._add_column_labels_one_level,
                2: DocxDocument._add_column_labels_two_levels,
            }[df.columns.nlevels]
        except KeyError as err:
            raise ValueError(
                'Only one- or two-level indexes allowed.') from err

        func_add_column_labels(df, tab)

        # top left cell
        if df.index.name:
            tab.cell(0, 0).text = df.index.name

    # pylint: disable-next=too-many-arguments
    def dataframe_to_table(self,  # noqa: PLR0913
                           df: pandas.DataFrame,
                           *,
                           caption: str,
                           note: str,
                           autofit: bool,
                           decimal_places: int = 2):
        """Convert a `pandas.DataFrame` in a MS Word table.

        Long tables can cause performance problems. A warning is logged on
        long tables.

        Args:
            df: The data frame.
            caption: String for the table caption.
            note: Used as (foot)note.
            autofit: Auto adjust the width of the table to its content.
            decimal_places: Round of float values.

        """
        if isinstance(df, pandas.Series):
            _log.error('Table is a series and will be converted to a data '
                       f'frame. Table caption: "{caption}".')
            df = df.to_frame()

        if len(df) > 200:
            _log.warning(
                f'The table seems to be very big ({len(df)} rows). '
                'Processing it can take its time.\n'
                f'Columns: {df.columns}\nCaption: {caption}')

        if caption and self._captions_position_table == 0:
            self._caption(caption=caption, target='Table')

        # create table object
        tab = self._doc.add_table(
            rows=df.columns.nlevels + df.shape[0],
            cols=df.index.nlevels + df.shape[1]
        )

        # borders for all cells (a "grid")
        tab.style = self._doc.styles['Table Grid']

        # each dataframe row
        for row_idx in range(df.shape[0]):

            # each dataframe cell/column
            for cell_idx in range(df.shape[1]):

                cell_val = df.iloc[row_idx, cell_idx]

                # correct display format based on data type
                cell_val = self._cell_value_to_formated_string(
                    cell_value=cell_val,
                    decimal_places=decimal_places
                )

                # corresponding cell in the docx table
                tab_cell = tab.cell(
                    row_idx=row_idx + df.columns.nlevels,
                    col_idx=cell_idx + df.index.nlevels
                )

                # add value to table cell
                tab_cell.text = cell_val

                # right aligned
                tab_cell.paragraphs[0].alignment \
                    = docx.enum.text.WD_ALIGN_PARAGRAPH.RIGHT

        # row & column labels
        self._add_row_and_column_labels(df, tab)

        if autofit:
            self._table_autofit(tab)

        if note:
            self.string_to_paragraph(text=note, style='BuhtzologyTableNote')

        if caption and self._captions_position_table == 1:
            self._caption(caption=caption, target='Table')

        self.string_to_paragraph(text='', style=None)
