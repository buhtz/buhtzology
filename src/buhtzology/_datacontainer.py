"""The bandas data container."""
# pylint: disable=too-many-lines
from __future__ import annotations
import sys
import io
import inspect
import pathlib
import logging
from typing import Union, List, Dict, Iterable
import contextlib
import zipfile
import json
import datetime
import shutil
import hashlib
import pandas
from buhtzology import break_paragraph, nested_dict_update

_log = logging.getLogger(__name__)


class DataContainer:
    """A container managing multiple `pandas.DataFrame`.

    The container can operate in two modes. In *folder mode* the container is
    located in a folder on the file system and each data frame has its own
    file. I *archive mode* the container is one single compressed (ZIP)
    archive with each data frame as an entry in it.

    Two types of data frames are distinguished. The *on demand* data frames
    loaded when they are accessed the first time. A *permanent* data frame is
    loaded right on time when the container is instantiated. Which mode is
    used depends on the path name used; ``*.zip`` results in *archive mode*
    and everything else in *folder mode*.

    In *folder mode* a data frame can be stored as ``*.pickle`` or as a (ZIP)
    compressed ``*.pickle.zip`` file. By default the former is used for
    *permanent* and the latter for *on demand* data frames.

    .. warning::

        Despite a container store (multiple) data frames in the file system it
        is **not** intended to be long term archive format. It is a temporary
        storage and can be used between running two different scripts
        or steps of research.

    Example:

        .. python::

            # Data for the container organized in a dict.
            dataframes = {
                'Foo': pandas.DataFrame(),
                'Bar': pandas.DataFrame()
            }

            # Create container object
            dc = DataContainer(dataframes=dataframes, permanent_names=['Bar'])

            # Save containers as one ZIP file (archive mode)
            fp = pathlib.Path('data.zip')
            dc.save(fp)

            # Load the container
            dc = DataContainer(fp)

            # 'Bar' is loaded immediately because it was marked as
            # permanent. But 'Foo' is not loaded yet.
            print(dc.names)  # ['Bar', 'Foo']
            print(dc.Bar.head())
            print(dc.Foo.head())  # <- Now "Foo" is going to be loaded

            # Add a new data frame
            dc.Elke = pandas.DataFrame()

            # Save in ZIP
            dc.store(fp)

            # Save again in folder mode
            fp = pathlib.Path('folder')
            dc.store(fp)

    """

    _ARCHIVE_SUFFIX = '.zip'
    """Filename suffix for ZIP compressed container in archive mode."""

    _PICKLE_SUFFIX = '.pickle'
    """Filename suffix for pickle files."""

    _PICKLE_ZIP_SUFFIX = '.pickle.zip'
    """Filename suffix for zip-compressed pickle files."""

    # There is a known bug.
    # https://github.com/python/cpython/issues/88605
    # Try again with 5 if latest Python 3.9 is installed (3.9.16)
    _PICKLE_PROTOCOL = 4
    """The Pickle protocol version used."""

    _META_FILENAME = '__meta'
    """Name of the meta data holding file."""

    def __init__(self,
                 dataframes: Dict[str, pandas.DataFrame] = None,
                 permanent_names: List[str] = None,
                 meta: Dict = None):
        """Create a container in memory.

        Args:
            dataframes: Dictionary of `pandas.DataFrame`'s.
            permanent_names: List of data frame names. See `load()` for
                details.
            meta: Dictionary with additional data.

        """
        self._source_obj = None
        """Path to the file or folder the container was loaded from."""

        self._dataframes = {}
        """Dictionary of date frame's."""

        # Development note: Use the dictionaieres entries and not the
        # dictionary itself. The later caused side effects under specific
        # circumstances.
        if dataframes:
            for df_name in dataframes:
                self._dataframes[df_name] = dataframes[df_name]

        if permanent_names is None:
            permanent_names = []

        self._names_on_demand \
            = list(filter(
                lambda dfn: dfn not in permanent_names,
                self._dataframes.keys()))
        """List of data frame names which are of type *on-demand*."""

        self._meta = {} if meta is None else meta
        """The meta data (e.g. version number) as `dict`."""

        self._dataframe_hashes = {}
        """Hash values of loaded data frames (folder mode only)."""

        # Test if the names are present in the dataframes list.
        unknown = set(permanent_names) - set(self._dataframes.keys())
        unknown = list(unknown)

        if unknown:
            raise ValueError(
                'Not all given permanent '  # pylint: disable=C0209
                'names are present as dataframes.\nKnown names: {}\n'
                'given permanent: {}\nunknown permanent: {}'
                .format(', '.join([f'"{n}"' for n in self._dataframes]),
                        ', '.join([f'"{n}"' for n in permanent_names]),
                        ', '.join([f'"{n}"' for n in unknown])))

    def __str__(self) -> str:
        """Literal description of the container.

        Literal description of the container containing number of data
        frames, names of permanent and on-demand data frames and meta
        information.
        """
        return ('{} dataframes. Permanent: {}; '  # pylint: disable=C0209
                'On-Demand: {}; Meta-Info: {}').format(
                    len(self),
                    ', '.join(self.names_permanent),
                    ', '.join(self._names_on_demand),
                    self.meta)

    def __delattr__(self, name):
        """Delete a data frame by its attribute name (e.g. ``del dc.foo``).

        The underlying files are not deleted.
        """
        if name in self._names_on_demand:
            self._names_on_demand.remove(name)

        elif name not in self.names_permanent:
            raise AttributeError(
                f'Data frame "{name}" does not exist in DataContainer.')

        try:
            del self._dataframes[name]
        except KeyError:
            # This isn't a problem and happens if "name" is an
            # ondemand dataframe not loaded yet.
            pass

        _log.info(f'Removed "{name}" from DataContainer.')

    def __delitem__(self, key):
        """Delete a data frame by its key name (e.g. ``del dc['foo']``).

        The underlying files are not deleted.
        """
        self.__delattr__(key)

    def __getitem__(self, key_or_idx: Union[int, str]
                    ) -> Union[str, pandas.DataFrame]:
        """Get a data frame by using its name as key (``dc[name]``).

        Dict like behavior of the container.
        Using a numerical index (``dc[0]``)
        will return the name of a data frame as `str`.
        *On-demand* data frames are loaded automatically in background.

        Example:

            .. python::
                dc = bandas.DataContainer.load('data.zip')

                # Iterate over dataframe names (like a dict)
                for name in dc:
                    print(name)

                    # Use the name like a key
                    print(dc[name].head())

        Args:
            key_or_idx: Name of a data frame or index of its name.

        """
        # dataframe by name
        if isinstance(key_or_idx, str):

            try:
                return getattr(self, key_or_idx)

            except AttributeError as err:
                raise KeyError(key_or_idx) from err

        # Name of a dataframe by its numeric index.
        # Used when iterate over the keys like with a dict.
        return self.names[key_or_idx]

    def __setitem__(self, key: str, dataframe: pandas.DataFrame):
        """Add new data frame via key.

        The data frame is added to the container as *on-demand*. Change it to
        *permanent* when using `save()`.

        Args:
            key: Name of dataframe.
            dataframe: The dataframe.

        """
        self.__setattr__(key, dataframe)

        _log.info(f'Added "{key}" to DataContainer.')

    def __len__(self) -> int:
        """Count of all data frames.

        The number is based on the `names` property.
        """
        return len(self.names)

    def __setattr__(self, attr: str, value: pandas.DataFrame):
        """Add new data frame via attribute name.

        Args:
            attr: Name of the data frame.
            value: The data frame.

        """
        if isinstance(value, pandas.DataFrame):

            if attr not in self.names:
                self._names_on_demand.append(attr)

            self._dataframes[attr] = value

            return

        super().__setattr__(attr, value)

    def __getattr__(self, attr: str) -> pandas.DataFrame:
        """Get data frame as instance attribute.

        Get a data frame by using its name as instance
        attribute name (``dc.foobar``).
        *On-demand* data frames are loaded automatically in background.

        Example:

            .. python::
                dc = bandas.DataContainer.load('data.zip')

                # Access dataframe as instance attribute.
                # The dataframe's name here is "foobar".
                dc.foobar.head()

        Args:
            attr: Name of a data frame.

        """
        if attr in self.names:

            try:
                return self._dataframes[attr]

            except KeyError:
                # existing but not loaded?
                self._load_dataframe(attr)

            return getattr(self, attr)

        raise AttributeError(
            f'type object {self.__class__} has no attribute "{attr}"')

    def describe_with_markdown(self,
                               head_n: int = 4,
                               width: int = 80,
                               **kwargs) -> str:
        """Describe all dataframe of the container as markdown tables.

        The ``width`` is respected for markdown tables also.

        Args:
            head_n: Number of data rows per frame.
            width: With in characters of each table.
            **kwargs: Used to handover arguments to
                `buhtzology.break_paragraph()`.

        Returns:
            The result as a string.

        """
        desc = []

        for dfn in self.names:
            desc.append(f'==== {dfn} ====')
            tab = self[dfn].iloc[:head_n].to_markdown().split('\n')
            if width:
                tab = break_paragraph(
                    tab, width=width, sep_line='', **kwargs)
            desc.extend(tab)
            desc.extend([''])

        return '\n'.join(desc[:-1])

    @property
    def meta(self) -> dict:
        """The meta data (e.g. version number) as `dict`.

        TODO explain '_created' key, etc pp
        """
        return self._meta

    def update_meta_info(self, value: dict):
        """Update meta info.

        New keys are added and existing keys are overwritten with respect to
        nested dicts.
        """
        self._meta = nested_dict_update(self._meta, value)

    def _compute_filepath_for_dataframe(self,
                                        name_prefix: str,
                                        df_name: str) -> pathlib.Path:
        """Try to find the path to a data frame's file.

        Returns:
            The path to the file if it exists.

        Raises:
            FileNotFoundError

        """
        if name_prefix is None:
            name_prefix = '_' if df_name in self.names_permanent else ''

        # construct the file path
        fp = self._source_obj / f'{name_prefix}{df_name}'

        # including suffix
        for suffix in [DataContainer._PICKLE_ZIP_SUFFIX,
                       DataContainer._PICKLE_SUFFIX]:

            if fp.with_suffix(suffix).exists():
                return fp.with_suffix(suffix)

        raise FileNotFoundError(
            f'Could not find file "{fp.with_suffix(suffix)}" for '
            f'dataframe "{df_name}" ({name_prefix=}).')

    def _is_compressed(self, name: str) -> bool:
        """Determine if the data frame file is compressed.

        Args:
            name: Name of a data frame.

        Raises:
            TypeError: If container was not stored yet or is in archive mode.

        """
        try:
            if self._source_obj.is_dir() is False:
                raise TypeError(
                    'Because the data container was not loaded in folder mode '
                    f'there is no separate file for the data frame "{name}".')
        except AttributeError as exc:
            raise TypeError('The data container seems to have not been saved '
                            'yet and therefore has no files.') from exc

        fp = self._compute_filepath_for_dataframe(
            name_prefix=None, df_name=name)

        return fp.name.endswith(self._PICKLE_ZIP_SUFFIX)

    def _generate_creation_info(self) -> dict:
        return {
            'isodatetime': datetime.datetime.now().isoformat(),
            'call': ' '.join(sys.argv),
            # Credits: https://stackoverflow.com/a/67201621/4865723
            'script': inspect.stack()[1].filename,
        }

    @classmethod
    def load(cls,
             file_path: Union[pathlib.Path],
             meta_to_check: Dict = None) -> DataContainer:
        """Load a data container from the path.

        Data frame files beginning with a ``_`` prefix are treated as
        *permanent* and loaded immediately. Other data frames are treated as
        *on-demand* and loaded from files only when they are accessed.

        If the container contain meta information's (file ``__meta``) and
        the argument ``meta_to_check`` is given this two dictionaries are
        compared. An exception is raised when they are not equal.
        Could be used for a version number or a date. See `_check_meta_info()`
        for details.

        Args:
            file_path: Path to file or folder of the container.
            meta_to_check: Meta info's to validate.

        Returns:
            The container object.

        """
        _log.info(f'Begin to load data container from {file_path}. '
                  'Please wait...')

        if isinstance(file_path, pathlib.Path) and file_path.is_dir():
            container = cls._load_from_folder(file_path)
        else:
            container = cls._load_from_archive(file_path)

        if meta_to_check:
            container._check_meta_info(meta_to_check)

        return container

    @classmethod
    def _load_from_folder(cls, folder_path: pathlib.Path) -> DataContainer:
        """Load a container from a folder.

        See `load()` for details.
        """
        _log.debug(f'Loading container in folder mode from "{folder_path}".')

        container = cls()

        names_on_demand = []
        names_permanent = []

        # Store indicators about the type/behavior of a dataframe
        dfs_status = []

        for fp in folder_path.iterdir():

            # meta info?
            if fp.name == cls._META_FILENAME:
                with fp.open('r') as handle:
                    container._meta = json.load(handle)
                continue

            # Determine if it is a pickle or a zip-compressed pickle
            if fp.name.endswith(cls._PICKLE_ZIP_SUFFIX):
                suffix = cls._PICKLE_ZIP_SUFFIX
                indicators = 'z'

            elif fp.name.endswith(cls._PICKLE_SUFFIX):
                suffix = cls._PICKLE_SUFFIX
                indicators = '_'

            else:
                raise ValueError(f'{fp.name=} {fp=}')

            # regular data entry
            df_name = fp.name.replace(suffix, '')

            # on-demand or permanent
            if df_name.startswith('_'):
                names_permanent.append(df_name[1:])
                dfs_status.append((df_name[1:], 'P' + indicators))
            else:
                names_on_demand.append(df_name)
                dfs_status.append((df_name, 'O' + indicators))

        container._source_obj = folder_path
        container._names_on_demand = names_on_demand

        # load permanent
        for df_name in names_permanent:
            container._load_dataframe(df_name)

        container._log_loaded(folder_path, 'FOLDER', dfs_status)

        return container

    @classmethod
    def _load_from_archive(cls, archive_path: pathlib.Path) -> DataContainer:
        """Load a container from an archive file.

        See `load()` for details.
        """
        _log.debug(f'Loading container in archive mode from "{archive_path}".')

        container = cls()

        names_on_demand = []
        names_permanent = []

        # Store indicators about the type/behavior of a dataframe
        dfs_status = []

        with zipfile.ZipFile(archive_path, mode='r') as zip_handle:

            # each entry in the archive file
            for entry_filename in zip_handle.namelist():

                # meta info?
                if entry_filename == cls._META_FILENAME:
                    with zip_handle.open(entry_filename, 'r') as zip_buf:
                        container._meta = json.load(zip_buf)
                    continue

                # regular data entry
                df_name = entry_filename.replace(
                    cls._PICKLE_SUFFIX, '')

                # on-demand or permanent
                if not df_name.startswith('_'):
                    names_on_demand.append(df_name)
                    dfs_status.append((df_name, 'O_'))
                else:
                    names_permanent.append(df_name[1:])
                    dfs_status.append((df_name[1:], 'P_'))

        container._source_obj = archive_path
        container._names_on_demand = names_on_demand

        container._log_loaded(archive_path, 'ARCHIVE', dfs_status)

        # load permanent
        for df_name in names_permanent:
            container._load_dataframe(df_name)

        return container

    def _log_loaded(self,
                    path: pathlib.Path,
                    mode: str,
                    indicators: list[tuple[str, str]]):
        """Create log message about a loaded container."""
        msg = self._assemble_log_message(path, mode, indicators)
        _log.info(f'Container loaded:\n{msg}')

    def _log_save(self,
                  path: pathlib.Path,
                  mode: str,
                  indicators: list[tuple[str, str]]):
        """Create log message about a stored container."""
        msg = self._assemble_log_message(path, mode, indicators)
        _log.info(f'Save Container:\n{msg}')

    def _assemble_log_message(self,
                              path: pathlib.Path,
                              mode: str,
                              indicators: list[tuple[str, str]]) -> str:

        frames = [
            f'[{indi}] '.ljust(5) + dfname for dfname, indi in indicators]

        frames = [f'n={len(self.names)}'] + frames

        template = [
            (mode, path.name),
            ('FULL PATH', path),
            ('DATAFRAMES', frames),
            ('META', None),
        ]

        msg = self._format_log_data(
            entries=template, meta=self.meta, njust=10)

        return msg

    @classmethod
    def _format_log_data(cls,
                         entries: list[tuple],
                         meta: dict,
                         njust: int) -> str:
        """Format log data.

        A helper method for `_log_save()` and `_log_loaded()`.
        """
        def _indent_text_block(text: str, indent: int) -> str:
            """Indents a multi line string."""
            return '\n'.join(
                '{}{}'.format(' ' * indent, line)  # pylint: disable=C0209
                for line in text.split('\n'))

        msg = ''
        sep = ' : '

        for label, value in entries:

            if isinstance(value, list):
                value = _indent_text_block(
                    text='\n'.join(value),
                    indent=njust + len(sep))
                value = value.lstrip()

            msg = '{}\n{}{}{}'.format(  # pylint: disable=C0209
                msg,
                label.rjust(njust),
                sep,
                value if value else '')

        msg = msg[1:]  # cut first newline

        indent = njust - 4

        if meta:
            meta_string = json.dumps(meta, indent=2)
            meta_string = _indent_text_block(meta_string, indent).lstrip()
        else:
            meta_string = '(n.a.)'

        msg = msg + meta_string

        msg = _indent_text_block(msg, 4)

        return msg

    def _check_meta_info(self, meta_check: Dict):
        """Compare the meta data given by ``meta_check`` and in `meta`.

        The ordering of the keys is ignored.

        Args:
            meta_check: A dictionary with expected meta data.

        Raises:
            ValueError if the dicts have different keys and/or values.

        """
        # no problem if nothing to check
        if not meta_check:
            return

        if meta_check.keys() != self.meta.keys():
            raise ValueError(
                'Fields '  # pylint: disable=consider-using-f-string
                "in Meta-Information's check different!\n"
                'Read:\n{}\nCheck:\n{}'
                .format(json.dumps(self.meta, indent=4),
                        json.dumps(meta_check, indent=4)))

        for key in meta_check:
            if meta_check[key] != self.meta[key]:
                raise ValueError(
                    'Values '  # pylint: disable=consider-using-f-string
                    "in Meta-Information's check different!\n"
                    'Read:\n{}\nCheck:\n{}'
                    .format(json.dumps(self.meta, indent=4),
                            json.dumps(meta_check, indent=4)))

    def _unload_dataframe(self, df_name: str):
        """Unload a data frame from the container.

        Only data frames of type on demand can be unloaded. Permanent data
        frames will raise a `ValueError`. If the data frame is not loaded
        but do exist nothing is raised.

        Args:
            df_name: Name of the data frame.

        Raises:
            KeyError: If data frame do not exist.
            ValueError: If the data frames is of permanent type.
            TypeError: If data frame is modified.

        """
        if df_name not in self.names:
            raise KeyError(f'Data frame "{df_name}" do not exist in this '
                           'data container.')

        if df_name in self.names_permanent:
            raise ValueError(f'Not allowed to unload "{df_name}" because it '
                             'is a permanent data frame.')

        if self._is_dataframe_modified(df_name):
            raise TypeError(f'Not allowed to unload "{df_name}" because it '
                            'is modified.')

        try:
            del self._dataframes[df_name]
        except KeyError:
            # ignore unloaded
            pass

    def _load_dataframe(self, df_name: str):
        """Load one specific data frame from its file.

        Args:
            df_name: Name of the data frame.

        """
        # 'ondemand' or '_permanent'
        name_prefix = '' if df_name in self._names_on_demand else '_'

        # Folder or archive mode?
        if (isinstance(self._source_obj, pathlib.Path)
                and self._source_obj.is_dir()):
            self._load_dataframe_from_folder(name_prefix, df_name)
        else:
            self._load_dataframe_from_archive(name_prefix, df_name)

        _log.debug(f'{len(self[df_name]):n} rows in "{df_name}"')

    def _load_dataframe_from_folder(self, name_prefix: str, df_name: str):
        """Load a data frame from a folder."""
        fp = self._compute_filepath_for_dataframe(name_prefix, df_name)

        dbg_msg = f'Loading dataframe "{df_name}" from "{fp}"'

        if fp.name.endswith(self._PICKLE_ZIP_SUFFIX):
            _log.debug(f'Unzip {fp} into RAM...')
            with zipfile.ZipFile(fp) as zip_handle:
                pickle_in_ram = zip_handle.read(fp.with_suffix('').name)
                pickle_in_ram = io.BytesIO(pickle_in_ram)
                _log.debug(f'{dbg_msg} via reading pickle from RAM...')
                df = pandas.read_pickle(pickle_in_ram)
        else:
            _log.debug(f'{dbg_msg} via regular loading...')
            # read the data frame
            df = pandas.read_pickle(fp)

        self._dataframes[df_name] = df

        # hash current state of the dataframe
        self._dataframe_hashes[df_name] = self._hash_dataframe(df_name)

    def _load_dataframe_from_archive(self, name_prefix: str, df_name: str):
        """Load a data frame from an archive."""
        # Path to pickle file as ZIP-file entry
        zip_entry_path = zipfile.Path(
            root=self._source_obj,
            at=f'{name_prefix}{df_name}{DataContainer._PICKLE_SUFFIX}'
        )

        try:
            zip_fn = str(self._source_obj)
        except TypeError:
            zip_fn = str(type(self._source_obj))

        _log.debug(f'Loading dataframe "{df_name}" from '
                   f'"{zip_fn}/{zip_entry_path.at}".')

        if sys.version_info[0:2] <= (3, 8):
            mode = 'r'
        else:
            mode = 'rb'

        with zip_entry_path.open(mode) as entry_handle:
            self._dataframes[df_name] = pandas.read_pickle(entry_handle)

    def load_all(self):
        """Load all data frames into memory."""
        unloaded_names = filter(
            lambda val: val not in self._dataframes,
            self.names)

        for name in unloaded_names:
            self._load_dataframe(name)

    def _hash_dataframe(self, df_name: str) -> str:
        """Create a hash value for a data frame and return it.

        Args:
            df_name: Name of the data frame.

        Returns:
            The hash (sha1) as a string (hexdigts).

        Raises:
            KeyError: If the data frame is not loaded yet.

        """
        df = self._dataframes[df_name]

        # Partly credits: https://stackoverflow.com/a/62754084/4865723

        # Dev not (2024-02) about hashing dtypes. Pandas hash the dtype names
        # but not details like categories and ordering. See workaround here.
        new_hash = pandas.concat(
            [
                # Hash for each row including the index (row name)
                pandas.util.hash_pandas_object(df),
                # Hash column names
                pandas.util.hash_pandas_object(df.columns),
                # Hash detailed dtype infos (via their string representation)
                pandas.util.hash_pandas_object(pandas.Series(
                    [repr(onetype) for onetype in df.dtypes]))
            ]
        )

        # hash the series of hashes
        new_hash = hashlib.sha1(new_hash.values).hexdigest()

        return new_hash

    def _is_dataframe_modified(self, df_name: str) -> bool:
        """Return the data frames modified state.

        Not loaded *on-demand* data frames assumed to be unmodified and
        `False` is returned. If a data frame was attached (from memory) to the
        container not stored yet it is assumed to be modified and `True` is
        returned.

        Args:
            df_name: Name of the data frame.

        Returns:
            Boolean given the modified state.

        """
        try:
            new_hash = self._hash_dataframe(df_name)
        except KeyError:
            # DataFrame is not loaded
            return False
        except TypeError as attr_err:
            if 'unhashable' in str(attr_err):
                _log.error(
                    f'The dataframe "{df_name}" is treated as modified. '
                    'It is not possible to determine its real modified state '
                    'because it contain unashable content. This can be '
                    'caused by unusual data types like list(). '
                    f'The original error raised was: "{str(attr_err)}"')
            else:
                raise attr_err

            return True

        try:
            old_hash = self._dataframe_hashes[df_name]
        except KeyError:
            # DataFrame was not loaded from a container but fresh attached
            # to the container.
            return True

        return not new_hash == old_hash

    @classmethod
    # pylint: disable-next=too-many-arguments
    def _write_folder(cls,  # noqa: PLR0913
                      *,
                      folder_path: pathlib.Path,
                      dataframes: dict[str, pandas.DataFrame],
                      permanent_names: Iterable[str],
                      meta: Dict,
                      zip_names: Iterable[str],
                      ) -> list[tuple[str]]:
        """Write data frames into a folder.

        By default on-demand data frames are written as compressed and
        permanent data frames as uncompressed pickle files. That default
        behavior is used if ``zip_names`` is ``None``. Otherwise if
        ``zip_names`` is empty (``[]``) none of the data frames are compressed.

        Args:
            folder_path: Destination path.
            dataframes: Dict of `pandas.DataFrame`'s indexed by their name.
            permanent_names: List of data frame names need to be persistent.
            meta: Dictioniary with additional (user defined) meta data.
            zip_names: List of data frame names should be compressed no matter
                if they are on-demand or permanent.

        Returns:
            A list of tuples indicating the status of each data frame.

        """
        # create folder
        folder_path.mkdir(parents=True, exist_ok=True)

        # meta information
        if meta:
            fp = folder_path / cls._META_FILENAME

            with fp.open('w', encoding='utf-8') as meta_handle:
                meta_handle.write(json.dumps(meta))

        # Store indicators about the type/behavior of a dataframe
        dfs_status = []

        for df_name in dataframes:

            if df_name in permanent_names:
                name_prefix = '_'
                suffix = cls._PICKLE_SUFFIX
                indicators = 'P_'
            else:
                name_prefix = ''
                suffix = cls._PICKLE_ZIP_SUFFIX
                indicators = 'Oz'

            # overwrite default zip-behavior if explicit zip names are given
            if zip_names is not None:
                if df_name in zip_names:
                    suffix = cls._PICKLE_ZIP_SUFFIX
                    indicators = indicators[0] + 'z'
                else:
                    suffix = cls._PICKLE_SUFFIX
                    indicators = indicators[0] + '_'

            fp = folder_path / f'{name_prefix}{df_name}{suffix}'

            _log.debug(f'Saving data frame [{indicators}] "{df_name}" '
                       f'to "{fp}".')

            dfs_status.append((df_name, indicators))

            # Write dataframe to one file
            dataframes[df_name].to_pickle(fp, protocol=cls._PICKLE_PROTOCOL)
            _log.debug(f'{len(dataframes[df_name]):n} rows in "{df_name}"')

        return dfs_status

    @classmethod
    def _write_zip_file(cls,
                        file_like_obj: Union[pathlib.Path,
                                             zipfile.ZipFile],
                        dataframes: Dict[str, pandas.DataFrame],
                        permanent_names: List[str],
                        meta: Dict) -> list[tuple[str]]:
        """Write data frames to a ZIP archive.

        Args:
            file_like_obj: Path to ZIP file to create or a `zipfile.ZipFile`
                           object.
            dataframes: Dictionary with `pandas.DataFrame`'s.
            permanent_names: List of dataframe names to mark as 'permanent'.
            meta: Meta data to add to the ZIP file.

        """
        # If a path was given open it and recall the method again with the
        # open file object.
        if isinstance(file_like_obj, pathlib.Path):

            with zipfile.ZipFile(file_like_obj, mode='w') as zip_handle:

                return cls._write_zip_file(
                    file_like_obj=zip_handle,
                    dataframes=dataframes,
                    permanent_names=permanent_names,
                    meta=meta)

        # We just have a ZipFile instance which was opened in a recursive
        # call before.

        # meta information
        if meta:

            fn = DataContainer._META_FILENAME

            with file_like_obj.open(fn, 'w') as meta_handle:
                meta_handle.write(json.dumps(meta).encode())

        dfs_status = []

        # each dataframe
        for name in dataframes:

            if name in permanent_names:
                name_prefix = '_'
                indicators = 'P_'
            else:
                name_prefix = ''
                indicators = 'O_'

            entry_name = f'{name_prefix}{name}{DataContainer._PICKLE_SUFFIX}'

            with file_like_obj.open(entry_name, 'w') as pickle_handle:
                dataframes[name].to_pickle(
                    pickle_handle,
                    protocol=DataContainer._PICKLE_PROTOCOL)

            dfs_status.append((name, indicators))

        return dfs_status

    def save(self,
             file_path: pathlib.Path,
             permanent_names: List[str] = None,
             zip_names: List[str] = None,
             meta: Dict = None):
        """Save the container in filesystem at ``file_path``.

        If ``file_path`` is a a ZIP file then the container is stored as a
        compressed archive containing all data frame files as pickles.
        Otherwise the path is used as a folder and each dataframe is stored
        as a single file.

        .. hint ::
            In folder mode by default the *on-demand* data frames are ZIP
            compressed. But if ``zip_names`` is given only data frames with
            that names are compressed no matter if they are *on-demand* or
            *permanent*. If all data frame files should be uncompressed set
            ``zip_names=[]``.

        Previously existing meta data in the container is not lost but only
        updated using `update_meta_info()` with the `meta` argument.

        Args:
            file_path: Path to a ZIP file or a folder.
            permanent_names: Names of dataframes marked as permanent.
            zip_names: Works only on modified data frames.
            meta: To update containers meta data with.

        """
        _log.info(f'Begin to save data container to {file_path}. '
                  'Please wait...')

        # use current permanent names as default
        if permanent_names is None:
            permanent_names = self.names_permanent[:]

        unknown = list(set(permanent_names) - set(self.names))

        if unknown:
            raise ValueError(
                'Not '  # pylint: disable=consider-using-f-string
                'all given permanent names are present as dataframes.'
                '\nKnown names: {}\ngiven permanent: {}\nunknown permanent: {}'
                .format(', '.join([f'"{n}"' for n in self._dataframes]),
                        ', '.join([f'"{n}"' for n in permanent_names]),
                        ', '.join([f'"{n}"' for n in unknown]))
            )

        # User defined meta data
        self.update_meta_info({} if meta is None else meta)

        # default meta data
        self.update_meta_info({'_created': self._generate_creation_info()})

        # Warn about overwriting
        if file_path.exists():
            _log.warning(f'A container already exists at "{file_path}"! '
                         'It is deleted and lost to save the new one.')

            if file_path == self._source_obj:
                raise FileExistsError(
                    'The file this container was loaded from is the same it '
                    'should be saved back. This is not allowed (implemented) '
                    'yet. The source folder/file will not get deleted.')

            # delete the old container
            if file_path.is_dir():
                shutil.rmtree(file_path)
            else:
                file_path.unlink()

        # mode specific save routines
        if file_path.name.endswith(DataContainer._ARCHIVE_SUFFIX):

            if zip_names:
                _log.warning('The argument "zip_names" is ignored because the'
                             ' container is stored in ARCHIVE mode.')

            self._save_as_archive(file_path, permanent_names)

        else:
            _log.debug(f'Saving container in folder mode at "{file_path}". ')
            # Previous container was archive mode
            if (self._source_obj
                    and self._source_obj.suffix == self._ARCHIVE_SUFFIX):

                self._save_previos_archive_as_folder(
                    file_path,
                    permanent_names,
                    zip_names)

            else:
                # regular save as folder
                self._save_as_folder(file_path, permanent_names, zip_names)

    def _save_previos_archive_as_folder(self,
                                        folder_path: pathlib.Path,
                                        permanent_names: List[str],
                                        zip_names: List[str]):
        _log.debug('Need to load all data frames into memory because '
                   'previous container was of archive mode type.')

        self.load_all()

        # Create a new container with the modified data frames
        dfs_status = self._write_folder(
            folder_path=folder_path,
            dataframes=self._dataframes,
            permanent_names=permanent_names,
            meta=self.meta,
            zip_names=zip_names,
        )

        self._log_save(folder_path, 'FOLDER', dfs_status)

    def _save_as_folder(self,
                        folder_path: pathlib.Path,
                        permanent_names: List[str],
                        zip_names: List[str]):
        """Store the container in folder mode.

        Args:
            folder_path: Destination path.
            permanent_names: List of data frame names need to be persistent.
            zip_names: List of data frame names should be compressed no matter
                if they are on-demand or permanent.

        """
        _log.debug(f'Saving container in folder mode at "{folder_path}". ')

        # Differentiate between (un)modified dataframes
        names_modified = list(filter(self._is_dataframe_modified, self.names))
        names_unmodified = list(set(self.names) - set(names_modified))

        # Additionally unmodified but loaded data frames that will change their
        # zip-state from compressed to uncompressed or the other way around.
        # It will save resources if the (un)compressing is done on in-memory
        # dataframes. The consequence is that their files don't need to be
        # copied in the end.
        names_loaded_unmodified_zip_change = []

        # Each name of unmodified but loaded data frame
        # for name in filter(lambda name: name in self._dataframes,
        #                    names_unmodified):
        # Each name of unmodified data frame
        for name in names_unmodified:

            # Determine if the data frame need to be compressed in the new
            # container.
            if zip_names == []:
                will_be_compressed = False
            elif zip_names is None:
                will_be_compressed = name not in permanent_names
            else:
                will_be_compressed = name in zip_names

            # compressed data frame become uncompressed or the other way
            if self._is_compressed(name) != will_be_compressed:
                names_loaded_unmodified_zip_change.append(name)

                # load if unloaded
                if name not in self._dataframes:
                    self._load_dataframe(name)

        names_save_via_new_dc \
            = names_modified + names_loaded_unmodified_zip_change

        names_save_via_copy \
            = set(names_unmodified) - set(names_loaded_unmodified_zip_change)

        # Determine data frames to compress. See `_write_folder()` for more
        # detailes.
        if zip_names:
            zip_names_for_new_dc = tuple(
                filter(lambda df_name: df_name in zip_names,
                       names_save_via_new_dc))
        else:
            # None or [] trigger different behavior
            zip_names_for_new_dc = zip_names

        # Create a new container with the modified data frames
        dfs_status = self._write_folder(
            folder_path=folder_path,
            dataframes={
                df_name: self[df_name] for df_name in names_save_via_new_dc
            },
            permanent_names=tuple(
                filter(lambda df_name: df_name in permanent_names,
                       names_save_via_new_dc)
            ),
            meta=self.meta,
            zip_names=zip_names_for_new_dc
        )

        # The rest is copied on the file system
        for df_name in names_save_via_copy:
            try:
                self._unload_dataframe(df_name)
            except ValueError:
                # permanent data frame
                pass

            status_indicator = self._copy_dataframe_file(
                name=df_name,
                folder_path=folder_path,
                permanent=df_name in permanent_names,
                zipped=None if zip_names is None else (df_name in zip_names)
            )

            dfs_status.append((df_name, status_indicator))

        self._log_save(folder_path, 'FOLDER', dfs_status)

    def _copy_dataframe_file(self,
                             name: str,
                             folder_path: pathlib.Path,
                             permanent: bool,
                             zipped: bool):
        """Copy a data frame file to a new folder.

        The data frame to copy is specified by ``name`` and must exist in the
        current data container. The status can be toggled between permanent and
        on-demand using ``permanent`` argument. Despite from that the argument
        ``zipped`` is used to validate if copy is possible. A `ValueError` is
        raised if the sources is compressed and the destination not or the
        other way around.

        Args:
            name: Name of the data frame used in this data container.
            folder_path: Destination folder to copy the data frame file into.
            permanent: Data frame should be permanent or not after copy it.
            zipped: Indicate if destination file is compressed or not. Will
                raise `ValueError` if different from source file.

        Returns:
            A string used as status indicator in `_log_save()`, e.g. ``P_``,
            ``Oz``.

        Raises:
            ValueError: If destination file should be compressed but source
                file is not or the other way around.

        """
        if permanent:
            name_prefix = '_'
            df_status = 'P'
        else:
            name_prefix = ''
            df_status = 'O'

        name_suffix = {
            True: self._PICKLE_ZIP_SUFFIX,
            False: self._PICKLE_SUFFIX,
            None: self._PICKLE_SUFFIX if permanent else self._PICKLE_ZIP_SUFFIX
        }[zipped]

        df_status += 'z' if name_suffix.endswith('.zip') else '_'

        dest_fp = folder_path / f'{name_prefix}{name}{name_suffix}'

        src_fp = self._compute_filepath_for_dataframe(
            name_prefix=None, df_name=name)

        # (un)compress?
        if src_fp.suffix == dest_fp.suffix:
            shutil.copy(src_fp, dest_fp)
        else:
            raise ValueError(f'The file "{src_fp}" of data frame "{name}" can '
                             f'not be copied to "{dest_fp}". Their compressed '
                             'state is different.')

        return df_status

    def _save_as_archive(self,
                         file_path: pathlib.Path,
                         permanent_names: List[str]):
        """Save the container instance as ZIP compressed file.

        See `save()` for details.
        """
        # Why this?
        self.load_all()

        dfs_status = self._write_zip_file(
            file_like_obj=file_path,
            dataframes=self._dataframes,
            permanent_names=permanent_names,
            meta=self.meta)

        self._log_save(file_path, 'ARCHIVE', dfs_status)

    @property
    def names(self) -> list[str]:
        """List all data frame names.

        List all data frame names no matter if they are *on-demand* or
        *permanent* type.
        """
        return self.names_permanent + self._names_on_demand

    @property
    def names_permanent(self) -> list[str]:
        """List names of *permanent* data frames."""
        # All (currently loaded) data frame names which are not on-demand
        return list(filter(
            lambda dfn: dfn not in self._names_on_demand,
            self._dataframes.keys()
        ))

    @contextlib.contextmanager
    def keep_row_count(self, name: str) -> DataContainer:
        """Context manager monitoring the number of rows of a data frame.

        The context manager can be used to make sure that an operation on
        the data do not delete or add rows. This can accidentally happen
        when using `pandas.merge()`.

        .. python::
            dc = get_container()

            with dc.keep_row_count('MyDataFrame'):
                dc.MyDataFrame = pandas.merge(
                    left=dc.MyDataFrame,
                    right=other_data_frame
                )

        Args:
            name: Name of the data frame.

        Returns:
            Itself.

        Raises:
            TypeError: If the length of the data frame changed.

        """
        initial_length = len(self[name])

        yield self

        if initial_length != len(self[name]):
            raise TypeError(f'The length of data frame "{name}" was modified!'
                            f' It was {initial_length} but '
                            f'now is {len(self[name])}.')
