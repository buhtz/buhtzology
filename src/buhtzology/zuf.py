#!/usr/bin/env python3
"""Scoring mode of the *ZUF-8 "Fragenbogen zur Patientenzufriedenheit"*.

A German translation based on the *CSQ-8 "Client Satisfaction Questionnaire"*.

References:
    - https://gfqg.de/forschung/assessment/zuf-8
    - Direkter Autorenkontakt: Dr. Jürgen Schmidt
    - Schmidt, J., Lamprecht, F., Wittmann, W.W. (1989). Zufriedenheit
      mit der stationären Versorgung. Entwicklung eines Fragebogens und
      erste Validitätsuntersuchungen. Psychother. med. Psychol., 39: 248-255.
    - Attkisson, C.C., Zwick, R. (1982): The Client Satisfaction
      Questionnaire. Evaluation and Program Planning, 5, 233 – 237.

"""

import pandas

AGGREGATION = {
    pandas.NA: '(fehlend)',
    1: 'ziemlich unzufrieden',      # 8-15
    2: 'teilweise unzufrieden',     # 16-23
    3: 'weitgehend zufrieden',      # 24-31
    4: 'sehr zufrieden'             # 32
}


# pylint: disable-next=invalid-name
def score_ZUF8(data, col_names=None, suffix='', do_not_invert=False):
    """Scoring mode of the *ZUF-8 "Fragenbogen zur Patientenzufriedenheit"*.

    A German translation based on the *CSQ-8 "Client Satisfaction
    Questionnaire"*.
    """
    # DataFrame?
    if not isinstance(data, pandas.DataFrame):
        raise TypeError('Please offer a pandas.DataFrame for "data". But '
                        f'you offered {type(data)}!')

    # COLUMNS
    if not col_names:
        # Use all columns by default
        col_names = list(data.columns)

    # column count correct?
    if len(col_names) != 8:
        raise TypeError(
            'Please offer a DataFrame with 8 columns or specify the 8 '
            'ZUF-8 related columns by "col_names".')

    # VALIDATION
    invalid_cols = []
    valid_values = [1, 2, 3, 4, pandas.NA]

    # test
    for c in col_names:
        if not data.loc[:, c].isin(valid_values).all():
            invalid_cols.append(c)

    if invalid_cols:
        raise ValueError(f'{data}\nThere are invalid item values in this '
                         f'columns: {invalid_cols}')
    # INVERT
    if do_not_invert is False:
        data = _do_invert(data, col_names)

    # IMPUTATION
    data = data.apply(
        _imputate_ZUF_row_by_row,
        args=(col_names, ),
        axis=1
    )

    # do CALC
    data = data.apply(
        _calculate_ZUF_score_row_by_row,
        args=(col_names, suffix),
        axis=1
    )

    # do AGGREGATION
    data[f'{suffix}ZUFagg'] = data[f'{suffix}ZUF'].apply(_aggregate_ZUF)

    return data


def _do_invert(data, col_names):
    # INVERT
    # Raw item values should always entered as they are. Invertation for
    # item 1, 3, 6, and 7 as stated in the ZUF-8 manual should be done by
    # the machine and not by human.
    # But sometimes we have to deal with humans who inverting by themselves.
    # Because of that invertation can be disabled by
    # argument 'do_not_invert'.

    # Names of columns to invert (Item 1, 3, 6 and 7)
    cols_to_invert = [col_names[idx] for idx in [0, 2, 5, 6]]

    # each column
    for c in cols_to_invert:
        # Don't worry: 5 - NA = NA
        data[c] = 5 - data[c]

    return data


def _imputate_ZUF_row_by_row(row, col_names):  # pylint: disable=invalid-name
    """Imputate mean in missing items if 2/3 of the others are valid.

    Imputation is done after inversion.
    """
    # count missing
    na_count = row[col_names].isna().sum()

    # max two missing allowed
    if na_count in [1, 2]:

        # mean from valid items
        imp_val = row[col_names].mean(skipna=True)

        # round it
        # imp_val = round(imp_val, 1)

        # replace missing with it
        row[col_names] = row[col_names].fillna(imp_val)

    return row


# pylint: disable-next=invalid-name
def _calculate_ZUF_score_row_by_row(row, col_names, suffix):
    """Berechnet ZUF-8 Score aus 8 items.

    Dabei wird davon ausgegangen, dass die Umpolung bereits vorab stattgefunden
    hat.

    """
    if row[col_names].isna().any():
        score = pandas.NA
    else:
        score = sum(row[col_names])

    row[f'{suffix}ZUF'] = score

    return row


def _aggregate_ZUF(score):  # pylint: disable=invalid-name
    """ZUF-Score in eine von 4 Kategorien einteilen."""
    if score is pandas.NA:
        return AGGREGATION[pandas.NA]

    # ORG elif score > 7 and score < 16:
    if 7 < score < 16:
        return AGGREGATION[1]

    if score < 24:
        return AGGREGATION[2]

    if score < 32:
        return AGGREGATION[3]

    if score == 32:
        return AGGREGATION[4]

    raise ValueError()
