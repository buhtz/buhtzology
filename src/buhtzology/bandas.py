# pylint: disable=too-many-lines
"""Helpers with `pandas`.

Some little helper's for and depending on the data science package
`pandas`.
"""
from __future__ import annotations
import sys
import pathlib
import logging
import io
import contextlib
import warnings
from collections.abc import Callable, Hashable
from typing import Union, Tuple, List, Any, Iterable, Dict
import zipfile
import json
import csv
import numpy
import pandas
# pylint: disable-next=unused-import
from ._datacontainer import DataContainer  # noqa: F401
from ._bandas_bins import (cut_bins,
                           cut_bins_via_interval_count,
                           generate_bin_labels)
from ._bandas_misc import (create_icd_catalog,  # noqa: F401
                           reorder_columns,
                           add_missing_category)
from ._bandas_parallelize import (cut_into_pieces,
                                  cut_by_row_keep_group,
                                  parallelize_job_on_dataframe,
                                  DECREASE_WORKERS_BY,
                                  SERVER_N_CORES,
                                  SERVER_USE_CORE_FX)

__all__ = [
    'DataContainer',
    'cut_bins',
    'cut_bins_via_interval_count',
    'generate_bin_labels',
    'create_icd_catalog',
    'reorder_columns',
    'add_missing_category',
    'cut_into_pieces',
    'cut_by_row_keep_group',
    'parallelize_job_on_dataframe',
    'DECREASE_WORKERS_BY',
    'SERVER_N_CORES',
    'SERVER_USE_CORE_FX',
]

_log = logging.getLogger(__name__)

FILENAME_PREFIX_WRONG = '_WRONG_'
"""Prefix used for the WRONG file. See `validate_csv()` for details."""

# ------------
# -- Validation
# ------------


def _zip_path_as_buffer(zip_path,
                        zip_entry_mode: str = 'r',
                        encoding: str = 'utf-8'):
    """Workaround to make pandas handle `zipfile.Path`.

    Pandas can not handle zipfile.Path instances.
    See: https://github.com/pandas-dev/pandas/issues/49906
    Here we open it as a byte stream.
    """
    if not isinstance(zip_path, zipfile.Path):
        return zip_path

    args_for_open = {
        'mode': zip_entry_mode,
        'encoding': encoding
    }

    # ignore encoding when opening in binary mode
    if 'b' in zip_entry_mode or sys.version_info[0:2] < (3, 9):
        del args_for_open['encoding']

    return zip_path.open(**args_for_open)


@contextlib.contextmanager
def _file_path_as_buffer(path_or_buffer: Union[pathlib.Path, io.IOBase],
                         mode: str = 'r',
                         encoding: str = 'utf-8',
                         replace_lines: Dict[str, str] = None,
                         replace_substrings: Dict[str, str] = None
                         ) -> io.IOBase:
    """Use a path or a in-memory file (buffer) with a with statement."""
    try:
        # It is a file-like object
        if 'b' in mode:
            # no encoding argument in binary mode
            path_or_buffer = path_or_buffer.open(mode=mode)
        else:
            path_or_buffer = path_or_buffer.open(mode=mode, encoding=encoding)

    except AttributeError:
        pass

    # Replace content
    if replace_lines or replace_substrings:
        path_or_buffer = _replace_lines_and_substrings(
            path_or_buffer, replace_lines, replace_substrings)

    try:
        path_or_buffer.seek(0)
    except AttributeError:
        pass

    # --- YIELD ---
    yield path_or_buffer

    try:
        if isinstance(path_or_buffer, io.BufferedIOBase):
            path_or_buffer.close()
        else:
            raise AttributeError

    except AttributeError:
        # move buffer cursor back start
        path_or_buffer.seek(0)


def _replace_lines_and_substrings(buffer: io.IOBase,
                                  replace_lines: Dict[str, str],
                                  replace_substrings: Dict[str, str],
                                  ) -> io.IOBase:
    r"""Replace lines and substrings in the content of a file or buffer.

    Lines are replaced complete only and not treated as like substrings. Line
    endings (e.g. ``\\n``) need to be part of the line to replace.
    Substrings are searched via ``is in`` operator.
    Line replacement has priority.

    Args:
        buffer: The buffer (file-like object) to read content from.
        replace_lines: Indexed by complete lines to replace.
        replace_substrings: Indexed by substrings replaced in lines.

    Returns:
        A buffer (file-like object) sougth back to 0.

    """
    if replace_lines is None:
        replace_lines = {}

    if replace_substrings is None:
        replace_substrings = {}

    if replace_lines or replace_substrings:
        _log.info('Try to replace lines while reading. '
                  f'With {len(replace_lines)} complete line rules '
                  f'and {len(replace_substrings)} substring rules.')
    else:
        # Do nothing
        return buffer

    if buffer.tell() != 0:
        raise RuntimeError(f'{buffer.tell()=}')

    stream = io.StringIO()

    for line in buffer:

        org_line = line

        # line replacement
        try:
            line = replace_lines[line]

            _log.debug(
                f'Replace complete line:\n\t{org_line}\n\t…with…\n\t{line}')

        except KeyError:
            pass

        # substring replacement
        for substring in replace_substrings:

            if substring in line:
                line = line.replace(substring, replace_substrings[substring])

                _log.debug(
                    f'Replace substring "{substring}" with '
                    f'"{replace_substrings[substring]}":\n'
                    f'  in line: "{org_line}"')

        stream.write(line)

    stream.seek(0)
    return stream


def _store_spec_na_values(column_name: str,
                          specs: Any,
                          NA_VALUES: dict  # pylint: disable=invalid-name
                          ) -> dict:
    """Store na values as `list` even if there is one value only.

    Called by `_parse_specs_and_rules()`.
    Missing values are specified in ``specs`` and stored in ``NA_VALUES``
    indexed by ```column_name``.
    """
    if isinstance(specs, List):
        # list is what we want
        NA_VALUES[column_name] = specs

    elif isinstance(specs, Tuple):
        # tuple to list
        NA_VALUES[column_name] = list(specs)

    elif specs is not None:
        # single list entry
        NA_VALUES[column_name] = [specs]

    return NA_VALUES


def _explode_rules(rules: list) -> list:
    """Convert lists with values and ranges to ranges only.

    Used by `_parse_specs_and_rules()`. Ranges are specified as tuples with
    two elements. Ranges are only allowed with type `int`.

    Args:
        rules: A list mixed with single values and value ranges as tuples.
            E.g. ``[1, 2, (4, 8), 11]``

    Returns:
        A list with single values only. E.g. ``[1, 2, 4, 5, 6, 7, 8, 11]``

    Raises:
        ValueError: If range is invalid. e.g. from ``5`` to ``2``

    """
    # if its not a list()
    if not isinstance(rules, list):
        # we assume a single value
        rules = [rules]

    for one_rule in rules[:]:
        # range of length/values
        if type(one_rule) in [list, tuple]:
            rules.remove(one_rule)
            # check range
            if one_rule[0] > one_rule[1]:
                raise ValueError(f'The range {one_rule} is not valid!')

            # convert range to list
            rules.extend(range(one_rule[0], one_rule[1] + 1))

    return rules


def _parse_specs_and_rules(specs_and_rules: dict,  # noqa: PLR0912, PLR0915
                           ignore_na_lengths: bool = False,
                           ) -> tuple[list, dict, dict, dict, dict]:
    """Parse, validate and separate the ``specs_and_rules`` argument.

    Columns without names are marked with negative integers. They are renamed
    based on `pandas` behavior into ``Unnamed: 1`` where the number is the
    index of the column.
    It is used in `read_and_validate_csv()`.

    Args:
        specs_and_rules: The dictionary to process.
        ignore_na_lengths: Don't add length of na values to len rules.

    Returns:
        A dictionary with six elements: columns, ignored columns, dtypes,
        missing values, length rules and value rules.

    """
    # pylint: disable=too-many-statements
    # pylint: disable=too-many-branches
    # pylint: disable=too-many-locals
    # pylint: disable=invalid-name

    # original column list (incl. ignored ones)
    ORG_COLUMNS = list(specs_and_rules.keys())

    # Correct ordered list of all column names"""
    COLUMNS = []
    # List of column index for columns not to read
    COLUMNS_IGNORE = []
    # types for each column to read
    DTYPES = {}
    # na/missing values per column
    NA_VALUES = {}
    # length rules for columns (by index)
    LEN_RULES = {}
    # value rules for columns (by name)
    VAL_RULES = {}
    # column names need to have unique data
    UNIQUE_COLUMNS = []

    # Warning about category dtype
    def _sub_check_category_dtype(dtype):
        if dtype == 'category':
            warnings.warn(
                message='It is not recommended to use "category" as type '
                'specification due to side effects and interactions between '
                'the components involved. Use "str" instead and convert to '
                '"category" by yourself after.',
                category=UserWarning,
                stacklevel=2
            )

    # iterate over column specs
    for col in specs_and_rules:
        # specification and rules for 'col'
        spec = specs_and_rules[col]

        # ignore?
        if spec is None:
            COLUMNS_IGNORE.append(col)
            continue

        # ignore but rules
        if type(spec) in [list, tuple] and spec[0] is None:
            COLUMNS_IGNORE.append(col)
        else:
            # read column
            COLUMNS.append(col)

        # type and missing
        if type(spec) not in [list, tuple]:
            # type only if not a tuple and not None
            if spec:
                DTYPES[col] = spec
                _sub_check_category_dtype(DTYPES[col])
            continue

        # as tuple if both specified
        if spec[0]:
            DTYPES[col] = spec[0]
            _sub_check_category_dtype(DTYPES[col])

        # store na values as list() even if there is one value only
        NA_VALUES = _store_spec_na_values(col, spec[1], NA_VALUES)

        # rules specified?
        try:
            rules = spec[2]
        except IndexError:
            continue

        # rules for field length are checked by pre_check_csv()
        try:
            len_rules = _explode_rules(rules['len'])
            # remember the rule for the current column
            # But use the original column index (incl. ignored columns)
            # because len rules are checked before columns are removed
            idx = ORG_COLUMNS.index(col)
            LEN_RULES[idx] = len_rules
        except KeyError:
            pass
        else:
            if not ignore_na_lengths:
                # Add the length of missing values to len rules
                try:
                    # get length of all na values
                    nas_len = [len(str(na_val)) for na_val in NA_VALUES[col]]
                    # remove len that still present as len_rules
                    nas_len = list(set(nas_len) - set(LEN_RULES[idx]))
                    # add missing length
                    LEN_RULES[idx].extend(nas_len)
                except KeyError:
                    # no na values or no len rules
                    pass

        # rules for values are check in the DataFrame after read_csv()
        try:
            val_rules = _explode_rules(rules['val'])
            # remember the rule for the current column
            VAL_RULES[col] = val_rules
        except KeyError:
            pass

        # unique rule
        try:
            if rules['unique'] is True:
                UNIQUE_COLUMNS.append(col)
        except KeyError:
            pass

    # Assume all columns to read if no columns are specified
    if not COLUMNS:
        COLUMNS = None

    return {
        'COLUMNS': COLUMNS,
        'COLUMNS_IGNORE': COLUMNS_IGNORE,
        'DTYPES': DTYPES,
        'NA_VALUES': NA_VALUES,
        'LEN_RULES': LEN_RULES,
        'VAL_RULES': VAL_RULES,
        'UNIQUE': UNIQUE_COLUMNS,
    }


# pylint: disable-next=too-many-arguments
def read_and_validate_csv(  # noqa: PLR0913
        file_path: Union[pathlib.Path, zipfile.Path],
        specs_and_rules: dict,
        *,
        no_header_line: bool = False,
        encoding: str = 'utf-8',
        delimiter: str = ';',
        replace_lines: Dict[str, str] = None,
        replace_substrings: Dict[str, str] = None,
        on_bad_lines: Union[str, Callable] = 'error',
        **kwargs) -> pandas.DataFrame:
    """Read and validate a CSV file.

    Read a CSV file with respect to specifications about format and
    rules about valid values and return a `pandas.DataFrame`.
    You have to give specifications for all existing columns in the correct
    order. The following aspects can be specified:

    - Columns to read and columns to ignore.
    - The data type of a column. Types from `pandas`, `numpy` or Pythons
      `builtins` are valid.
    - Missing values. They are converted to ``pandas.NA`` in the resulting
      data frame.
    - Length of a data field in the raw CSV file.
    - Valid values in a column (checked in the resulting data frame).
    - Ignoring a column is also a specification.

    Example:
        Here you see a complex example with all possible options.

        - ``ColumnA`` is of type `str`.
        - ``ColumnB`` is of type `str` and the value ``no answer`` is treated
          as missing (``pandas.NA``).
        - ``ColumnC`` exist in the ``*.csv`` file but will be ignored while
          reading and won't be a part of the resulting data frame.
        - ``ColumnD`` is of type `pandas.Int16Dtype`. The value ``-9`` is a
          missing. The field length can be ``1``, ``2`` or ``4`` to ``8``.
          Possible or valid values are ``0``, ``1``, ``3`` to ``9`` and the
          missing ``-9``. The column need to have unique values.
        - The 5th column (named ``-1``) has no name in the header line. The
          name ``Unnamed: 4`` will be used for it.


        .. python::
            specs_and_rules = {
                'ColumnA': 'str',
                'ColumnB': ('str', 'no answer'),
                'ColumnC': None,
                'ColumnD': (
                    'Int16',
                    -9,
                    {
                        'len': [1, 2, (4-8)],
                        'val': [0, 1, (3-9)],
                        'unique': True,
                    }
                },
                -1: int,
            }

    Example:
        Here we expect a CSV file with three columns but only one ``ColumnB``
        is in the resulting data frame and the others are ignored while
        reading. Despite the third column ``ColumnC`` is not contained in the
        result its content will be validated with a ``val``-rule.

        .. python::
            specs_and_rules = {
                'ColumnA': None,
                'ColumnB': 'int',
                'ColumnC': (None, None, {'val': [1]}),
            }


    .. Important::
        Do not use objects of type `type` when specifying the column type.
        For example when the column is a string use ``"str"`` instead
        of `str`.

    .. Hint::
        To passthrough arguments to `pandas.read_csv()` the ``**kwargs`` can
        be used. For example ``skiprows`` or ``skipfooter``.

    .. Hint::
        The ``file_path`` can also be of type `zipfile.Path` to specify an
        entry in a ZIP file.

    Args:
        file_path: Path to the CSV file to read from.
        specs_and_rules: A column named indexed dictionary.
        no_header_line: Indicates if the first line contains column names.
        encoding: Optional encoding type used for reading the CSV file.
        delimiter: Delimiter to separate the fields.
        replace_lines: Replace dictionary to replace complete lines.
        replace_substrings: Replace dictionary to replace sub strings.
        on_bad_lines: See `pandas.read_csv()` for details.
        **kwargs: Used to handover arguments to `pandas.read_csv()`.

    Returns:
        The resulting data frame.

    """
    # validate format and separate specifications and rules information's
    parsed_specs = _parse_specs_and_rules(specs_and_rules)

    if no_header_line:
        columns = len(specs_and_rules.keys())
        header = None
        names = list(specs_and_rules.keys())
    else:
        # Column "names" with negative integers are missing and treated
        # as empty
        columns = [
            '' if isinstance(col, int) and col < 0
            else col
            for col in specs_and_rules
        ]
        header = 0
        names = None

    # check basic conditions of CSV file line by line
    validate_csv(
        file_path=file_path,
        delimiter=delimiter,
        quoting=csv.QUOTE_MINIMAL,
        encoding=encoding,
        field_length_rules=parsed_specs['LEN_RULES'],
        columns=columns,
        replace_lines=replace_lines,
        replace_substrings=replace_substrings,
        **kwargs)  # raise Exception if not valid

    try:
        file_path.seek(0)
    except AttributeError:
        pass

    file_path = _zip_path_as_buffer(
        zip_path=file_path,
        encoding=encoding)

    with _file_path_as_buffer(file_path,
                              encoding=encoding,
                              replace_lines=replace_lines,
                              replace_substrings=replace_substrings
                              ) as fpb:

        # do we need this?
        try:
            fpb.seek(0)
        except AttributeError:
            pass

        # Prevent ParserWarning from pandas about unsupported "skipfooter"
        # with "c" engine and fallback to "python" engine
        if kwargs.get('skipfooter', 0) > 0:
            kwargs['engine'] = 'python'
            _log.warning('Use engine "python" with "pandas.read_csv()" '
                         'because the argument "skipfooter" is used. This '
                         'may cause performance and parser issues.')

        df = pandas.read_csv(
            filepath_or_buffer=fpb,
            encoding=encoding,
            sep=delimiter,
            quoting=csv.QUOTE_MINIMAL,
            header=header,
            names=names,
            # usecols=parsed_specs['COLUMNS'],
            dtype=parsed_specs['DTYPES'],
            skipinitialspace=True,
            na_values=parsed_specs['NA_VALUES'],
            on_bad_lines=on_bad_lines,
            **kwargs)

    # Name of columns and value rules
    validate_dataframe(
        df_to_check=df,
        expected_columns=parsed_specs['COLUMNS'],
        ignored_columns=parsed_specs['COLUMNS_IGNORE'],
        value_rules=parsed_specs['VAL_RULES'],
        unique_columns=parsed_specs['UNIQUE'],
    )

    # Drop columns to ignore
    df = df.drop(columns=parsed_specs['COLUMNS_IGNORE'])

    # Try to close file_path if its was opened from a zipfile.Path
    try:
        file_path.close()
    except AttributeError:
        pass

    return df


# pylint: disable-next=too-many-locals,too-many-branches
def read_and_validate_excel(  # noqa: PLR0912
        file_path: Union[pathlib.Path, zipfile.Path],
        specs_and_rules: dict,
        *,
        no_header_line: bool = False,
        **kwargs) -> pandas.DataFrame:
    """Read and validate an Excel file.

    Read an Excel file with respect to specifications about format and
    rules about valid values and return a `pandas.DataFrame`.
    You have to give specifications for all existing columns in the correct
    order. But ignoring a column is also a specification. See
    `read_and_validate_csv()` for details and examples about usage of
    ``specs_and_rules``.

    .. Warning::
        If missing values specified via ``specs_and_rules`` their length will
        be ignored according to ``len`` rules. Missing values (``.isna()``)
        are not considered in length rules when reading from Excel files.

    .. Tip::
        To passthrough arguments to `pandas.read_excel()` the ``**kwargs`` can
        be used. For example ``sheet_name``, ``skiprows`` or ``skipfooter``.

    """
    # validate format and separate specifications and rules information's
    parsed_specs = _parse_specs_and_rules(
        specs_and_rules,
        ignore_na_lengths=True)

    file_path = _zip_path_as_buffer(zip_path=file_path, zip_entry_mode='rb')

    with _file_path_as_buffer(path_or_buffer=file_path, mode='rb') as fp:
        df = pandas.read_excel(
            io=fp,
            header=None if no_header_line is True else 0,
            names=None,
            usecols=None,  # SPALTEN,
            dtype=parsed_specs['DTYPES'],
            na_values=parsed_specs['NA_VALUES'],
            **kwargs)

    # Are all read columns where part of the specifications?
    column_names_specified = list(specs_and_rules.keys())
    column_names_read = list(df.columns)

    # check number of columns
    if len(column_names_read) != len(column_names_specified):
        raise ValueError(
            'Number of columns mismatch!\n'
            f'Expected headers: {column_names_specified}\n'
            f'But read        : {column_names_read}')

    if not no_header_line:
        # Replace specified positions with real column names.
        # This will raise exception if it is wrong.
        # e.g. This is OK
        # specified: ['a', 1, 'c']
        # read: ['a', 'X', 'c']
        # e.g. This raise an exception
        # specified: ['a', 1, 'c']
        # read: ['a', 'b', 'C']
        try:
            _ = [
                column_names_read[spec_e]
                if isinstance(spec_e, int)
                else column_names_read[column_names_read.index(spec_e)]
                for spec_e in column_names_specified
            ]

        except ValueError as exc:
            comp_str = ''
            for cspec, cread in zip(column_names_specified, column_names_read):
                if cspec == cread:
                    comp_str += f'\n    {cspec} == {cread}'
                else:
                    comp_str += f'\n !! {cspec} != {cread}'
            raise ValueError('Header of the Excel file do not match '
                             f'with expected headers!\n{comp_str}') from exc

        # if COLUMNS or COLUMNS_IGNORE contain only index of a columns
        # replace it with the real name read
        for key in ['COLUMNS', 'COLUMNS_IGNORE']:
            parsed_specs[key] = [
                column_names_read[spec_e]
                if isinstance(spec_e, int)
                else column_names_read[column_names_read.index(spec_e)]
                for spec_e in parsed_specs[key]
            ]

    else:  # noqa
        if df.shape[1] == len(parsed_specs['COLUMNS']):  # noqa
            df.columns = parsed_specs['COLUMNS']

        else:
            columns_keep = parsed_specs['COLUMNS'][::-1]
            rename_columns = {}

            # Give the not ignored columns a name and keep the ignored ones
            # with their index number.
            for idx in df.columns:

                # ignored column
                if idx in parsed_specs['COLUMNS_IGNORE']:
                    continue

                rename_columns[idx] = columns_keep.pop()

            df = df.rename(columns=rename_columns)

    # Header passen zusammen?
    validate_dataframe(
        df_to_check=df,
        expected_columns=parsed_specs['COLUMNS'],
        ignored_columns=parsed_specs['COLUMNS_IGNORE'],
        value_rules=parsed_specs['VAL_RULES'],
        unique_columns=parsed_specs['UNIQUE'])

    validate_len_rules_on_dataframe(
        df_to_check=df,
        len_rules=parsed_specs['LEN_RULES'],
        ignore_na=True
    )

    if no_header_line:
        # drop columns by index
        df = df.drop(columns=df.iloc[:, parsed_specs['COLUMNS_IGNORE']])

        # set column names
        df.columns = parsed_specs['COLUMNS']
    else:

        # Drop unwanted columns
        df = df.drop(columns=parsed_specs['COLUMNS_IGNORE'])

    # Try to close file_path if its was opened from a zipfile.Path
    try:
        file_path.close()
    except AttributeError:
        pass

    return df


def _csv_line_fit_field_length_rules(line: list[str],
                                     rules: dict[int, list[int]],
                                     columns: list[str]) -> bool:
    """See `validate_csv()` for details."""
    err = False

    # each field
    for field_idx in rules:

        # length of field fit rule?
        if len(line[field_idx]) in rules[field_idx]:
            continue

        # no rule met
        try:
            col = f' (column "{columns[field_idx]}")'
        except TypeError:
            col = ''

        _log.error(f'Error at field index {field_idx}{col}! '
                   'Field rules are '
                   f'{rules[field_idx]}.')

        err = True

    return err is False


def _construct_file_path_wrong(file_path: Union[pathlib.Path,
                                                zipfile.Path,
                                                io.IOBase]
                               ) -> pathlib.Path:
    """Construct a file name suitable for documenting wrong lines.

    Args:
        file_path: Path object which used as a base for construction.

    Returns:
        The new file path.

    Example 1:
        Based on ``/home/user/data.csv`` the path
        ``/home/user/_WRONG_data.csv`` will be returned.

    Example 2:
        Based on ``/foo.zip/foo/bar/data.csv`` the path
        ``/_WRONG_foo.zip.foo_bar_data.csv`` will be returned.

    """
    try:
        at_path = file_path.at

    except AttributeError:
        # not a zipfile.Path

        # treat it as pathlib.Path
        try:
            file_path_wrong = f'{FILENAME_PREFIX_WRONG}{file_path.name}'
            file_path_wrong = file_path.parent / file_path_wrong
        except AttributeError:
            file_path_wrong = f'{FILENAME_PREFIX_WRONG}in-memory-buffer'
            file_path_wrong = pathlib.Path.cwd() / file_path_wrong

    else:
        # no exception
        # must be a zipfile.Path
        at_path = at_path.replace(pathlib.os.sep, '_')
        at_path = at_path.replace('/', '_')
        zip_path = pathlib.Path(file_path.root.filename)
        file_path_wrong = zip_path.parent \
            / pathlib.Path(f'{FILENAME_PREFIX_WRONG}{zip_path.name}') \
                     .with_suffix(f'{zip_path.suffix}.{at_path}')

    return file_path_wrong


def validate_csv(  # noqa: PLR0912,PLR0913,PLR0915
        file_path: Union[pathlib.Path, zipfile.Path],
        *,
        columns: Union[list[str], int],
        delimiter: str = ';',
        quoting: int = csv.QUOTE_MINIMAL,
        encoding: str = 'utf-8',
        field_length_rules: dict = None,
        stop_after_n_errors: int = 50,
        skiprows: int = 0,
        skipfooter: int = 0,
        nrows: int = None,
        replace_lines: Dict[str, str] = None,
        replace_substrings: Dict[str, str] = None) -> bool:
    """Validate the structure of a CSV file.

    Args:
        file_path: Path and name of the csv file to check.
        delimiter: Field delimiter.
        quoting: Quoting dialect.
        encoding: Used while reading the file.
        field_length_rules: Column index indexed dict with list of valid field
            length.
        columns: Names or count of expected columns.
        stop_after_n_errors: Stop checking for further errors or rule
            violations when this number is reached.
        skiprows: Skipping n rows from the beginning.
        skipfooter: Skipping n rows from the end.
        nrows: Read nrows from beginning (including header) after skipping.
        replace_lines: Replace dictionary to replace complete lines.
        replace_substrings: Replace dictionary to replace sub strings.

    See `read_and_validate_csv()` for more details. Malformed or invalid rows
    are logged to a file (``*.wrong.csv``). The need for that function arises
    from the fact that the checks of `pandas.read_csv()` are quite sluggish.

    .. Note ::
        *Development notes*: Maybe move that function to ``buhtzology.misc``
        because there are no pandas dependencies in it.

    Returns:
        bool: ``True`` if everything is fine. Otherwise exception is raised.

    Raises:
        ValueError: If the header or one or more lines do not fit the rules.

    """
    # pylint: disable=too-many-arguments
    # pylint: disable=too-many-locals
    # pylint: disable=too-many-statements
    # pylint: disable=too-many-branches

    if field_length_rules is None:
        field_length_rules = {}

    # Each field length rule should be a list even if it
    # contains only one value.
    for field_idx in field_length_rules:

        if not isinstance(field_length_rules[field_idx], list):
            field_length_rules[field_idx] = [field_length_rules[field_idx]]

    # Remove lines from the end of the CSV file.
    # Attention: The whole CSV file will be read into memory first.
    if skipfooter > 0:
        with _file_path_as_buffer(file_path, 'r', encoding) as csv_file:
            # Read all lines
            lines = csv_file.readlines()

        # Cut lines from the end
        lines = lines[:-skipfooter]

        # Create a in-memory file (buffer) with that lines
        file_path = io.StringIO()
        file_path.writelines(lines)
        file_path.seek(0)
        del lines

    # open CSV file
    with _file_path_as_buffer(
            path_or_buffer=file_path,
            mode='r',
            encoding=encoding,
            replace_lines=replace_lines,
            replace_substrings=replace_substrings) as csv_file:

        count = 0
        enumerate_start_idx = 1
        real_header = None
        # https://stackoverflow.com/a/31892849/4865723
        csv_reader = csv.reader(csv_file,
                                delimiter=delimiter,
                                quoting=quoting)

        # skip lines?
        for _ in range(skiprows):
            next(csv_reader)

        n_lines_left_to_read = nrows

        # No header line. Just check number of fields
        if isinstance(columns, int):
            field_count = columns

        # Header line specified.
        elif isinstance(columns, list):
            enumerate_start_idx += 1
            field_count = len(columns)
            real_header = next(csv_reader)

            if n_lines_left_to_read:
                n_lines_left_to_read -= 1

            # Compare to expected
            if columns != real_header:
                raise ValueError(
                    'Header of the CSV file do not match with expected '
                    'headers!\n'
                    f'Expected: {columns}\n'
                    f'But read: {real_header}')
        else:
            raise AttributeError(
                'Use the "columns" argument to specify a list of column '
                'names or the count of columns/fields. But current value '
                f'is "{columns}"!')

        # linenumbers and lines with rule violations
        violations = {}

        count_err_msg = ''

        # check each CSV line
        for idx, row in enumerate(csv_reader,
                                  start=enumerate_start_idx + skiprows):
            err = False

            # lines left?
            if n_lines_left_to_read is not None:
                # decrease
                n_lines_left_to_read -= 1

                if n_lines_left_to_read < 0:
                    break

            # field count
            if len(row) != field_count:
                err = True
            else:  # length of each field
                does_fit = _csv_line_fit_field_length_rules(
                    row, field_length_rules, columns)
                err = does_fit is False

            # validation of rule?
            if err:
                count = count + 1
                violations[idx] = row

                if count > stop_after_n_errors:
                    count_err_msg = ' But could be more!'
                    # stop reading more lines
                    break

        _announce_csv_violations(
            file_path=file_path,
            violations=violations,
            delimiter=delimiter,
            quoting=quoting,
            encoding=encoding,
            real_header=real_header,
            count_err_msg=count_err_msg
        )

    return True


# pylint: disable-next=too-many-arguments
def _announce_csv_violations(*,  # noqa: PLR0913
                             file_path: pathlib.Path,
                             violations: dict,
                             delimiter: str,
                             quoting: int,
                             encoding: str,
                             real_header: list[str],
                             count_err_msg: str):
    # Create name for "wrong" file and delete the file if existent.
    # That file is used to document bad/invalid lines in the CSV file.
    file_path_wrong = _construct_file_path_wrong(file_path)
    file_path_wrong.unlink(missing_ok=True)

    # Specifications and/or rules where violated
    if not violations:
        return

    _log.error(f'Found bad lines. See file {file_path_wrong} for details.')

    # open file to report invalid lines
    with file_path_wrong.open('w', newline='', encoding=encoding) as fp:
        csv_writer = csv.writer(fp,
                                delimiter=delimiter,
                                quoting=quoting,
                                lineterminator='\n')

        if real_header:
            csv_writer.writerow(real_header)

        csv_writer.writerows(violations.values())

    # bad lines
    bad = '\n'.join([f'{n}:{violations[n]}' for n in violations])
    _log.error(f'\n{bad}')

    raise ValueError(
        f'Found {len(violations)} invalid line(s).{count_err_msg}')


def _name_unnamed_columns(columns: Iterable, rules: dict, unique: list
                          ) -> tuple[list, dict, list]:
    """Replace integers less then 0 with strings of format ``Unnamed: {idx}``.

    Check for integers less then 0 and replace them with strings of format
    ``Unnamed: {idx}``. It is used in `validate_dataframe()`.

    Args:
        columns: A list of column names including unnamed columns marked with
            negative integers.
        rules: Considered while renaming and returned as a result.
        unique: Considered while renaming and returned as a result.

    Returns:
        A three-item tuple. First a list of columns using placeholder names for
            unnamed columns. Second the rules with replaced names. Third the
            list of unique columns using replace names.

    """
    result = []
    for idx, item in enumerate(columns):
        try:
            if item < 0:
                new_name = f'Unnamed: {idx}'
                result.append(new_name)

                if item in rules:
                    rules[new_name] = rules[item]
                    del rules[item]

                if item in unique:
                    unique.append(new_name)
                    unique.remove(item)

                continue

        except TypeError:
            pass

        result.append(item)

    if columns != result or unique:
        _log.debug(f'Replace {columns} into {result} (unique: {unique})')

    return result, rules, unique


def validate_dataframe(df_to_check: pandas.DataFrame,
                       expected_columns: Iterable,
                       ignored_columns: Iterable,
                       value_rules: Dict[Hashable, Iterable],
                       unique_columns: Iterable[str]):
    """Validate a dataframes content and format.

    If the dataframe is valid nothing happens; no return value. If something
    is invalid a ``TypeError`` is raised.

    The ``expected_columns`` is allowed to have negative integers. They are
    replaced using `_name_unnamed_columns()`.

    .. Development note::
        It is not clear why ``ignored_columns`` is there. It might be possible
        to remove the ignored columns before calling validate_dataframe.

    Args:
        df_to_check: The dataframe to perform the validation on.
        expected_columns: List of columns names that should exist.
        ignored_columns: List of columns excluded from the validation.
        value_rules: See `read_and_validate_csv()` for details.
        unique_columns: Names of columns that should have unique values.

    Raises:
        TypeError: When the dataframe do not fit the rules.

    """
    expected_columns, value_rules, unique_columns \
        = _name_unnamed_columns(expected_columns, value_rules, unique_columns)

    validate_value_rules_on_dataframe(df_to_check, value_rules)

    validate_unique_rules_on_dataframe(df_to_check, unique_columns)

    # exclude ignored columns from the list of columns in the dataframe
    df_columns = list(df_to_check.columns)

    for icol in ignored_columns:
        df_columns.remove(icol)

    if expected_columns and df_columns != expected_columns:
        import difflib  # pylint: disable=import-outside-toplevel
        import pprint  # pylint: disable=import-outside-toplevel

        diff_msg = difflib.ndiff(
            pprint.pformat(expected_columns).splitlines(),
            pprint.pformat(list(df_to_check.columns)).splitlines()
        )

        diff_msg = '\n'.join(diff_msg)

        raise TypeError(
            f"The expected and actual header don't match.\n{diff_msg}")


def validate_value_rules_on_dataframe(df_to_check: pandas.DataFrame,
                                      value_rules: dict):
    """Validate if the DataFrame fit to value rules.

    Validate if the DataFrame (``df_to_check``) fit to ``value_rules``.
    Otherwise an `ValueError` is raised.

    Args:
        df_to_check: Dataframe to validate.
        value_rules: Column name indexed dict with valid values.

    Raises:
        ValueError: If value rules not fit.

    """
    # list of value rules violating columns
    err_cols = {}

    # each rule
    for col in value_rules:
        # get all values except NA
        vals = df_to_check.loc[~df_to_check[col].isna(), col]

        # remember violating column and unexpected values
        if not vals.isin(value_rules[col]).all():
            err_cols[col] = vals.loc[~vals.isin(value_rules[col])] \
                .unique().tolist()

            _log.error(f'Violated value rules in "{col}". Existing values '
                       f'are: {vals.unique()=}')

    if err_cols:
        raise ValueError(
            'This column(s) do not match the value rules:\n'
            + json.dumps(err_cols, indent=4, ensure_ascii=False))


def validate_unique_rules_on_dataframe(
        df_to_check: pandas.DataFrame,
        unique_columns: Iterable[Union[str, int]]):
    """Validate if columns contain unique values.

    Args:
        df_to_check: Dataframe to validate.
        unique_columns: List of column names.

    Raises:
        ValueError: If value rules not fit.

    """
    err_cols = []

    # each rule
    for col in unique_columns:
        if df_to_check[col].is_unique:
            continue

        err_cols.append(col)

    if err_cols:
        raise ValueError(
            f'This column(s) do not contain unique values: {err_cols}')


def validate_len_rules_on_dataframe(df_to_check: pandas.DataFrame,
                                    len_rules: dict[int, list[int]],
                                    ignore_na: bool = False) -> None:
    """Validate if the DataFrame fit to length rules.

    Validate if the DataFrame (``df_to_check``) fit to ``len_rules``.
    Otherwise an `ValueError` is raised.

    Args:
        df_to_check: Dataframe to validate.
        len_rules: Dictionary with column index (not name) ask keys and as
            value a list valid lengths.
        ignore_na: Missing values not considered.

    Raises:
        ValueError: If value rules not fit.

    """
    # list of value rules violating columns
    err_cols = []

    for column_idx, valid_lengths in len_rules.items():
        the_column = df_to_check.iloc[:, column_idx]
        if ignore_na:
            the_column = the_column.dropna()
        existing_lengths = the_column.astype(str).str.len().unique()

        if not numpy.isin(existing_lengths, valid_lengths).all():
            err_cols.append(column_idx)

            _log.error(f'Violated len rules in column index {column_idx}. '
                       f'Existing lengths: {existing_lengths}. '
                       f'Valid lengths: {valid_lengths}.')

    if err_cols:
        raise ValueError(
            f'This column(s) do not match the len rules: {err_cols}')


def batch_read_and_validate(read_this: dict) -> DataContainer:
    """Read and validate multiple files at once.

    The argument ``read_this`` is a complex dictionary containing all relevant
    information about how to read and validate the files. The following example
    illustrate how to use it. The element ``specs_and_rules`` is
    described elsewhere in `bandas.read_and_validate_csv()`.

    .. python::
        read_this = {
            'Foo':  # name of resulting dataframe
                {
                    'file': Path('foo.csv'),
                    'specs_and_rules': {...},
                    'encoding': 'utf-8',  # optional
                    'no_header': True,  # optional
                    'skip_rows': 1,  # optional
                    'skip_footer': 4,  # optional
                },
            'Bar':
                {
                    'file': Path('bar.xlsx'),
                    'sheet': 'Money',  # optional
                    # ...
                },
            'FromArchive':
                {
                    'file': zipfile.Path('data.zip', 'inzip_file.xlsx'),
                    # ...
                },
             # ...
        }

    Args:
        read_this: A complexe dictionary with dataframe names as keys.

    Raises:
        BaseException: See `read_and_validate_csv()` and
            `read_and_validate_excel()` about details.

    Returns:
        DataContainer: A complete data container.
    """
    logging.info(f'Start reading and validating from {len(read_this)} files '
                 'into a data container …')

    data_container = DataContainer({})

    for dfname, kwargs in read_this.items():
        specs = kwargs.pop('specs_and_rules')
        fp = kwargs.pop('file')

        try:
            # CSV
            if fp.suffix.lower() == '.csv':
                data_container[dfname] = read_and_validate_csv(
                    file_path=fp,
                    specs_and_rules=specs,
                    **kwargs)
                continue

            # EXCEL
            if fp.suffix.lower() == '.xlsx':
                data_container[dfname] = read_and_validate_excel(
                    file_path=fp,
                    specs_and_rules=specs,
                    **kwargs)
                continue

        except Exception:
            logging.critical(f'Not able to finish reading "{dfname}" from '
                             f'file "{fp}" with {kwargs=}.')
            raise

        logging.debug(f'Finished reading "{dfname}" from file '
                      f'"{fp}" with {kwargs=}.')

    return data_container
