# pylint: disable=missing-module-docstring
import sys
import platform
import math
import pathlib
import logging
import logging.handlers
import copy
import datetime
try:
    import tomllib  # Python 3.11 and higher
except ImportError:
    import tomli as tomllib
from typing import Iterable, Dict
from collections import Counter
from collections.abc import MutableMapping

_log = logging.getLogger(__name__)

_CONFIG_DEFAULT = {
    'bandas': {
        'decrease_workers_by': 0,
    }
}
"""Default configuration"""


# ------------
# -- Misc
# ------------

# pylint: disable-next=too-many-arguments,too-many-locals
def setup_logging(*,  # noqa: PLR0913
                  log_directory: pathlib.Path = None,
                  console_level: int = logging.INFO,
                  console_format_str: str = ('%(log_color)s[%(levelname)s]'
                                             '%(reset)s %(message)s'),
                  file_level: int = logging.DEBUG,
                  file_format_str: str = ('%(asctime)s - %(name)s - '
                                          '%(levelname)s - %(message)s'),
                  file_backup_count: int = 20,
                  exceptional_loggers: Dict[str, int] = None):
    """Set up the root logger with a console and a file handler.

    The file handler is rotated for each session. The file name is based on
    ``sys.argv[0]`` (the filename of the current running python script). With
    ``exceptional_loggers`` it is possible to set levels to specific loggers;
    e.g. `matplotlib.font_manager` or `PIL` which are know for annoying debug
    messages.

    Colorized console output is activated by default if the package
    `colorlog`_ is available.

    .. _colorlog:
       https://pypi.org/project/colorlog

    Args:
        log_directory: Path to log file directory.
        console_level: Log level for console (stdout) handler.
        console_format_str: Format of the console log messages.
        file_level: Log level for file handler. Deactivate with ``None``.
        file_format_str: Format of the file log messages.
        file_backup_count: Keep that amount of log files.
        exceptional_loggers: Levels for specific loggers.

    """
    # default log directory
    if log_directory is None:
        log_directory = pathlib.Path.cwd() / 'logs'

    # root must be as verbose as possible
    logging.getLogger().setLevel(logging.DEBUG)

    if file_level:
        name = pathlib.Path(sys.argv[0] if sys.argv[0] else 'noname')

        # create log folder if necessary
        log_directory.mkdir(parents=True, exist_ok=True)

        # credits to: https://stackoverflow.com/a/4654956/4865723
        rotating_handler = logging.handlers.RotatingFileHandler(
            # log file
            filename=log_directory / name.with_suffix('.log').name,
            backupCount=file_backup_count,
            encoding='utf-8')
        rotating_handler.setLevel(file_level)
        rotating_handler.setFormatter(logging.Formatter(file_format_str))

        logging.getLogger().addHandler(rotating_handler)

    # console log
    try:
        # pylint: disable=invalid-name
        import colorlog  # pylint: disable=import-outside-toplevel
        TheStreamHandlerClass = colorlog.StreamHandler
        TheFormatterClass = colorlog.ColoredFormatter

    except ModuleNotFoundError:
        TheStreamHandlerClass = logging.StreamHandler
        TheFormatterClass = logging.Formatter

        # Remove colorlog specific fields
        for field in ('%(log_color)s', '%(reset)s'):
            console_format_str = console_format_str.replace(field, '')

    console_logging = TheStreamHandlerClass()
    console_logging.setLevel(console_level)
    console_logging.setFormatter(TheFormatterClass(console_format_str))
    logging.getLogger().addHandler(console_logging)

    # Specific levels for exceptional loggers
    if exceptional_loggers:
        for logger in exceptional_loggers:
            level = exceptional_loggers[logger]
            logging.getLogger(logger).setLevel(level)

    if file_level:
        # new log file for new session
        try:
            rotating_handler.doRollover()
        except PermissionError as err:
            if platform.system() == 'Windows':
                _log.warning('Rotating log file handler was not able to '
                             'roll over. Might occur if the same script is '
                             'open twice. Because it is Windows the error '
                             'is ignored.')
            else:
                raise err

        _log.info(f'Log-file is "{rotating_handler.baseFilename}".')


def runtime_as_string(start_time: datetime.datetime) -> str:
    """Give the runtime timestamp as a string.

    The runtime is calculated from the difference between current time and
    'start_time'. Example results are "20 seconds" or "4.5 minutes".

    Args:
        start_time: Timestamp of start.

    """
    runtime_sec = (datetime.datetime.now() - start_time).total_seconds()

    or_minutes = '' if runtime_sec < 120 \
        else f' or {(runtime_sec/60):.1f} minutes'

    return f'{runtime_sec:.2f} seconds{or_minutes}'


def how_much_elements_per_piece(total_elements_n: int,
                                pieces_n: int = None,
                                min_max_per_piece: int = None,
                                min_pieces_n: int = None) -> int:
    """Calculate the number of elements needed in one piece.

    Calculate the number of elements in one piece when a bigger list of
    elements is cut into pieces depending on rules.
    If 'pieces_n' is given all other arguments are ignored and the result are
    the number of elements per piece you have to cut from your list to get
    the number of pieces specified by 'pieces_n'.

    If 'pieces_n' is None then the number of elements per piece is calculated
    based on 'min_max_per_piece'. But if 'min_pieces_n' is given
    also then the min/max rules can be overwritten to give you as much
    elements per pieces to result with 'min_pieces_n.

    There is a hierarchy of the rules.
        1. If 'pieces_n' is given the rest of arguments are ignored.
        2. If 'pieces_n' is None then 'min_max_per_piece' is taken
           into account. If present 'min_pieces_n' is also taken into account.

    Be aware that the last piece in your list can have less then the
    resulting number of elements per piece.

    Args:
        total_elements_n: x
        pieces_n: y
        min_max_per_piece: z
        min_pieces_n: a

    Returns:
        Number of elements one piece should have to fulfill the rules.

    Raises:
        ValueError: No rules specified with arguments.

    """
    if (not pieces_n and not min_max_per_piece and not min_pieces_n):
        raise ValueError('No rules specified with arguments!')

    # Is number of pieces is specified?
    if pieces_n:
        elements_per_piece = int(total_elements_n / pieces_n)
        return elements_per_piece

    # Min-Max-Rule valid?
    if min_max_per_piece:
        if not (
            (min_max_per_piece[0] > 0)
            and (min_max_per_piece[1] > 0)
            and (min_max_per_piece[0] < min_max_per_piece[1])
        ):
            raise ValueError(f'Invalid min-max rule. {min_max_per_piece}')

        # Calculate based on min-max-rule
        if total_elements_n < min_max_per_piece[1]:  # less then max?
            pn = total_elements_n / min_max_per_piece[0]  # based on min
        else:
            pn = total_elements_n / min_max_per_piece[1]
        pn = int(math.ceil(pn))  # always round up
    else:
        pn = 1

    # min number of pieces?
    if min_pieces_n:
        pn = max(pn, min_pieces_n)

    # calculate elements per piece
    elements_per_piece = int(total_elements_n / pn)

    return elements_per_piece


def break_paragraph(paragraph: list[str],
                    width: int,
                    suffix: str = '',
                    prefix: str = '',
                    sep_line: str = None) -> list[str]:
    r"""Break a paragraph as a whole.

    The lines of the ``paragraph`` are cut into chunks of ``width`` length.
    Word wrapping is not done. The length of ``suffix`` and ``prefix`` is not
    calculated but added to the desired ``width``.

        .. python::

            # input
            [
                'line one with banana',
                'line two with strawberry',
                'line two with ice cream',
            ]

            # output; width = 10
            [
                'line one w',
                'line two w',
                'line two w'
                'ith banana',
                'ith strawberry',
                'ith ice cream',
            ]

    Args:
        paragraph: The paragraph as a List of strings.
        width: Paragraph is cut into chunks of that width.
        suffix: String added to the end of every cut line (e.g. ``…``).
        prefix: String inserted to the beginning of every cut line
            (e.g. ``\\t``).
        sep_line: String to separate paragraphs with.

    """
    result = []
    longest_line_length = max((len(line) for line in paragraph))
    number_of_iterations = int(longest_line_length / width) + 1

    for iteration in range(1, number_of_iterations+1):
        idx_to = width * iteration
        idx_from = idx_to - width

        for line in paragraph:
            one_piece = line[idx_from:idx_to]

            if (one_piece and iteration > 1):
                prefix_to_use = prefix
            else:
                prefix_to_use = ''

            if (one_piece
                    and iteration != number_of_iterations
                    and not line.endswith(one_piece)):
                suffix_to_use = suffix
            else:
                suffix_to_use = ''

            result.append(f'{prefix_to_use}{one_piece}{suffix_to_use}')

        # separation line
        if sep_line is not None and iteration < number_of_iterations:
            result.append(sep_line)

    # remove empty lines from the end
    while result[-1] == '':
        result = result[:-1]

    return result


def shorten_strings_but_unique(string_list: Iterable[str],
                               limit: int = 25,
                               omit_string: str = '…') -> Iterable[str]:
    """Shorten strings to a given limit but keep them unique to the list.

    Args:
        string_list: List of strings to shorten.
        limit: Number of characters each string is shorten to.
        omit_string: Character used to visualize the omit.

    Returns:
        List of shorten strings.

    """
    def _make_unique_strings(one_string: str, n: int) -> Iterable[str]:
        try:
            omit_idx = one_string.index(omit_string)
        except ValueError:
            omit_idx = int(len(one_string) / 2)

        digits_n = len(str(n))
        format_string = '{:0' + str(digits_n) + '}'

        omit_n = len(omit_string)

        unique_numbers = range(1, n + 1)
        unique_numbers = [format_string.format(num) for num in unique_numbers]

        unique_string_list = []

        for un in unique_numbers:
            left = one_string[:(omit_idx - digits_n - omit_n)]
            right = one_string[omit_idx:]
            unique_string_list.append(left + un + right)

        return unique_string_list

    result = []
    half_limit = int(limit / 2)

    for e in string_list:
        # Don't shorten if it is still short enough
        if len(e) <= limit:
            result.append(e)
            continue

        # do shorten
        e = e[:half_limit] + omit_string + e[-half_limit:]

        # remember
        result.append(e)

    duplicated_strings = {s: n for s, n in Counter(result).items() if n > 1}

    for key in duplicated_strings:
        uni = _make_unique_strings(
            one_string=key,
            n=duplicated_strings[key]
        )

        for u in uni:
            idx = result.index(key)
            result[idx] = u

    return result


def nested_dict_update(org: dict, update: dict) -> dict:
    """Nested update of dict-like 'org' with dict-like 'update'.

    See *Deep merge dictionaries of dictionaries in Python* at
    StackOverflow: https://stackoverflow.com/q/7204805/4865723
    Credits for current solution:

    https://stackoverflow.com/a/52319248/4865723
    """
    for key in update:

        if (key in org
                and isinstance(org[key], MutableMapping)
                and isinstance(update[key], MutableMapping)):

            nested_dict_update(org[key], update[key])

            continue

        org[key] = update[key]

    return org


def read_config_data(path: pathlib.Path = None) -> dict:
    """Read a config file if present.

    Read a config file if present, combine its data with config default
    values and return them.

    .. code::

        # Filename is buhtzology.toml
        [bandas]
        decrease_workers_by=0

    Args:
        path: The path to look for a file named ``buhtzology.toml``.

    Returns:
        dict: A dictionary with config data.

    """
    if path is None:
        path = pathlib.Path.cwd() / 'buhtzology.toml'

    data = copy.deepcopy(_CONFIG_DEFAULT)

    if path.exists():
        with path.open('rb') as handle:
            msg = f'Reading config from {path}'
            _log.debug(msg)
            # Need a print because logging is not setup at this time.
            print(msg)  # noqa: T201
            data = nested_dict_update(data, tomllib.load(handle))

    else:
        _log.debug('No config file found. Use default values.')

    return data


config = read_config_data()

# ------------
# -- File Management
# ------------


def generate_filepath(basename: str,
                      file_suffix: str,
                      folder_path: pathlib.Path = None,
                      age_offset: int = 0) -> pathlib.Path:
    """Combine basename, path and timestamp to a filepath.

    Combine basename, path and timestamp to a filepath of an existing
    file.
    Generates a path of an existing file with a timestamp in its name
    based on a name stem, a file suffix. Optionally a folder different from
    current working dir (default) and an offset for the file age (based on
    the timestamp in filename) can be set.

    The original file names should contain a timestamp (e.g. YYYY-MM-DD) or
    another numeric or lexicographical sortable element. The
    =folder_path= is searched for all files matching the
    name pattern =*basename*file_suffix=.
    The found names are sorted reverse lexicographical. The 'age_offset'
    element of that list is returned which is by default the first element in
    the list which in turn is the youngest file provided the timestamp's in
    the filenames are correct.

    Args:
        basename: Name stem of the file's to search.
        file_suffix: Suffix of the file's to search.
        folder_path: Path to the folder where to search in
            (default: current working dir).
        age_offset: Element to use after reverse lexicographic sorting.

    Returns:
        File path relative to current working dir.

    Raises:
        FileNotFoundError: When no file was found.
        IndexError: If the 'age_offset' does not work.

    """
    if folder_path is None:
        folder_path = pathlib.Path.cwd()

    fn_pattern = f'*{basename}*{file_suffix}'

    nd = '' if age_offset == 0 else f'{age_offset+1}nd '  # codespell-ignore
    _log.debug(f'Try to find the {nd}youngest '  # codespell-ignore
               f'({fn_pattern}) file '
               f'myself in {folder_path}...')

    # search for the files matching the pattern
    the_glob = list(folder_path.glob(fn_pattern))

    # nothing found
    if not the_glob:
        raise FileNotFoundError(f'No file found in {folder_path} matching '
                                f'the pattern {fn_pattern}.')

    # Sort filenames reverse lexicographic
    fn = sorted(the_glob, reverse=True)
    try:
        fn = fn[age_offset]
    except IndexError as err:
        raise IndexError(
            f'Invalid age_offset. There are {len(the_glob)} filenames in the '
            f'list but you asked for the {age_offset+1}th.') from err

    # return the full path
    return fn
