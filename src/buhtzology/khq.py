#!/usr/bin/env python3
"""Scoring mode of the German version of the King's Health Questionnaire.

Described in *Bjelic-Radisci V et al. (2005)* DOI: `10.1055/s-2005-872957
<https://doi.org/10.1055/s-2005-872957>`_.
"""

import collections
import numpy
import pandas


# pylint: disable-next=invalid-name
def score_KHQ(data, col_names=None, suffix=''):
    """Auswertungsmodus der deutschen Version des King's Health Questionnaire.

    Nach Bjelic-Radisci V et al. (2005) doi: 10.1055/s-2005-872957
    Missing values (pandas.NA) werden durch 0 ersetzt bei den KHQ Items 9-11
    und 22-32.  Weitere missing values können imputiert werden, wenn
    mindestens 2/3 der Items einer Subskala beantwortet wurden. Bedingt durch
    die Struktur des KHQ, besteht diese Möglichkeit nur bei den Subskalen 7
    und 9 bzw. den Items 12-14 und 17-21 möglich. Imputiert wird der gerundete
    mittlere Wert der validen Items. Gemittelt wird mit
    'pandas.Series.mean()'. Gerundet wird mit 'decimal.ROUND_HALF_EVEN';
    d.h. zur nächstgelegenen geraden Zahl ('to the nearest even
    value'). Bzgl. Mittelwert und Rundungsmethode macht das deutsche KHQ
    Manual keine Angaben und auch die Erstautoren hat diese Frage nicht
    beantworten können.

    Args:
        data (pandas.DataFrame): Data including all raw KHQ items.
        col_names (list): Naming relevant columns in 'data'.
        suffix (str): Added to the name of resulting columns.

    Returns:
        (pandas.DataFrame): The 'data' with raw item values and new KHQ scale
        columns.

    Raises:
        (TypeError): If 'data' is not a 'pandas.DataFrame'.
        (ValueError): If the KHQ items have invalid values.

    """
    # pylint: disable=duplicate-code

    # DataFrame?
    if not isinstance(data, pandas.DataFrame):
        raise TypeError('Please offer a pandas.DataFrame for "data". But '
                        f'you offered {type(data)}!')

    # COLUMNS
    if not col_names:
        # Use all columns by default
        col_names = list(data.columns)

    # column count correct?
    if len(col_names) != 32:
        raise TypeError(
            'Please offer a DataFrame with 32 columns or specify the 32 '
            "KHQ related columns by 'col_names'.")

    # MISSING
    # In diesen Fragen (9-12 & 22-31) wird die Antwortoption "nicht
    # zutreffend" mit dem Wert 0 angeboten. Dies wird mit NA gleichgesetzt und
    # NA entsprechend durch 0 ersetzt.
    c = [col_names[i] for i in [*range(8, 11), *range(21, 32)]]
    data.loc[:, c] = data.loc[:, c].apply(lambda col: col.fillna(0))

    # VALIDATION
    # btw: Because of readability range() is not used here.
    valid_values = [
        (
            [0],  # Q1
            [1, 2, 3, 4, 5, pandas.NA, numpy.nan]  # valid item values
        ),
        (
            [1, 2, 3, 4, 5, 6, 7, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
            [1, 2, 3, 4, pandas.NA, numpy.nan]
        ),
        (
            [8, 9, 10],
            [0, 1, 2, 3, 4]
        ),
        (
            [21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
            [0, 1, 2, 3]
        )
    ]

    invalid_cols = []

    for idx, vals in valid_values:

        # each column
        for i in idx:
            c = col_names[i]

            # test
            if not data.loc[:, c].isin(vals).all():
                invalid_cols.append(c)

    if invalid_cols:
        raise ValueError(f'{data}\nThere are invalid item values in '
                         f'this columns: {invalid_cols}')

    # IMPUTATION
    data = data.apply(
        _imputate_KHQ_row_by_row,
        args=(col_names, ),
        axis=1
    )

    # do CALC
    data = data.apply(
        _calculate_KHQ_score_row_by_row,
        args=(col_names, suffix),
        axis=1
    )

    # truncate (not rounding!) resulting scores to 1 digit
    s = [f'{suffix}KHQ_S{i}' for i in range(1, 11)]
    s.extend([f'{suffix}KHQ_SC{i}' for i in range(26, 32)])
    data.loc[:, s] = data.loc[:, s] \
        .map(lambda cell: float(int(cell * 10) / 10), na_action='ignore')

    return data


# pylint: disable-next=invalid-name
def _imputate_KHQ_row_by_row(row, col_names):
    """Imputate missing values.

    See 'score_KHQ()' for more details.
    """
    index_lists = [
        # Subskala 7: Gefühlszustand
        [11, 12, 13],
        # Subskala 9: Umgang mit Inkontinenz
        [16, 17, 18, 19, 20]
    ]

    for idxl in index_lists:  # each scale
        # Names of items in that scale
        c = [col_names[i] for i in idxl]
        # count missing
        na_count = row[c].isna().sum()
        # only one missing
        if na_count == 1:
            # mean from valid items
            imp_val = row[c].mean(skipna=True)
            # round it
            # imp_val = round(imp_val, 1)
            # replace missing with it
            row[c] = row[c].fillna(imp_val)

    return row


def _calculate_one_score(score, minus_offset, divide_by):
    # Helper for `_calculate_KHQ_score_row_by_row()` to reduce cyclomatic
    # complexity.
    if score is not pandas.NA:
        score = (score - minus_offset) / divide_by * 100

    return score


# pylint: disable-next=invalid-name
def _calculate_KHQ_score_row_by_row(row, col_names, suffix):
    """Calculate KHQ score.

    Attention: Keep in mind that pythons list index for 'col_names'
    starts with 0 but the questionnaire starts with item 1. So list item
    number 10 means questionnaire item number 11.

    Raises:
        ValueError: Calculate score out of range.

    """
    # Subskala 1: Allgemeiner Gesundheitszustand
    row[f'{suffix}KHQ_S1'] = _calculate_one_score(
        row[col_names[0]], minus_offset=1, divide_by=4)

    # Subskala 2: Inkontinenzbelastung
    row[f'{suffix}KHQ_S2'] = _calculate_one_score(
        row[col_names[1]], minus_offset=1, divide_by=3)

    # Subskala 3: Einschränkungen in Alltagsaktivitäten
    row[f'{suffix}KHQ_S3'] = _calculate_one_score(
        row[[col_names[2], col_names[3]]].sum(skipna=False),
        minus_offset=2,
        divide_by=6
    )

    # Subskala 4: Körperliche Einschränkungen
    row[f'{suffix}KHQ_S4'] = _calculate_one_score(
        row[[col_names[4], col_names[5]]].sum(skipna=False),
        minus_offset=2,
        divide_by=6
    )

    # Subskala 5: Soziale Einschränkungen
    score = row[[col_names[6], col_names[7]]].sum(skipna=False)
    if score is not pandas.NA:
        # Frage 11
        item11 = row[col_names[10]]

        if item11 == 0:
            score = (((score + item11) - 2) / 6) * 100
        # unchained org: elif item11 >= 1 and item11 <= 4:
        elif 1 <= item11 <= 4:
            score = (((score + item11) - 3) / 9) * 100
        else:
            raise ValueError(
                f'Something is wrong here with subscale 5!\n{row}')

    row[f'{suffix}KHQ_S5'] = score

    # Subskala 6: Persönliche Beziehungen
    item9 = row[col_names[8]]
    item10 = row[col_names[9]]

    if (item9 + item10) == 0:
        score = pandas.NA

    elif 0 in [item9, item10] and (item9 >= 1 or item10 >= 1):
        score = (((item9 + item10) - 1) / 3) * 100

    elif 0 not in [item9, item10]:
        score = (((item9 + item10) - 2) / 6) * 100

    else:
        raise ValueError(f'Something is wrong here with subscale 6!\n{row}')

    row[f'{suffix}KHQ_S6'] = score

    # Subskala 7: Gefühlszustand
    row[f'{suffix}KHQ_S7'] = _calculate_one_score(
        row[[col_names[11], col_names[12], col_names[13]]].sum(skipna=False),
        minus_offset=3,
        divide_by=9
    )

    # Subskala 8: Schlaf / Energie
    row[f'{suffix}KHQ_S8'] = _calculate_one_score(
        row[[col_names[14], col_names[15]]].sum(skipna=False),
        minus_offset=2,
        divide_by=6
    )

    # Subskala 9: Umgang mit Inkontinenz
    score = row[[col_names[16],
                 col_names[17],
                 col_names[18],
                 col_names[19],
                 col_names[20]]].sum(skipna=False)

    row[f'{suffix}KHQ_S9'] = _calculate_one_score(
        score,
        minus_offset=5,
        divide_by=15
    )

    # Subskala 10: Überaktive Blase
    items_for_10 = row[[col_names[21],
                        col_names[22],
                        col_names[23],
                        col_names[24]]]

    counted = collections.Counter(items_for_10)

    score = {
        4: lambda: pandas.NA,
        3: lambda: (items_for_10.sum(skipna=False) - 1) / 2 * 100,
        2: lambda: (items_for_10.sum(skipna=False) - 2) / 4 * 100,
        1: lambda: (items_for_10.sum(skipna=False) - 3) / 6 * 100,
        0: lambda: (items_for_10.sum(skipna=False) - 4) / 8 * 100
    }[counted[0]]()

    row[f'{suffix}KHQ_S10'] = score

    # Skala 26-32: Symtopbelastung
    for s in [25, 26, 27, 28, 29, 30, 31]:

        score = row[col_names[s]]

        if score == 0:
            score = pandas.NA

        elif score in [1, 2, 3]:
            score = ((score - 1) / 2) * 100

        s_colname = f'{suffix}KHQ_SC{s + 1}'
        row[s_colname] = score

    return row
