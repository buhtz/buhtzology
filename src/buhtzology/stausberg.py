"""Stausberg comorbidity score calculation.

Calculation of comorbidity score based on the structure of the ICD-10
described in *Stausberg; Hagn (2015): New Morbidity
and Comorbidity Scores based on the Structure of the ICD-10*
DOI: `10.1371/journal.pone.0143365
<https://doi.org/10.1371/journal.pone.0143365>`_.

There are two variants described: ICD-10-groups or ICD-10-chapters. The
module currently implements the group variant only.
"""
import logging
import copy
import pandas

_log = logging.getLogger(__name__)
"""Logger instance for that module."""


def _groups_code_dataframe(icd_groups: list) -> pandas.DataFrame:
    """Return data frame with ICD3 codes corresponding to ``icd_groups``.

    Args:
        icd_groups: List of ICD groups. e.g. ``["A01-A06", "X14-X20"]``


    Returns:
        A data frame with column ``ICD3`` and ``GROUP``.

    Raises:
        ValueError: If the format of the group string is invalid.

    Example:
        The two groups ``['T15-T19', 'T80-T88']`` will be converted into
        ``['T15', 'T16', .., 'T19', 'T80', .., 'T88']``.

    """
    result = {
        'ICD3': [],
        'GROUP': []
    }

    for g in icd_groups:
        # length
        if len(g) != 7:
            raise ValueError(f'Length problem in "{g}"!')

        # check the format
        if not g[1:3].isdigit() or not g[5:7].isdigit():
            raise ValueError(f'Digit problem in "{g}"!')

        # letter
        if not g[0].isalpha() or not g[4].isalpha():
            raise ValueError(f'Alpha problem in "{g}"!')

        # split character
        if g[3] != '-':
            raise ValueError(f'Problem with split character in "{g}"!')

        start, end = g.split('-')

        # groups start with the same letter?
        if start[0] != end[0]:
            raise ValueError(
                f'{g}')

        curr = int(start[1:3])
        while curr <= int(end[1:3]):
            # create new key
            result['ICD3'].append(f'{start[0]}{curr:02}')

            # current group
            result['GROUP'].append(g)

            # increment
            curr += 1

    return pandas.DataFrame(result)


_GROUP_WEIGHTS = {
    'C00-C97': 4,     # Malignant neoplasms
    'D10-D36': -5,    # Benign neoplasms
    'D60-D64': 1,     # Aplastic and other anaemias
    'D65-D69': 1,     # Coagulation defects, purpura and other
                      # hemorrhagic conditions
    'E00-E07': -2,    # Disorders of thyroid gland
    'E50-E64': -7,    # Other nutritional deficiencies
    'E65-E68': -2,    # Obesity and other hyperalimentation
    'F10-F19': -3,    # Mental and behavioral disorders due to
                      # psychoactive substance use
    'F30-F39': -3,    # Mood [affective] disorders
    'G10-G14': 5,     # Systemic atrophies primarily affecting the
                      # central nervous system
    'G90-G99': 3,     # Other disorders of the nervous system
    'H53-H54': -4,    # Visual disturbances and blindness
    'I10-I15': -2,    # Hypertensive diseases
    'I26-I28': 2,     # Pulmonary heart disease and diseases of
                      # pulmonary circulation
    'I30-I52': 3,     # Other forms of heart disease
    'I60-I69': 2,     # Cerebrovascular diseases
    'I70-I79': 1,     # Diseases of arteries, arterioles and
                      # capillaries
    'I80-I89': -1,    # Diseases of veins, lymphatic vessels and
                      # lymph nodes, not elsewhere classified
    'J09-J18': 3,     # Influenza and pneumonia
    'J30-J39': -3,    # Other diseases of upper respiratory tract
    'J60-J70': 5,     # Lung diseases due to external agents
    'J80-J84': 3,     # Other respiratory diseases principally
                      # affecting the interstitium
    'J90-J94': 2,     # Other diseases of pleura
    'J95-J99': 3,     # Other diseases of the respiratory system
    'K35-K38': -131,  # Diseases of appendix
    'K40-K46': -2,    # Hernia
    'K55-K64': 1,     # Other diseases of intestines
    'K65-K67': 3,     # Diseases of peritoneum
    'K70-K77': 2,     # Diseases of liver
    'L80-L99': 3,     # Other disorders of the skin and subcutaneous
                      # tissue
    'M00-M25': -2,    # Arthropathies
    'M40-M54': -3,    # Dorsopathies
    'N17-N19': 4,     # Renal failure
    'N25-N29': -4,    # Other disorders of kidney and ureter
    'N30-N39': -1,    # Other diseases of urinary system
    'Q35-Q37': 6,     # Cleft lip and cleft palate
    'R10-R19': 2,     # Symptoms and signs involving the digestive
                      # system and abdomen
    'R30-R39': 1,     # Symptoms and signs involving the
                      # urinary system
    'R40-R46': 2,     # Symptoms and signs involving cognition,
                      # perception, emotional state and behavior
    'R50-R69': 3,     # General symptoms and signs
    'T15-T19': 6,     # Effects of foreign body entering through
                      # natural orifice
    'T80-T88': -1,    # Complications of surgical and medical care,
                      # not elsewhere classified
}
"""Stausberg scores per ICD group."""

_ARTIFACT_APPENDIX = 'K35-K38'


# pylint: disable-next=too-many-arguments
def weight_by_icd_group(data: pandas.DataFrame,  # noqa: PLR0913
                        *,
                        index_column: str,
                        icd_column: str,
                        weight_name: str = 'weight',
                        ignored_icds: tuple = ('UUU'),
                        exclude_appendix_artifact: bool = True
                        ) -> pandas.DataFrame:
    """Give the comorbidity-score per person.

    The function use the ICD-10-groups variant as *Comorbidity Scores based
    on the Structure of the ICD-10* by `Stausberg and Hagn,
    2015 DOI: 10.1371/journal.pone.0143365
    <https://doi.org/10.1371/journal.pone.0143365>`_.

    One ICD-group can be scored with values between ``-131`` and ``+6``.
    The range of the score per person can be between ``-176`` and
    ``+71``. Regarding to the discussion in the original publication the
    ICD group of appendix diseases (``K35-K38`` with score value of ``-131``)
    can (and is by default)
    excluded from the scores calculation. Because of that by default the
    lowest possible score per group is ``-5`` and per person ``-45``.

    Args:
        data: Input date.
        index_column: Name of column which is grouped for (e.g. Person ID).
        icd_column: Name of column with specific ICD codes internally
            converted into ICD-10-groups.
        weight_name: Name of resulting column with weight/score values.
        ignored_icds: Codes that are ignored in the input data.
        exclude_appendix_artifact: ICD codes belonging to the ICD group
            ``K35-K38`` is excluded from score calculation.

    Returns:
        A new data frame with two columns.

    """
    # extract relevant data from input data frame
    df = data.loc[:, [index_column, icd_column]].copy()

    # check for split character (groups not allowed)
    if df[icd_column].apply(lambda val: ('-' in val and val[-1] != '-')).any():
        raise TypeError('Found "-" in ICD codes. Groups are not allowed.'
                        ' Use concrete ICD codes please.')

    # reduce to length 3
    df[icd_column] = df[icd_column].apply(lambda val: val[:3])

    # ignore ICD
    if ignored_icds:
        logging.debug(f"Ignore (remove) this ICD's: {ignored_icds}")

    for ignore_this_icd in ignored_icds:
        df = df.loc[~(df[icd_column].eq(ignore_this_icd))]

    # extract duplicates (e.g. duplicate diagnoses groups)
    df = df.drop_duplicates()

    # check format (LETTER + DIGIT + DIGIT)
    mask = df[icd_column].apply(
        lambda val: val[0].isalpha() and val[1:3].isdigit())
    if not mask.all():
        raise TypeError(
            'Not all ICD are of format LETTER + DIGIT + DIGIT! -> '
            + str(df.loc[~mask, icd_column].unique()))

    # prepare the stausberg ICD groups with their weights
    group_weights = copy.deepcopy(_GROUP_WEIGHTS)
    if exclude_appendix_artifact:
        _log.info('Exclude "diseases of appendix" ICD-Group '
                  f'{_ARTIFACT_APPENDIX} from calculation of the '
                  'Stausberg score.')
        group_weights.pop(_ARTIFACT_APPENDIX)

    # Get GROUPS <-> ICD3 relations
    icd3_groups = _groups_code_dataframe(group_weights.keys())

    df = pandas.merge(
        how='left',
        left=df,
        left_on=[icd_column],
        right=icd3_groups,
        right_on=[icd3_groups.columns[0]]
    )

    # remove ICD
    df = df.loc[:, [index_column, icd3_groups.columns[1]]]

    # remove missing
    df = df.loc[~(df[df.columns[-1]].isna())]

    # Drop duplicates because by Stausberg ICD groups can't count more than
    # once
    df = df.drop_duplicates()

    # weights dataframe
    weights = pandas.DataFrame.from_dict(orient='index', data=group_weights)
    weights = weights.reset_index()

    weights.columns = [df.columns[-1], weight_name]

    # add weight based on groups
    df = pandas.merge(
        how='left',
        left=df,
        on=df.columns[-1],
        right=weights
    )

    # Remove ICD-Groups column
    df = df.drop(columns=[weights.columns[0]])

    # sum the weight per person
    df = df.groupby(index_column).sum()
    df = df.reset_index()

    return df
