"""Helpers with `pandas`.

Some little helper's for and depending on the data science package
`pandas`.
"""
from __future__ import annotations
import logging
import pathlib
import io
from typing import Union, Tuple
import zipfile
import requests
import re
import pandas

_log = logging.getLogger(__name__)


def _get_original_icd10gm(
        file_or_url: Union[pathlib.Path, str]) -> Tuple[pandas.DataFrame,
                                                        pandas.DataFrame,
                                                        pandas.DataFrame]:
    """Original WHO ICD-Data as Pandas DataFrames.

    See `create_icd_catalog()` for details.

    Args:
        file_or_url: File path or URL.

    Returns:
        Three data frames for chapters, blocks and codes.

    Raises:
        FileNotFoundError: If the files wasn't found.
        requests.exceptions.HTTPError: If there's a problem with the URL.

    """
    _log.info(f'Reading ICD-10-GM data from "{file_or_url}".')

    try:
        if file_or_url.startswith('https://'):
            _log.debug('Downloading via "requests"...')
            response = requests.get(file_or_url, timeout=10)

            _log.debug(f'Response code is {response.status_code}.')
            response.raise_for_status()
            file_or_url = io.BytesIO(response.content)

    except AttributeError:
        # starts_with() not existing
        # must be a pathlib.Path object
        pass

    # extract year
    if isinstance(file_or_url, pathlib.Path):
        fn = file_or_url.name
    else:
        fn = pathlib.Path(file_or_url).name

    year = re.findall(r'^icd10gm(\d{4})syst.*', fn)[0]

    with zipfile.ZipFile(file_or_url, mode='r') as zip_handle:
        base_path = 'Klassifikationsdateien/icd10gm' + year + 'syst_{}.txt'

        # CHAPTERS
        with zip_handle.open(base_path.format('kapitel')) as file_handle:
            chapters = pandas.read_csv(
                file_handle,
                delimiter=';',
                header=None,
                names=['CODE', 'TEXT'],
                dtype={'CODE': 'str', 'TEXT': 'str'},
                encoding='utf-8')
            chapters['TYP'] = 'CHAPTER'

        # BLOCKS (e.g. G30-G32)
        with zip_handle.open(base_path.format('gruppen')) as file_handle:
            blocks = pandas.read_csv(
                file_handle,
                delimiter=';',
                header=None,
                names=['START', 'END', 'CHAPTER', 'TEXT'],
                dtype={
                    'START': 'str',
                    'END': 'str',
                    'CHAPTER': 'str',
                    'TEXT': 'str'},
                encoding='utf-8')
            blocks['TYP'] = 'BLOCK'
            blocks['CODE'] = blocks.START + '-' + blocks.END

        # CODES
        with zip_handle.open(base_path.format('kodes')) as file_handle:
            codes = pandas.read_csv(
                file_handle,
                delimiter=';',
                header=None,
                usecols=[3, 4, 5, 8],
                names=['CHAPTER', 'BLOCKS3', 'ICD', 'TEXT'],
                dtype={
                    'CHAPTER': 'str',
                    'BLOCKS3': 'str',
                    'ICD': 'str',
                    'TEXT': 'str',
                }
            )
            codes['TYP'] = 'CODE'  # full ICD Code

    return chapters, blocks, codes


def create_icd_catalog(
        file_or_url: Union[pathlib.Path, str]) -> pandas.DataFrame:
    """Create an ICD Catalog based on data from BfArM or WHO.

    The data source can be a zip file (download from BfArM_ or WHO_)
    or a download URL for that zip file (e.g. ``icd10gm2024syst-meta.zip``).

    The result looks like this::

        CODE      TYP CHAPTER    BLOCK        TEXT         CODE_TEXT
        0    A00.-     CODE      01  A00-A09   Cholera..   A00.- Cholera..
        1    A00.0     CODE      01  A00-A09   Cholera..   A00.0 Cholera..
        2  A00-A09    BLOCK      01  A00-A09  Infektiö..  A00-A09 Infekt..
        3  A15-A19    BLOCK      01  A15-A19  Tuberkul..  A15-A19 Tuberk..
        4       01  CHAPTER      01           Bestimmt..  01 Bestimmte i..
        5       02  CHAPTER      02           Neubildu..  02 Neubildunge..

    Attention:
        Most of the downloads offering zip archives. But the internal
        structure of them differs. This can cause problems with that function
        here because it doesn't know all structures yet. Please open an
        `Issue report`_

    .. _BfArM:
        https://www.bfarm.de/DE/Kodiersysteme/Services/Downloads/_node.html
    .. _WHO:
        https://www.who.int/standards/classifications/classification-of-diseases
    .. _Issue report:
        https://codeberg.org/buhtz/buhtzology/issues/new

    Args:
        file_or_url: File path or URL.

    Returns:
        Data frame with columns named ???

    """
    _log.info('Create ICD-Catalog.')
    chapters, blocks, codes = _get_original_icd10gm(file_or_url)

    # Codes
    codes = codes.rename(columns={'ICD': 'CODE'})
    codes = codes.loc[:, ['CODE', 'TEXT', 'TYP']]

    # Chapters
    codes = pandas.concat([codes, chapters])

    # Blocks
    codes = pandas.concat([codes, blocks.loc[:, ['CODE', 'TYP', 'TEXT']]])

    # Now we specify the chapter and block for each full qualified ICD-Code.
    codes['CHAPTER'] = ''
    codes['BLOCK'] = ''

    codes = codes.reset_index(drop=True)

    def _sub_apply_block_chapter(row):
        if row.TYP == 'CHAPTER':
            # A chapter doesn't have a block.
            row.CHAPTER = row.CODE

        elif row.TYP in ['BLOCK', 'CODE']:
            if row.TYP == 'BLOCK':
                # A block is a block
                row.BLOCK = row.CODE

            if row.TYP == 'CODE':
                # Bereich
                bereich = row.CODE[:3]
                mask = (blocks.START.le(bereich) & blocks.END.ge(bereich))
                row.BLOCK = blocks[mask].CODE.item()

            # Chapter
            row.CHAPTER = blocks[blocks.CODE.eq(row.BLOCK)].CHAPTER.item()

        return row

    codes = codes.apply(_sub_apply_block_chapter, axis=1)

    # CODE_TEXT
    codes['CODE_TEXT'] = codes.CODE + ' ' + codes.TEXT

    # PLAUSI
    if codes[codes.TYP.ne('CHAPTER')].CHAPTER.eq('').any():
        raise ValueError('Some entries without a CHAPTER!')
    if codes[codes.TYP.eq('CODE')].BLOCK.eq('').any():
        raise ValueError('Some entries without a BLOCK!')

    codes = reorder_columns(codes, ['TYP'], 'CODE')
    codes = reorder_columns(codes, ['CHAPTER', 'BLOCK'], 'TYP')

    for col in ['TYP', 'CHAPTER', 'BLOCK']:
        codes[col] = codes[col].astype('category')

    return codes


def reorder_columns(dataframe: pandas.DataFrame,
                    this_columns: Union[list[str], str],
                    behind_this_column: str = None) -> pandas.DataFrame:
    """Rearrange the columns of a DataFrame.

    The columns named in ``this_columns`` are moved behind the column named
    via ``behind_this_column``.

    Args:
        dataframe: The complete data frame.
        this_columns: List of names or one name of column(s) to move.
        behind_this_column: Name of column before the insertion position.

    Returns:
        The new ordered data frame.

    Raises:
        AttributeError: Column names not unique.
        KeyError: Column to move not exist.
        ValueError: Behind column not exist.

    """
    if not dataframe.columns.is_unique:
        raise AttributeError('The data frame Column names need to be unique.')

    if isinstance(this_columns, str):
        this_columns = [this_columns]

    if len(this_columns) != len(set(this_columns)):
        if isinstance(this_columns, str):
            raise TypeError('The columns to rearrange (argument '
                            '"this_columns" should be of type list!')

        raise AttributeError('The columns to rearrange need to be unique.')

    # List of columns without the columns to move
    columns = list(filter(lambda item: item not in this_columns,
                          dataframe.columns))

    if behind_this_column:
        # Insert position
        idx_insert = columns.index(behind_this_column) + 1
        # Insert via slice
        columns = columns[:idx_insert] + this_columns + columns[idx_insert:]
    else:
        # first position
        columns = this_columns + columns

    # re-order
    return dataframe.loc[:, columns]


def add_missing_category(data: pandas.Series,
                         missing_label: str,
                         insert_at_end: bool = True
                         ) -> pandas.Series:
    """Add category label for missing values to a categorical data series.

    Args:
        data: The data series (e.g. a column).
        missing_label: The new label.
        insert_at_end: Append label to the end (default) or in front (False).

    Returns:
        The data series with new categorical dtype attached.

    """
    catlist = data.cat.categories.tolist()

    catlist = [missing_label] + catlist

    if insert_at_end:
        catlist.append(catlist.pop(0))

    cat = pandas.CategoricalDtype(catlist, ordered=data.cat.ordered)

    data = data.astype(cat)
    data = data.fillna(missing_label)

    return data
