# pylint: disable=too-many-lines
"""Helpers with `pandas`.

Some little helper's for and depending on the data science package
`pandas`.
"""
from __future__ import annotations
import logging
from typing import Union, Tuple, Iterable
import math
import pandas

_log = logging.getLogger(__name__)


def generate_bin_labels(bins: Iterable,
                        format_string: str = '{} to {}',
                        begin_format_string: str = 'less than {}',
                        end_format_string: str = '{} and more'
                        ) -> list[str]:
    """Create labels for the associated intervals.

    The function is used by `cut_bins()`. Two ways exists to give the intervals
    in the ``bin`` argument.

    Example:
        .. python::
            # Pairs of intervals (both ends included)
            bins = [
                (0, 4),
                (5, 9),
                (10, 20)
            ]
            result = ['0 to 4', '5 to 9', '10 to 20']

            # Or with infinity ends
            bins = [
                (-math.inf, 4),
                (5, 9),
                (10, math.inf)
            ]
            result = ['less than 5', '5 to 9', '10 and more']

            # As list of edges (only right end included)
            bins = [0, 5, 10]
            result = ['1 to 5', '6 to 10']

            # Or with infinity ends
            bins = [-math.inf, 0, 5, 10, math.inf]
            result = ['less than 1', '1 to 5', '6 to 10', '11 and more']

    Args:
        bins: The intervals as a list of bin edges or a list of start-end
              pairs.
        format_string: Used for labels like ``"x to y"``.
        begin_format_string: Used for the first label with infinity begin.
        end_format_string: Used for the last label with infinity end.

    Returns:
        A list of labels.

    """
    if isinstance(bins[0], tuple):
        # it is a list of (begin, end) pairs
        intervals = bins
        offset = 0
    else:
        # interleave to get begin-end pairs
        intervals = list(zip(bins, bins[1:]))
        offset = 1

    result = []

    for begin, end in intervals:

        if begin == -math.inf:
            if isinstance(bins[0], tuple):
                first_edge = intervals[1][0]
            else:
                first_edge = end

            # infinity start
            result.append(begin_format_string.format(first_edge + offset))

        elif end == math.inf:
            # infinity end
            result.append(end_format_string.format(begin + offset))

        else:
            # usual middle
            result.append(format_string.format(begin + offset, end))

    return result


def cut_bins_via_interval_count(
        values: Union[list, pandas.Series],
        interval_range: int,
        interval_count: int,
        first_interval_begin: int = 0,
        **kwargs) -> Union[list, pandas.Series]:
    """Cutting values into specific number of bins of the same range.

    If one of the values is not covered by the resulting intervals (bins)
    a `ValueError` is raised. NA values are an exception of that rule.
    This function is a wrapper around `cut_bins()`.

    Args:
        values: List of values to cut.
        interval_range: Size for each interval.
        interval_count: Number of intervals (or bins).
        first_interval_begin: First value in the first interval.
        kwargs: See `cut_bins()` for additional arguments.

    Returns:
        List of labels corresponding to ``values``.

    Raises:
        ValueError: If not all values covered by the bins/intervals
                    (except NA values).

    """
    interval_list = [
        (first_interval_begin, first_interval_begin - 1 + interval_range)
    ]

    for _ in range(1, interval_count):
        # last_value = interval_list[-1][1]
        # next_interval = (last_value+1, (interval_range*(idx+1)-1))
        # next_interval = (last_value+1, last_value+interval_range)
        next_interval = (
            interval_list[-1][0] + interval_range,
            interval_list[-1][1] + interval_range,
        )

        interval_list.append(next_interval)

    return cut_bins(
        values=values,
        interval_list=interval_list,
        **kwargs
    )


# pylint: disable-next=too-many-arguments
def cut_bins(values: Union[list, pandas.Series],  # noqa: PLR0913
             *,
             infinity_is_less_than: int = 0,
             interval_range: int = None,
             infinity_is_equal_or_more_than: int = None,
             interval_list: Iterable[Tuple] = None,
             labels: Iterable[str] = None,
             format_string: str = '{} to {}',
             begin_format_string: str = 'less than {}',
             end_format_string: str = '{} and more',
             remove_unused_labels: bool = False
             ) -> Union[list, pandas.Series]:
    """Cutting values into bins similar to `pandas.cut()`.

    See also `cut_bins_via_interval_count()` for a simplified wrapper.
    There are two major methods to specify the bins or intervals.

    .. python::

        # Specify steps using "interval_range" to get intervals of
        # same length:
        cut_bins(the_values, interval_range=10)

        # Or using "interval_list" to specify explicit each interval
        # with pairs of start and begin value which are both included
        # in that interval:
        bins = [(0, 4), (5, 9), (10, 50)]
        cut_bins(the_values, interval_list=bins)

    The interval range arguments are ignored when ``interval_list`` is given.

    Infinity starts and ends are created automatically when ``interval_range``
    is used but not with ``interval_list``. For the latter you have to specify
    them yourself like so:

    .. python::

        bins = [(-math.inf, -1), (0, 9), (10, math.inf)]
        cut_bins(the_values, interval_list=bins)

    The wording of the labels can be modified via ``format_string`` and when
    using infinity ends also via ``begin_format_string`` and
    ``end_format_string``. Or you can set explicit labels via ``labels``.

    Args:
        values: List of values to cut.
        infinity_is_less_than: The right edge (or end) of the first infinity
            interval. For example if ``0`` the first two intervals are
            ``[(math.inf, -1), (0, ...)]``.
        interval_range: Size for each interval.
        infinity_is_equal_or_more_than: Value covered by the last infinity
            interval. For example if ``100`` the last two intervals are
            ``[(..., 99), (100, math.inf)]``.
        interval_list: List of pairs of start and end points of
            intervals. Both points are included in that interval. Overwrites
            ``interval_range`` and related arguments. If one of ``values`` is
            not covered in that list a `ValueError` is raised.
        labels: Explicit list of labels to use.
        format_string: Used for in between intervals.
        begin_format_string: Used for the first interval including infinity
            start.
        end_format_string: Used for the last interval including infinity end.
        remove_unused_labels: Labels removed from the categories if they don't
            exist in the values.

    Returns:
        List of labels corresponding to ``values``.

    Raises:
        ValueError: If ``interval_list`` is used and its length isn't equal
                    to the number of provided ``labels``.
        ValueError: If not all values covered by the bins/intervals
                    (except NA values).

    """
    if isinstance(values, list):
        values = pandas.Series(values)

    if interval_list:
        # labels
        if not labels:
            labels = generate_bin_labels(
                bins=interval_list,
                format_string=format_string,
                begin_format_string=begin_format_string,
                end_format_string=end_format_string
            )

        bins = pandas.IntervalIndex.from_tuples(interval_list, closed='both')

    else:
        # compute last interval end if needed
        # if not last_interval_end:
        if not infinity_is_equal_or_more_than:
            infinity_is_equal_or_more_than = values.max() + interval_range

        # create bins
        bins = [infinity_is_less_than - 1]
        while bins[-1] < (infinity_is_equal_or_more_than - interval_range):
            bins.append(bins[-1] + interval_range)

        # infinity end points
        bins = [-math.inf] + bins + [math.inf]

        # labels
        if not labels:
            labels = generate_bin_labels(
                bins=bins,
                format_string=format_string,
                begin_format_string=begin_format_string,
                end_format_string=end_format_string
            )

    result = pandas.cut(
        x=pandas.to_numeric(values),
        bins=bins,
        right=True,
        include_lowest=False,
        ordered=True,
        labels=labels,
    )

    # Count NA entries to find values uncovered by bins.
    if result.isna().sum() > values.isna().sum():
        raise ValueError('Not all values are included in the given bins.')

    # Workaround because pandas do ignored labels when using IntervalIndex
    # Issue : https://github.com/pandas-dev/pandas/issues/21233
    # Credits for that workaround goes to
    # https://github.com/pandas-dev/pandas/issues/21233#issuecomment-603759862
    if isinstance(bins, pandas.IntervalIndex):

        # Development note: Python 3.10 introduced the "strict=" argument
        # as a solution. See: https://stackoverflow.com/a/62773251/4865723
        # Until then we check it ourself.

        if len(bins) != len(labels):
            raise ValueError(f'Number of labels (n={len(labels)}) need to be '
                             f'equal to bins (n={len(bins)}).')

        result = result.map(dict(zip(bins, labels)))

    if remove_unused_labels:
        result = result.cat.remove_unused_categories()

    return result
