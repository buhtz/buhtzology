"""A lose collection of code I reuse in different (scientific) projects."""
import os
import pathlib
from collections import Counter
from importlib.metadata import metadata
# Wildcard imports not allowed because of an known Issue in pydoctor.
# see: https://github.com/twisted/pydoctor/issues/565
from ._buhtzology import (generate_filepath,
                          runtime_as_string,
                          how_much_elements_per_piece,
                          setup_logging,
                          shorten_strings_but_unique,
                          break_paragraph,
                          nested_dict_update,
                          read_config_data,
                          config)


def _package_metadata_as_dict(exclude_keys: list = None) -> dict:
    """Get package metadata and return it as a dict."""
    if exclude_keys is None:
        # default
        exclude_keys = ['License', 'Classifier']

    m = metadata(__name__)

    result = {}

    for key, count in Counter(m.keys()).items():

        if key in exclude_keys:
            continue

        # single value key
        if count == 1:
            result[key] = m[key]
            continue

        # multi value key
        result[key] = []
        for val in m.get_all(key):
            result[key].append(val)

    # postprocess project URLs
    urls = result['Project-URL']
    result['Project-URL'] = {}
    for entry in urls:
        key, val = entry.split(',')
        result['Project-URL'][key.strip()] = val.strip()

    return result


meta = _package_metadata_as_dict()

__version__ = meta['Version']
"""Version string of the package."""


def get_full_application_string() -> str:
    """Build a string representing the application name and its version.

    If a git repo is present also information about its current state are
    added.
    """
    try:
        git = get_git_repository_info()
        git = f' (branch: {git["branch"]} at {git["hash"]})'
    except FileNotFoundError:
        git = ''

    return f'{__name__} {__version__}{git}'


def get_git_repository_info() -> dict:
    """Return the current branch and last commit hash.

    A special case is when that script runs on a Read The Docs instance. In
    that case the branch name is extracted from environment variables.

    Credits: https://stackoverflow.com/a/51224861/4865723
    """
    git_folder = pathlib.Path.cwd() / '.git'
    result = {}

    # If run on a Read The Docs instance
    if 'READTHEDOCS' in os.environ:
        if os.environ['READTHEDOCS_VERSION_TYPE'] == 'branch':
            result['branch'] = os.environ['READTHEDOCS_VERSION']
    else:
        # branch name
        with (git_folder / 'HEAD').open('r') as handle:
            result['branch'] = '/'.join(handle.read().split('/')[2:]).strip()

    # commit hash
    with (git_folder / 'refs' / 'heads' / result['branch']) \
         .open('r') as handle:
        result['hash'] = handle.read().strip()

    return result


__full_name__ = get_full_application_string()
"""Full name string including version, branch, commit."""

# __version_date__ = 'UNRELEASED'

# see: https://stackoverflow.com/q/71349403/4865723
# see: https://stackoverflow.com/a/31594545/4865723

# this we need also for pydoctor
__all__ = [
    'generate_filepath',
    'runtime_as_string',
    'how_much_elements_per_piece',
    'setup_logging',
    'shorten_strings_but_unique',
    'break_paragraph',
    'nested_dict_update',
    'read_config_data',
    'config',
]
