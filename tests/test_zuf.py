"""ZUF testing"""
import unittest
import pandas
from buhtzology import zuf
from . import helper


class TestZUF(unittest.TestCase):
    """ZUF tests"""

    def _zuf_dataframe(self, vals=None):
        """Create a pandas DataFrame with all KHQ items using item value 1.
        The columns are named via integers beginning from 0.
        """
        if vals:
            return pandas.DataFrame([vals])

        return pandas.DataFrame([[1] * 8])

    def test_dataframe(self):
        """Default DataFrame for testing"""
        df = self._zuf_dataframe()

        # column names
        self.assertTrue((df.columns == range(8)).all())

        # rows & columns
        self.assertEqual(df.shape, (1, 8))

    def _do_column_names(self, suffix):
        """Specific column names"""
        df = self._zuf_dataframe()

        # create 32 column names
        c = [helper.rand_string(8, 3) for i in range(8)]

        # rename columns
        df.columns = c

        # duplicated columns not allowed for some operations
        if df.columns.duplicated().any():
            # try again
            self._do_column_names(suffix)

        # insert extra columns
        cfull = c[:]
        df.insert(loc=0, column='A', value='a')
        cfull.insert(0, 'A')
        df.insert(loc=2, column='B', value='b')
        cfull.insert(2, 'B')
        df.insert(loc=5, column='C', value='c')
        cfull.insert(5, 'C')
        self.assertEqual(cfull, list(df.columns))

        # result columns
        cresult = cfull[:]
        cresult.extend([f'{suffix}ZUF', f'{suffix}ZUFagg'])

        df = zuf.score_ZUF8(data=df, col_names=c, suffix=suffix)

        self.assertEqual(cresult, list(df.columns))

    def test_column_names(self):
        """Specific column names"""
        self._do_column_names('')

    def test_column_names_with_suffix(self):
        """Specific column names with suffix"""
        self._do_column_names(helper.rand_string(7))

    # pylint: disable-next=invalid-name
    def _do_Q_valid_values(self,
                           values_and_results,
                           destination_column_name,
                           do_not_invert=False):
        """Helper"""
        # all valid values
        for val, res in values_and_results:
            df = self._zuf_dataframe(val)

            df = zuf.score_ZUF8(data=df,
                                col_names=list(range(8)),
                                suffix='',
                                do_not_invert=do_not_invert)

            m = f'=> Values: {val}; Destination: {destination_column_name}'

            if pandas.isna([val, res]).any():
                # NA does not work with assertEqual()
                self.assertTrue(
                    df.loc[0, destination_column_name] is res, m)
            else:
                self.assertEqual(
                    df.loc[0, destination_column_name], res, m)

    def test_valid_scores_agg(self):
        """Aggregated results for items with valid values.

        Keep in mind that item 1, 3, 6 and 7 are inverted."""
        self._do_Q_valid_values(
            values_and_results=[
                ((1, 4, 1, 4, 4, 1, 1, 4), 'sehr zufrieden'),
                ((1, 4, 1, 4, 4, 1, 2, 4), 'weitgehend zufrieden'),
                ((1, 1, 1, 1, 1, 1, 1, 1), 'teilweise unzufrieden'),
                ((2, 1, 1, 1, 1, 1, 1, 1), 'teilweise unzufrieden'),
                ((3, 1, 1, 1, 1, 1, 1, 1), 'teilweise unzufrieden'),
                ((1, 2, 3, 3, 1, 2, 3, 4), 'teilweise unzufrieden'),
                ((2, 4, 4, 4, 4, 4, 4, 4), 'teilweise unzufrieden'),
                ((4, 4, 4, 4, 4, 4, 4, 4), 'teilweise unzufrieden'),
                ((1, 2, 4, 1, 1, 1, 1, 1), 'teilweise unzufrieden'),
                ((1, 3, 4, 1, 1, 1, 1, 1), 'teilweise unzufrieden'),
                ((1, 4, 4, 1, 1, 4, 4, 1), 'ziemlich unzufrieden'),
                ((1, 1, 1, pandas.NA,
                  pandas.NA, pandas.NA, 1, 1), '(fehlend)'),
            ],
            destination_column_name='ZUFagg')

    def test_invalid_values(self):
        """Invalid (not allowed) item values."""
        invalid_values = [
            (0, 1, 1, 1, 1, 1, 1, 1),
            (1, 0, 1, 1, 1, 1, 1, 1),
            (1, 1, 5, 1, 1, 1, 1, 1),
            (1, 1, 1, 5, 1, 1, 1, 1),
            (1, 1, 1, -1, 1, 1, 1, 1),
            (1, 1, 1, -3, 1, 1, 1, 1),
        ]
        # invalid values
        for val in invalid_values:
            df = self._zuf_dataframe(val)

            m = f'=> Values: {val}'
            with self.assertRaises(ValueError, msg=m):
                zuf.score_ZUF8(data=df)

    def test_invertation(self):
        """Invertation on/off."""
        # Invertation is done by default
        df = self._zuf_dataframe()
        df = zuf.score_ZUF8(data=df,
                            col_names=list(range(8)),
                            suffix='')
        self.assertEqual(df.loc[0, 'ZUF'], 20)

        # Invertation explicit OFF
        df = self._zuf_dataframe()
        df = zuf.score_ZUF8(data=df,
                            col_names=list(range(8)),
                            suffix='',
                            do_not_invert=True)
        self.assertEqual(df.loc[0, 'ZUF'], 8)

    def test_imputate_foreign_cols(self):
        """Imputation if foreign columns present."""
        df = self._zuf_dataframe()

        # make missing values
        df.loc[0, 17] = pandas.NA

        # insert some foreign columns
        df.insert(loc=0, column='A', value='a')
        df.insert(loc=7, column='X', value=pandas.NA)

        # calc
        df = zuf.score_ZUF8(df, list(range(8)))

        # test
        self.assertEqual(df.loc[0, 'ZUF'], 20)
        self.assertEqual(df.loc[0, 'ZUFagg'], 'teilweise unzufrieden')
        self.assertEqual(df.loc[0, 'A'], 'a')
        self.assertTrue(df.loc[0, 'X'] is pandas.NA)

    def test_valid_scores(self):
        """All items with valid values.

        Keep in mind that item 1, 3, 6 and 7 are inverted."""
        self._do_Q_valid_values(
            values_and_results=[
                ((1, 4, 1, 4, 4, 1, 1, 4), 32),
                ((1, 4, 1, 4, 4, 1, 2, 4), 31),
                ((1, 1, 1, 1, 1, 1, 1, 1), 20),
                ((2, 1, 1, 1, 1, 1, 1, 1), 19),
                ((3, 1, 1, 1, 1, 1, 1, 1), 18),
                ((1, 4, 1, 1, 1, 1, 1, 1), 23),
                ((1, 1, 1, 1, 1, 1, 1, 1), 20),
                ((1, 2, 1, 1, 1, 1, 1, 1), 21),
                ((1, 3, 1, 1, 1, 1, 1, 1), 22),
                ((1, 1, 4, 1, 1, 1, 1, 1), 17),
                ((1, 1, 1, 1, 1, 1, 1, 1), 20),
                ((1, 1, 2, 1, 1, 1, 1, 1), 19),
                ((1, 1, 3, 1, 1, 1, 1, 1), 18),
                ((1, 2, 3, 4, 1, 2, 3, 4), 22),
                ((1, 2, 3, 3, 1, 2, 3, 4), 21),
                ((2, 4, 4, 4, 4, 4, 4, 4), 22),
                ((4, 4, 4, 4, 4, 4, 4, 4), 20),
            ],
            destination_column_name='ZUF')

    def test_valid_scores_with_missing(self):
        """All items with 3 missing.

        Three or more missing items are valid and cause a
        missing score. Just two or one missing item would
        cause an imputation and valid non-missing score."""
        self._do_Q_valid_values(
            values_and_results=[
                ((pandas.NA, pandas.NA, pandas.NA, 1, 1, 1, 1, 1), pandas.NA),
                ((1, pandas.NA, pandas.NA, pandas.NA, 1, 1, 1, 1), pandas.NA),
                ((1, 1, pandas.NA, pandas.NA, pandas.NA, 1, 1, 1), pandas.NA),
                ((1, 1, 1, pandas.NA, pandas.NA, pandas.NA, 1, 1), pandas.NA),
                ((1, 1, 1, 1, pandas.NA, pandas.NA, pandas.NA, 1), pandas.NA),
                ((1, 1, 1, 1, 1, pandas.NA, pandas.NA, pandas.NA), pandas.NA),
                ((pandas.NA, pandas.NA, 1, 1, 1, 1, pandas.NA, 1), pandas.NA),
                ((1, pandas.NA, pandas.NA, 1, 1, 1, 1, pandas.NA), pandas.NA),
            ],
            destination_column_name='ZUF')

        # # Q1: invalid values
        # self._do_Q_invalid_values(
        #     invalid_values=[0, 6],
        #     source_column_idx=0)

    def test_imputation(self):
        """Imputation of missing. Invertation on/off"""
        values_and_results = [
            ((pandas.NA, 1, 1, 1, 1, 1, 1, 1), 18.285714285714285),
            ((pandas.NA, pandas.NA, 1, 1, 1, 1, 1, 1), 20),
            # no imputation of more then 2 missing
            ((pandas.NA, pandas.NA, pandas.NA, 1, 1, 1, 1, 1), pandas.NA),
        ]

        # invertation (default) ON
        self._do_Q_valid_values(
            values_and_results=values_and_results,
            destination_column_name='ZUF', do_not_invert=False)

        # invertation OFF
        values_and_results = [
            ((pandas.NA, 1, 1, 1, 1, 1, 1, 1), 8),
            ((pandas.NA, pandas.NA, 1, 1, 1, 1, 1, 1), 8),
            # no imputation of more then 2 missing
            ((pandas.NA, pandas.NA, pandas.NA, 1, 1, 1, 1, 1), pandas.NA),
        ]
        self._do_Q_valid_values(
            values_and_results=values_and_results,
            destination_column_name='ZUF', do_not_invert=True)
