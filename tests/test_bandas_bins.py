"""Testing behavior of bandas module"""
import unittest
import math
import numpy
import pandas
from pandas.testing import assert_frame_equal
from buhtzology import bandas


class CutBins(unittest.TestCase):
    """Tests about cut bins"""

    def test_infinity_ends(self):
        """Cut bins including infinity start- and end-intervals.
        """
        vals = [-1, 0, 1, 9, 10, 11, 19, 20, 21, 29, 30, 31, 39, 40, 41, 49,
                50, 51, 100]

        df = pandas.DataFrame({'vals': vals})

        expect = df.copy()
        expect['bins'] = [
            'less than 0', '0 to 9', '0 to 9', '0 to 9',
            '10 to 19', '10 to 19', '10 to 19',
            '20 to 29', '20 to 29', '20 to 29',
            '30 to 39', '30 to 39', '30 to 39',
            '40 to 49', '40 to 49', '40 to 49',
            '50 to 59', '50 to 59',
            '60 and more']
        expect.bins = expect.bins.astype(
            pandas.CategoricalDtype(
                categories=[
                    'less than 0', '0 to 9', '10 to 19', '20 to 29',
                    '30 to 39', '40 to 49', '50 to 59', '60 and more'
                ],
                ordered=True
            )
        )

        for infinity_is_or_more in [60, 65, 69]:
            x = bandas.cut_bins(
                values=df.vals,
                # less then 0
                infinity_is_less_than=0,
                interval_range=10,
                # 60 or more
                infinity_is_equal_or_more_than=infinity_is_or_more
            )
            # print(f'\n\n{infinity_is_or_more=}\n{x.to_markdown()}')
            df['bins'] = x
            assert_frame_equal(df, expect)

    def test_via_interval_count(self):
        """Cut bins using exact number of intervals.
        """
        vals = [0, 1, 9, 10, 11, 19, 20, 21, 29, 30, 31, 39, 40, 41, 49]

        df = pandas.DataFrame({'vals': vals})

        expect = df.copy()
        expect['bins'] = [
            '0 to 9', '0 to 9', '0 to 9',
            '10 to 19', '10 to 19', '10 to 19',
            '20 to 29', '20 to 29', '20 to 29',
            '30 to 39', '30 to 39', '30 to 39',
            '40 to 49', '40 to 49', '40 to 49']
        expect.bins = expect.bins.astype(
            pandas.CategoricalDtype(
                categories=[
                    '0 to 9', '10 to 19', '20 to 29',
                    '30 to 39', '40 to 49'],
                ordered=True
            )
        )

        df['bins'] = bandas.cut_bins_via_interval_count(
            values=df.vals,
            interval_range=10,
            interval_count=5,
            first_interval_begin=0
        )
        assert_frame_equal(df, expect)

    def test_via_interval_count_kwargs(self):
        """Cut bins using exact number of intervals.
        """
        df = pandas.DataFrame({'vals': list(range(100))})

        df['bins'] = bandas.cut_bins_via_interval_count(
            values=df.vals,
            interval_range=20,
            interval_count=5,
            begin_format_string='foo {}',
            end_format_string='bar {}',
            format_string='x {} {} y'
        )
        self.assertEqual(df.loc[df.vals.eq(0)].iloc[0].bins, 'x 0 19 y')

    def test_age(self):
        """Cut into age categories.
        """

        ages = [[ten - 1, ten, ten + 1] for ten in range(0, 70, 10)]
        ages = [a for ten in ages for a in ten]  # unlist

        df = pandas.DataFrame({'age': ages})

        expect = df.copy()
        expect['AgeCat'] = [
            'less than 0', '0 to 9', '0 to 9', '0 to 9',
            '10 to 19', '10 to 19', '10 to 19',
            '20 to 29', '20 to 29', '20 to 29',
            '30 to 39', '30 to 39', '30 to 39',
            '40 to 49', '40 to 49', '40 to 49',
            '50 to 59', '50 to 59', '50 to 59',
            '60 and more', '60 and more']
        expect.AgeCat = expect.AgeCat.astype(
            pandas.CategoricalDtype(
                categories=[
                    'less than 0', '0 to 9', '10 to 19', '20 to 29',
                    '30 to 39', '40 to 49', '50 to 59', '60 and more'
                ],
                ordered=True
            )
        )

        df['AgeCat'] = bandas.cut_bins(
            values=df.age,
            infinity_is_less_than=0,
            interval_range=10,
            infinity_is_equal_or_more_than=60,
        )

        assert_frame_equal(df, expect)

    def test_age_labeled(self):
        """Cut into labeled age categories."""

        ages = [[ten - 1, ten, ten + 1] for ten in range(0, 30, 10)]
        ages = [a for ten in ages for a in ten]  # unlist

        df = pandas.DataFrame({'age': ages})

        expect = df.copy()
        expect['AgeCat'] = [
            'less than 0', '0 to 9', '0 to 9', '0 to 9',
            '10 to 19', '10 to 19', '10 to 19',
            '20 and more', '20 and more']
        expect.AgeCat = expect.AgeCat.astype(
            pandas.CategoricalDtype(
                categories=[
                    'less than 0', '0 to 9', '10 to 19', '20 and more'
                ],
                ordered=True
            )
        )

        df['AgeCat'] = bandas.cut_bins(
            values=df.age,
            infinity_is_less_than=0,
            interval_range=10,
            infinity_is_equal_or_more_than=20,
        )

        assert_frame_equal(df, expect)

    def test_age_with_auto_end(self):
        """Last intervals end computed automatically.
        """

        # [-1, 0, 1, 9, 10, 11, 19, 20, 21]
        ages = [[ten - 1, ten, ten + 1] for ten in range(0, 30, 10)]
        ages = [a for ten in ages for a in ten]  # unlist

        df = pandas.DataFrame({'age': ages})

        # See that "30 and more" don't exist in real data but will be added
        # as an empty category because of auto computation.
        expect = df.copy()
        expect['AgeCat'] = [
            'less than 0', '0 to 9', '0 to 9', '0 to 9',
            '10 to 19', '10 to 19', '10 to 19',
            '20 to 29', '20 to 29']

        expect.AgeCat = expect.AgeCat.astype(
            pandas.CategoricalDtype(
                categories=[
                    'less than 0', '0 to 9', '10 to 19', '20 to 29',
                    '30 and more'
                ],
                ordered=True
            )
        )

        # The end of the last interval is not set and will be computed
        # automatically. It will become the maximum given age (21) plus the
        # interval range (10); in result 31.
        # Because of that the last interval is from 30 to 39 plus a "40 and
        # more" infinity end
        df['AgeCat'] = bandas.cut_bins(
            values=df.age,
            infinity_is_less_than=0,
            interval_range=10,
        )

        assert_frame_equal(df, expect)

    def test_format_strings_via_cut(self):
        """Format strings to cut."""

        expect = pandas.DataFrame(
            {
                'vals': [3, 13, 23],
                'vcat': [
                    'weniger als 10 Eier',  # codespell-ignore
                    '10 bis 19 Eier',
                    'mehr als 20 Eier'  # codespell-ignore
                ]
            })
        expect.vcat = expect.vcat.astype(
            pandas.CategoricalDtype(
                categories=[
                    'weniger als 10 Eier',  # codespell-ignore
                    '10 bis 19 Eier',
                    'mehr als 20 Eier'  # codespell-ignore
                ],
                ordered=True
            )
        )

        df = expect.loc[:, ['vals']].copy()

        df['vcat'] = bandas.cut_bins(
            values=df.vals,
            interval_list=[(-math.inf, 9), (10, 19), (20, math.inf)],
            format_string='{} bis {} Eier',  # codespell-ignore
            begin_format_string='weniger als {} Eier',  # codespell-ignore
            end_format_string='mehr als {} Eier'  # codespell-ignore
        )

        assert_frame_equal(df, expect)

    def test_diabetes(self):
        """Interval ranges."""

        vals = [-1, 5, 5.69, 5.70, 5.701, 6.49, 6.50, 6.51, 8]

        df = pandas.DataFrame({'vals': vals})

        expect = df.copy()
        expect['Norm'] = [
            'less than 5.7',
            'less than 5.7',
            'less than 5.7',
            '5.7 to 6.49',
            '5.7 to 6.49',
            '5.7 to 6.49',
            '6.5 and more',
            '6.5 and more',
            '6.5 and more'
        ]
        expect.Norm = expect.Norm.astype(pandas.CategoricalDtype(
            categories=['less than 5.7', '5.7 to 6.49', '6.5 and more'],
            ordered=True))

        bins = [
            (float('-inf'), 5.69),
            (5.70, 6.49),
            (6.50, float('inf')),
        ]

        df['Norm'] = bandas.cut_bins(
            values=df.vals,
            interval_list=bins,
        )

        assert_frame_equal(df, expect)

    def test_diabetes_labeled(self):
        """Cut different ranges."""

        vals = [-1, 5, 5.69, 5.70, 5.701, 6.49, 6.50, 6.51, 8]

        df = pandas.DataFrame({'vals': vals})

        expect = df.copy()
        expect['Norm'] = [
            'under', 'under', 'under',
            'norm', 'norm', 'norm',
            'over', 'over', 'over'
        ]
        expect.Norm = expect.Norm.astype(pandas.CategoricalDtype(
            categories=['under', 'norm', 'over'],
            ordered=True))

        bins = [
            (float('-inf'), 5.69),
            (5.70, 6.49),
            (6.50, float('inf')),
        ]

        df['Norm'] = bandas.cut_bins(
            values=df.vals,
            interval_list=bins,
            labels=['under', 'norm', 'over']
        )

        assert_frame_equal(df, expect)

    def test_range_compute_end(self):
        """Use interval_range but without explicit end."""

        # list
        vals = list(range(20)) + [pandas.NA]

        result = bandas.cut_bins(vals, interval_range=10)

        # four categories plus missing
        self.assertEqual(5, result.value_counts(dropna=False).count())

    def test_to_much_labels(self):
        """More labels then bins."""

        with self.assertRaises(ValueError):
            bandas.cut_bins(
                values=[2, 7, 11],
                interval_list=[(float('-inf'), 4), (5, 8), (9, float('inf'))],
                labels=['A', 'B', 'C', 'to much']
            )

    def test_not_enough_labels(self):
        """More labels then bins."""

        with self.assertRaises(ValueError):
            bandas.cut_bins(
                values=[2, 7, 11],
                interval_list=[(float('-inf'), 4), (5, 8), (9, float('inf'))],
                labels=['A', 'B']
            )

    def test_droping_unused_labels(self):
        """Unused categories (labels) dropped when requested."""

        sut = bandas.cut_bins(
            values=[1, 12, 32],
            interval_range=10,
            remove_unused_labels=True
        )

        expect = [
            '0 to 9',
            '10 to 19',
            '30 to 39'
        ]

        # Resulting values
        self.assertEqual(sut.tolist(), expect)

        # Resulting categories
        self.assertEqual(sut.cat.categories.tolist(), expect)

    def test_droping_unused_labels_given_bins(self):
        """Unused categories (labels) dropped when requested."""

        sut = bandas.cut_bins(
            values=[1, 12, 32],
            interval_list=[(0, 9), (10, 19), (20, 29), (30, 39)],
            remove_unused_labels=True,
        )

        expect = [
            '0 to 9',
            '10 to 19',
            '30 to 39'
        ]

        # Resulting values
        self.assertEqual(sut.tolist(), expect)

        # Resulting categories
        self.assertEqual(sut.cat.categories.tolist(), expect)

    def test_values_uncovered_via_interval_list(self):
        """If vales not covered by bins."""

        with self.assertRaises(ValueError):
            bandas.cut_bins(
                # 7 is not covered
                values=[7, pandas.NA, 12, 23, 27],
                interval_list=[(10, 20), (25, 30)],
            )

    def test_values_uncovered_via_interval_count(self):
        """If vales not covered by bins."""

        with self.assertRaises(ValueError):
            bandas.cut_bins_via_interval_count(
                # NA is ignored
                # 7 is not covered
                values=[7, pandas.NA, 12, 23, 27],
                # (10, 19), (20, 29)
                interval_count=2,
                first_interval_begin=10,
                interval_range=10,
            )

    def test_na_values_can_be_uncovered(self):
        """No need to include Na values in the bins."""

        sut = bandas.cut_bins(
            values=[pandas.NA, 12, 26, 27],
            interval_list=[(10, 20), (25, 30)],
        )

        self.assertTrue(pandas.isna(sut.tolist()[0]))
        self.assertEqual(
            sut.tolist()[1:],
            ['10 to 20', '25 to 30', '25 to 30']
        )


class CutBinsLabels(unittest.TestCase):
    """Test about labels in cut_bins"""

    def test_edges(self):
        """List of bin edges.
        """
        result = bandas.generate_bin_labels(
            [-math.inf, -1, 9, 19, math.inf])

        self.assertEqual(
            result,
            ['less than 0', '0 to 9', '10 to 19', '20 and more']
        )

    def test_intervals(self):
        """List of bin intervals.
        """
        result = bandas.generate_bin_labels([
            (-math.inf, -1),
            (0, 9),
            (10, 19),
            (20, math.inf)
        ])

        self.assertEqual(
            result,
            ['less than 0', '0 to 9', '10 to 19', '20 and more']
        )

    def test_format(self):
        """Different format strings.
        """
        bins = [-math.inf, -1, 9, math.inf]

        self.assertEqual(
            bandas.generate_bin_labels(bins, format_string='x{}-{}y'),
            ['less than 0', 'x0-9y', '10 and more']
        )

        self.assertEqual(
            bandas.generate_bin_labels(bins,
                                       begin_format_string='under {} Kilo'),
            ['under 0 Kilo', '0 to 9', '10 and more']
        )

        self.assertEqual(
            bandas.generate_bin_labels(bins,
                                       end_format_string='and {}plus'),
            ['less than 0', '0 to 9', 'and 10plus']
        )

    def test_infinity(self):
        """Different infinity types."""

        test_data = [
            [-math.inf, -1, 9, math.inf],
            [-numpy.inf, -1, 9, numpy.inf],
            [float('-inf'), -1, 9, float('inf')]
        ]

        for bins in test_data:
            self.assertEqual(
                bandas.generate_bin_labels(bins),
                ['less than 0', '0 to 9', '10 and more']
            )

    def test_no_infinity(self):
        """Labels without infinity begin/end"""
        result = bandas.generate_bin_labels(
            [-1, 9, 19])

        self.assertEqual(
            result,
            ['0 to 9', '10 to 19']
        )

    def test_wrong_order(self):
        """Illogical order of bins.

        This won't cause an error. It is allowed. The user is responsible for
        correct order of the bins.
        """
        result = bandas.generate_bin_labels(
            [math.inf, 20, 5, -2, 7, -math.inf])

        self.assertEqual(
            result,
            ['inf to 20', '21 to 5', '6 to -2', '-1 to 7', '8 to -inf']
        )
