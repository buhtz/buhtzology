"""Testing DataContainer in bandas module"""
# pylint: disable=too-many-lines
import unittest
from unittest import mock
import inspect
import copy
import pathlib
import zipfile
from typing import Iterable
import pyfakefs.fake_filesystem_unittest as pyfakefs_ut
import pandas
from buhtzology import bandas
from . import helper


def _create_container(permanent, ondemand, meta=None, k=3):

    if meta is None:
        meta = {}

    dataframes = {}
    for df_name in permanent + ondemand:
        dataframes[df_name] = pandas.DataFrame({
            df_name: range(k),
            helper.rand_string(9, 4): range(k)
        })

    return bandas.DataContainer(dataframes, permanent, meta=meta)


class Generally(unittest.TestCase):
    """General container behavior"""

    # pylint: disable=protected-access,pointless-statement

    def test_delete_dataframe_by_attribute(self):
        """Delete a specific data frame via attribute."""

        sut = _create_container(list('XY'), list('AB'))

        # initial state
        self.assertCountEqual(sut.names, list('ABXY'))

        # delete permanent
        del sut.X

        # delete ondemand
        del sut.B

        self.assertCountEqual(sut.names, list('AY'))
        self.assertEqual(sut.names_permanent, ['Y'])
        self.assertEqual(sut._names_on_demand, ['A'])

    def test_delete_dataframe_by_key(self):
        """Delete a specific data frame via key."""
        sut = _create_container(list('XY'), list('AB'))

        # initial state
        self.assertCountEqual(sut.names, list('ABXY'))

        # delete permanent
        del sut['X']

        # delete ondemand
        del sut['B']

        self.assertCountEqual(sut.names, list('AY'))
        self.assertEqual(sut.names_permanent, ['Y'])
        self.assertEqual(sut._names_on_demand, ['A'])

    def test_get_attr(self):
        """__getattr__()"""
        sut = _create_container(list('A'), list('B'))

        # Access as instance attribute
        _ = sut.A
        _ = sut.B

        # invalid attribute name
        with self.assertRaises(AttributeError):
            _ = sut.foobar

    def test_get_item(self):
        """__getitem__()"""
        sut = _create_container(list('A'), list('B'))

        # Access via dict-like key
        sut['A']
        sut['B']

        # Get names via numerical index
        self.assertEqual(sut[0], 'A')
        self.assertEqual(sut[1], 'B')

        # iterate over the names
        for idx, name in enumerate(sut):
            # pylint: disable-next=unnecessary-list-index-lookup
            self.assertEqual(sut[idx], name)  # noqa: PLR1736
            self.assertEqual(sut.names[idx], name)

        # invalid index
        with self.assertRaises(IndexError):
            sut[2]

        # invalid key name
        with self.assertRaises(KeyError):
            sut['foobar']

    def test_set_attr(self):
        """__set_attr__"""
        sut = _create_container(list('A'), list('B'))

        self.assertCountEqual(sut.names, ['A', 'B'])

        sut.foobar = pandas.DataFrame()
        self.assertCountEqual(sut.names, ['A', 'B', 'foobar'])

        self.assertIn('foobar', sut._names_on_demand)

    def test_set_item(self):
        """__set_item__"""
        sut = _create_container(list('A'), list('B'))

        self.assertCountEqual(sut.names, list('AB'))

        sut['elke'] = pandas.DataFrame()

        self.assertCountEqual(sut.names, ['A', 'B', 'elke'])
        self.assertCountEqual(sut._names_on_demand, ['B', 'elke'])
        self.assertCountEqual(sut.names_permanent, ['A'])

    def test_len(self):
        """len"""
        sut = _create_container(list('A'), list('B'))

        self.assertEqual(len(sut), 2)

        for df_name in list('cde'):
            sut[df_name] = pandas.DataFrame()

        self.assertEqual(len(sut), 5)

    def test_iterate_keys(self):
        """Order of dataframe names (keys)"""
        sut = _create_container(list('XY'), list('AB'))

        result = []
        for key in sut:
            result.append(key)

        self.assertCountEqual(result, list('ABXY'))

    def test_permanent_existance_while_build(self):
        """Permanent declared dataframes should exist"""
        with self.assertRaises(ValueError):
            bandas.DataContainer(
                dataframes={
                    'Foo': pandas.DataFrame(),
                    'Bar': pandas.DataFrame()
                },
                permanent_names=['GibtsNicht']
            )

    def test_create_in_memory(self):
        """Create a container instance."""
        dataframes = {
            'Foo': pandas.DataFrame({'Foo': range(3)}),
            'Bar': pandas.DataFrame({'Bar': range(4)}),
            'Elke': pandas.DataFrame({'Elke': range(5)}),
            'Wurst': pandas.DataFrame({'Wurst': range(6)})
        }

        dc = bandas.DataContainer(
            dataframes=dataframes,
            permanent_names={'Elke', 'Foo'},
            meta={'ver': 78}
        )

        self.assertCountEqual(dc.names_permanent, ['Elke', 'Foo'])
        self.assertCountEqual(dc._names_on_demand, ['Bar', 'Wurst'])
        self.assertCountEqual(
            dc._dataframes.keys(), ['Bar', 'Elke', 'Foo', 'Wurst'])

    def test_empty_container(self):
        """Container is empty"""
        sut = bandas.DataContainer()
        self.assertFalse(sut.names)

    def test_describe_with_markdown(self):
        """Describe container with markdown"""
        dc = _create_container(list('XY'), list('AB'), k=50)
        dc.X.columns = ['X', 'A']
        dc.Y.columns = ['Y', 'B']
        dc.A.columns = ['A', 'X']
        dc.B.columns = ['B', 'Y']
        sut = dc.describe_with_markdown()

        expect = inspect.cleandoc("""==== X ====
            |    |   X |   A |
            |---:|----:|----:|
            |  0 |   0 |   0 |
            |  1 |   1 |   1 |
            |  2 |   2 |   2 |
            |  3 |   3 |   3 |

            ==== Y ====
            |    |   Y |   B |
            |---:|----:|----:|
            |  0 |   0 |   0 |
            |  1 |   1 |   1 |
            |  2 |   2 |   2 |
            |  3 |   3 |   3 |

            ==== A ====
            |    |   A |   X |
            |---:|----:|----:|
            |  0 |   0 |   0 |
            |  1 |   1 |   1 |
            |  2 |   2 |   2 |
            |  3 |   3 |   3 |

            ==== B ====
            |    |   B |   Y |
            |---:|----:|----:|
            |  0 |   0 |   0 |
            |  1 |   1 |   1 |
            |  2 |   2 |   2 |
            |  3 |   3 |   3 |
        """)
        self.maxDiff = None
        self.assertEqual(sut, expect)


class ModifiedState(unittest.TestCase):
    """Hashing of data frames to get their modified state"""
    # pylint: disable=protected-access,invalid-name

    def setUp(self):
        self.dc = bandas.DataContainer(
            {
                'Void': pandas.DataFrame(
                    {
                        'foo': list('ABCDE'),
                        'bar': range(5),
                    }
                )
            }
        )

        # make it unmodified
        self.dc._dataframe_hashes['Void'] = self.dc._hash_dataframe('Void')

        # be sure
        self.assertFalse(self.dc._is_dataframe_modified('Void'))

    def test_content(self):
        """Modify content"""
        self.dc.Void.loc[2, 'foo'] = 'X'

        self.assertTrue(self.dc._is_dataframe_modified('Void'))

    def test_row_label(self):
        """Modify row label"""
        idx = self.dc.Void.index.to_list()
        idx[2] = 77
        self.dc.Void.index = idx

        self.assertTrue(self.dc._is_dataframe_modified('Void'))

    def test_column_label(self):
        """Modify row label"""
        cols = self.dc.Void.columns.to_list()
        cols[0] = 'newname'
        self.dc.Void.columns = cols

        self.assertTrue(self.dc._is_dataframe_modified('Void'))

    def test_dtype(self):
        """Modify row label"""
        self.dc.Void.foo = self.dc.Void.foo.astype('category')

        self.assertTrue(self.dc._is_dataframe_modified('Void'))

    def test_dtype_modified_category(self):
        """Modify via remove unused category"""

        # Create categories
        self.dc.Void.foo = self.dc.Void.foo.astype('category')
        # Remove one value so one category is unused
        self.dc.Void = self.dc.Void.iloc[:-1]

        # make it unmodified
        self.dc._dataframe_hashes['Void'] = self.dc._hash_dataframe('Void')
        # be sure
        self.assertFalse(self.dc._is_dataframe_modified('Void'))

        # remove unused category
        self.dc.Void.foo = self.dc.Void.foo.cat.remove_unused_categories()
        self.assertTrue(self.dc._is_dataframe_modified('Void'))


class MetaData(unittest.TestCase):
    """Tests about container meta data"""
    # pylint: disable=protected-access,missing-function-docstring

    def test_empty_by_default(self):
        dc = bandas.DataContainer({'foo': pandas.DataFrame()})

        # empty
        self.assertEqual(dc.meta, {})

    def test_flat_update(self):
        dc = bandas.DataContainer(
            {'foo': pandas.DataFrame()},
            meta={'bar': 7, 'picard': 3}
        )

        self.assertEqual(len(dc.meta), 2)
        self.assertEqual(dc.meta['bar'], 7)
        self.assertEqual(dc.meta['picard'], 3)

    def test_nested_update(self):
        dc = bandas.DataContainer({'bar': pandas.DataFrame()})

        # empty in beginning
        self.assertEqual(len(dc.meta), 0)

        # init a nested dict
        init_dict = {
            'alma': 2,
            'bob': {
                'cat': 'kitty',
                'dog': 'schröder'
            },
            'last': 'end'
        }

        # deepcopy to be sure it is not manipulated!
        dc.update_meta_info(copy.deepcopy(init_dict))

        self.assertEqual(dc.meta, init_dict)

        # update dict
        update_dict = {
            'bob': {
                'dog': 'george',
                'fish': 'sharky'
            },
            'newkey': 'flat',
            'nested': {
                8: 'a',
                12: 'b'
            }
        }

        dc.update_meta_info(update_dict)

        # expected result
        expected_dict = {
            'alma': 2,
            'bob': {
                'cat': 'kitty',
                'dog': 'george',
                'fish': 'sharky'
            },
            'last': 'end',
            'newkey': 'flat',
            'nested': {
                8: 'a',
                12: 'b'
            }
        }

        self.assertEqual(dc.meta, expected_dict)

    def test_check_meta_info(self):
        """Check meta info"""

        sut = bandas.DataContainer(meta={'version': 2})

        sut._check_meta_info(meta_check={'version': 2})

        with self.assertRaises(ValueError):
            sut._check_meta_info(meta_check={'version': 1})

    def test_meta_creation_info(self):
        dc = _create_container(['Foo'], [])

        sut = dc._generate_creation_info()

        self.assertCountEqual(sut.keys(), ('isodatetime', 'call', 'script'))

    @pyfakefs_ut.patchfs
    def test_meta_creation(self, fake_fs):  # pylint: disable=W0613
        """Creation metadata generated while saving"""
        fp = pathlib.Path('foo-archive')
        dc = _create_container(['Elke', 'Foo'], ['Bar', 'Wurst'], {'ver': 3})
        dc.save(fp)

        dc = bandas.DataContainer.load(fp)

        self.assertCountEqual(dc.meta.keys(), ('ver', '_created'))
        self.assertCountEqual(
            dc.meta['_created'], ('script', 'call', 'isodatetime'))


class KeepRowCount(unittest.TestCase):
    """Tests about keep_row_count()"""

    def test_unmodified(self):
        """Data frame is not modified."""
        sut = _create_container(['foo', 'bar'], [])

        rows = len(sut.foo)

        # nothing will be raised
        with sut.keep_row_count('foo'):
            self.assertEqual(len(sut.foo), rows)

    def test_modify(self):
        """Add new row to data frame."""
        sut = _create_container(['picard', 'barkley'], [])

        with self.assertRaises(TypeError):
            with sut.keep_row_count('picard'):
                # add new row
                sut.picard.loc[len(sut.picard)] = 789

    def test_replace(self):
        """Replace the data frame with a new instance."""
        sut = bandas.DataContainer(
            dataframes={
                'foodf': pandas.DataFrame({'bar': range(10)})
            }
        )

        with self.assertRaises(TypeError):
            with sut.keep_row_count('foodf'):
                # Replace with a new data frame
                sut.foodf = pandas.DataFrame({'crusher': list('ABC')})


# pylint: disable-next=too-many-public-methods
class FolderModeFS(pyfakefs_ut.TestCase):
    """DataContainer folder mode tests using a fake filesystem

    All possible cases and combinations should be covered. Data frames are
    different by the following attributes:
    - unloaded / loaded (in-memory)
    - unmodified (unmod) / modified (mod)
    - ondemand (ondem) / permanent (perm)
    - compressed or not (zip)

    The substring ``zip`` in the test methods names covers the case that the
    compressed stated of the data frame to test will change different from the
    default behavior. For example a permanent name is uncompressed by default
    and will be compressed by default when stored as ondemand. In this example
    the stored ondemand dataframe will explicitly marked as uncompressed.
    """

    # pylint: disable=protected-access,pointless-statement,
    @classmethod
    def setUpClass(cls):
        # Determine the loggers name (should be buhtzology._datacontainer)
        # independent from implementation.
        logger = pathlib.Path(inspect.getfile(bandas.DataContainer))
        cls.target_logger_name = logger.parts[-2] + '.' + logger.stem

    def setUp(self):
        self.setUpPyfakefs()

    def _generate_container_files(self, names: list[str]) -> pathlib.Path:
        """Folder is random named and returned as a path object. Names starting
        with ``_`` are permanent and without ondemand data frames.
        """
        cpath = pathlib.Path(helper.rand_string(min_length=5, max_length=9))
        cpath.mkdir()

        for fn in names:
            pandas.DataFrame().to_pickle(cpath / fn)

        return cpath

    def assert_files_count_equal(self,
                                 file_names: list[str],
                                 folder: pathlib.Path):
        """`folder` and `file_names` have the same elements in the same number,
        regardless of their order."""
        existing_file_names \
            = sorted(fp.relative_to(folder).name for fp in folder.glob('*'))

        # Output of this assert is IMHO hard to interpret
        # self.assertCountEqual(
        #     file_names, existing_file_names, f'Folder: {folder}')

        self.assertEqual(
            sorted(file_names), existing_file_names, f'Folder: {folder}')

    def test_save_empty_container(self):
        """Container without any data frame"""
        fp = pathlib.Path('emptydcfolder')
        sut = bandas.DataContainer()
        sut.save(fp)

        self.assert_files_count_equal(['__meta'], fp)

    def test_unmod_unloaded_ondem_to_perm(self):
        """Unmodified, unloaded, O->P"""
        src_fp = self._generate_container_files(['ondemand.pickle.zip'])
        sut = bandas.DataContainer.load(src_fp)

        dest_fp = src_fp.with_suffix('.' + helper.rand_string(6, 3))
        sut.save(dest_fp, permanent_names=['ondemand'])

        self.assert_files_count_equal(['__meta', '_ondemand.pickle'], dest_fp)

    def test_unmod_unloaded_ondem_to_perm_zip(self):
        """Unmodified, unloaded, O->P but none-default zip-behavior"""
        src_fp = self._generate_container_files(['ondemand.pickle.zip'])
        sut = bandas.DataContainer.load(src_fp)

        dest_fp = src_fp.with_suffix('.' + helper.rand_string(6, 3))
        sut.save(dest_fp, permanent_names=['ondemand'], zip_names=['ondemand'])

        self.assert_files_count_equal(
            ['__meta', '_ondemand.pickle.zip'], dest_fp)

    def test_unmod_unloaded_ondem_to_ondem(self):
        """Unmodified, unloaded, O->O"""
        src_fp = self._generate_container_files(['ondemand.pickle.zip'])
        sut = bandas.DataContainer.load(src_fp)

        dest_fp = src_fp.with_suffix('.' + helper.rand_string(6, 3))
        sut.save(dest_fp)

        self.assert_files_count_equal(
            ['__meta', 'ondemand.pickle.zip'], dest_fp)

    def test_unmod_unloaded_ondem_to_ondem_zip(self):
        """Unmodified, unloaded, O->O but none-default zip-behavior"""
        src_fp = self._generate_container_files(['ondemand.pickle.zip'])
        sut = bandas.DataContainer.load(src_fp)

        dest_fp = src_fp.with_suffix('.' + helper.rand_string(6, 3))
        sut.save(dest_fp, zip_names=[])

        self.assert_files_count_equal(['__meta', 'ondemand.pickle'], dest_fp)

    def test_unmod_loaded_perm_to_perm(self):
        """Unmodified, loaded, P->P"""
        src_fp = self._generate_container_files(['_perm.pickle'])
        sut = bandas.DataContainer.load(src_fp)

        dest_fp = src_fp.with_suffix('.' + helper.rand_string(6, 3))
        sut.save(dest_fp)

        self.assert_files_count_equal(['__meta', '_perm.pickle'], dest_fp)

    def test_unmod_loaded_perm_to_perm_zip(self):
        """Unmodified, loaded, P->P but none-default zip-behavior"""
        src_fp = self._generate_container_files(['_perm.pickle'])
        sut = bandas.DataContainer.load(src_fp)

        dest_fp = src_fp.with_suffix('.' + helper.rand_string(6, 3))
        sut.save(dest_fp, zip_names=['perm'])

        self.assert_files_count_equal(['__meta', '_perm.pickle.zip'], dest_fp)

    def test_unmod_loaded_perm_to_ondem(self):
        """Unmodified, loaded, P->O"""
        src_fp = self._generate_container_files(['_perm.pickle'])
        sut = bandas.DataContainer.load(src_fp)

        dest_fp = src_fp.with_suffix('.' + helper.rand_string(6, 3))
        sut.save(dest_fp, permanent_names=[])

        self.assert_files_count_equal(['__meta', 'perm.pickle.zip'], dest_fp)

    def test_unmod_loaded_perm_to_ondem_zip(self):
        """Unmodified, loaded, P->O but none-default zip-behavior"""
        src_fp = self._generate_container_files(['_perm.pickle'])
        sut = bandas.DataContainer.load(src_fp)

        dest_fp = src_fp.with_suffix('.' + helper.rand_string(6, 3))
        sut.save(dest_fp, permanent_names=[], zip_names=[])

        self.assert_files_count_equal(['__meta', 'perm.pickle'], dest_fp)

    def test_unmod_loaded_ondem_to_ondem(self):
        """Unmodified, loaded, O->O"""
        src_fp = self._generate_container_files(['ondem.pickle.zip'])
        sut = bandas.DataContainer.load(src_fp)

        _ = sut.ondem

        dest_fp = src_fp.with_suffix('.' + helper.rand_string(6, 3))
        sut.save(dest_fp)

        self.assert_files_count_equal(['__meta', 'ondem.pickle.zip'], dest_fp)

    def test_unmod_loaded_ondem_to_ondem_zip(self):
        """Unmodified, loaded, O->O but none-default zip-behavior"""
        src_fp = self._generate_container_files(['ondem.pickle.zip'])
        sut = bandas.DataContainer.load(src_fp)

        _ = sut.ondem

        dest_fp = src_fp.with_suffix('.' + helper.rand_string(6, 3))
        sut.save(dest_fp, zip_names=[])

        self.assert_files_count_equal(['__meta', 'ondem.pickle'], dest_fp)

    def test_unmod_loaded_ondem_to_perm(self):
        """Unmodified, loaded, O->P"""
        src_fp = self._generate_container_files(['ondem.pickle.zip'])
        sut = bandas.DataContainer.load(src_fp)

        _ = sut.ondem

        dest_fp = src_fp.with_suffix('.' + helper.rand_string(6, 3))
        sut.save(dest_fp, permanent_names=['ondem'])

        self.assert_files_count_equal(['__meta', '_ondem.pickle'], dest_fp)

    def test_unmod_loaded_ondem_to_perm_zip(self):
        """Unmodified, loaded, O->P but none-default zip-behavior"""
        src_fp = self._generate_container_files(['ondem.pickle.zip'])
        sut = bandas.DataContainer.load(src_fp)

        _ = sut.ondem

        dest_fp = src_fp.with_suffix('.' + helper.rand_string(6, 3))
        sut.save(dest_fp, permanent_names=['ondem'], zip_names=['ondem'])

        self.assert_files_count_equal(['__meta', '_ondem.pickle.zip'], dest_fp)

    def test_mod_perm_to_perm(self):
        """Modified (loaded), P->P"""
        src_fp = self._generate_container_files(['_perm.pickle'])
        sut = bandas.DataContainer.load(src_fp)

        # modify
        sut.perm['X'] = 7

        # save (permanent by default)
        dest_fp = src_fp.with_suffix('.' + helper.rand_string(6, 3))
        sut.save(dest_fp)
        self.assert_files_count_equal(['__meta', '_perm.pickle'], dest_fp)

        # save (permanent explicit)
        dest_fp = src_fp.with_suffix('.' + helper.rand_string(6, 3))
        sut.save(dest_fp, permanent_names=['perm'])
        self.assert_files_count_equal(['__meta', '_perm.pickle'], dest_fp)

    def test_mod_perm_to_perm_zip(self):
        """Modified (loaded) P->P but none-default zip-behavior"""
        src_fp = self._generate_container_files(['_perm.pickle'])
        sut = bandas.DataContainer.load(src_fp)

        # modify
        sut.perm['X'] = 7

        # save (permanent by default)
        dest_fp = src_fp.with_suffix('.' + helper.rand_string(6, 3))
        sut.save(dest_fp, zip_names=['perm'])
        self.assert_files_count_equal(['__meta', '_perm.pickle.zip'], dest_fp)

        # save (permanent explicit)
        dest_fp = src_fp.with_suffix('.' + helper.rand_string(6, 3))
        sut.save(dest_fp, permanent_names=['perm'], zip_names=['perm'])
        self.assert_files_count_equal(['__meta', '_perm.pickle.zip'], dest_fp)

    def test_mod_perm_to_ondem(self):
        """Modified (loaded), P->O"""
        src_fp = self._generate_container_files(['_perm.pickle'])
        sut = bandas.DataContainer.load(src_fp)

        # modify
        sut.perm['X'] = 7

        # save
        dest_fp = src_fp.with_suffix('.' + helper.rand_string(6, 3))
        sut.save(dest_fp, permanent_names=[])

        self.assert_files_count_equal(['__meta', 'perm.pickle.zip'], dest_fp)

    def test_mod_perm_to_ondem_zip(self):
        """Modified (loaded) P->O but none-default zip-behavior"""
        src_fp = self._generate_container_files(['_perm.pickle'])
        sut = bandas.DataContainer.load(src_fp)

        # modify
        sut.perm['X'] = 7

        # save
        dest_fp = src_fp.with_suffix('.' + helper.rand_string(6, 3))
        sut.save(dest_fp, permanent_names=[], zip_names=[])

        self.assert_files_count_equal(['__meta', 'perm.pickle'], dest_fp)

    def test_mod_ondem_to_ondem(self):
        """Modified (loaded), O->O"""
        src_fp = self._generate_container_files(['ondem.pickle.zip'])
        sut = bandas.DataContainer.load(src_fp)

        # modify
        sut.ondem['X'] = 7

        # save
        dest_fp = src_fp.with_suffix('.' + helper.rand_string(6, 3))
        sut.save(dest_fp)

        self.assert_files_count_equal(['__meta', 'ondem.pickle.zip'], dest_fp)

    def test_mod_ondem_to_ondem_zip(self):
        """Modified (loaded) P->O but none-default zip-behavior"""
        src_fp = self._generate_container_files(['ondem.pickle.zip'])
        sut = bandas.DataContainer.load(src_fp)

        # modify
        sut.ondem['X'] = 7

        # save
        dest_fp = src_fp.with_suffix('.' + helper.rand_string(6, 3))
        sut.save(dest_fp, zip_names=[])

        self.assert_files_count_equal(['__meta', 'ondem.pickle'], dest_fp)

    def test_mod_ondem_to_perm(self):
        """Modified (loaded), O->P"""
        src_fp = self._generate_container_files(['ondem.pickle.zip'])
        sut = bandas.DataContainer.load(src_fp)

        # modify
        sut.ondem['X'] = 7

        # save
        dest_fp = src_fp.with_suffix('.' + helper.rand_string(6, 3))
        sut.save(dest_fp, permanent_names=['ondem'])

        self.assert_files_count_equal(['__meta', '_ondem.pickle'], dest_fp)

    def test_mod_ondem_to_perm_zip(self):
        """Modified (loaded), O->P but none-default zip-behavior"""
        src_fp = self._generate_container_files(['ondem.pickle.zip'])
        sut = bandas.DataContainer.load(src_fp)

        # modify
        sut.ondem['X'] = 7

        # save
        dest_fp = src_fp.with_suffix('.' + helper.rand_string(6, 3))
        sut.save(dest_fp, permanent_names=['ondem'], zip_names=['ondem'])

        self.assert_files_count_equal(['__meta', '_ondem.pickle.zip'], dest_fp)

    def test_all_together(self):
        """All possible cases in one data frame"""

        src_fp = self._generate_container_files([
            'unmod_unload_OP.pickle.zip',
            'unmod_unload_OP_zip.pickle.zip',
            'unmod_unload_OO.pickle.zip',
            'unmod_unload_OO_zip.pickle.zip',
            '_unmod_load_PP.pickle',
            '_unmod_load_PP_zip.pickle',
            '_unmod_load_PO.pickle',
            '_unmod_load_PO_zip.pickle',
            'unmod_load_OO.pickle.zip',
            'unmod_load_OO_zip.pickle.zip',
            'unmod_load_OP.pickle.zip',
            'unmod_load_OP_zip.pickle.zip',
            '_mod_PP.pickle',
            '_mod_PP_zip.pickle',
            '_mod_PO.pickle',
            '_mod_PO_zip.pickle',
            'mod_OO.pickle.zip',
            'mod_OO_zip.pickle.zip',
            'mod_OP.pickle.zip',
            'mod_OP_zip.pickle.zip',
        ])
        sut = bandas.DataContainer.load(src_fp)

        # modify some
        for name in sut.names:
            if 'load' in name and 'unload' not in name:
                _ = sut[name]

            if 'mod' in name and 'unmod' not in name:
                sut[name]['X'] = 7

        # save
        dest_fp = src_fp.with_suffix('.' + helper.rand_string(6, 3))
        sut.save(
            dest_fp,
            permanent_names=[
                'unmod_unload_OP',
                'unmod_unload_OP_zip',
                'unmod_load_PP',
                'unmod_load_PP_zip',
                'unmod_load_OP',
                'unmod_load_OP_zip',
                'mod_PP',
                'mod_PP_zip',
                'mod_OP',
                'mod_OP_zip',
            ],
            zip_names=[
                'unmod_unload_OP_zip',
                'unmod_unload_OO',
                'unmod_load_PP_zip',
                'unmod_load_PO',
                'unmod_load_OO',
                'unmod_load_OP_zip',
                'mod_PP_zip',
                'mod_PO',
                'mod_OO',
                'mod_OP_zip',
            ]
        )

        expected_files = [
            '__meta',
            '_unmod_unload_OP.pickle',
            '_unmod_unload_OP_zip.pickle.zip',
            'unmod_unload_OO.pickle.zip',
            'unmod_unload_OO_zip.pickle',
            '_unmod_load_PP.pickle',
            '_unmod_load_PP_zip.pickle.zip',
            'unmod_load_PO.pickle.zip',
            'unmod_load_PO_zip.pickle',
            'unmod_load_OO.pickle.zip',
            'unmod_load_OO_zip.pickle',
            '_unmod_load_OP.pickle',
            '_unmod_load_OP_zip.pickle.zip',
            '_mod_PP.pickle',
            '_mod_PP_zip.pickle.zip',
            'mod_PO.pickle.zip',
            'mod_PO_zip.pickle',
            'mod_OO.pickle.zip',
            'mod_OO_zip.pickle',
            '_mod_OP.pickle',
            '_mod_OP_zip.pickle.zip',
        ]
        self.assert_files_count_equal(expected_files, dest_fp)


# pylint: disable-next=too-many-public-methods
class ContainerFS(pyfakefs_ut.TestCase):
    """DataContainer tests using a fake filesystem"""

    # pylint: disable=protected-access,pointless-statement
    @classmethod
    def setUpClass(cls):
        # Determine the loggers name (should be buhtzology._datacontainer)
        # independent from implementation.
        logger = pathlib.Path(inspect.getfile(bandas.DataContainer))
        cls.target_logger_name = logger.parts[-2] + '.' + logger.stem

    def setUp(self):
        self.setUpPyfakefs()

    def test_load_names(self):
        """Correct names while loading"""

        # construct a datacontainter folder manually
        cpath = pathlib.Path('folder_container')
        cpath.mkdir()
        # Permanent
        pandas.DataFrame().to_pickle(cpath / '_foo.pickle')
        # Ondemand
        pandas.DataFrame().to_pickle(cpath / 'bar.pickle')
        # zipped Permanent
        pandas.DataFrame().to_pickle(cpath / '_worf.pickle.zip')
        # zipped Ondemand
        pandas.DataFrame().to_pickle(cpath / 'sisko.pickle.zip')

        with self.assertLogs(self.target_logger_name, 'INFO') as logs:
            sut = bandas.DataContainer.load(cpath)

            output = '\n'.join(logs.output)

            for content in ['n=4', 'foo', 'bar', 'worf', 'sisko']:
                self.assertTrue(content in output)

        self.assertCountEqual(sut.names, ['foo', 'bar', 'worf', 'sisko'])

    def test_save_names_with_unmodified(self):
        """Correct names while saving a container with an unmodified df"""

        # Create and save container
        dc = _create_container(['kira', 'jake'], ['morn', 'nog'])
        fp = pathlib.Path('universe') / 'terocnor'
        dc.save(fp, zip_names=['jake', 'nog'])

        # Load container and modify one dataframe
        dc = bandas.DataContainer.load(fp)
        dc.kira['B'] = pandas.DataFrame({'col': [1, 2, 3]})

        with self.assertLogs(self.target_logger_name, 'INFO') as logs:

            fp = fp.parent / 'newfolder'
            dc.save(fp, zip_names=['jake', 'nog'])

            output = '\n'.join(logs.output)

            self._assert_indicator_output(output, 'P_', 'kira')
            self._assert_indicator_output(output, 'Pz', 'jake')
            self._assert_indicator_output(output, 'O_', 'morn')
            self._assert_indicator_output(output, 'Oz', 'nog')

        self.assertCountEqual(dc.names, ['kira', 'jake', 'morn', 'nog'])

    def test_save_names(self):
        """Correct names while saving"""

        dc = _create_container(['kira', 'jake'], ['morn', 'nog'])

        # construct a datacontainter folder manually

        with self.assertLogs(self.target_logger_name, 'INFO') as logs:
            dc.save(pathlib.Path('fdc') / 'ds9', zip_names=['jake', 'nog'])

            output = '\n'.join(logs.output)

            for content in ['n=4', 'kira', 'jake', 'morn', 'nog']:
                self.assertTrue(content in output)

        self.assertCountEqual(dc.names, ['kira', 'jake', 'morn', 'nog'])

    def test_save_archive_mode(self):
        """Save fresh container in archive mode."""
        fp = pathlib.Path('foo-archive.zip')
        dc = _create_container(['Elke', 'Foo'], ['Bar', 'Wurst'], {'ver': 78})
        dc.save(fp)

        self.assertTrue(fp.exists())
        self.assertCountEqual(
            zipfile.ZipFile(fp).namelist(),  # pylint: disable=R1732
            (
                bandas.DataContainer._META_FILENAME,
                '_Elke.pickle', '_Foo.pickle',
                'Bar.pickle', 'Wurst.pickle'
            )
        )

    def test_save_folder_mode(self):
        """Save fresh container in folder mode."""
        fp = pathlib.Path('foo-folder')
        dc = _create_container(['Elke', 'Foo'], ['Bar', 'Wurst'], {'ver': 78})
        dc.save(fp)

        expected_files = [
            fp,
            fp / bandas.DataContainer._META_FILENAME,
            fp / '_Elke.pickle',
            fp / '_Foo.pickle',
            fp / 'Bar.pickle.zip',
            fp / 'Wurst.pickle.zip'
        ]
        for ef in expected_files:
            self.assertTrue(ef.exists())

    def test_load_archive_mode(self):
        """Load container in archive mode."""
        fp = pathlib.Path('archive.zip')
        dc = _create_container(
            ['Anna', 'Gabi'], ['Peter', 'George'], {'ver': 83})
        dc.save(fp)

        sut = bandas.DataContainer.load(fp)

        self.assertCountEqual(sut.names, ['Anna', 'Peter', 'George', 'Gabi'])
        self.assertCountEqual(sut._dataframes.keys(), ['Anna', 'Gabi'])
        self.assertEqual(sut.meta['ver'], 83)

    def test_load_folder_mode(self):
        """Load container in folder mode."""
        fp = pathlib.Path('bar-folder')
        dc = _create_container(
            ['Brandon', 'Helene'], ['Michael', 'Tim'], {'ver': 12})
        dc.save(fp)

        sut = bandas.DataContainer.load(fp)

        self.assertCountEqual(
            sut.names, ['Michael', 'Brandon', 'Tim', 'Helene'])
        self.assertCountEqual(
            sut._dataframes.keys(), ['Brandon', 'Helene'])
        self.assertEqual(sut.meta['ver'], 12)

    def test_load_all_dataframes(self):
        """Load all dataframes."""
        fp = pathlib.Path('loadall-folder')
        dc = _create_container(['Y', 'X'], ['R', 'L'])
        dc.save(fp)

        sut = bandas.DataContainer.load(fp)

        self.assertCountEqual(sut.names, list('RLYX'))
        self.assertCountEqual(sut._dataframes.keys(), list('YX'))

        sut.load_all()

        self.assertCountEqual(sut._dataframes.keys(), list('RLXY'))

    def test_load_dataframes_on_demand(self):
        """Load ondemand data frames."""
        fp = pathlib.Path('test_load_dataframes.zip')
        dc = _create_container(['one', 'two'], ['three', 'four'])
        dc.save(fp)

        sut = bandas.DataContainer.load(fp)

        self.assertCountEqual(sut.names, ['one', 'two', 'three', 'four'])

        # only "permanent" is loaded
        self.assertCountEqual(sut._dataframes.keys(), ['one', 'two'])

        # load on-demand
        _ = sut.four
        self.assertCountEqual(sut._dataframes.keys(), ['one', 'two', 'four'])

        # next on-demand
        _ = sut['three']
        self.assertCountEqual(
            sut._dataframes.keys(), ['one', 'two', 'four', 'three'])

    def test_save_with_modified_permanent_archive(self):
        """Store container but modify list of permanent dataframes."""
        fp = pathlib.Path('modified_permanent.zip')
        dc = _create_container(list('ABC'), list('XYZ'))
        dc.save(fp)

        sut = bandas.DataContainer.load(fp)

        # pre-check permanent names
        self.assertCountEqual(sut._dataframes.keys(), list('ABC'))

        fp = pathlib.Path('newdc.zip')
        sut.save(fp, permanent_names=list('AXZ'))

        # re-read the data container
        sut = bandas.DataContainer.load(fp)
        self.assertCountEqual(sut.names, list('ABCXYZ'))
        self.assertCountEqual(sut._dataframes.keys(), list('AXZ'))

    def test_save_with_modified_permanent_folder(self):
        """Store container but modify list of permanent dataframes."""
        fp = pathlib.Path('modified_permanent_folder')
        dc = _create_container(list('ABC'), list('XYZ'))
        dc.save(fp)

        sut = bandas.DataContainer.load(fp)

        # pre-check permanent names
        self.assertCountEqual(sut._dataframes.keys(), list('ABC'))

        sut.A.A = 7
        sut.X.X = 8
        fp = pathlib.Path('newdc_folder')
        sut.save(fp, permanent_names=list('AXZ'))

        # re-read the data container
        sut = bandas.DataContainer.load(fp)
        self.assertCountEqual(sut.names, list('ABCXYZ'))
        self.assertCountEqual(sut._dataframes.keys(), list('AXZ'))

    def test_store_again_remove_previous_container(self):
        """Store again the previous container is removed."""

        # pylint: disable=consider-using-with

        fp = pathlib.Path('store_again.zip')
        dc = _create_container(['Foo'], ['Bar'], {'x': 'y'})
        dc.save(fp)

        # pre-check
        self.assertCountEqual(
            zipfile.ZipFile(fp).namelist(),
            ['Bar.pickle', '_Foo.pickle', '__meta']
        )

        # create a different container in the same place
        sut = _create_container([], ['Black'])
        sut.save(fp)

        self.assertCountEqual(
            zipfile.ZipFile(fp).namelist(), ['Black.pickle', '__meta']
        )

    def test_build_container_default_zips(self):
        """Create a data container."""

        fp = pathlib.Path.cwd() / 'Beverly'
        sut = _create_container(['Elke', 'Foo'], ['Bar', 'Wurst'], {'ver': 62})
        sut.save(fp)

        self.assertTrue(fp.exists())

        expect = [
            fp / bandas.DataContainer._META_FILENAME,
            fp / '_Elke.pickle',
            fp / '_Foo.pickle',
            fp / 'Bar.pickle.zip',
            fp / 'Wurst.pickle.zip',
        ]
        for expected_path in expect:
            self.assertTrue(expected_path.exists())

    def test_build_container_explicite_zips(self):
        """Create a data container with explicit zip names."""

        fp = pathlib.Path.cwd() / 'Cardassia'
        sut = _create_container(
            ['Rigel', 'Riza'], ['Veridian', 'Vulkan'], {'ver': 12})
        sut.save(fp, zip_names=['Riza', 'Vulkan'])

        self.assertTrue(fp.exists())

        expect = [
            fp / bandas.DataContainer._META_FILENAME,
            fp / '_Riza.pickle.zip',
            fp / '_Rigel.pickle',
            fp / 'Vulkan.pickle.zip',
            fp / 'Veridian.pickle',
        ]
        for expected_path in expect:
            self.assertTrue(expected_path.exists(), expected_path)

    def test_do_not_zip(self):
        """Don't zip anything."""

        # Create and save a container in folder mode
        dataframes = {key: pandas.DataFrame() for key in list('ABXY')}
        fp = pathlib.Path.cwd() / 'Worf'
        dc = bandas.DataContainer(
            dataframes=dataframes,
            permanent_names=list('AB'),
            meta={'Wesley': 12}
        )
        dc.save(fp, zip_names=[])

        # All files are pickles and not ZIP
        self.assertEqual(len(list(fp.glob('*'))), 5)
        for fn in ['_A.pickle', '_B.pickle', 'X.pickle', 'Y.pickle',
                   bandas.DataContainer._META_FILENAME]:
            self.assertTrue((fp / fn).exists())

        sut = bandas.DataContainer.load(fp)

        self.assertCountEqual(sut.names, list('ABXY'))
        self.assertEqual(sut.meta['Wesley'], 12)

        # permanent loaded
        self.assertCountEqual(sut._dataframes.keys(), list('AB'))

        # auto-load that dataframes via accessing them
        _ = sut.X
        _ = sut.Y

        self.assertCountEqual(sut._dataframes.keys(), list('ABXY'))

    def test_save_unhashable(self):
        """Data frame with list() in its cell are not hashable."""
        fp = pathlib.Path('unhashble-folder')

        dc = bandas.DataContainer(
            {
                'Foo': pandas.DataFrame({
                    'ids': range(3),
                    'col': [
                        [1, 2],
                        [3, 4],
                        [5, 6]
                    ]})
            })
        dc.save(fp)

    def test_warn_about_zipnames_in_archive_mode(self):
        """In ARCHIVE mode warn about use of zip_names"""
        fp = pathlib.Path('archive.zip')
        dc = _create_container(['A'], ['B'])

        with self.assertLogs(logger=self.target_logger_name,
                             level='WARNING') as logs:
            dc.save(fp, zip_names=['B'])

            self.assertEqual(len(logs.output), 1)
            self.assertRegex(
                logs.output[0], r'.*argument.*zip.*ignore.*ARCHIVE.*')

    def _assert_indicator_output(self, output, indicator, dfname):
        """Test existence of specific dataframe indicators.

        Example ::

               ARCHIVE : folder.zip
             FULL PATH : folder.zip
            DATAFRAMES : n=2
                        [P]  Elke
                        [O]  Bar
                 META : { ...}

        """
        pattern = r'.*\[{}\]\s+{}.*'
        self.assertRegex(output, pattern.format(indicator, dfname))

    def test_archive_mode_indicators(self):
        """DataFrame indicators in archive mode: save & load"""
        fp = pathlib.Path('ouch.zip')
        dc = _create_container(['Elke'], ['Bar'])

        # Save
        with self.assertLogs(logger=self.target_logger_name,
                             level='INFO') as logs:
            dc.save(fp)
            output = '\n'.join(logs.output)

            self._assert_indicator_output(output, 'P_', 'Elke')
            self._assert_indicator_output(output, 'O_', 'Bar')

        # Load
        with self.assertLogs(logger=self.target_logger_name,
                             level='INFO') as logs:
            dc = bandas.DataContainer.load(fp)
            output = '\n'.join(logs.output)

            self._assert_indicator_output(output, 'P_', 'Elke')
            self._assert_indicator_output(output, 'O_', 'Bar')

    def test_folder_mode_indicators(self):
        """DataFrame indicators in folder mode: save & load"""
        fp = pathlib.Path('folder')
        dc = _create_container(['Worf', 'Alexander'], ['Barkley', 'Nelix'])

        # Save
        with self.assertLogs(logger=self.target_logger_name,
                             level='INFO') as logs:
            dc.save(fp, zip_names=['Worf', 'Nelix'])

            output = '\n'.join(logs.output)

            self._assert_indicator_output(output, 'Pz', 'Worf')
            self._assert_indicator_output(output, 'P_', 'Alexander')
            self._assert_indicator_output(output, 'O_', 'Barkley')
            self._assert_indicator_output(output, 'Oz', 'Nelix')

        # Load
        with self.assertLogs(logger=self.target_logger_name,
                             level='INFO') as logs:
            dc = bandas.DataContainer.load(fp)

            output = '\n'.join(logs.output)

            self._assert_indicator_output(output, 'Pz', 'Worf')
            self._assert_indicator_output(output, 'P_', 'Alexander')
            self._assert_indicator_output(output, 'O_', 'Barkley')
            self._assert_indicator_output(output, 'Oz', 'Nelix')


@mock.patch('shutil.copy')
class CopyUnmodifiedDataframeFilesInFolderModeFS(pyfakefs_ut.TestCase):
    """Storing a container in folder mode cause copy unmodified dataframes
    instead of open and (zip)pickeling them.

    To store a container all dataframes need to get loaded into the memory
    and then pickeld and zipped again. No need for this when the dataframe
    wasn't in memory because it is ondemand and unloaded. And also for still
    loaded dataframes when they are not modified. Don't waste time on
    pickle and compress them. Just copy their underlying dataframe file from
    the old to the new container folder.
    """

    def setUp(self):
        self.setUpPyfakefs()

        self.old_container_path = pathlib.Path.cwd() / 'Patrick'
        dc = _create_container(
            permanent=['Bachata', 'Tango'],
            ondemand=['Zappeln', 'Foxtrot']
        )
        dc.save(self.old_container_path)

    def _prepare_list_of_copy_calls(self,
                                    old_folder: pathlib.Path,
                                    new_folder: pathlib.Path,
                                    df_names: Iterable[str]
                                    ):
        """
        Returns:
            list[mock.call]
        """

        # pylint: disable=protected-access

        expected_calls = []
        for fn in df_names:
            suffix = bandas.DataContainer._PICKLE_SUFFIX \
                if fn[0] == '_' else bandas.DataContainer._PICKLE_ZIP_SUFFIX

            c = mock.call(
                old_folder / f'{fn}{suffix}',
                new_folder / f'{fn}{suffix}'
            )

            expected_calls.append(c)

        return expected_calls

    def test_copy_all_because_no_modifications(self, mock_shutil_copy):
        """All dataframes are copied because no modification was done"""

        sut = bandas.DataContainer.load(self.old_container_path)

        # pre-check
        self.assertCountEqual(
            sut.names, ['Bachata', 'Foxtrot', 'Tango', 'Zappeln'])

        # Load one on-demand data frame
        _ = sut.Foxtrot  # pylint: disable=pointless-statement

        sut.save(pathlib.Path.cwd() / 'Kirk')

        expected_calls = self._prepare_list_of_copy_calls(
            pathlib.Path.cwd() / 'Patrick',
            pathlib.Path.cwd() / 'Kirk',
            ['_Tango', '_Bachata', 'Zappeln', 'Foxtrot'],
        )

        self.assertEqual(len(expected_calls), mock_shutil_copy.call_count)
        mock_shutil_copy.assert_has_calls(expected_calls, any_order=True)

        self.assertCountEqual(
            sut.names, ['Bachata', 'Foxtrot', 'Tango', 'Zappeln'])
        self.assertCountEqual(
            sut.names_permanent, ['Bachata', 'Tango'])

    def test_permanent_modified(self, mock_shutil_copy):
        """Modified permanent dataframe not copied"""

        sut = bandas.DataContainer.load(self.old_container_path)

        sut.Tango.iloc[0, 0] = 7
        sut.save(pathlib.Path.cwd() / 'Kirk')

        expected_calls = self._prepare_list_of_copy_calls(
            pathlib.Path.cwd() / 'Patrick',
            pathlib.Path.cwd() / 'Kirk',
            ['_Bachata', 'Zappeln', 'Foxtrot'],
        )

        # "Tango" wasn't copied
        self.assertEqual(len(expected_calls), mock_shutil_copy.call_count)
        mock_shutil_copy.assert_has_calls(expected_calls, any_order=True)

        # But created
        self.assertTrue(
            (pathlib.Path.cwd() / 'Kirk' / '_Tango.pickle').exists())

    def test_column_modified(self, mock_shutil_copy):
        """Modified only the column names."""

        # load container file
        sut = bandas.DataContainer.load(self.old_container_path)

        # modify column name of one dataframe
        sut.Tango = sut.Tango.rename(columns={'Tango': 'Foo'})

        # save container to another file
        sut.save(pathlib.Path.cwd() / 'Kirk')

        expected_calls = self._prepare_list_of_copy_calls(
            pathlib.Path.cwd() / 'Patrick',
            pathlib.Path.cwd() / 'Kirk',
            ['_Bachata', 'Zappeln', 'Foxtrot'],
        )

        # "Tango" wasn't copied
        self.assertEqual(len(expected_calls), mock_shutil_copy.call_count)
        mock_shutil_copy.assert_has_calls(expected_calls, any_order=True)

        # But created
        self.assertTrue(
            (pathlib.Path.cwd() / 'Kirk' / '_Tango.pickle').exists())

    def test_ondemand_loaded_but_not_modified(self, mock_shutil_copy):
        """Loaded but unmodified ondemand dataframe are copied"""

        sut = bandas.DataContainer.load(self.old_container_path)

        # load ondemand but don't modify it
        _ = sut.Zappeln  # pylint: disable=pointless-statement

        sut.save(pathlib.Path.cwd() / 'Kirk')

        expected_calls = self._prepare_list_of_copy_calls(
            pathlib.Path.cwd() / 'Patrick',
            pathlib.Path.cwd() / 'Kirk',
            ['_Tango', '_Bachata', 'Zappeln', 'Foxtrot'],
        )

        self.assertEqual(len(expected_calls), mock_shutil_copy.call_count)
        mock_shutil_copy.assert_has_calls(expected_calls, any_order=True)

    def test_ondemand_loaded_and_modified(self, mock_shutil_copy):
        """Loaded but unmodified ondemand dataframe are copied"""

        sut = bandas.DataContainer.load(self.old_container_path)

        # load ondemand but modify it
        sut.Zappeln.iloc[0, 0] = 9

        sut.save(pathlib.Path.cwd() / 'Kirk')

        expected_calls = self._prepare_list_of_copy_calls(
            pathlib.Path.cwd() / 'Patrick',
            pathlib.Path.cwd() / 'Kirk',
            ['_Tango', '_Bachata', 'Foxtrot'],
        )

        self.assertEqual(len(expected_calls), mock_shutil_copy.call_count)
        mock_shutil_copy.assert_has_calls(expected_calls, any_order=True)

    def test_from_archive_container(self, mock_shutil_copy):
        """Don't copy old dataframe files in archive mode."""

        fp = pathlib.Path.cwd() / 'archive.zip'
        dc = _create_container(['Green'], ['Blue'])
        dc.save(fp)

        sut = bandas.DataContainer.load(fp)
        sut.save(fp.parent / 'Folder')

        self.assertEqual(0, mock_shutil_copy.call_count)
