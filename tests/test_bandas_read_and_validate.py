"""Test about read and validate data files"""
import unittest
from unittest import mock
import inspect
import io
import string
import re
import zipfile
import pandas
import pyfakefs.fake_filesystem_unittest as pyfakefs_ut
from buhtzology import bandas


class ValidateDataFrame(unittest.TestCase):
    """Validate dataframe"""

    def _df(self):
        df = pandas.DataFrame({
            'A': range(5),
            'colB': list('abcde'),
            'CC': [1.3, 4.1, -9, 102, 9731],
            'Lists': [range(3), range(3), range(3), range(3), range(3)]
        })
        return df

    def test_empty_rules(self):
        """Empty rules."""
        ec = ['A', 'colB', 'CC', 'Lists']

        bandas.validate_dataframe(
            df_to_check=self._df(),
            expected_columns=ec,
            ignored_columns=[],
            value_rules={},
            unique_columns=[])

    def test_empty_columns(self):
        """No column names."""
        # value rules
        vr = {
            'A': range(5),
            'colB': list(string.ascii_lowercase),
            'CC': [1.3, 4.1, -9, 102, 9731],
            'Lists': [range(3)]
        }

        bandas.validate_dataframe(
            df_to_check=self._df(),
            expected_columns=[],
            ignored_columns=[],
            value_rules=vr,
            unique_columns=[])

    def test_columns_without_name(self):
        """Some columns with name"""
        # value rules
        sut = pandas.DataFrame({
            'foo': [1],
            'Unnamed: 1': [2],
            'bar': [3],
            'Unnamed: 3': [4],
            'end': [5]
        })

        # assert: Nothing raised
        bandas.validate_dataframe(
            df_to_check=sut,
            expected_columns=['foo', -1, 'bar', -2, 'end'],
            ignored_columns=[],
            value_rules={},
            unique_columns=[])

    def test_valid(self):
        """Valid DataFrame."""
        ec = ['A', 'colB', 'CC', 'Lists']
        vr = {
            'A': range(5),
            'colB': list(string.ascii_lowercase),
            'CC': [1.3, 4.1, -9, 102, 9731],
            'Lists': [range(3)]
        }

        # this would raise an error if something is wrong
        # The test is that nothing is thrown.
        bandas.validate_dataframe(
            df_to_check=self._df(),
            expected_columns=ec,
            ignored_columns=[],
            value_rules=vr,
            unique_columns=[])

    def test_ignored_column_with_rule(self):
        """Valid DataFrame with ignored column."""
        ec = ['A', 'CC', 'Lists']
        vr = {
            'A': range(5),
            'colB': list(string.ascii_lowercase),
            'CC': [1.3, 4.1, -9, 102, 9731],
            'Lists': [range(3)]
        }

        bandas.validate_dataframe(
            df_to_check=self._df(),
            expected_columns=ec,
            ignored_columns=['colB'],
            value_rules=vr,
            unique_columns=[])

        with self.assertRaises(TypeError) as cm:
            bandas.validate_dataframe(
                df_to_check=self._df(),
                expected_columns=ec,
                ignored_columns=[],
                value_rules=vr,
                unique_columns=[])

        self.assertEqual(
            str(cm.exception),
            ("The expected and actual header don't match.\n"
             "- ['A', 'CC', 'Lists']\n"
             "+ ['A', 'colB', 'CC', 'Lists']\n"
             "?       ++++++++\n")
        )

    def test_ignored_column_by_index_with_rule(self):
        """Valid DataFrame with ignored column specified by index instead of a
        name.
        i"""
        df = pandas.DataFrame({
            'foo': [1, 2, 3],
            1: ['A', 'A', 'A'],
            'bar': [4, 5, 6]
        })

        bandas.validate_dataframe(
            df_to_check=df,
            expected_columns=['foo', 'bar'],
            ignored_columns=[1],
            value_rules={
                'foo': [1, 2, 3],
                'bar': [4, 5, 6]
            },
            unique_columns=[]
        )

    def test_ignored_column_with_rule_violated(self):
        """DataFrame with ignored column violating rule."""
        ec = ['A', 'CC', 'Lists']
        vr = {
            'A': range(5),
            'colB': list('bcde'),
            'CC': [1.3, 4.1, -9, 102, 9731],
            'Lists': [range(3)]
        }

        with self.assertRaises(ValueError) as cm:
            bandas.validate_dataframe(
                df_to_check=self._df(),
                expected_columns=ec,
                ignored_columns=['colB'],
                value_rules=vr,
                unique_columns=[])

        self.assertTrue('colB' in str(cm.exception))
        self.assertTrue('value rules' in str(cm.exception))

    def test_value_rules_not_all_columns(self):
        """Don't need value rules for all columns."""
        ec = ['A', 'colB', 'CC', 'Lists']
        vr = {
            'colB': list(string.ascii_lowercase),
        }

        # rule for one column
        bandas.validate_dataframe(
            df_to_check=self._df(),
            expected_columns=ec,
            ignored_columns=[],
            value_rules=vr,
            unique_columns=[])

        # no rules
        bandas.validate_dataframe(
            df_to_check=self._df(),
            expected_columns=ec,
            ignored_columns=[],
            value_rules=[],
            unique_columns=[])

        # missing(None) rules
        err_msg = re.escape("'NoneType' object is not iterable")
        with self.assertRaisesRegex(TypeError, err_msg):
            bandas.validate_dataframe(
                df_to_check=self._df(),
                expected_columns=ec,
                ignored_columns=[],
                value_rules=None,
                unique_columns=[])

    def test_invalid_headers(self):
        """Expected headers."""
        df = self._df()

        # names don't match
        ec = ['A', 'cXlB', 'CC', 'Lists']
        err_msg = re.escape(
            "The expected and actual header don't match.\n"
            "- ['A', 'cXlB', 'CC', 'Lists']\n"
            "?         ^\n"
            "\n"
            "+ ['A', 'colB', 'CC', 'Lists']\n"
            "?         ^")
        with self.assertRaisesRegex(TypeError, err_msg):
            bandas.validate_dataframe(
                df_to_check=df,
                expected_columns=ec,
                ignored_columns=[],
                value_rules=[],
                unique_columns=[])

        # number of headers don't match
        ec = ['A', 'CC']
        err_msg = re.escape(
            "The expected and actual header don't match.\n"
            "- ['A', 'CC']\n"
            "+ ['A', 'colB', 'CC', 'Lists']")
        with self.assertRaisesRegex(TypeError, err_msg):
            bandas.validate_dataframe(
                df_to_check=df,
                expected_columns=ec,
                ignored_columns=[],
                value_rules=[],
                unique_columns=[])

    def test_violated_value_rules(self):
        """Violated value rules."""
        ec = ['A', 'colB', 'CC', 'Lists']
        vr = {
            'A': range(4),
            'colB': list(string.ascii_uppercase),
            'CC': [1.3, 4.1, -9, 102, 9731],
            'Lists': [range(3)]
        }

        err_msg = re.escape(
            'This column(s) do not match the value rules:\n'
            '{\n'
            '    "A": [\n'
            '        4\n'
            '    ],\n'
            '    "colB": [\n'
            '        "a",\n'
            '        "b",\n'
            '        "c",\n'
            '        "d",\n'
            '        "e"\n'
            '    ]\n'
            '}'
        )

        with self.assertRaisesRegex(ValueError, err_msg):
            bandas.validate_dataframe(
                df_to_check=self._df(),
                expected_columns=ec,
                ignored_columns=[],
                value_rules=vr,
                unique_columns=[])

    def test_value_rules_expect_lists(self):
        """Only list() allowed for 'value_rules'."""

        # Exception type
        with self.assertRaises(TypeError) as cm:
            bandas.validate_dataframe(
                df_to_check=pandas.DataFrame({'A': ['a']}),
                expected_columns=['A'],
                ignored_columns=[],
                value_rules={'A': 'a'},
                unique_columns=[])

        # Exception message
        msg = str(cm.exception)
        self.assertIn('only list-like objects are allowed', msg)
        self.assertIn('isin', msg)

    def test_unique_rule_valid(self):
        """Unique rule"""
        df = self._df()
        # this would raise an error if something is wrong
        # The test is that nothing is thrown.
        bandas.validate_dataframe(
            df_to_check=df,
            expected_columns=df.columns.to_list(),
            ignored_columns=[],
            value_rules={},
            unique_columns=['colB'])

    def test_unique_rule_not_valid(self):
        """Not valid because of Unique rule"""
        df = self._df()
        df.colB = ['X'] * len(df)

        with self.assertRaises(ValueError) as cm:
            bandas.validate_dataframe(
                df_to_check=df,
                expected_columns=df.columns.to_list(),
                ignored_columns=[],
                value_rules={},
                unique_columns=['colB'])

        self.assertIn('colB', str(cm.exception))

    def test_len_rule_valid(self):
        """Len rule ok"""
        df_init = pandas.DataFrame({'foo': [1, 62]})

        bandas.validate_len_rules_on_dataframe(df_init, {0: [1, 2]})

    def test_len_rule_not_valid(self):
        """Len rule violated"""
        df_init = pandas.DataFrame({'foo': [1, 62]})

        with self.assertRaises(ValueError):
            bandas.validate_len_rules_on_dataframe(df_init, {0: [1]})

    def test_len_rule_valid_ignore_missings(self):
        """Len rule ok and ignore missing"""
        # pandas.NA is "<NA>" 4 chars long
        df_init = pandas.DataFrame({'foo': [1, 392, pandas.NA]})

        with self.assertRaises(ValueError):
            bandas.validate_len_rules_on_dataframe(
                df_init,
                {0: [1, 3]},
                ignore_na=False  # default!
            )

        bandas.validate_len_rules_on_dataframe(
            df_init,
            {0: [1, 3]},
            ignore_na=True
        )


class ParseSpecsAndRules(unittest.TestCase):
    """Testing two helper functions used in `read_and_validate_csv()`.

    - {'col': ('dtype', -9, {'len': [], 'val': []}}
    """

    # pylint: disable=protected-access

    def test_store_spec_na_values(self):
        """Different na-values specs"""
        res = bandas._store_spec_na_values('Foo', -9, {})
        self.assertEqual(res, {'Foo': [-9]})

        res = bandas._store_spec_na_values('Foo', [-9], {})
        self.assertEqual(res, {'Foo': [-9]})

        res = bandas._store_spec_na_values('Foo', [-9, -3], {})
        self.assertEqual(res, {'Foo': [-9, -3]})

        res = bandas._store_spec_na_values('Foo', (-9, -3), {})
        self.assertEqual(res, {'Foo': [-9, -3]})

        res = bandas._store_spec_na_values('Foo', ['-9'], {})
        self.assertEqual(res, {'Foo': ['-9']})

        res = bandas._store_spec_na_values('Foo', '-9', {})
        self.assertEqual(res, {'Foo': ['-9']})

        res = bandas._store_spec_na_values('Foo', ['-9', '-3'], {})
        self.assertEqual(res, {'Foo': ['-9', '-3']})

        res = bandas._store_spec_na_values('Foo', None, {})
        self.assertEqual(res, {})

        res = bandas._store_spec_na_values('Foo', '', {})
        self.assertEqual(res, {'Foo': ['']})

    def test_psr_full(self):
        """Specs and rules."""
        self.assertEqual(
            bandas._parse_specs_and_rules(
                {
                    'col': ('Int32', -9, {
                        'len': [2, (4, 6)],
                        'val': [(1, 3), 5],
                        'unique': True
                    })
                }
            ),
            {
                'COLUMNS': ['col'],
                'COLUMNS_IGNORE': [],
                'DTYPES': {'col': 'Int32'},
                'NA_VALUES': {'col': [-9]},
                'LEN_RULES': {0: [2, 4, 5, 6]},
                'VAL_RULES': {'col': [5, 1, 2, 3]},
                'UNIQUE': ['col'],
            }
        )

    def test_psr_dtype(self):
        """Dtype."""
        sut = bandas._parse_specs_and_rules({'col': 'Int32'})
        self.assertEqual(sut['COLUMNS'], ['col'])
        self.assertEqual(sut['DTYPES']['col'], 'Int32')

    def test_psr_ignore_column_but_rules(self):
        """Ignored column with len and value rules.
        """
        specs = {
            'col': 'Int32',
            'ignore_len': (None, None, {'len': [1, 2]}),
            'ignore_val': (None, None, {'val': ['A', 'B']}),
        }

        self.assertEqual(
            bandas._parse_specs_and_rules(specs),
            {
                'COLUMNS': ['col'],
                'COLUMNS_IGNORE': ['ignore_len', 'ignore_val'],
                'DTYPES': {'col': 'Int32'},
                'NA_VALUES': {},
                'LEN_RULES': {1: [1, 2]},
                'VAL_RULES': {'ignore_val': ['A', 'B']},
                'UNIQUE': []
            }
        )

    def test_psr_ignore_column(self):
        """Column not to read."""
        sut = bandas._parse_specs_and_rules(
            {'col': 'Int32', 'ignoreme': None})
        self.assertEqual(sut['COLUMNS'], ['col'])
        self.assertEqual(sut['COLUMNS_IGNORE'], ['ignoreme'])

    def test_psr_ignored_column_and_len_rule(self):
        """Len rule and ignored column."""
        # Keep in mind that the field length rules are checked with the raw
        # csv file. Ignored columns doesn't matter at that early point.
        # Because of that the index for the length rules need to orientate
        # on the original csv header and not at the resulting dataframe where
        # columns are excluded.

        # len only
        self.assertEqual(
            bandas._parse_specs_and_rules({
                'colA': ('Int32', -9, {'len': [2, (4, 6)]}),
                'colB': None,
                'colC': None,
                'colD': ('str', None, {'len': [2, 3]}),
                'colE': ('str', None, {'len': 7}),
            }),
            {
                'COLUMNS': ['colA', 'colD', 'colE'],
                'COLUMNS_IGNORE': ['colB', 'colC'],
                'DTYPES': {
                    'colA': 'Int32',
                    'colD': 'str',
                    'colE': 'str'
                },
                'NA_VALUES': {'colA': [-9]},
                'LEN_RULES': {
                    0: [2, 4, 5, 6],
                    3: [2, 3],
                    4: [7]
                },
                'VAL_RULES': {},
                'UNIQUE': [],
            }
        )

    def test_psr_missing_len_added_to_len_rules(self):
        """Consider length of missing values as len rules by default."""
        sut = bandas._parse_specs_and_rules({
            'foo': ('int', -9, {'len': [1]}),
            'bar': ('str', '(n.a.)', {'len': [2]}),
            'drei': ('str', '', {'len': 1}),
        })

        # Keep in mind: Len rules use column index instead of name
        # -9 is 2 chars long
        self.assertEqual(sut['LEN_RULES'][0], [1, 2])
        # missing value is 6 chars long
        self.assertEqual(sut['LEN_RULES'][1], [2, 6])
        # missing '' is 0 chars long
        self.assertEqual(sut['LEN_RULES'][2], [1, 0])

    def test_psr_ignore_missing_len_in_len_rules(self):
        """Ignore the length of missing values in len rules"""
        sut = bandas._parse_specs_and_rules(
            {
                'foo': ('int', -9, {'len': [1]}),
                'bar': ('str', '(n.a.)', {'len': [2]}),
                'drei': ('str', '', {'len': 1}),
            },
            ignore_na_lengths=True,
        )

        # Keep in mind: Len rules use column index instead of name
        # -9 is 2 chars long
        self.assertEqual(sut['LEN_RULES'][0], [1])
        # missing value is 6 chars long
        self.assertEqual(sut['LEN_RULES'][1], [2])
        # missing '' is 0 chars long
        self.assertEqual(sut['LEN_RULES'][2], [1])

    def test_psr_missing_always_a_list(self):
        """Missing are a list even if it is only one value."""

        sut = bandas._parse_specs_and_rules({'col': ('int', -9)})
        self.assertIsInstance(sut['NA_VALUES']['col'], list)
        self.assertEqual(sut['NA_VALUES']['col'], [-9])

        # tuples converted to list
        sut = bandas._parse_specs_and_rules({'col': ('int', (-9, -3))})
        self.assertIsInstance(sut['NA_VALUES']['col'], list)
        self.assertEqual(sut['NA_VALUES']['col'], [-9, -3])

        # string as single entry list
        sut = bandas._parse_specs_and_rules({'col': ('int', '(na)')})
        self.assertIsInstance(sut['NA_VALUES']['col'], list)
        self.assertEqual(sut['NA_VALUES']['col'], ['(na)'])

    def test_psr_dtype_and_missing(self):
        """Dtype and missing."""

        # single missing value
        sut = bandas._parse_specs_and_rules({'col': ('Int32', -9)})
        self.assertEqual(sut['DTYPES']['col'], 'Int32')
        self.assertEqual(sut['NA_VALUES']['col'], [-9])

        # multiple missing value
        sut = bandas._parse_specs_and_rules(
            {'col': ('Int32', [-9, -3, 'k.A.'])})
        self.assertEqual(sut['DTYPES']['col'], 'Int32')
        self.assertEqual(sut['NA_VALUES']['col'], [-9, -3, 'k.A.'])

        # no missing value but len rule
        sut = bandas._parse_specs_and_rules(
            {'col': ('Int32', None, {'len': 1})})
        self.assertEqual(sut['DTYPES']['col'], 'Int32')
        self.assertEqual(sut['NA_VALUES'], {})
        self.assertEqual(sut['LEN_RULES'][0], [1])

    def test_psr_len(self):
        """Len rules."""
        self.assertEqual(
            bandas._parse_specs_and_rules(
                {'col': ('Int32', [-9], {'len': [2, (4, 6)]})}
            ),
            {
                'COLUMNS': ['col'],
                'COLUMNS_IGNORE': [],
                'DTYPES': {'col': 'Int32'},
                'NA_VALUES': {'col': [-9]},
                'LEN_RULES': {0: [2, 4, 5, 6]},
                'VAL_RULES': {},
                'UNIQUE': [],
            }
        )

    def test_psr_val(self):
        """Val rules."""
        # val only
        self.assertEqual(
            bandas._parse_specs_and_rules(
                {'col': ('Int32', -9, {'val': [(1, 3), 5]})}
            ),
            {
                'COLUMNS': ['col'],
                'COLUMNS_IGNORE': [],
                'DTYPES': {'col': 'Int32'},
                'NA_VALUES': {'col': [-9]},
                'LEN_RULES': {},
                'VAL_RULES': {'col': [5, 1, 2, 3]},
                'UNIQUE': [],
            }
        )

    def test_explode_rules(self):
        """Test exploding rule lists."""
        self.assertEqual(
            bandas._explode_rules(([1, 2, (4, 8), 11])),
            [1, 2, 11, 4, 5, 6, 7, 8]
        )

    def test_explode_rules_single_value(self):
        """Test exploding rule as single value."""
        self.assertEqual(
            bandas._explode_rules((1)),
            [1])

    def test_explode_rules_invalid_range(self):
        """Invalid range"""
        with self.assertRaises(ValueError):
            bandas._explode_rules([1, (8, 4)])
            bandas._explode_rules([1, (4, 4)])

    def test_category_column(self):
        """Warning when specify category column.
        """
        with self.assertWarns(UserWarning) as cm:
            bandas._parse_specs_and_rules({'col': 'category'})

        self.assertIsInstance(cm.warning, UserWarning)
        self.assertTrue(
            'not recommended to use "category"' in str(cm.warning))


class ReplaceLines(unittest.TestCase):
    """Test line and sub-string replacement while reading and validating CSV"""

    # pylint: disable=unused-argument
    def test_replace_invalide_lines(self):
        """Replace lines (in-memory file)"""
        sut = io.StringIO(inspect.cleandoc("""
            OID;Foo;Bar;Count
            01;Suse;Miller;3
            02;"Invalid;Name";1
            03;Peter;Blow;9

        """))

        # Fails without replacement
        with mock.patch('pathlib.Path.open'):  # don't write WRONG file
            with self.assertRaises(ValueError):
                bandas.validate_csv(sut, columns=4)

        sut.seek(0)

        replace = {
            '02;"Invalid;Name";1\n': '02;Invalid;Name;1\n',
            '04|Falsch,Wrong";1\n': '04;Falsch;Wrong;1\n'
        }

        # Succeed with replacing invalid lines
        rc = bandas.validate_csv(
            sut, columns=4, replace_lines=replace)

        self.assertTrue(rc)

    def test_replace_invalide_strings(self):
        """Replace sub-strings (in-memory file)"""
        sut = io.StringIO(inspect.cleandoc("""
            OID;Foo;Bar;Count
            01;Suse;Miller;3
            02;Invalid; SUB;Name;1
            03;Peter;Blow;9

        """))

        # Fails without replacement
        with mock.patch('pathlib.Path.open'):
            with self.assertRaises(ValueError):
                bandas.validate_csv(sut, columns=4)

        sut.seek(0)

        replace = {
            '; SUB;': ', SUB;'
        }

        # Succeed with replacing invalid lines
        rc = bandas.validate_csv(
            sut, columns=4, replace_substrings=replace)

        self.assertTrue(rc)

    @pyfakefs_ut.patchfs
    def test_replace_invalide_lines_in_file(self, fake_fs):
        """Replace lines and substrings while reading. (FakeFS)"""
        content = io.StringIO(inspect.cleandoc("""
            OID;Foo;Bar;Count
            01;Suse;Miller; SUB;3
            02;"Invalid;Name";1
            03;Pieter; SUB;Blow;9
            04|Falsch,Wrong";1
        """))

        specs_and_rules = {
            'OID': ('str', None, {'len': 2}),
            'Foo': 'str',
            'Bar': 'str',
            'Count': int}

        # fail because of invalid line
        with self.assertRaises(ValueError):
            bandas.read_and_validate_csv(
                file_path=content,
                specs_and_rules=specs_and_rules)

        # replace
        replace_lines = {
            '02;"Invalid;Name";1\n': '02;Invalid;Name;1\n',
            '04|Falsch,Wrong";1': '04;Falsch;Wrong;1'
        }
        replace_substrings = {
            '; SUB;': ', SUB;'
        }

        content = io.StringIO(inspect.cleandoc("""
            OID;Foo;Bar;Count
            01;Suse;Miller; SUB;3
            02;"Invalid;Name";1
            03;Pieter; SUB;Blow;9
            04|Falsch,Wrong";1
        """))

        df = bandas.read_and_validate_csv(
            file_path=content,
            specs_and_rules=specs_and_rules,
            replace_lines=replace_lines,
            replace_substrings=replace_substrings

        )

        self.assertEqual(df.shape, (4, 4))
        self.assertEqual(
            df.columns.to_list(),
            ['OID', 'Foo', 'Bar', 'Count']
        )

    @pyfakefs_ut.patchfs
    def test_replace_invalide_lines_zipfile(self, fake_fs):
        """Replace lines and substrings while reading a zip file. (FakeFS)"""

        content = inspect.cleandoc("""
            OID;Foo;Bar;Count
            01;Suse;Miller; SUB;3
            02;"Invalid;Name";1
            03;Pieter; SUB;Blow;9
            04|Falsch,Wrong";1
        """)

        # prepare zip file
        with zipfile.ZipFile('the.zip', 'w') as zip_handle:
            with zip_handle.open('data.csv', 'w') as entry_handle:
                entry_handle.write(content.encode())

        specs_and_rules = {
            'OID': ('str', None, {'len': 2}),
            'Foo': 'str',
            'Bar': 'str',
            'Count': int}

        with self.assertRaises(ValueError):
            bandas.read_and_validate_csv(
                file_path=zipfile.Path('the.zip', 'data.csv'),
                specs_and_rules=specs_and_rules)

        # replace
        replace_lines = {
            '02;"Invalid;Name";1\n': '02;Invalid;Name;1\n',
            '04|Falsch,Wrong";1': '04;Falsch;Wrong;1'
        }
        replace_substrings = {
            '; SUB;': ', SUB;'
        }

        df = bandas.read_and_validate_csv(
            file_path=zipfile.Path('the.zip', 'data.csv'),
            specs_and_rules=specs_and_rules,
            replace_lines=replace_lines,
            replace_substrings=replace_substrings

        )

        self.assertEqual(df.shape, (4, 4))
        self.assertEqual(
            df.columns.to_list(),
            ['OID', 'Foo', 'Bar', 'Count']
        )
