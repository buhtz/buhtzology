"""Tests about analy module"""
import unittest
import random
import numpy
import pandas
from buhtzology import analy


# pylint: disable-next=missing-class-docstring
class Frequencies(unittest.TestCase):
    # pylint: disable=protected-access,missing-function-docstring

    def setUp(self):
        random.seed(0)

    def test_simple(self):
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        sut = analy.frequencies(data)

        self.assertEqual(sut.shape, (3, 1))  # one column, three rows
        self.assertEqual(sut.columns, ['n'])
        self.assertCountEqual(sut.index, ['Worf', 'Riker', 'Data'])

    def test_simple_values_indented_index(self):
        data = pandas.Series(list('AAABBBBBBBBCC'), name='Foobar')

        sut = analy.frequencies(data, index=True)

        self.assertEqual(sut.shape, (4, 1))
        self.assertEqual(
            sut.index.tolist(),
            [
                'Foobar',
                '    B',
                '    A',
                '    C'
            ]
        )
        self.assertEqual(sut.loc[:, 'n'].tolist(), [None, 8, 3, 2])

    def test_missings_by_default(self):
        data = pandas.Series(
            list(random.choices(['Worf', pandas.NA, 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        sut = analy.frequencies(data)

        self.assertEqual(sut.shape, (4, 1))  # one column, three rows
        self.assertEqual(sut.columns, ['n'])
        self.assertCountEqual(sut.index, ['Worf', 'Riker', 'Data', pandas.NA])

    def test_drop_missings(self):
        data = pandas.Series(
            list(random.choices(['Worf', pandas.NA, 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        sut = analy.frequencies(data, dropna=True)

        self.assertEqual(sut.shape, (3, 1))  # one column, three rows
        self.assertEqual(sut.columns, ['n'])
        self.assertCountEqual(sut.index, ['Worf', 'Riker', 'Data'])

    def test_result_label(self):
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        sut = analy.frequencies(data, result_label='Foobar')

        self.assertEqual(sut.shape, (3, 1))  # one column, three rows
        self.assertEqual(sut.columns, ['Foobar'])

    def test_multiindex(self):
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        sut = analy.frequencies(data, index='multi')

        self.assertEqual(sut.shape, (3, 1))  # one column, three rows
        self.assertEqual(sut.columns, ['n'])
        self.assertEqual(sut.index.nlevels, 2)
        self.assertEqual(sut.index.levels[0], ['Characters'])
        self.assertCountEqual(sut.index.levels[1], ['Worf', 'Riker', 'Data'])

    def test_named_multiindex(self):
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        sut = analy.frequencies(data, index='multi', data_label='Foobar')

        self.assertEqual(sut.shape, (3, 1))  # one column, three rows
        self.assertEqual(sut.columns, ['n'])
        self.assertEqual(sut.index.nlevels, 2)
        self.assertEqual(sut.index.levels[0], ['Foobar'])
        self.assertCountEqual(sut.index.levels[1], ['Worf', 'Riker', 'Data'])

    def test_indentedindex(self):
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        suts = [
            analy.frequencies(data, index=True),
            analy.frequencies(data, index='indented')
        ]

        for sut in suts:
            self.assertEqual(sut.shape, (4, 1))  # one column, four rows
            self.assertEqual(sut.columns, ['n'])
            self.assertCountEqual(
                sut.index,
                ['    Worf', '    Riker', '    Data', 'Characters'])

    def test_named_indentedindex(self):
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        suts = [
            analy.frequencies(data, index=True, data_label='RoLaren'),
            analy.frequencies(data, index='indented', data_label='RoLaren')
        ]

        for sut in suts:
            self.assertEqual(sut.shape, (4, 1))  # one column, four rows
            self.assertEqual(sut.columns, ['n'])
            self.assertCountEqual(
                sut.index,
                ['    Worf', '    Riker', '    Data', 'RoLaren'])

    def test_indentedindex_none_value(self):
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        sut = analy.frequencies(data, index=True)

        self.assertEqual(sut.shape, (4, 1))  # one column, four rows
        self.assertEqual(sut.loc['Characters', 'n'], None)


# pylint: disable-next=missing-class-docstring
class Percentages(unittest.TestCase):
    # pylint: disable=protected-access,missing-function-docstring

    def setUp(self):
        random.seed(0)

    def test_simple(self):
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        sut = analy.percentages(data)

        self.assertEqual(sut.shape, (3, 1))  # one column, three rows
        self.assertEqual(sut.columns, ['%'])
        self.assertCountEqual(sut.index, ['Worf', 'Riker', 'Data'])

    def test_missings_by_default(self):
        data = pandas.Series(
            list(random.choices(['Worf', pandas.NA, 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        sut = analy.percentages(data)

        self.assertEqual(sut.shape, (4, 1))  # one column, three rows
        self.assertEqual(sut.columns, ['%'])
        self.assertCountEqual(sut.index, ['Worf', 'Riker', 'Data', pandas.NA])

    def test_drop_missings(self):
        data = pandas.Series(
            list(random.choices(['Worf', pandas.NA, 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        sut = analy.percentages(data, dropna=True)

        self.assertEqual(sut.shape, (3, 1))  # one column, three rows
        self.assertEqual(sut.columns, ['%'])
        self.assertCountEqual(sut.index, ['Worf', 'Riker', 'Data'])

    def test_result_label(self):
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        sut = analy.percentages(data, result_label='Foobar')

        self.assertEqual(sut.shape, (3, 1))  # one column, three rows
        self.assertEqual(sut.columns, ['Foobar'])

    def test_multiindex(self):
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        sut = analy.percentages(data, index='multi')

        self.assertEqual(sut.shape, (3, 1))  # one column, three rows
        self.assertEqual(sut.columns, ['%'])
        self.assertEqual(sut.index.nlevels, 2)
        self.assertEqual(sut.index.levels[0], ['Characters'])
        self.assertCountEqual(sut.index.levels[1], ['Worf', 'Riker', 'Data'])

    def test_named_multiindex(self):
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        sut = analy.percentages(data, index='multi', data_label='Foobar')

        self.assertEqual(sut.shape, (3, 1))  # one column, three rows
        self.assertEqual(sut.columns, ['%'])
        self.assertEqual(sut.index.nlevels, 2)
        self.assertEqual(sut.index.levels[0], ['Foobar'])
        self.assertCountEqual(sut.index.levels[1], ['Worf', 'Riker', 'Data'])

    def test_indentedindex(self):
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        suts = [
            analy.percentages(data, index=True),
            analy.percentages(data, index='indented')
        ]

        for sut in suts:
            self.assertEqual(sut.shape, (4, 1))  # one column, four rows
            self.assertEqual(sut.columns, ['%'])
            self.assertCountEqual(
                sut.index,
                ['    Worf', '    Riker', '    Data', 'Characters'])

    def test_named_indentedindex(self):
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        sut = analy.percentages(data, index=True, data_label='RoLaren')

        self.assertEqual(sut.shape, (4, 1))  # one column, four rows
        self.assertEqual(sut.columns, ['%'])
        self.assertCountEqual(
            sut.index,
            ['    Worf', '    Riker', '    Data', 'RoLaren'])


# pylint: disable-next=missing-class-docstring
class FreqAndPerc(unittest.TestCase):
    # pylint: disable=protected-access,missing-function-docstring

    def setUp(self):
        random.seed(0)

    def test_combined_by_default(self):
        data = pandas.Series(
            ['Worf'] * 55 + ['Riker'] * 35 + ['Data'] * 110,
            name='Characters'
        )

        sut = analy.frequencies_and_percentages(data)

        self.assertEqual(sut.shape, (3, 1))
        self.assertEqual(sut.columns.to_list(), ['n (%)'])

        # Keep in mind: Result is sorted by "n" column
        self.assertEqual(
            sut.index.tolist(),
            ['Data', 'Worf', 'Riker'])
        self.assertEqual(
            sut.iloc[:, 0].tolist(),
            ['110 (55)', '55 (27.5)', '35 (17.5)'])

    def test_combined_customized_combine(self):
        data = pandas.Series(
            ['Worf'] * 55 + ['Riker'] * 35 + ['Data'] * 110,
            name='Characters'
        )

        sut = analy.frequencies_and_percentages(
            data, combine='foo {} bar {}')

        self.assertEqual(sut.shape, (3, 1))
        self.assertEqual(sut.columns.to_list(), ['foo n bar %'])

        self.assertEqual(sut.iloc[0, 0], 'foo 110 bar 55')

    def test_combined_result_labels(self):
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        sut = analy.frequencies_and_percentages(
            data,
            result_labels=['Foo', 'Bar'])

        self.assertEqual(sut.shape, (3, 1))
        self.assertEqual(sut.columns.to_list(), ['Foo (Bar)'])

    def test_combined_reversed_columns(self):
        """Show "% (n)" instead of "n (%)"""
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        sut = analy.frequencies_and_percentages(
            data,
            reverse_columns=True)

        self.assertEqual(sut.shape, (3, 1))
        self.assertEqual(sut.columns.to_list(), ['% (n)'])

    def test_separate(self):
        """Do not combine."""
        data = pandas.Series(
            ['Worf'] * 55 + ['Riker'] * 35 + ['Data'] * 110,
            name='Characters'
        )

        sut = analy.frequencies_and_percentages(data, combine=False)

        self.assertEqual(sut.shape, (3, 2))
        self.assertEqual(sut.columns.to_list(), ['n', '%'])

        # Keep in mind: Result is sorted by "n" column
        self.assertEqual(
            sut.index.tolist(),
            ['Data', 'Worf', 'Riker'])
        self.assertEqual(
            sut.iloc[:, 0].tolist(),
            [110, 55, 35])
        numpy.testing.assert_almost_equal(
            sut.iloc[:, 1].tolist(),
            [55, 27.5, 17.5])

    def test_simple(self):
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        sut = analy.frequencies_and_percentages(data)

        self.assertEqual(sut.shape, (3, 1))
        self.assertEqual(sut.columns.to_list(), ['n (%)'])
        self.assertCountEqual(sut.index, ['Worf', 'Riker', 'Data'])

    def test_simple_values(self):
        data = pandas.Series(list('AAABBBBBBBBCC'), name='Foobar')

        sut = analy.frequencies_and_percentages(data)

        self.assertEqual(sut.shape, (3, 1))
        self.assertEqual(sut.index.tolist(), ['B', 'A', 'C'])
        self.assertEqual(
            sut.loc[:, 'n (%)'].tolist(),
            [
                '8 (61.54)',
                '3 (23.08)',
                '2 (15.38)'
            ]
        )

    def test_simple_values_indented_index(self):
        data = pandas.Series(list('AAABBBBBBBBCC'), name='Foobar')

        sut = analy.frequencies_and_percentages(data, index=True)

        self.assertEqual(sut.shape, (4, 1))
        self.assertEqual(
            sut.index.tolist(),
            [
                'Foobar',
                '    B',
                '    A',
                '    C'
            ]
        )
        self.assertEqual(
            sut.loc[:, 'n (%)'].tolist(),
            [
                '',
                '8 (61.54)',
                '3 (23.08)',
                '2 (15.38)'
            ]
        )

    def test_separate_reverse_columns(self):
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        sut = analy.frequencies_and_percentages(
            data,
            reverse_columns=True,
            combine=False)

        self.assertEqual(sut.shape, (3, 2))
        self.assertEqual(sut.columns.to_list(), ['%', 'n'])
        self.assertCountEqual(sut.index, ['Worf', 'Riker', 'Data'])

        self.assertEqual(sut['%'].sum(), 100)
        self.assertEqual(sut['n'].sum(), 200)

    def test_separate_customized_reverse_columns(self):
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        sut = analy.frequencies_and_percentages(
            data,
            result_labels=['Foo', 'Bar'],
            reverse_columns=True,
            combine=False)

        self.assertEqual(sut.columns.to_list(), ['Bar', 'Foo'])

        self.assertEqual(sut['Bar'].sum(), 100)
        self.assertEqual(sut['Foo'].sum(), 200)

    def test_separate_customized_labels(self):
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        sut = analy.frequencies_and_percentages(
            data,
            result_labels=['Foo', 'Bar'],
            combine=False)

        self.assertEqual(sut.shape, (3, 2))
        self.assertEqual(sut.columns.to_list(), ['Foo', 'Bar'])

    def test_index_multi(self):
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        suts = [
            analy.frequencies_and_percentages(
                data, index='multi'),
            analy.frequencies_and_percentages(
                data, index='multi', combine=False)
        ]

        for sut in suts:
            self.assertEqual(sut.index.nlevels, 2)
            self.assertEqual(sut.index.levels[0], ['Characters'])
            self.assertCountEqual(
                sut.index.levels[1], ['Worf', 'Riker', 'Data'])

    def test_index_multi_customized(self):
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        suts = [
            analy.frequencies_and_percentages(
                data, index='multi', data_label='Foobar'),
            analy.frequencies_and_percentages(
                data, index='multi', data_label='Foobar', combine=True)
        ]

        for sut in suts:
            self.assertEqual(sut.index.nlevels, 2)
            self.assertEqual(sut.index.levels[0], ['Foobar'])
            self.assertCountEqual(
                sut.index.levels[1], ['Worf', 'Riker', 'Data'])

    def test_index_indented(self):
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        suts = [
            analy.frequencies_and_percentages(
                data, index=True),
            analy.frequencies_and_percentages(
                data, index=True, combine=False),
            analy.frequencies_and_percentages(
                data, index='indented'),
            analy.frequencies_and_percentages(
                data, index='indented', combine=False)
        ]

        for sut in suts:

            self.assertCountEqual(
                sut.index,
                ['    Worf', '    Riker', '    Data', 'Characters'])

    def test_index_indented_customized(self):
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        suts = [
            analy.frequencies_and_percentages(
                data, index=True, data_label='RoLaren'),
            analy.frequencies_and_percentages(
                data, index=True, data_label='RoLaren', combine=False)
        ]

        for sut in suts:
            self.assertCountEqual(
                sut.index,
                ['    Worf', '    Riker', '    Data', 'RoLaren'])

    def test_combined_index_dataname_values_empty(self):
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        sut = analy.frequencies_and_percentages(data, index=True)

        self.assertEqual(sut.loc['Characters', 'n (%)'], '')

    def test_combined_reverse_index_dataname_values_empty(self):
        data = pandas.Series(
            list(random.choices(['Worf', 'Riker', 'Data'], k=200)),
            name='Characters'
        )

        sut = analy.frequencies_and_percentages(
            data, index=True, reverse_columns=True)

        self.assertEqual(sut.loc['Characters', '% (n)'], '')
