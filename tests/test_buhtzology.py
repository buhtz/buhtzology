"""Testing buhtzology module"""
import unittest
from unittest import mock
import importlib
import datetime
import pathlib
import logging
import re
import platform
import pyfakefs.fake_filesystem_unittest as pyfakefs_ut
import buhtzology


class Buhtzology(unittest.TestCase):
    """Several tests not fitting into other categories"""

    def test_runtime_info_string(self):
        """Runtime string."""
        start_time = datetime.datetime(
            year=1982, month=8, day=6, hour=18, minute=23)

        test_data = [
            (datetime.datetime(1982, 8, 6, 18, 23, 17),
             '17.00 seconds'),
            (datetime.datetime(1982, 8, 6, 18, 23, 49),
             '49.00 seconds'),
            (datetime.datetime(1982, 8, 6, 18, 23, 49, 989999),
             '49.99 seconds'),
            (datetime.datetime(1982, 8, 6, 18, 24, 59),
             '119.00 seconds'),
            (datetime.datetime(1982, 8, 6, 18, 25, 00),
             '120.00 seconds or 2.0 minutes'),
            (datetime.datetime(1982, 8, 6, 18, 45, 36),
             '1356.00 seconds or 22.6 minutes'),
            (datetime.datetime(1982, 8, 6, 18, 45, 37),
             '1357.00 seconds or 22.6 minutes'),
        ]

        with mock.patch('datetime.datetime') as mock_now:
            for end_time, target_string in test_data:
                mock_now.now.return_value = end_time
                result_string = buhtzology.runtime_as_string(start_time)
                self.assertEqual(result_string, target_string)


class GenerateFilepath(unittest.TestCase):
    """Test generate_filepath()"""

    # pylint: disable-next=unused-argument
    def _fake_generator_csv(self, *args):
        fns = ['2000-12-01_foobar.csv',
               '1932-04-01_foobar.csv',
               '2000-12-04_foobar.csv',
               '2001-07-01_foobar.csv',
               '1982-06-04_foobar.csv',
               '2017-12-01_foobar.csv']
        yield from [pathlib.Path(p) for p in fns]

    # pylint: disable-next=unused-argument
    def _fake_generator_csv_in_subfolder(self, *args):
        fns = ['sub-Folder/2000-12-01_foobar.csv',
               'sub-Folder/2017-12-01_foobar.csv']
        yield from [pathlib.Path(p) for p in fns]

    # pylint: disable-next=unused-argument
    def _fake_generator_txt(self, *args):
        fns = ['2000-12-05_foobar.txt',
               '2000-12-06_foobar.txt',
               '2000-09-01_foobar.txt']
        yield from [pathlib.Path(p) for p in fns]

    # pylint: disable-next=unused-argument
    def _fake_generator_empty(self, *args):
        yield from []

    @mock.patch('pathlib.Path.glob', autospec=True)
    def test_find_csv(self, mock_glob):
        """Find youngest csv."""
        mock_glob.side_effect = GenerateFilepath._fake_generator_csv
        res = buhtzology.generate_filepath(
            basename='foobar', file_suffix='.csv')
        self.assertEqual(res, pathlib.Path('2017-12-01_foobar.csv'))

    @mock.patch('pathlib.Path.glob', autospec=True)
    def test_find_csv_in_sub_folder(self, mock_glob):
        """Search in a sub-folder.
        """
        mock_glob.side_effect \
            = GenerateFilepath._fake_generator_csv_in_subfolder
        res = buhtzology.generate_filepath(
            basename='foobar',
            file_suffix='.csv',
            folder_path=pathlib.Path('sub-Folder')
        )
        self.assertEqual(
            res, pathlib.Path('sub-Folder') / '2017-12-01_foobar.csv')

    @mock.patch('pathlib.Path.glob', autospec=True)
    def test_find_txt(self, mock_glob):
        """Find youngest txt."""
        mock_glob.side_effect = GenerateFilepath._fake_generator_txt
        res = buhtzology.generate_filepath(
            basename='foobar', file_suffix='.txt')
        self.assertEqual(res, pathlib.Path('2000-12-06_foobar.txt'))

    @mock.patch('pathlib.Path.glob', autospec=True)
    def test_find_second_and_third(self, mock_glob):
        """Find 2nd and 3rd youngest txt."""
        mock_glob.side_effect = GenerateFilepath._fake_generator_txt
        # 2nd
        res = buhtzology.generate_filepath(
            basename='foobar', file_suffix='.txt', age_offset=1)
        self.assertEqual(res, pathlib.Path('2000-12-05_foobar.txt'))
        # 3rd
        res = buhtzology.generate_filepath(
            basename='foobar', file_suffix='.txt', age_offset=2)
        self.assertEqual(res, pathlib.Path('2000-09-01_foobar.txt'))

    @mock.patch('pathlib.Path.glob', autospec=True)
    def test_invalid_age_offset(self, mock_glob):
        """Age offset to big/old."""
        mock_glob.side_effect = GenerateFilepath._fake_generator_txt
        err_msg = 'Invalid age_offset. There are 3 filenames in ' \
                  'the list but you asked for the 4th.'
        with self.assertRaisesRegex(IndexError, err_msg):
            buhtzology.generate_filepath(
                basename='foobar', file_suffix='.txt', age_offset=3)

        mock_glob.side_effect = GenerateFilepath._fake_generator_csv
        err_msg = 'Invalid age_offset. There are 6 filenames in ' \
                  'the list but you asked for the 7th.'
        with self.assertRaisesRegex(IndexError, err_msg):
            buhtzology.generate_filepath(
                basename='foobar', file_suffix='.csv', age_offset=6)

    @mock.patch('pathlib.Path.glob', autospec=True)
    def test_oldest_csv(self, mock_glob):
        """Get the oldest csv by age_offset -1."""
        mock_glob.side_effect = GenerateFilepath._fake_generator_csv
        res = buhtzology.generate_filepath(
            basename='foobar', file_suffix='.csv', age_offset=-1)
        self.assertEqual(res, pathlib.Path('1932-04-01_foobar.csv'))

    @mock.patch('pathlib.Path.glob', autospec=True)
    def test_file_not_found(self, mock_glob):
        """No file found."""
        mock_glob.side_effect = GenerateFilepath._fake_generator_empty

        pattern_path = re.escape(str(pathlib.Path.cwd()))
        pattern_csv = re.escape('*pinkelephant*.csv')

        err_msg = f'No file found in {pattern_path} matching ' \
                  f'the pattern {pattern_csv}.'

        with self.assertRaisesRegex(FileNotFoundError, err_msg):
            buhtzology.generate_filepath(
                basename='pinkelephant', file_suffix='.csv')


class HowMuchElementsPerPiece(unittest.TestCase):
    """Test how_much_elements_per_piece()"""

    def test_invalid_arguments(self):
        """Invalid arguments"""
        with self.assertRaises(ValueError):
            buhtzology.how_much_elements_per_piece(1000)
            buhtzology.how_much_elements_per_piece(
                1000, min_max_per_piece=(None, None))
            buhtzology.how_much_elements_per_piece(
                1000, min_max_per_piece=(1, None))
            buhtzology.how_much_elements_per_piece(
                1000, min_max_per_piece=(None, 1))
            buhtzology.how_much_elements_per_piece(
                1000, min_max_per_piece=(1000, 500))
            buhtzology.how_much_elements_per_piece(
                1000, min_max_per_piece=(500, 500))

    def test_how_much_elements_per_piece(self):
        """Valid values"""
        # (total_elements_n, pieces_n,
        #  min_elements_per_piece, max_elements_per_piece,
        #  min_pieces_n)
        arg_result_list = [
            # pieces_n
            ((1000, 1, None, None), 1000),
            ((1000, 2, None, None), 500),
            ((1000, 4, None, None), 250),
            ((1000, 5, None, None), 200),
            ((1000, 7, None, None), 142),
            # min_elements_per_piece (ignored) because of pieces_n
            ((1000, 1, (500, 600), None), 1000),
            ((1000, 2, (600, 650), None), 500),
            ((1000, 4, (300, 400), None), 250),
            ((1000, 5, (250, 350), None), 200),
            ((1000, 7, (200, 500), None), 142),
            # min_elements_per_piece only
            ((1000, None, (200, 499), None), 333),
            ((1000, None, (200, 500), None), 500),
            ((1000, None, (200, 501), None), 500),
            ((1000, None, (500, 600), None), 500),
            ((1000, None, (600, 650), None), 500),
            ((1000, None, (300, 400), None), 333),
            ((5000, None, (200, 1500), None), 1250),
            # pieces_n with ignored min_pieces_n
            ((1000, 1, None, 3), 1000),
            ((1000, 2, None, 3), 500),
            ((1000, 4, None, 5), 250),
            ((1000, 5, None, 6), 200),
            ((1000, 7, None, 8), 142),
            # again without pieces_n
            ((1000, None, None, 2), 500),
            ((1000, None, None, 3), 333),
            ((1000, None, None, 4), 250),
            ((1000, None, None, 5), 200),
            ((1000, None, None, 6), 166),
            ((1000, None, None, 7), 142),
        ]

        for args, result in arg_result_list:
            self.assertEqual(
                buhtzology.how_much_elements_per_piece(
                    total_elements_n=args[0],
                    pieces_n=args[1],
                    min_max_per_piece=args[2],
                    min_pieces_n=args[3]
                ),
                result,
                f'args: {args} result: {result}'
            )


class SetupLogging(pyfakefs_ut.TestCase):  # pylint: disable=C0115

    def setUp(self):
        # this do reset all handlers
        importlib.reload(logging)

        self.setUpPyfakefs()

    def test_simple_defaults(self):
        """Default behavior and setup"""

        # log folder doesn't exist yet
        log_dir = pathlib.Path.cwd() / 'logs'

        # name of script is used for log file names
        with mock.patch('sys.argv', ['foobar.py']):
            # suppress stdout
            with mock.patch('logging.StreamHandler.emit'):
                buhtzology.setup_logging()

        # log file exists
        log_path = log_dir / 'foobar.log'
        self.assertTrue(log_path.exists())

        # root loggers handlers and their level's
        self.assertEqual(len(logging.root.handlers), 2)

        # RotatingFileHanlder DEBUG
        self.assertIsInstance(
            logging.root.handlers[0],
            logging.handlers.RotatingFileHandler)
        self.assertEqual(
            logging.root.handlers[0].level,
            logging.DEBUG)

        # StreamHandler INFO
        self.assertIsInstance(
            logging.root.handlers[1],
            logging.StreamHandler)
        self.assertEqual(
            logging.root.handlers[1].level,
            logging.INFO)

    def test_deactivate_handler(self):
        """Deactivate file handler."""

        # console handler only
        buhtzology.setup_logging(file_level=None)

        # root loggers handlers and their level's
        self.assertEqual(len(logging.root.handlers), 1)

        # StreamHandler INFO
        self.assertIsInstance(
            logging.root.handlers[0],
            logging.StreamHandler)
        self.assertEqual(
            logging.root.handlers[0].level,
            logging.INFO)

    def test_log_folder(self):
        """User defined log folder."""
        log_dir = pathlib.Path('mylogs')

        # name of script is used for log file names
        with mock.patch('sys.argv', ['foobar.py']):
            # suppress stdout
            with mock.patch('logging.StreamHandler.emit'):
                buhtzology.setup_logging(log_directory=log_dir)

        # Because of the rotation there always exists
        # two files at the beginning.
        log_path = log_dir / 'foobar.log'
        self.assertTrue(log_path.exists())
        log_path = log_dir / 'foobar.log.1'
        self.assertTrue(log_path.exists())

    @unittest.skipIf(platform.system() == 'Windows', 'Not on Windows')
    def test_log_per_session(self):
        """Session based logging."""
        # name of script is used for log file names
        with mock.patch('sys.argv', ['foobar.py']):
            # suppress stdout
            with mock.patch('logging.StreamHandler.emit'):
                # 3 sessions
                for _ in range(3):
                    buhtzology.setup_logging()

        # log file exists
        log_dir = pathlib.Path.cwd() / 'logs'
        self.assertEqual(
            sorted(log_dir.glob('*')),
            [log_dir / 'foobar.log']
            + [log_dir / f'foobar.log.{idx}' for idx in range(1, 4)])

    def test_execptional_loggers(self):
        """Exceptional loggers."""
        with mock.patch('sys.argv', ['foobar.py']):
            # suppress stdout
            with mock.patch('logging.StreamHandler.emit'):
                buhtzology.setup_logging(
                    exceptional_loggers={
                        'matplotlib.font_manager': logging.ERROR,
                        'PIL': logging.ERROR})

        self.assertEqual(
            logging.getLogger('matplotlib.font_manager').level,
            logging.ERROR)
        self.assertEqual(
            logging.getLogger('PIL').level,
            logging.ERROR)


class ParagraphBreak(unittest.TestCase):
    """Tests about break_paragraph()"""
    # pylint: disable=line-too-long
    def test_three_parts(self):
        """Break pagaragraph into three pieces"""
        sut = [
            # 1234567890    5    0    5    0    5    0    5    0    5    0    5    0    5    0    5    0  # noqa: E501
            'You claim there are problems among us that you need to solve. You use this claim as an',  # noqa: E501
            'excuse to invade our precincts. Many of these problems do not exist. Where there are real',  # noqa: E501
            'conflicts, where there are wrongs, we will identify them and address them by our means. We',  # noqa: E501
            'are forming our own Social Contract. This governance will arise according to the conditions',  # noqa: E501
            'of our world, not yours. Our world is different. Cyberspace consists of transactions,',  # noqa: E501
        ]

        expect = [
            # 1234567890    5    0    5    0    5    0
            'You claim there are problems among us th',
            'excuse to invade our precincts. Many of ',
            'conflicts, where there are wrongs, we wi',
            'are forming our own Social Contract. Thi',
            'of our world, not yours. Our world is di',
            # break
            'at you need to solve. You use this claim',
            'these problems do not exist. Where there',
            'll identify them and address them by our',
            's governance will arise according to the',
            'fferent. Cyberspace consists of transact',
            # break
            ' as an',
            ' are real',
            ' means. We',
            ' conditions',
            'ions,'
        ]

        result = buhtzology.break_paragraph(sut, 40)
        self.assertEqual(expect, result)

    def test_separation_line(self):
        """Extra line between each paragraph piece"""
        sut = [
            # 1234567890    5    0    5    0    5    0    5    0    5    0    5    0    5    0    5    0  # noqa: E501
            'You claim there are problems among us that you need to solve. You use this claim as an',  # noqa: E501
            'excuse to invade our precincts. Many of these problems do not exist. Where there are real',  # noqa: E501
            'conflicts, where there are wrongs, we will identify them and address them by our means. We',  # noqa: E501
            'are forming our own Social Contract. This governance will arise according to the conditions',  # noqa: E501
            'of our world, not yours. Our world is different. Cyberspace consists of transactions,',  # noqa: E501
        ]

        expect = [
            # 1234567890    5    0    5    0    5    0
            'You claim there are problems among us th',
            'excuse to invade our precincts. Many of ',
            'conflicts, where there are wrongs, we wi',
            'are forming our own Social Contract. Thi',
            'of our world, not yours. Our world is di',
            '---',
            'at you need to solve. You use this claim',
            'these problems do not exist. Where there',
            'll identify them and address them by our',
            's governance will arise according to the',
            'fferent. Cyberspace consists of transact',
            '---',
            ' as an',
            ' are real',
            ' means. We',
            ' conditions',
            'ions,'
        ]

        result = buhtzology.break_paragraph(sut, 40, sep_line='---')
        self.assertEqual(expect, result)

    def test_sep_line_default_none(self):
        """None by default"""
        sut = [
            'foo bar 123456',
            'noch mal 1234567'
        ]
        expect = [
            'foo bar 12',
            'noch mal 1',
            '3456',
            '234567'
        ]
        result = buhtzology.break_paragraph(sut, width=10)
        self.assertEqual(result, expect)

    def test_sep_line_empty(self):
        """Empty string as sep line"""
        sut = [
            'foo bar 123456',
            'noch mal 1234567'
        ]
        expect = [
            'foo bar 12',
            'noch mal 1',
            '',
            '3456',
            '234567'
        ]
        result = buhtzology.break_paragraph(sut, width=10, sep_line='')
        self.assertEqual(result, expect)

    def test_sep_line_newline(self):
        """Newline as sep line"""
        sut = [
            'foo bar 123456',
            'noch mal 1234567'
        ]
        expect = [
            'foo bar 12',
            'noch mal 1',
            '\n',
            '3456',
            '234567'
        ]
        result = buhtzology.break_paragraph(sut, width=10, sep_line='\n')
        self.assertEqual(result, expect)

    def test_no_empty_lines(self):
        """Empty lines at the end are removed."""
        sut = [
            # 1234567890    5    0    5    0    5    0    5    0    5    0    5    0    5    0    5    0  # noqa: E501
            'You claim there are problems among us that you need to solve. You use this claim as an',  # noqa: E501
            'excuse to invade our precincts. Many of these problems do not exist. Where there are real',  # noqa: E501
            'conflicts, where there are wrongs, we will identify them and address them by our means. We',  # noqa: E501
            'are forming our own Social Contract. This governance will arise according to the conditions',  # noqa: E501
            'of our world, not yours. Our world is different.',
        ]

        expect = [
            # 1234567890    5    0    5    0    5    0
            'You claim there are problems among us th',
            'excuse to invade our precincts. Many of ',
            'conflicts, where there are wrongs, we wi',
            'are forming our own Social Contract. Thi',
            'of our world, not yours. Our world is di',
            # break
            'at you need to solve. You use this claim',
            'these problems do not exist. Where there',
            'll identify them and address them by our',
            's governance will arise according to the',
            'fferent.',
            # break
            ' as an',
            ' are real',
            ' means. We',
            ' conditions',
        ]

        result = buhtzology.break_paragraph(sut, 40)
        self.assertEqual(expect, result)

    def test_short_line_inbetween(self):
        """One not breakable line in the middle."""
        sut = [
            # 1234567890    5    0    5    0    5    0    5    0    5    0    5    0    5    0    5    0  # noqa: E501
            'You claim there are problems among us that you need to solve. You use this claim as an',  # noqa: E501
            'excuse to invade our precincts. Many of these problems do not exist. Where there are real',  # noqa: E501
            'conflicts,',
            'are forming our own Social Contract. This governance will arise according to the conditions',  # noqa: E501
        ]

        expect = [
            # 1234567890    5    0    5    0    5    0
            'You claim there are problems among us th',
            'excuse to invade our precincts. Many of ',
            'conflicts,',
            'are forming our own Social Contract. Thi',
            # break
            'at you need to solve. You use this claim',
            'these problems do not exist. Where there',
            '',
            's governance will arise according to the',
            # break
            ' as an',
            ' are real',
            '',
            ' conditions',
        ]

        result = buhtzology.break_paragraph(sut, 40)
        self.assertEqual(expect, result)

    def test_break_suffix(self):
        """Suffix at each line"""
        self.maxDiff = None
        sut = [
            # 1234567890    5    0    5    0    5    0    5    0    5    0    5
            'You claim there are problems among us that you need to solve. ',
            'You use this claim as an excuse to invade our precincts. Many ',
            'of these problems do not exist. Where there are real conflicts,',
            'are forming our own Social Contract.',
        ]

        expect = [
            # 1234567890    5    0    5    0    5    0
            'You claim there are problems among us th..',
            'You use this claim as an excuse to invad..',
            'of these problems do not exist. Where th..',
            'are forming our own Social Contract.',
            # break
            'at you need to solve. ',
            'e our precincts. Many ',
            'ere are real conflicts,',
        ]

        result = buhtzology.break_paragraph(sut, 40, suffix='..')
        self.assertEqual(expect, result)

    def test_break_prefix(self):
        """Prefix at each line"""
        self.maxDiff = None
        sut = [
            # 1234567890    5    0    5    0    5    0    5    0    5    0    5
            'You claim there are problems among us that you need to solve. ',
            'You use this claim as an excuse to invade our precincts. Many ',
            'of these problems do not exist. Where there are real conflicts,',
            'are forming our own Social Contract.',
        ]

        expect = [
            # 1234567890    5    0    5    0    5    0
            'You claim there are problems among us th',
            'You use this claim as an excuse to invad',
            'of these problems do not exist. Where th',
            'are forming our own Social Contract.',
            # break
            '..at you need to solve. ',
            '..e our precincts. Many ',
            '..ere are real conflicts,',
        ]

        result = buhtzology.break_paragraph(sut, 40, prefix='..')
        self.assertEqual(expect, result)


class ShortenStrings(unittest.TestCase):  # pylint: disable=C0115

    def test_simple(self):
        """Simple case."""

        vals = 'abcdefghijklmno'  # 15 chars
        vals = [vals] * 3
        limit = 10

        expect = [
            'abc1…klmno',
            'abc2…klmno',
            'abc3…klmno',
        ]

        vals = buhtzology.shorten_strings_but_unique(vals, limit)

        self.assertEqual(vals, expect)

        # limit OK?
        self.assertTrue(all(len(e) <= limit for e in vals))

    def test_limit(self):
        """Limit length."""

        vals = 'abcdefghijklmno'  # 15 chars
        vals = [vals] * 3

        for limit in [5, 10, 14, 15, 16, 20]:
            result = buhtzology.shorten_strings_but_unique(vals, limit)
            # limit OK?
            self.assertTrue(all(len(e) <= limit for e in result))

    def test_omit_string(self):
        """Omit string."""

        vals = 'abcdefghijklmno'  # 15 chars
        vals = [vals] * 3
        limit = 10

        expect_a = [
            'abc1Xklmno',
            'abc2Xklmno',
            'abc3Xklmno',
        ]

        expect_b = [
            'a1_-Tklmno',
            'a2_-Tklmno',
            'a3_-Tklmno',
        ]

        result_a = buhtzology.shorten_strings_but_unique(vals, limit, 'X')
        result_b = buhtzology.shorten_strings_but_unique(vals, limit, '_-T')

        self.assertEqual(result_a, expect_a)
        self.assertEqual(result_b, expect_b)


class NestedDictUpdate(unittest.TestCase):
    """About nested_dict_update()"""
    def test_simple(self):
        """Simple"""
        org = {
            'foo': 7,
            'bar': {
                'a': 'drei',
                'b': 'uhr'
            }
        }
        update = {
            'planet': 'erde',
            'bar': {
                'b': 'wecker'
            },
            'ecke': 9
        }

        expect = {
            'planet': 'erde',
            'foo': 7,
            'bar': {
                'a': 'drei',
                'b': 'wecker'
            },
            'ecke': 9
        }

        self.assertDictEqual(
            buhtzology.nested_dict_update(org, update),
            expect)


class ConfigTests(pyfakefs_ut.TestCase):
    """Test the config file"""
    def setUp(self):
        self.setUpPyfakefs()
        Buhtzology.config = None

    def test_02_no_file(self):
        """No config file"""

        # reset config
        buhtzology.config = buhtzology.read_config_data()

        self.assertEqual(
            buhtzology.config['bandas']['decrease_workers_by'], 0)

    def test_01_file_found(self):
        """Config file exist"""

        content = [
            'foo = "Bar"',
            '[bandas]',
            'decrease_workers_by = 1'
        ]
        content = '\n'.join(content)

        fp = pathlib.Path.cwd() / 'conf.toml'
        fp.write_text(content)

        # reset config
        buhtzology.config = buhtzology.read_config_data(fp)

        self.assertEqual(buhtzology.config['foo'], 'Bar')
        self.assertEqual(buhtzology.config['bandas']['decrease_workers_by'], 1)
