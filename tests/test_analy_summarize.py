"""Tests about analy module"""
import unittest
import random
import pandas
from pandas.api.types import CategoricalDtype
from buhtzology import analy


# pylint: disable-next=missing-class-docstring
class Summarize(unittest.TestCase):
    # pylint: disable=missing-function-docstring
    @classmethod
    def setUpClass(cls):
        random.seed(0)
        k = 349
        cls.data = pandas.DataFrame({
            'Planet': random.choices(['Trill', 'Bajor', 'Betazed'], k=k),
            'Person': random.choices(
                ['Worf', 'Sarek', 'Picard', 'Diana', 'Quark'], k=k),
            'Gender': random.choices(
                ['male', 'female', 'androgyn', 'diverse', 'unknown'], k=k),
            'Latinum': [
                val / 10 for val in random.choices(range(1, 100), k=k)],
            'Age': random.choices(range(1, 25), k=k),
            'Guess': random.choices(range(0, 10000), k=k),
            'Parsek': random.choices(range(0, 60000), k=k),
        })

    def test_simple_defaults(self):
        sut = analy.summarize(
            data=__class__.data,
            values_in='Person')

        self.assertEqual(sut.shape, (7, 1))
        self.assertEqual(sut.columns.tolist(), ['n (%)'])
        self.assertCountEqual(
            sut.index.tolist(),
            [
                'Total',
                'Person',
                '    Sarek',
                '    Worf',
                '    Diana',
                '    Picard',
                '    Quark'
            ]
        )

        idx_person = sut.index.isin(['Person'])

        self.assertEqual(sut.loc[idx_person, 'n (%)'].tolist(), [''])

        for val in sut.loc[~idx_person, 'n (%)']:
            # check for integer with float in brackets behind it
            # e.g. 32 (2.6)
            # e.g. 32 (6)
            # e.g. NOT 32.4 (5.6)
            self.assertRegex(val, r'^[0-9]+\s\([0-9]+\.*[0-9]*\)$')

    def test_simple_more_variables(self):
        sut = analy.summarize(
            data=__class__.data,
            values_in=['Person', 'Planet'])

        self.assertEqual(sut.shape, (11, 1))
        self.assertEqual(sut.columns.tolist(), ['n (%)'])
        self.assertCountEqual(
            sut.index.tolist(),
            [
                'Total',
                'Person',
                '    Sarek',
                '    Worf',
                '    Diana',
                '    Picard',
                '    Quark',
                'Planet',
                '    Bajor',
                '    Betazed',
                '    Trill',
            ]
        )

    def test_group_by(self):
        sut = analy.summarize(
            data=__class__.data,
            values_in='Person',
            group_by='Planet')

        self.assertEqual(sut.shape, (7, 4))

        # Column labels
        self.assertCountEqual(
            sut.columns.tolist(),
            [
                ('Betazed', 'n (%)'),
                ('Bajor', 'n (%)'),
                ('Trill', 'n (%)'),
                ('Total', 'n (%)')
            ]
        )

        # Rows labels
        self.assertCountEqual(
            sut.index.tolist(),
            [
                'Total',
                'Person',
                '    Sarek',
                '    Worf',
                '    Diana',
                '    Picard',
                '    Quark'
            ]
        )

    def test_group_by_more_variables(self):
        sut = analy.summarize(
            data=__class__.data,
            values_in=['Person', 'Gender'],
            group_by='Planet')

        self.assertEqual(sut.shape, (13, 4))

        # Column labels
        self.assertCountEqual(
            sut.columns.tolist(),
            [
                ('Betazed', 'n (%)'),
                ('Bajor', 'n (%)'),
                ('Trill', 'n (%)'),
                ('Total', 'n (%)')
            ]
        )

        # Rows labels
        self.assertCountEqual(
            sut.index.tolist(),
            [
                'Total',
                'Person',
                '    Sarek',
                '    Worf',
                '    Diana',
                '    Picard',
                '    Quark',
                'Gender',
                '    male',
                '    female',
                '    unknown',
                '    diverse',
                '    androgyn'
            ]
        )

    def test_combine_false(self):
        sut = analy.summarize(
            data=__class__.data,
            values_in='Person',
            combine=False)

        self.assertEqual(sut.shape, (7, 2))
        self.assertEqual(sut.columns.tolist(), ['n', '%'])

    def test_combine_false_groupby(self):
        sut = analy.summarize(
            data=__class__.data,
            values_in='Person',
            group_by='Planet',
            combine=False)

        self.assertEqual(sut.shape, (7, 8))
        self.assertEqual(
            sut.columns.tolist(),
            [
                ('Bajor', 'n'),
                ('Bajor', '%'),
                ('Betazed', 'n'),
                ('Betazed', '%'),
                ('Trill', 'n'),
                ('Trill', '%'),
                ('Total', 'n'),
                ('Total', '%')
            ]
        )

    def test_combine_reverted(self):
        sut = analy.summarize(
            data=__class__.data,
            values_in='Person',
            frequency=(False, True),
            combine=True)

        self.assertEqual(sut.columns.tolist(), ['% (n)'])

    def test_two_columns_same_values(self):
        """Row index not unique.

        The key of this test is that the resulting table will have a row index
        that is not unique.
        """

        df = pandas.DataFrame({
            'Foo': list('YAAAABBBXYZ'),
            'Bar': list('TBYWRRRWWWT'),
            'Group': list('GGHGHGHGHHG')
        })

        sut = analy.summarize(df, values_in=['Foo', 'Bar'], group_by='Group')

        self.assertEqual(sut.shape, (13, 3))
        self.assertEqual(
            sut.columns.to_list(),
            [
                ('G', 'n (%)'),
                ('H', 'n (%)'),
                ('Total', 'n (%)')
            ]
        )

        self.assertEqual(sut.index[0], 'Total')
        self.assertEqual(sut.index[1], 'Foo')
        self.assertEqual(sut.index[7], 'Bar')
        self.assertCountEqual(
            [e.strip() for e in sut.index.to_list()[2:7]],
            list('ABXYZ'))
        self.assertCountEqual(
            [e.strip() for e in sut.index.to_list()[8:]],
            list('BRTWY'))

    def test_without_total_but_groupby(self):
        """Without total column while using groupby"""
        sut = analy.summarize(
            data=self.data,
            values_in=['Gender', 'Person'],
            group_by='Planet',
            total=False,
        )

        self.assertEqual(sut.shape, (13, 3))
        self.assertEqual(
            sut.columns.tolist(),
            [
                ('Bajor', 'n (%)'),
                ('Betazed', 'n (%)'),
                ('Trill', 'n (%)')
            ]
        )

    def test_columns_and_rows_with_ordered_category(self):
        """Column ordered if ordered categorical."""

        sut_data = self.data.copy()
        sut_data['Year'] = random.choices([2018, 2016, 2017], k=len(sut_data))
        sut_data['Order'] = random.choices(list('CBA'), k=len(sut_data))

        sut_data.Year = sut_data.Year.astype(
            CategoricalDtype([2016, 2017, 2018], ordered=True))
        sut_data.Order = sut_data.Order.astype(
            CategoricalDtype(list('ABC'), ordered=True))

        sut = analy.summarize(
            data=sut_data,
            values_in=['Gender', 'Order'],
            group_by='Year',
        )

        # The last four rows are "Order"
        order_labels = [label.strip() for label in sut.iloc[-4:].index]
        self.assertEqual(order_labels, ['Order', 'A', 'B', 'C'])

        # columns
        self.assertEqual(
            sut.columns.tolist(),
            [
                (2016, 'n (%)'),
                (2017, 'n (%)'),
                (2018, 'n (%)'),
                ('Total', 'n (%)')
            ]
        )


class FrequencyArgument(unittest.TestCase):
    """Test frequency argument"""
    @classmethod
    def setUpClass(cls):
        random.seed(0)
        k = 48
        cls.data = pandas.DataFrame({
            'Person': random.choices(
                ['Worf', 'Sarek', 'Picard', 'Diana', 'Quark'], k=k),
            'Planet': random.choices(['Trill', 'Bajor'], k=k),
        })

    def test_default(self):
        """Default is frequency and percent."""
        sut = analy.summarize(
            data=__class__.data,
            values_in='Person',
            combine=False)

        self.assertEqual(sut.columns.to_list(), ['n', '%'])
        self.assertEqual(sut.shape, (7, 2))

    def test_default_groupby(self):
        """Default is frequency and percent."""
        sut = analy.summarize(
            data=__class__.data,
            values_in='Person',
            group_by='Planet',
        )

        self.assertEqual(
            sut.columns.to_list(),
            [
                ('Bajor', 'n (%)'),
                ('Trill', 'n (%)'),
                ('Total', 'n (%)')
            ]
        )
        self.assertEqual(sut.shape, (7, 3))

    def test_false_true(self):
        """Other way around"""
        sut = analy.summarize(
            data=__class__.data,
            values_in='Person',
            frequency=(False, True),
            combine=False)

        self.assertEqual(sut.columns.to_list(), ['%', 'n'])

    def test_false_true_groupby(self):
        """Other way around"""
        sut = analy.summarize(
            data=__class__.data,
            values_in='Person',
            group_by='Planet',
            frequency=(False, True)
        )

        self.assertEqual(
            sut.columns.to_list(),
            [
                ('Bajor', '% (n)'),
                ('Trill', '% (n)'),
                ('Total', '% (n)')
            ]
        )
        self.assertEqual(sut.shape, (7, 3))

    def test_false(self):
        """False means percentages only."""
        sut = analy.summarize(
            data=__class__.data,
            values_in='Person',
            frequency=False)

        self.assertEqual(sut.columns.to_list(), ['%'])
        self.assertEqual(sut.shape, (7, 1))

    def test_false_groupby(self):
        """frequency=False but groupby"""
        sut = analy.summarize(
            data=__class__.data,
            values_in='Person',
            group_by='Planet',
            frequency=True  # only 'n'
        )

        self.assertEqual(
            sut.columns.to_list(),
            [
                ('Bajor', 'n'),
                ('Trill', 'n'),
                ('Total', 'n')
            ]
        )
        self.assertEqual(sut.shape, (7, 3))

    def test_true(self):
        """True means frequencies only."""
        sut = analy.summarize(
            data=__class__.data,
            values_in='Person',
            frequency=True)

        self.assertEqual(sut.columns.to_list(), ['n'])
        self.assertEqual(sut.shape, (7, 1))
