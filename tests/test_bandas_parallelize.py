"""Testing parallelizing jobs"""
import unittest
import pandas
from pandas.testing import assert_frame_equal
from buhtzology import bandas


def _worker_for_parallelize_pieces(a, b, sub_df):
    sub_df['B'] = f'{b}{a}'

    return sub_df


def _worker_for_drop_columns(sub_df):
    sub_df = sub_df.loc[:, ['A', 'B']]

    return sub_df


def _worker_for_drop_rows(sub_df):
    sub_df = sub_df.iloc[0:1]  # first row only

    return sub_df


def _worker_for_parallelize_groups(a, b, sub_df):
    sub_df['B'] = f'{b}{a}'

    return sub_df


class Parallelize(unittest.TestCase):
    """Parallelization via multiprocessing."""

    def test_parallelize_pieces(self):
        """Parallelize n pieces."""
        # test data
        df = pandas.DataFrame(range(100), columns=['A'])

        result = bandas.parallelize_job_on_dataframe(
            data=df,
            worker_func=_worker_for_parallelize_pieces,
            worker_args=('X', 8))

        expected = df.copy()
        expected['B'] = '8X'

        assert_frame_equal(
            result.sort_values('A').reset_index(),
            expected.sort_values('A').reset_index())

    def test_drop_columns(self):
        """DroPping columns in worker."""
        df = pandas.DataFrame(range(100), columns=['A'])
        df['B'] = 7
        expected = df.copy()
        df['C'] = 8
        df['d'] = 'foo'

        result = bandas.parallelize_job_on_dataframe(
            data=df,
            worker_func=_worker_for_drop_columns)

        self.assertEqual(list(result.columns), ['A', 'B'])
        assert_frame_equal(
            result.sort_values('A').reset_index(),
            expected.sort_values('A').reset_index())

    def test_drop_rows(self):
        """Dropping rows in worker."""
        df = pandas.DataFrame(range(100), columns=['A'])

        result = bandas.parallelize_job_on_dataframe(
            data=df,
            worker_func=_worker_for_drop_rows,
            n_pieces=2)

        self.assertEqual(len(result), 2)
        self.assertEqual(sorted(result.A), [0, 50])

    def test_parallelize_groups(self):
        """Parallelize groups."""
        # test data
        x = pandas.DataFrame(range(30), columns=['A'])
        x['group'] = 'Foo'
        y = pandas.DataFrame(range(70), columns=['A'])
        y['group'] = 'Bar'
        df = pandas.concat([x, y])

        # bandas.cut_into_pieces(df, 6)

        result = bandas.parallelize_job_on_dataframe(
            data=df,
            group_column='group',
            worker_func=_worker_for_parallelize_groups,
            worker_args=('X', 8))

        expected = df.copy()
        expected['B'] = '8X'

        assert_frame_equal(
            result.sort_values(['group', 'A', 'B']),
            expected.sort_values(['group', 'A', 'B'])
        )
