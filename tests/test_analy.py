"""Tests about analy module"""
import unittest
import random
import statistics
import numpy
import pandas
from buhtzology import analy


# pylint: disable-next=missing-class-docstring
class JoinColumns(unittest.TestCase):
    # pylint: disable=protected-access,missing-function-docstring

    def setUp(self):
        random.seed(0)

    def test_simple(self):
        data = pandas.DataFrame({
            'n': [10, 20, 30],
            '%': [5, 40.2, 81.9]
        })

        sut = analy._join_columns(
            data=data,
            columns=['n', '%'],
            joined_column_name='n (%)',
        )

        self.assertEqual(sut.shape, (3, 1))
        self.assertEqual(sut.columns.to_list(), ['n (%)'])
        self.assertEqual(
            sut.iloc[:, 0].tolist(),
            ['10 (5)', '20 (40.2)', '30 (81.9)']
        )

    def test_nan_default(self):
        data = pandas.DataFrame({
            'n': [None, pandas.NA, numpy.nan, 4.56, None, 10.2],
            '%': [numpy.nan, pandas.NaT, 1.23, pandas.NA, 7.89, numpy.nan]
        })

        sut = analy._join_columns(
            data=data,
            columns=['n', '%'],
            joined_column_name='n (%)',
        )

        self.assertEqual(
            sut.iloc[:, 0].tolist(),
            [
                'None (nan)',
                '<NA> (NaT)',
                'nan (1.23)',
                '4.56 (<NA>)',
                'None (7.89)',
                '10.2 (nan)'
            ]
        )

    def test_reverse(self):
        data = pandas.DataFrame({
            'n': [10, 20, 30],
            '%': [5, 40.2, 81.9]
        })

        sut = analy._join_columns(
            data=data,
            columns=('%', 'n'),
            joined_column_name='% (n)',
        )

        self.assertEqual(sut.shape, (3, 1))
        self.assertEqual(sut.columns.to_list(), ['% (n)'])
        self.assertEqual(
            sut.iloc[:, 0].tolist(),
            ['5 (10)', '40.2 (20)', '81.9 (30)']
        )

    def test_fstring(self):
        data = pandas.DataFrame({
            'n': [10, 20, 30],
            '%': [5, 40.2, 81.9]
        })

        sut = analy._join_columns(
            data=data,
            columns=('%', 'n'),
            joined_column_name='a -- b',
            join_fstring='{}a -- {}b'
        )

        self.assertEqual(sut.columns.to_list(), ['a -- b'])
        self.assertEqual(
            sut.iloc[:, 0].tolist(),
            ['5a -- 10b', '40.2a -- 20b', '81.9a -- 30b']
        )

    def test_rounding(self):
        data = pandas.DataFrame({
            'n': [10, 20, 30],
            '%': [5, 40.123, 81.155]
        })

        sut = analy._join_columns(
            data=data,
            columns=['n', '%'],
            joined_column_name='n (%)',
            round_digits=2
        )

        self.assertEqual(sut.shape, (3, 1))
        self.assertEqual(sut.columns.to_list(), ['n (%)'])
        self.assertEqual(
            sut.iloc[:, 0].tolist(),
            ['10 (5)', '20 (40.12)', '30 (81.16)']
        )

    def test_rounding_separate(self):
        """Number of rounding digits can handled different between the two
        columns to join.
        """
        data = pandas.DataFrame({
            'X': [10.3, 20.1234, 30.9876],
            'Y': [5, 40.123, 81.155387]
        })

        sut = analy._join_columns(
            data=data,
            columns=['X', 'Y'],
            joined_column_name='n (%)',
            round_digits=(2, 4)
        )

        self.assertEqual(
            sut.iloc[:, 0].tolist(),
            ['10.3 (5)', '20.12 (40.123)', '30.99 (81.1554)']
        )

    def test_default_arguments(self):
        data = pandas.DataFrame({
            'foo': [10, 20, 30],
            'bar': [5, 40.2, 81.9]
        })

        sut = analy._join_columns(data)

        self.assertEqual(sut.shape, (3, 1))
        self.assertEqual(sut.columns.to_list(), ['foo (bar)'])
        self.assertEqual(
            sut.iloc[:, 0].tolist(),
            ['10 (5)', '20 (40.2)', '30 (81.9)']
        )

    def test_data_name_row(self):
        """Input data like shown here comes from frequencies_and_percentages()

        The key aspect here is that the first row contain None/NA/nan value
        because that row is the data name (e.g. "Gender" or "Age Group").
        But integers in a column that also containing a missing value are
        converted into floats.

        I first thought to handle it here. But I decided it otherway because
        this problem is to high-level for such a low-level helper function.
        """
        data = pandas.DataFrame({
            'n': [None, 10, 20],
            '%': [pandas.NA, 40.2, 81.9]
        })

        sut = analy._join_columns(
            data=data,
            columns=['n', '%'],
            joined_column_name='n (%)',
        )

        self.assertEqual(sut.shape, (3, 1))
        self.assertEqual(sut.columns.to_list(), ['n (%)'])

        self.assertEqual(
            sut.iloc[:, 0].tolist(),
            ['nan (<NA>)', '10 (40.2)', '20 (81.9)']
        )

        # Now I show how to deal with it on a higher level.
        # Use a na-value supporting data type from pandas.
        data.n = data.n.astype('Int32')

        sut = analy._join_columns(
            data=data,
            columns=['n', '%'],
            joined_column_name='n (%)',
        )

        sut = sut.iloc[:, 0].tolist()
        self.assertIn(sut[0], ('<NA> (<NA>)', 'nan (<NA>)'))
        self.assertEqual(sut[1], '10 (40.2)')
        self.assertEqual(sut[2], '20 (81.9)')


# pylint: disable-next=missing-class-docstring
class Aggregate(unittest.TestCase):
    # pylint: disable=missing-function-docstring
    @classmethod
    def setUpClass(cls):
        random.seed(0)
        k = 100
        cls.data = pandas.DataFrame({
            'Town': random.choices(['Tokyo', 'Berlin'], k=k),
            'Product': random.choices(['Apple', 'Banana', 'Onion'], k=k),
            'Price': [val / 10 for val in random.choices(range(1, 100), k=k)],
            'Count': random.choices(range(1, 25), k=k),
            'foo': random.choices(range(0, 10000), k=k),
            'bar': random.choices(range(0, 60000), k=k),
        })

    def test_one_value_column(self):
        sut = analy.aggregate(
            data=__class__.data,
            group_by='Town',
            values_in='Price',
            aggfunc=statistics.mean)

        # Shape
        self.assertEqual(sut.shape, (1, 3))

        # Row label
        self.assertEqual(sut.index, ['Price'])

        # Column labels
        self.assertCountEqual(
            sut.columns.levels[0], ['Tokyo', 'Berlin', 'Total'])
        self.assertEqual(set(sut.columns.levels[1]), {'Mean'})

    def test_customized_value_column_label(self):
        sut = analy.aggregate(
            data=__class__.data,
            group_by='Town',
            values_in={'Price': 'Geld'},
            aggfunc=statistics.mean)

        # Shape
        self.assertEqual(sut.shape, (1, 3))

        # Row label
        self.assertEqual(sut.index, ['Geld'])

    def test_two_value_column(self):
        sut = analy.aggregate(
            data=__class__.data,
            group_by='Town',
            values_in=['Count', 'Price'],
            aggfunc=statistics.mean)

        # Shape
        self.assertEqual(sut.shape, (2, 3))

        # Row label
        self.assertEqual(sut.index.tolist(), ['Count', 'Price'])

        # Column labels
        self.assertCountEqual(
            sut.columns.levels[0], ['Tokyo', 'Berlin', 'Total'])
        self.assertEqual(set(sut.columns.levels[1]), {'Mean'})

    def test_two_aggfunc(self):
        sut = analy.aggregate(
            data=__class__.data,
            group_by='Town',
            values_in='Price',
            aggfunc=[statistics.mean, sum])

        # Shape
        self.assertEqual(sut.shape, (1, 6))

        # Row label
        self.assertEqual(sut.index, ['Price'])

        # Column labels
        self.assertEqual(
            sut.columns.tolist(),
            [
                ('Berlin', 'Mean'),
                ('Berlin', 'Sum'),
                ('Tokyo', 'Mean'),
                ('Tokyo', 'Sum'),
                ('Total', 'Mean'),
                ('Total', 'Sum')
            ]
        )

    def test_customized_aggfunc_label(self):
        sut = analy.aggregate(
            data=__class__.data,
            values_in='Price',
            aggfunc={statistics.mean: 'Mittelwert'},
        )

        self.assertEqual(sut.shape, (1, 1))
        self.assertEqual(sut.columns.tolist(), ['Mittelwert'])
        self.assertEqual(sut.index.tolist(), ['Price'])

    def test_customized_aggfunc_labels(self):
        sut = analy.aggregate(
            data=__class__.data,
            values_in='Price',
            aggfunc={statistics.mean: 'MD', statistics.stdev: 'FourtyTwo'},
        )

        self.assertEqual(sut.shape, (1, 2))
        self.assertEqual(sut.columns.tolist(), ['MD', 'FourtyTwo'])
        self.assertEqual(sut.index.tolist(), ['Price'])

    def test_no_group_column(self):
        sut = analy.aggregate(
            data=__class__.data,
            values_in='Price',
            aggfunc=statistics.mean
        )

        self.assertEqual(sut.shape, (1, 1))
        self.assertEqual(sut.columns.tolist(), ['Mean'])
        self.assertEqual(sut.index.tolist(), ['Price'])

    def test_no_group_column_two_values_one_aggfunc(self):
        sut = analy.aggregate(
            data=__class__.data,
            values_in=['Price', 'Count'],
            aggfunc=statistics.mean
        )

        self.assertEqual(sut.shape, (2, 1))
        self.assertEqual(sut.columns.tolist(), ['Mean'])
        self.assertEqual(sut.index.tolist(), ['Price', 'Count'])

    def test_no_group_column_one_value_two_aggfunc(self):
        sut = analy.aggregate(
            data=__class__.data,
            values_in='Price',
            aggfunc=[statistics.mean, sum])

        self.assertEqual(sut.shape, (1, 2))
        self.assertEqual(sut.columns.tolist(), ['Mean', 'Sum'])
        self.assertEqual(sut.index.tolist(), ['Price'])

    def test_no_group_column_two_values_and_aggfunc(self):
        sut = analy.aggregate(
            data=__class__.data,
            values_in=['Price', 'Count'],
            aggfunc=[statistics.mean, sum])

        self.assertEqual(sut.shape, (2, 2))
        self.assertEqual(sut.columns.tolist(), ['Mean', 'Sum'])
        self.assertEqual(sut.index.tolist(), ['Price', 'Count'])

    def test_full_features(self):
        sut = analy.aggregate(
            data=__class__.data,
            values_in={
                'Price': 'Kosten',
                'Count': 'Menge'
            },
            aggfunc={
                statistics.mean: 'Mittel',
                sum: 'Summiert'
            },
            group_by='Town',
            total='YoLo',
        )

        self.assertEqual(sut.shape, (2, 6))
        self.assertEqual(
            sut.columns.tolist(),
            [
                ('Berlin', 'Mittel'),
                ('Berlin', 'Summiert'),
                ('Tokyo', 'Mittel'),
                ('Tokyo', 'Summiert'),
                ('YoLo', 'Mittel'),
                ('YoLo', 'Summiert')
            ])
        self.assertEqual(sut.index.tolist(), ['Kosten', 'Menge'])

    def test_calculations(self):
        """Take care that values calculated and placed correct."""
        # pylint: disable=too-many-locals
        data = __class__.data.copy()

        sut = analy.aggregate(
            data=data,
            values_in=['Price', 'Count'],
            aggfunc=[
                sum,
                pandas.Series.mean,
                pandas.Series.median,
                pandas.Series.std,
            ],
            group_by='Town',
        )

        # Calculate the expected values
        mean_total_price = data.Price.mean()
        mean_tokyo_price = data.loc[data.Town.eq('Tokyo')].Price.mean()
        mean_berlin_price = data.loc[data.Town.eq('Berlin')].Price.mean()
        mean_total_count = data.Count.mean()
        mean_tokyo_count = data.loc[data.Town.eq('Tokyo')].Count.mean()
        mean_berlin_count = data.loc[data.Town.eq('Berlin')].Count.mean()

        median_total_price = data.Price.median()
        median_tokyo_price = data.loc[data.Town.eq('Tokyo')].Price.median()
        median_berlin_price = data.loc[data.Town.eq('Berlin')].Price.median()
        median_total_count = data.Count.median()
        median_tokyo_count = data.loc[data.Town.eq('Tokyo')].Count.median()
        median_berlin_count = data.loc[data.Town.eq('Berlin')].Count.median()

        sum_total_price = data.Price.sum()
        sum_tokyo_price = data.loc[data.Town.eq('Tokyo')].Price.sum()
        sum_berlin_price = data.loc[data.Town.eq('Berlin')].Price.sum()
        sum_total_count = data.Count.sum()
        sum_tokyo_count = data.loc[data.Town.eq('Tokyo')].Count.sum()
        sum_berlin_count = data.loc[data.Town.eq('Berlin')].Count.sum()

        stdev_total_price = data.Price.std()
        stdev_tokyo_price = data.loc[data.Town.eq('Tokyo')].Price.std()
        stdev_berlin_price = data.loc[data.Town.eq('Berlin')].Price.std()
        stdev_total_count = data.Count.std()
        stdev_tokyo_count = data.loc[data.Town.eq('Tokyo')].Count.std()
        stdev_berlin_count = data.loc[data.Town.eq('Berlin')].Count.std()

        self.assertEqual(sut.shape, (2, 12))
        self.assertEqual(sut.index.tolist(), ['Price', 'Count'])
        self.assertEqual(
            sut.columns.tolist(),
            [
                ('Berlin', 'Sum'),
                ('Berlin', 'Mean'),
                ('Berlin', 'Median'),
                ('Berlin', 'SD'),
                ('Tokyo', 'Sum'),
                ('Tokyo', 'Mean'),
                ('Tokyo', 'Median'),
                ('Tokyo', 'SD'),
                ('Total', 'Sum'),
                ('Total', 'Mean'),
                ('Total', 'Median'),
                ('Total', 'SD'),
            ]
        )

        expected_price_row = [
            sum_berlin_price,
            mean_berlin_price,
            median_berlin_price,
            stdev_berlin_price,
            sum_tokyo_price,
            mean_tokyo_price,
            median_tokyo_price,
            stdev_tokyo_price,
            sum_total_price,
            mean_total_price,
            median_total_price,
            stdev_total_price,
        ]

        # Using numpy because of rounding issues and
        # unittest.assertAlmostEqual() do not support lists
        numpy.testing.assert_almost_equal(
            sut.loc['Price'].tolist(),
            expected_price_row
        )

        expected_count_row = [
            sum_berlin_count,
            mean_berlin_count,
            median_berlin_count,
            stdev_berlin_count,
            sum_tokyo_count,
            mean_tokyo_count,
            median_tokyo_count,
            stdev_tokyo_count,
            sum_total_count,
            mean_total_count,
            median_total_count,
            stdev_total_count,
        ]

        numpy.testing.assert_almost_equal(
            sut.loc['Count'].tolist(),
            expected_count_row
        )

    def test_aggfunc_existing_label(self):
        sut = analy.aggregate(
            data=__class__.data,
            values_in='Price',
            aggfunc=[statistics.mean, sum])

        # Column labels
        self.assertEqual(sut.columns.tolist(), ['Mean', 'Sum'])

    def test_aggfunc_unknown_label(self):
        def foobar(vals):
            return vals.sum()

        sut = analy.aggregate(
            data=__class__.data,
            values_in='Price',
            aggfunc=[statistics.mean, foobar, min])

        # Column labels
        self.assertEqual(sut.columns.tolist(), ['Mean', 'foobar', 'min'])

    def test_round_digits(self):
        """Take care that values calculated and placed correct."""
        data = pandas.DataFrame({
            'vals': [1, 2, 1]
        })

        # Now with rounding
        sut = analy.aggregate(
            data=data,
            values_in=['vals'],
            aggfunc=[
                pandas.Series.mean,
            ],
            round_digits=2
        )

        self.assertEqual(
            str(sut.loc['vals', 'Mean']),
            '1.33'
        )

    def test_round_digits_not_by_default(self):
        """No rounding by default"""
        data = pandas.DataFrame({
            'vals': [1, 2, 1]
        })

        sut = analy.aggregate(
            data=data,
            values_in=['vals'],
            aggfunc=[
                pandas.Series.mean,
            ],
        )

        self.assertEqual(sut.shape, (1, 1))
        self.assertEqual(sut.index.tolist(), ['vals'])

        # Not rounded
        self.assertTrue(str(sut.loc['vals', 'Mean']).startswith('1.333333'))

    def test_round_zero_digits(self):
        """Zero decimal digits"""
        data = pandas.DataFrame({
            'vals': [1, 2, 1]
        })

        # Now with rounding
        sut = analy.aggregate(
            data=data,
            values_in=['vals'],
            aggfunc=[
                pandas.Series.mean,
            ],
            round_digits=0
        )

        self.assertEqual(
            str(sut.loc['vals', 'Mean']),
            '1'
        )

    def test_nan_and_two_aggfunc(self):
        """Covers Issue #144.

        Using two aggfunc and one column has some NA in it.
        """
        df = pandas.DataFrame({
            'OID': range(7),
            'A': range(20, 27),
            'B': ['R', 'T', 'T', 'T', pandas.NA, 'R', pandas.NA]
        })

        # no exception
        sut = analy.aggregate(
            df,
            values_in='A',
            aggfunc=[pandas.Series.mean, sum],
            group_by='B'
        )

        self.assertEqual(sut.index, ['A'])
        self.assertEqual(len(sut.columns.levels), 2)
        self.assertEqual(
            sut.columns.to_list()[0:4],
            [
                ('R', 'Mean'),
                ('R', 'Sum'),
                ('T', 'Mean'),
                ('T', 'Sum'),
            ]
        )
        self.assertTrue(pandas.isna(sut.columns.to_list()[4][0]))
        self.assertEqual(sut.columns.to_list()[4][1], 'Mean')
        self.assertTrue(pandas.isna(sut.columns.to_list()[5][0]))
        self.assertEqual(sut.columns.to_list()[5][1], 'Sum')
        self.assertEqual(
            sut.columns.to_list()[6:],
            [
                ('Total', 'Mean'),
                ('Total', 'Sum')
            ]
        )
