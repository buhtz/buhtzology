"""Testing codestyle, code smell, typos and misspellings"""
import unittest
import pathlib
import subprocess
import logging
from importlib import metadata
from typing import Iterable

# Not all linters follow strict PEP8
PEP8_MAX_LINE_LENGTH = 79


class MirrorMirrorOnTheWall(unittest.TestCase):
    """Check all py-files in the package incl. its test files if they are
    PEP8 compliant.
    """

    @classmethod
    def setUpClass(cls):
        cls.PACKAGE_NAME = 'buhtzology'
        cls.TEST_FOLDER_NAME = pathlib.Path(__file__).parent.name
        cls.test_files = cls._get_test_files()
        cls.package_files = cls._get_package_files()
        cls.collected_py_files = cls.test_files + cls.package_files

    @classmethod
    def _get_test_files(cls) -> Iterable[pathlib.Path]:
        """Return list of Python files in the tests folder"""
        test_path = pathlib.Path.cwd() / cls.TEST_FOLDER_NAME

        return list(test_path.rglob('*.py'))

    @classmethod
    def _get_package_files(cls) -> Iterable[pathlib.Path]:
        """Return list of all py-files of the (editable) installed package."""

        # get all files in this package
        files = metadata.files(cls.PACKAGE_NAME)
        files = [f.locate() for f in files]

        # py-files only
        files = filter(
            lambda fp: fp.suffix == '.py'  # Python file
            and not fp.name.startswith('.#')  # no editor temp files
            and fp.parent.name is not cls.TEST_FOLDER_NAME,  # no test file
            files)

        return list(files)

    def test010_ruff(self):
        """Ruff in default mode."""
        cmd = [
            'ruff',
            'check',
            '--line-length', str(PEP8_MAX_LINE_LENGTH),
            # Additionally activate subset of PyLint (PL)
            # and PyCodestyle (E, W), flake8-bandit (S), flake8-bugbear (B),
            # flake8-builtins (A), flake8-print (T20), flake8-quotes (Q),
            # flake8-commas (COM) rules
            '--extend-select=E,W,PL,B,A,T20,Q,COM',
            # But ignore...
            '--ignore='
            'PLR2004,'  # magic value in comparison
            'PLW2901,'  # `for` loop variable `e` overwritten by
                        # assignment target
            'COM812',   # Trailing comma missing
            # Ruff counting branches different from PyLint.
            # See: <https://www.reddit.com/r/learnpython/comments/
            #      1buojae/comment/kxu0mp3>
            '--config', 'pylint.max-branches=13',
            '--config', 'flake8-quotes.inline-quotes = "single"',
            # one error per line (no context lines)
            '--output-format=concise',
            '--quiet',
            '.',
        ]

        r = subprocess.run(
            cmd,
            check=False,
            universal_newlines=True,
            capture_output=True
        )

        # No errors other then linter rules
        self.assertIn(r.returncode, [0, 1], r.stderr)

        error_n = len(r.stdout.splitlines())
        if error_n > 0:
            logging.error(r.stdout)
        self.assertEqual(0, error_n, f'Ruff found {error_n} problems.')

    def test020_flake8(self):
        """Flake8 in default mode."""
        cmd = [
            'flake8',
            f'--max-line-length={PEP8_MAX_LINE_LENGTH}',
            # '--enable-extensions='
        ]

        cmd.extend(self.collected_py_files)

        proc = subprocess.run(
            cmd,
            check=False,
            universal_newlines=True,
            capture_output=True
        )

        error_n = len(proc.stdout.splitlines())
        if error_n > 0:
            print(proc.stdout)  # noqa: T201

        self.assertEqual(0, error_n, f'Flake8 found {error_n} problem(s).')

        # any other errors?
        self.assertEqual(proc.stderr, '')

    def test030_pylint(self):
        """Use Pylint to check for specific error codes.

        Some facts about PyLint
         - It is one of the slowest available linters.
         - It is able to catch lints none of the other linters
        """

        # Pylint base command
        cmd = [
            'pylint',
            # # Output format (default: text)
            # '--output-format=colorized',
            # Storing results in a pickle file is unnecessary
            '--persistent=n',
            # autodetec number of parallel jobs (pylint default)
            '--jobs=0',
            # Disable scoring  ("Your code has been rated at xx/10")
            '--score=n',
            # PEP8 conform line length (see PyLint Issue #3078)
            f'--max-line-length={PEP8_MAX_LINE_LENGTH}',
            # prevent false-positive no-module-member errors
            '--extension-pkg-allow-list=lxml',
            # List of members which are set dynamically and missed by pylint
            # inference system, and so shouldn't trigger E1101 when accessed.
            '--generated-members=WD_ALIGN_PARAGRAPH,WD_ORIENT,'
            'PARAGRAPH,winerror',
            # Allowlist variable names
            '--good-names=idx,fp,df,maxDiff',
            # Allow fstrings when logging
            '--disable=logging-fstring-interpolation',
            # Ignore nothing (e.g. too-many-arguments).
            # https://github.com/pylint-dev/pylint/issues/9547
            '--ignored-argument-names=""',
        ]

        # Add py files
        cmd.extend(str(v) for v in self.collected_py_files)

        # subprocess.run(cmd, check=True)
        r = subprocess.run(
            cmd,
            check=False,
            universal_newlines=True,
            capture_output=True)

        # Count lines except module headings
        error_n = len(list(filter(lambda line: not line.startswith('*****'),
                                  r.stdout.splitlines())))

        if r.returncode != 0:
            logging.error(r.stdout)

        self.assertEqual(0, r.returncode, f'PyLint found {error_n} problems.')

        # any other errors?
        self.assertEqual(r.stderr, '')

    def test040_codespell(self):
        """Codespell to check for common typos."""

        cmd = [
            'codespell',
            # Skip these files
            '--skip',
            '.git,.pytest_cache,.ruff_cache,.venv,build,'
            '*.egg-info,docs,#*.py#',
            # Print N lines of surrounding context
            '--context', '1',
            # Check hidden files also
            '--check-hidden',
            # Dictionaries to use (default: "clear,rare"). Current: all.
            '--builtin',
            'clear,rare,informal,usage,code,names,en-GB_to_en-US',
            # Print number of errors as last line on stderr
            '--count',
            # Simulate "# noqa" and ignore all lines with
            # "# codespell-ignore" at the end.
            # Credits: https://github.com/codespell-project/codespell/
            #          issues/1212#issuecomment-1721152455
            '--ignore-regex', '.*# codespell-ignore$',
            # # Allowed (ignored) words
            '--ignore-words-list=assertin',
            # Allowed (ignored) words in URLs and URIs
            '--uri-ignore-words-list=master',
            '--enable-colors',
        ]

        r = subprocess.run(
            cmd,
            check=False,
            universal_newlines=True,
            capture_output=True)

        error_n = int(r.stderr)
        if r.stdout:
            logging.error(r.stdout)

        self.assertEqual(0, error_n, f'Codespell found {error_n} problems.')

    def test050_pydocstyle_via_ruff(self):
        """Ruff using pydocstyle rules "D" only, excluding test_*.py files.
        """
        cmd = [
            'ruff',
            'check',
            '--line-length', str(PEP8_MAX_LINE_LENGTH),
            # PyDocstyle rules only
            '--select=D',
            # one error per line (no context lines)
            '--output-format=concise',
            '--quiet',
            '--extend-exclude=tests',
            '.'
        ]

        r = subprocess.run(
            cmd,
            check=False,
            universal_newlines=True,
            capture_output=True
        )

        # No errors other then linter rules
        self.assertIn(r.returncode, [0, 1], r.stderr)

        error_n = len(r.stdout.splitlines())
        if error_n > 0:
            logging.error(r.stdout)
        self.assertEqual(
            0,
            error_n,
            f'Ruff (pydocstyle rules only) found {error_n} problems.')
