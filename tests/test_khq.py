# pylint: disable=missing-module-docstring
import unittest
import sys
import pandas
from buhtzology.khq import score_KHQ
from . import helper


# Skip because pandas 2.1 is not available for Python 3.8
@unittest.skipIf(sys.version_info[0:2] < (3, 9), 'Only Python3.9 or higher')
class KHQ(unittest.TestCase):
    """KHQ"""
    # pylint: disable=too-many-public-methods,invalid-name
    @classmethod
    def setUpClass(cls):
        cls.khq_df = pandas.DataFrame([[1] * 32])

    def test_dataframe(self):
        """Default DataFrame for testing"""
        df = self.khq_df.copy()

        # column names
        self.assertTrue((df.columns == range(32)).all())

        # rows & columns
        self.assertEqual(df.shape, (1, 32))

    def _do_column_names(self, suffix):
        """Specific column names"""
        df = self.khq_df.copy()

        # create 32 column names
        c = [helper.rand_string(8, 3) for i in range(32)]

        # rename columns
        df.columns = c

        # duplicated columns not allowed for some operations
        if df.columns.duplicated().any():
            # try again
            # return self._do_column_names(suffix)
            self._do_column_names(suffix)

        # insert extra columns
        columns_full = c[:]
        df.insert(loc=0, column='A', value='a')
        columns_full.insert(0, 'A')
        df.insert(loc=7, column='B', value='b')
        columns_full.insert(7, 'B')
        df.insert(loc=30, column='C', value='c')
        columns_full.insert(30, 'C')
        df.insert(loc=32, column='D', value='d')
        columns_full.insert(32, 'D')

        self.assertEqual(columns_full, list(df.columns))

        # result columns
        cresult = columns_full[:]
        cresult.extend([f'{suffix}KHQ_S{i}' for i in range(1, 11)])
        cresult.extend([f'{suffix}KHQ_SC{i}' for i in range(26, 33)])

        df = score_KHQ(data=df, col_names=c, suffix=suffix)

        self.assertEqual(cresult, list(df.columns))

    def test_column_names(self):
        """Specific column names"""
        self._do_column_names('')

    def test_column_names_with_suffix(self):
        """Specific column names with suffix"""
        self._do_column_names(helper.rand_string(7))

    def test_replace_NA_with_Zero(self):
        """Replace pandas.NA with 0 in (9-12, 22-31)"""
        df = self.khq_df.copy()

        # affected columns
        index_list = [*range(8, 11), *range(21, 31)]

        # place pandas.NA
        for idx in index_list:
            df.loc[0, idx] = pandas.NA

        df = score_KHQ(data=df)

        for idx in index_list:
            self.assertEqual(df.loc[0, idx], 0, msg=f'{idx}')

    def _do_Q_valid_values(self,
                           values_and_results,
                           source_column_idx,
                           destination_column_name,
                           ):
        """Helper"""
        # all valid values
        for val, res in values_and_results:
            df = self.khq_df.copy()
            df.loc[0, source_column_idx] = val

            df = score_KHQ(data=df,
                           col_names=list(range(32)),
                           suffix='')

            # m = ('=> Values: {}; Destination: {}; res: {}'
            #      .format(val, destination_column_name, res))

            if pandas.isna([val, res]).any():
                # NA does not work with operator "=="
                self.assertTrue(
                    pandas.isna([
                        df.loc[0, destination_column_name],
                        res]).all()
                )
            else:
                self.assertEqual(df.loc[0, destination_column_name], res)

    def _do_Q_invalid_values(self, invalid_values, source_column_idx):
        """
        """
        # invalid values
        for val in invalid_values:
            df = self.khq_df.copy()
            df.loc[0, source_column_idx] = val

            # m = '=> Values: {}; idx {}'.format(val, source_column_idx)
            with self.assertRaises(ValueError):  # , msg=m):
                score_KHQ(
                    data=df,
                    col_names=list(range(32)),
                    suffix='')

    def test_S1_valid(self):
        """KHQ Subscale 1 valid values"""
        self._do_Q_valid_values(
            values_and_results=[
                # (1, 0.0),
                # (2, 25.0),
                # (3, 50.0),
                # (4, 75.0),
                # (5, 100.0),
                (pandas.NA, pandas.NA)],
            source_column_idx=0,
            destination_column_name='KHQ_S1')

    def test_S1_invalid(self):
        """KHQ Subscale 1 invalid values"""
        self._do_Q_invalid_values(
            invalid_values=[0, 6],
            source_column_idx=0)

    def test_S2(self):
        """KHQ Subscale 2"""
        # Q2
        self._do_Q_valid_values(
            values_and_results=[
                (1, 0.0),
                (2, 33.3),
                (3, 66.6),
                (4, 100.0),
                (pandas.NA, pandas.NA)],
            source_column_idx=1,
            destination_column_name='KHQ_S2')

        self._do_Q_invalid_values(
            invalid_values=[0, 5],
            source_column_idx=1)

    def test_S3(self):
        """KHQ Subscale 3"""
        self._do_Q_valid_values(
            values_and_results=[
                ((1, 1), 0.0),
                ((1, 2), 16.6),
                ((2, 2), 33.3),
                ((3, 2), 50),
                ((3, 3), 66.6),
                ((4, 3), 83.3),
                ((4, 4), 100.0),
                ((pandas.NA, pandas.NA), pandas.NA)],
            source_column_idx=[2, 3],
            destination_column_name='KHQ_S3')

        self._do_Q_invalid_values(
            invalid_values=[
                (0, 0),
                (0, 1),
                (1, 0),
                (5, 1),
                (1, 5),
                (5, 5)],
            source_column_idx=[2, 3])

    def test_S4(self):
        """KHQ Subscale 4"""
        self._do_Q_valid_values(
            values_and_results=[
                ((1, 1), 0.0),
                ((1, 2), 16.6),
                ((2, 2), 33.3),
                ((3, 2), 50),
                ((3, 3), 66.6),
                ((4, 3), 83.3),
                ((4, 4), 100.0),
                ((pandas.NA, pandas.NA), pandas.NA)],
            source_column_idx=[4, 5],
            destination_column_name='KHQ_S4')

        self._do_Q_invalid_values(
            invalid_values=[
                (0, 0),
                (0, 1),
                (1, 0),
                (5, 1),
                (1, 5),
                (5, 5)],
            source_column_idx=[4, 5])

    def test_S5(self):
        """KHQ Subscale 5"""
        self._do_Q_valid_values(
            values_and_results=[
                ((1, 1, 0), 0.0),
                ((1, 2, 0), 16.6),
                ((1, 3, 0), 33.3),
                ((1, 4, 0), 50),
                ((1, 1, 0), 0.0),
                ((2, 1, 0), 16.6),
                ((3, 1, 0), 33.3),
                ((4, 1, 0), 50),
                ((2, 4, 0), 66.6),
                ((3, 4, 0), 83.3),
                ((4, 4, 0), 100.0),
                ((1, 1, 1), 0.0),
                ((1, 2, 1), 11.1),
                ((1, 3, 1), 22.2),
                ((1, 4, 1), 33.3),
                ((2, 4, 1), 44.4),
                ((3, 4, 1), 55.5),
                ((4, 4, 1), 66.6),
                ((4, 4, 2), 77.7),
                ((4, 4, 3), 88.8),
                ((4, 4, 4), 100),
                ((pandas.NA, pandas.NA, 0), pandas.NA),
                ((pandas.NA, pandas.NA, 1), pandas.NA),
            ],
            source_column_idx=[6, 7, 10],
            destination_column_name='KHQ_S5')

        self._do_Q_invalid_values(
            invalid_values=[
                (0, 0, 0),
                (0, 0, 1),
                (5, 5, 0),
                (5, 5, 1),
                (1, 1, 5),
                (5, 1, 6)],
            source_column_idx=[6, 7, 10])

    def test_S6(self):
        """KHQ Subscale 6"""
        self._do_Q_valid_values(
            values_and_results=[
                ((0, 0), pandas.NA),
                ((pandas.NA, 0), pandas.NA),
                ((0, pandas.NA), pandas.NA),
                ((1, 0), 0),
                ((2, 0), 33.3),
                ((3, 0), 66.6),
                ((4, 0), 100),
                ((0, 1), 0),
                ((0, 2), 33.3),
                ((0, 3), 66.6),
                ((0, 4), 100),
                ((1, 1), 0),
                ((2, 1), 16.6),
                ((3, 1), 33.3),
                ((4, 1), 50),
                ((4, 2), 66.6),
                ((4, 3), 83.3),
                ((4, 4), 100),
            ],
            source_column_idx=[8, 9],
            destination_column_name='KHQ_S6')

        self._do_Q_invalid_values(
            invalid_values=[
                (5, 0),
                (0, 5),
                (6, 7),
                (-1, 1)],
            source_column_idx=[8, 9])

    def test_S7(self):
        """KHQ Subscale 7"""
        self._do_Q_valid_values(
            values_and_results=[
                ((1, 1, 1), 0),
                ((2, 1, 1), 11.1),
                ((1, 2, 1), 11.1),
                ((1, 1, 2), 11.1),
                ((3, 1, 1), 22.2),
                ((4, 1, 1), 33.3),
                ((4, 2, 1), 44.4),
                ((4, 3, 1), 55.5),
                ((4, 4, 1), 66.6),
                ((4, 4, 2), 77.7),
                ((4, 4, 3), 88.8),
                ((4, 4, 4), 100),
            ],
            source_column_idx=[11, 12, 13],
            destination_column_name='KHQ_S7')

        self._do_Q_invalid_values(
            invalid_values=[
                (0, 1, 1),
                (5, 1, 1),
                (1, 0, 1),
                (1, 5, 1),
                (1, 1, 0),
                (1, 1, 5),
            ],
            source_column_idx=[11, 12, 13])

    def test_S8(self):
        """KHQ Subscale 8"""
        self._do_Q_valid_values(
            values_and_results=[
                ((1, 1), 0.0),
                ((1, 2), 16.6),
                ((2, 2), 33.3),
                ((3, 2), 50),
                ((3, 3), 66.6),
                ((4, 3), 83.3),
                ((4, 4), 100.0),
                ((pandas.NA, pandas.NA), pandas.NA),
            ],
            source_column_idx=[14, 15],
            destination_column_name='KHQ_S8')

        self._do_Q_invalid_values(
            invalid_values=[
                (0, 0),
                (0, 1),
                (1, 0),
                (5, 1),
                (1, 5),
                (5, 5),
            ],
            source_column_idx=[14, 15])

    def test_S9(self):
        """KHQ Subscale 9"""
        self._do_Q_valid_values(
            values_and_results=[
                ((1, 1, 1, 1, 1), 0),
                ((2, 1, 1, 1, 1), 6.6),
                ((3, 1, 1, 1, 1), 13.3),
                ((4, 1, 1, 1, 1), 20),
                ((2, 4, 1, 1, 1), 26.6),
                ((3, 4, 1, 1, 1), 33.3),
                ((4, 4, 1, 1, 1), 40),
                ((2, 4, 4, 1, 1), 46.6),
                ((3, 4, 4, 1, 1), 53.3),
                ((4, 4, 4, 1, 1), 60),
                ((2, 4, 4, 4, 1), 66.6),
                ((3, 4, 4, 4, 1), 73.3),
                ((4, 4, 4, 4, 1), 80),
                ((2, 4, 4, 4, 4), 86.6),
                ((3, 4, 4, 4, 4), 93.3),
                ((4, 4, 4, 4, 4), 100),
            ],
            source_column_idx=[16, 17, 18, 19, 20],
            destination_column_name='KHQ_S9')

        self._do_Q_invalid_values(
            invalid_values=[
                (0, 1, 1, 1, 1),
                (1, 0, 1, 1, 1),
                (1, 1, 0, 1, 1),
                (1, 1, 1, 0, 1),
                (1, 1, 1, 1, 0),
                (5, 1, 1, 1, 1),
                (1, 5, 1, 1, 1),
                (1, 1, 5, 1, 1),
                (1, 1, 1, 5, 1),
                (1, 1, 1, 1, 5),
            ],
            source_column_idx=[16, 17, 18, 19, 20])

    def test_S10_valid(self):
        """KHQ Subscale 10 valid values"""
        self._do_Q_valid_values(
            values_and_results=[
                ((0, 0, 0, 0), pandas.NA),
                ((1, 0, 0, 0), 0),
                ((1, 1, 0, 0), 0),
                ((1, 1, 1, 0), 0),
                ((1, 1, 1, 1), 0),
                ((2, 0, 0, 0), 50),
                ((3, 0, 0, 0), 100),
                ((1, 2, 0, 0), 25),
                ((1, 3, 0, 0), 50),
                ((2, 3, 0, 0), 75),
                ((3, 3, 0, 0), 100),
                ((2, 1, 1, 0), 16.6),
                ((3, 1, 1, 0), 33.3),
                ((3, 2, 1, 0), 50),
                ((3, 3, 1, 0), 66.6),
                ((3, 3, 3, 0), 100),
                ((2, 1, 1, 1), 12.5),
                ((3, 1, 1, 1), 25),
                ((3, 2, 1, 1), 37.5),
                ((3, 3, 1, 1), 50),
                ((3, 3, 2, 1), 62.5),
                ((3, 3, 3, 1), 75),
                ((3, 3, 3, 2), 87.5),
                ((3, 3, 3, 3), 100),
            ],
            source_column_idx=[21, 22, 23, 24],
            destination_column_name='KHQ_S10')

    def test_S10_invalid(self):
        """KHQ Subscale 10 invalid values"""
        self._do_Q_invalid_values(
            invalid_values=[
                (4, 0, 0, 0),
                (0, 4, 0, 0),
                (0, 0, 4, 0),
                (0, 0, 0, 4),
            ],
            source_column_idx=[21, 22, 23, 24])

    def test_SC26_32(self):
        """KHQ Subscale C26 to 32"""
        for idx in [25, 26, 27, 28, 29, 30, 31]:
            self._do_Q_valid_values(
                values_and_results=[
                    (0, pandas.NA),
                    (1, 0),
                    (2, 50),
                    (3, 100),
                ],
                source_column_idx=idx,
                destination_column_name=f'KHQ_SC{idx+1}'
            )

            self._do_Q_invalid_values(
                invalid_values=[
                    4
                ],
                source_column_idx=idx)

    def test_S7_imputation(self):
        """KHQ Subscale 7 Imputation"""
        self._do_Q_valid_values(
            values_and_results=[
                ((1, 1, pandas.NA), 0),
                ((1, pandas.NA, 1), 0),
                ((pandas.NA, 1, 1), 0),
                ((2, 1, pandas.NA), 16.6),
                ((1, 2, pandas.NA), 16.6),
                ((2, 2, pandas.NA), 33.3),
                ((3, 2, pandas.NA), 50),
                ((4, 2, pandas.NA), 66.6),
                ((4, 3, pandas.NA), 83.3),
                ((4, 4, pandas.NA), 100),
            ],
            source_column_idx=[11, 12, 13],
            destination_column_name='KHQ_S7')

    def test_S9_imputation(self):
        """KHQ Subscale 9 Imputation"""
        self._do_Q_valid_values(
            values_and_results=[
                ((1, 1, 1, 1, pandas.NA), 0),
                ((2, 1, 1, 1, pandas.NA), 8.3),
                ((3, 1, 1, 1, pandas.NA), 16.6),
                ((4, 1, 1, 1, pandas.NA), 25),
                ((4, 2, 1, 1, pandas.NA), 33.3),
                ((4, 3, 1, 1, pandas.NA), 41.6),
            ],
            source_column_idx=[16, 17, 18, 19, 20],
            destination_column_name='KHQ_S9')

    def test_imputate_foreign_cols(self):
        """Imputation if foreign columns present."""
        df = self.khq_df.copy()

        # make missing values
        df.loc[0, 17] = pandas.NA

        # insert some foreign columns
        df.insert(loc=0, column='A', value='a')
        df.insert(loc=7, column='X', value=pandas.NA)

        # calc
        df = score_KHQ(df, list(range(32)))

        # test
        self.assertEqual(df.loc[0, 'KHQ_S9'], 0)
        self.assertEqual(df.loc[0, 'A'], 'a')
        self.assertTrue(df.loc[0, 'X'] is pandas.NA)
