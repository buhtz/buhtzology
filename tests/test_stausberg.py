"""Tests about Stausberg score calculation"""
import unittest
import inspect
import pandas
from pandas.testing import assert_frame_equal
from buhtzology import stausberg


class WeightByGroup(unittest.TestCase):
    """Stausberg weight by group"""

    def test_customize_weight_name(self):
        """Customized name of weight column.
        """
        data = pandas.DataFrame({
            'anonym': list('A'),
            'icd': ['K55.3']})
        expected = pandas.DataFrame({
            'anonym': list('A'),
            'foobar': 1})
        expected.foobar = expected.foobar.astype('Int16')

        result = stausberg.weight_by_icd_group(
            data,
            index_column='anonym',
            icd_column='icd',
            weight_name='foobar')

        assert_frame_equal(result, expected, check_dtype=False)

    def test_no_weight(self):
        """Persons without weight.

        Persons without a relevant diagnosis group have no weight. They are
        ignored and excluded from the result.
        """
        data = pandas.DataFrame({
            'anonym': list('AB'),
            'icd': ['K55.4', 'X01.7']})
        expected = pandas.DataFrame({
            'anonym': ['A'],
            'weight': [1]})

        result = stausberg.weight_by_icd_group(
            data, index_column='anonym', icd_column='icd')

        assert_frame_equal(result, expected, check_dtype=False)

    def test_weight_by_icd_group_error(self):
        """ICD-Groups as input data do not work."""
        data = pandas.DataFrame({
            'person': list('AB'),
            'foobar': range(2),
            'diagnosis': [
                'K55-K63',
                'R30-R39',
            ]})

        # The two ICD-Groups will cause an error because only specific ICD
        # codes are allowed.
        with self.assertRaises(TypeError):
            stausberg.weight_by_icd_group(
                data, index_column='person', icd_column='diagnosis')

        # This ICD code is allowed now matter that there is a "-" at
        # the end.
        data = pandas.DataFrame({
            'person': list('A'),
            'foobar': range(1),
            'diagnosis': ['Z01.8-']
        })

        # nothing raised here
        stausberg.weight_by_icd_group(
            data, index_column='person', icd_column='diagnosis')

    def test_weight_by_full_icd_code(self):
        """Full ICD codes as input data."""
        data = pandas.DataFrame({
            'person': list('ABBCCCC'),
            'foobar': range(7),
            'diagnosis': [
                'K55.32',  # 1
                'R30.9',  # 1
                'Y43.9!',  # -
                'X19.9!',  # -
                'D32.4',  # -5
                'A03.8',  # -
                'G92.0'  # 3
            ]})

        expected = pandas.DataFrame(
            {
                'person': list('ABC'),
                'weight': [1, 1, -2]
            }
        )
        expected.weight = expected.weight.astype('Int16')
        result = stausberg.weight_by_icd_group(
            data, index_column='person', icd_column='diagnosis')

        assert_frame_equal(result, expected, check_dtype=False)

    def test_multiple_icd_one_group(self):
        """Full ICD codes as input data."""
        data = pandas.DataFrame({
            'person': list('BBB'),
            'foobar': range(3),
            'diagnosis': [
                'K55.32',  # 1
                'K56.14',  # 0 because same group as K55.32
                'J10.1',  # 3
            ]})

        expected = pandas.DataFrame(
            {
                'person': list('B'),
                'weight': [4]
            }
        )
        expected.weight = expected.weight.astype('Int16')
        result = stausberg.weight_by_icd_group(
            data, index_column='person', icd_column='diagnosis')

        assert_frame_equal(result, expected, check_dtype=False)

    def test_exclude_appendix_artifact_default(self):
        """Appendix excluded by default"""
        val = inspect.signature(stausberg.weight_by_icd_group) \
            .parameters['exclude_appendix_artifact'].default

        self.assertEqual(val, True)

    def test_appendix_artifact_exclude(self):
        """Appendix excluded"""
        data = pandas.DataFrame({
            'person': list('BBBB'),
            'diagnosis': [
                'R30.9',  # 1
                'Y43.9!',  # -
                'K36.1',  # Artifact -131
                'G92.0'  # 3
            ]})

        excluded = stausberg.weight_by_icd_group(
            data,
            index_column='person',
            icd_column='diagnosis',
            exclude_appendix_artifact=True)

        self.assertEqual(len(excluded), 1)
        self.assertEqual(excluded.iloc[0].weight, 4)

    def test_appendix_artifact_include(self):
        """Appendix Artifect in/exclude"""
        data = pandas.DataFrame({
            'person': list('BBBB'),
            'diagnosis': [
                'R30.9',  # 1
                'Y43.9!',  # -
                'K36.1',  # Artifact -131
                'G92.0'  # 3
            ]})

        included = stausberg.weight_by_icd_group(
            data,
            index_column='person',
            icd_column='diagnosis',
            exclude_appendix_artifact=False)

        self.assertEqual(len(included), 1)
        self.assertEqual(included.iloc[0].weight, -127)


class GroupsCodeDataFrame(unittest.TestCase):
    """Conversion between ICD groups and codes"""
    # pylint: disable=protected-access

    def test_length_error(self):
        """Group length must be 7.
        """
        with self.assertRaises(ValueError):
            stausberg._groups_code_dataframe(['A1-A23'])

    def test_digit_error(self):
        """Digits at right position.

        The group string need digits at string index 1, 2, 5, 6.
        """
        with self.assertRaises(ValueError):
            stausberg._groups_code_dataframe(['AA1-A23'])

        with self.assertRaises(ValueError):
            stausberg._groups_code_dataframe(['A0A-A23'])

        with self.assertRaises(ValueError):
            stausberg._groups_code_dataframe(['A01-AA3'])

        with self.assertRaises(ValueError):
            stausberg._groups_code_dataframe(['A01-A2A'])

    def test_alpha_error(self):
        """Letters at the right position.

        The group string need alphabetic letters at string index 0, 4.
        """
        with self.assertRaises(ValueError):
            stausberg._groups_code_dataframe(['001-A23'])

        with self.assertRaises(ValueError):
            stausberg._groups_code_dataframe(['A01-023'])

    def test_split_error(self):
        """Letters at the right position.

        The group string need alphabetic letters at string index 0, 4.
        """
        with self.assertRaises(ValueError):
            stausberg._groups_code_dataframe(['A01_A23'])

        with self.assertRaises(ValueError):
            stausberg._groups_code_dataframe(['A01+A23'])

        with self.assertRaises(ValueError):
            stausberg._groups_code_dataframe(['A01uA23'])

    def test_equal_letter(self):
        """Letters need to be the same.
        """
        with self.assertRaises(ValueError):
            stausberg._groups_code_dataframe(['A01-B23'])

    def test_groups_keys(self):
        """ICD Group keys"""
        groups = ['A01-A05', 'T10-T13']

        expect = pandas.DataFrame(
            {
                'ICD3': [
                    'A01', 'A02', 'A03', 'A04', 'A05',
                    'T10', 'T11', 'T12', 'T13'],
                'GROUP': [
                    'A01-A05', 'A01-A05', 'A01-A05',
                    'A01-A05', 'A01-A05',
                    'T10-T13', 'T10-T13', 'T10-T13', 'T10-T13']
            }
        )

        result = stausberg._groups_code_dataframe(groups)

        assert_frame_equal(result, expect)
