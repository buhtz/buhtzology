"""Testing report module"""
import unittest
import pathlib
from typing import NamedTuple
import random
import pandas
import pyfakefs.fake_filesystem_unittest as pyfakefs_ut
import PIL
import PIL.Image
import seaborn
from buhtzology import report


class ReportFS(pyfakefs_ut.TestCase):
    """Test with fake filesystem"""

    @classmethod
    def setUpClass(cls):
        cls.setUpClassPyfakefs()

        img = PIL.Image.new('RGB', (1, 1), color=(255, 255, 255))
        img.save(pathlib.Path('img.png'))

    def test_image_from_file(self):
        """Add image from file"""

        r = report.Report()
        r.image(pathlib.Path('img.png'), scale_factor=.5, caption='foobar')

        self.assertIsInstance(r[-1], report.Report.Image)
        self.assertEqual(r[-1].scale_factor, .5)
        self.assertEqual(r[-1].caption, 'foobar')
        self.assertFalse(not r[-1].image_bytes)

    def test_image_from_bytes(self):
        """Image as bytes"""
        r = report.Report()
        r.image(pathlib.Path('img.png'), scale_factor=.5, caption='foobar')

        img_bytes = r[-1].image_bytes

        r.image(img_bytes, caption='two')
        self.assertEqual(r[-1].image_bytes, img_bytes)

    def test_image_file_to_bytes(self):
        """Convert figure to bytes."""
        ib = report.Report.image_to_bytes(pathlib.Path('img.png'))
        self.assertIsInstance(ib, bytes)
        self.assertTrue(len(ib) > 0)


class Generally(unittest.TestCase):
    """Basics tests about report"""

    # pylint: disable=protected-access

    def test_basic(self):
        """Default values"""
        r = report.Report()

        self.assertEqual(r._elements, [])
        self.assertEqual(r._element_tags, [])

        self.assertEqual(len(r), len(r._elements))

        self.assertEqual(r._current_heading_level, 1)
        self.assertEqual(r._current_auto_tags, None)

    def test_heading_level(self):
        """Manipulate hading level"""

        r = report.Report()

        self.assertEqual(r._current_heading_level, 1)

        r.heading_level(down=True)
        self.assertEqual(r._current_heading_level, 2)

        r.heading_level(level=4)
        self.assertEqual(r._current_heading_level, 4)

        r.heading_level(up=True)
        self.assertEqual(r._current_heading_level, 3)

        # level 7 -> down -> up
        r.heading_level(up=True, down=True, level=7)
        self.assertEqual(r._current_heading_level, 7)

    def test_title(self):
        """(Sub)title"""

        r = report.Report('foo', 'bar')

        self.assertEqual(len(r._elements), 2)
        self.assertIsInstance(r._elements[0], report.Report.Heading)
        self.assertIsInstance(r._elements[1], report.Report.Paragraph)

    def test_heading(self):
        """Heading elements"""

        r = report.Report()

        r.heading('One')
        r.heading('Two', down=True)
        r.heading('Three', down=True)
        r.heading('Four', level=1)
        r.heading('Five')
        r.heading('Six', level=5)
        r.heading('Seven', up=True)

        expect = [
            ('One', 1),
            ('Two', 2),
            ('Three', 3),
            ('Four', 1),
            ('Five', 1),
            ('Six', 5),
            ('Seven', 4)
        ]

        for idx, e in enumerate(expect):
            self.assertEqual(r[idx].text, e[0])
            self.assertEqual(r[idx].level, e[1])

    def test_heading_none_persistent_level(self):
        """Setting heading level temporary by user request."""
        sut = report.Report()

        sut.heading('1')
        sut.heading('1.1 - step to level 2', down=True)
        sut.heading('1.2 - keep level 2')
        sut.heading('2 - back to level 1', level=1)
        sut.heading('2.1 - to level 2 temporary!', down=True, persistent=False)
        sut.heading('3 - back on level 1')  # <-- See!?

        levels_to_expect = [1, 2, 2, 1, 2, 1]
        for h, expected_level in zip(sut, levels_to_expect):
            self.assertEqual(h.level, expected_level)

    def test_heading_temporary(self):
        """Setting heading level temporary by user request."""
        sut = report.Report()

        sut.heading('1')
        sut.heading('1.1 - step to level 2', down=True)
        sut.heading('1.2 - keep level 2')
        sut.heading('2 - back to level 1', level=1)
        sut.heading_temporary('2.1 - to level 2 temporary!', down=True)
        sut.heading('3 - back on level 1')  # <-- See!?

        levels_to_expect = [1, 2, 2, 1, 2, 1]
        for h, expected_level in zip(sut, levels_to_expect):
            self.assertEqual(h.level, expected_level)

    def test_heading_level_not_persistent_by_default(self):
        """Heading level is persistent by default."""
        sut = report.Report()

        sut.heading('1')
        sut.heading('1.1 - step to level 2', down=True)
        sut.heading('1.2 - keep level 2')
        sut.heading('2 - back to level 1', level=1)
        sut.heading('2.1 - to level 2 temporary!', down=True)
        sut.heading('2.2 - stay on level 2')  # <-- See!?

        levels_to_expect = [1, 2, 2, 1, 2, 2]
        for h, expected_level in zip(sut, levels_to_expect):
            self.assertEqual(h.level, expected_level)

    def test_paragraph(self):
        """Paragraph element"""
        r = report.Report()

        r.paragraph('Foo')
        r.paragraph('Bar', 'Nice')

        self.assertEqual(r[0].text, 'Foo')
        self.assertEqual(r[0].style, None)

        self.assertEqual(r[1].text, 'Bar')
        self.assertEqual(r[1].style, 'Nice')

    def test_empty_paragraph(self):
        """Empyt paragraph element"""
        r = report.Report()

        r.paragraph()

        self.assertEqual(r[0].text, '')
        self.assertEqual(r[0].style, None)

    def test_table(self):
        """Table element"""
        df = pandas.DataFrame()

        r = report.Report()
        r.table(df)
        r.table(df, caption='Foobar')
        r.table(df, autofit=False)

        for idx in range(3):
            # Table element
            self.assertIsInstance(r[idx], report.Report.Table)
            # with dataframe
            self.assertIsInstance(r[idx].df, pandas.DataFrame)

        self.assertEqual(r[0].autofit, True)
        self.assertEqual(r[0].caption, None)
        self.assertEqual(r[2].autofit, False)

    def test_table_with_note(self):
        """Table with note"""
        sut = report.Report()
        sut.table(pandas.DataFrame(), note='Notiz')

        self.assertEqual(sut[0].note, 'Notiz')

    def test_references(self):
        """References lists, e.g. TOC"""
        r = report.Report()

        def _sub_test_refe(kind):
            # always check the last element in report
            self.assertIsInstance(r[-1], report.Report.References)
            self.assertEqual(r[-1].kind, kind)

        r.list_of_tables()
        _sub_test_refe(report._ReferenceKind.TABLES)
        self.assertEqual(len(r), 2)

        r.list_of_figures()
        _sub_test_refe(report._ReferenceKind.FIGURES)
        self.assertEqual(len(r), 4)

        r.table_of_contents()
        _sub_test_refe(report._ReferenceKind.CONTENT)
        self.assertEqual(len(r), 6)

    def test_control(self):
        """Control element
            PAGE_BREAK = 1
            ORIENTATION_TOGGLE = 2
            ORIENTATION_LANDSCAPE = 3
            ORIENTATION_PORTRAIT = 4
        """

        r = report.Report()

        def _sub_test_cte(ct):
            # always check the last element in report
            self.assertIsInstance(r[-1], report.Report.Control)
            self.assertEqual(r[-1].control_type, ct)

        r.page_break()
        _sub_test_cte(report._ControlType.PAGE_BREAK)
        self.assertEqual(len(r), 1)

        r.page_break_landscape()
        _sub_test_cte(report._ControlType.ORIENTATION_LANDSCAPE)
        self.assertEqual(len(r), 3)

        r.page_break_portrait()
        _sub_test_cte(report._ControlType.ORIENTATION_PORTRAIT)
        self.assertEqual(len(r), 5)

        r.toggle_orientation()
        _sub_test_cte(report._ControlType.ORIENTATION_TOGGLE)
        self.assertEqual(len(r), 6)

    def test_figures_to_bytes(self):
        """Image from Seaborn elements"""
        # df = seaborn.load_dataset('iris')
        df = pandas.DataFrame({
            'sepal_width': [
                round(random.uniform(2.0, 4.4), 2) for _ in range(150)
            ]
        })

        # facet grid
        facet_grid = seaborn.catplot(kind='bar', data=df, x='sepal_width')
        ib = report.Report.image_to_bytes(facet_grid)
        self.assertIsInstance(ib, bytes)
        self.assertTrue(len(ib) > 0)

        # axes
        axes = seaborn.barplot(data=df, x='sepal_width')
        ib = report.Report.image_to_bytes(axes)
        self.assertIsInstance(ib, bytes)
        self.assertTrue(len(ib) > 0)

        # figure
        figure = seaborn.barplot(data=df, x='sepal_width').figure
        ib = report.Report.image_to_bytes(figure)
        self.assertIsInstance(ib, bytes)
        self.assertTrue(len(ib) > 0)

    def test_image_from_figure(self):
        """Image from Seaborn elements"""
        # df = seaborn.load_dataset('iris')
        df = pandas.DataFrame({
            'sepal_width': [
                round(random.uniform(2.0, 4.4), 2) for _ in range(150)
            ]
        })

        r = report.Report()

        # facet grid
        r.image(seaborn.catplot(kind='bar', data=df, x='sepal_width'))

        # axes
        r.image(seaborn.barplot(data=df, x='sepal_width'))

        # figure
        r.image(seaborn.barplot(data=df, x='sepal_width').figure)

        for idx in range(3):
            self.assertIsInstance(r[idx], report.Report.Image)
            self.assertFalse(not r[idx].image_bytes)

    def test_image_dict(self):
        """Dictionary of images"""

        img_dict = {
            'Foo': {
                'One': b'one',
                'Two': b'two',
            },
            'Bar': {
                'Deeper': {
                    'This is the end': {
                        'A': b'A',
                        'B': b'B',
                        'C': b'C'
                    }
                }
            }
        }

        r = report.Report()
        r.image_dict(img_dict)

        self.assertEqual(len(r), 9)

        # Heading elements
        for idx, level in zip([0, 3, 4, 5], [2, 2, 3, 4]):
            self.assertIsInstance(r[idx], report.Report.Heading)
            self.assertEqual(r[idx].level, level)

        # Image elements
        for idx in [1, 2, 6, 7, 8]:
            self.assertIsInstance(r[idx], report.Report.Image)

        # with page breaks
        r = report.Report()
        r.image_dict(img_dict, page_break=True)

        self.assertEqual(len(r), 14)

    def test_table_dict(self):
        """Dictionary of tables"""
        df = pandas.DataFrame({'A': [1]})
        table_dict = {
            'Foo': {
                'One': df,
                'Two': df,
            },
            'Bar': {
                'Deeper': {
                    'This is the end': {
                        'A': df,
                        'B': df,
                        'C': df,
                    }
                }
            }
        }

        r = report.Report()
        r.table_dict(table_dict)

        self.assertEqual(len(r), 9)

        # Heading elements
        for idx, level in zip([0, 3, 4, 5], [2, 2, 3, 4]):
            self.assertIsInstance(r[idx], report.Report.Heading)
            self.assertEqual(r[idx].level, level)

        # Image elements
        for idx in [1, 2, 6, 7, 8]:
            self.assertIsInstance(r[idx], report.Report.Table)

        # with page breaks
        r = report.Report()
        r.table_dict(table_dict, page_break=True)

        self.assertEqual(len(r), 14)


class Tags(unittest.TestCase):
    """Using Report with tags
    """

    # pylint: disable=protected-access,disallowed-name

    class Foo(NamedTuple):
        """Helper"""
        bar: str

    def test_add_element(self):
        """Add element with tags."""
        r = report.Report()

        r._add_element(__class__.Foo('no tag'))
        r._add_element(__class__.Foo('one tag'), 'red')
        r._add_element(__class__.Foo('three tag'), ['pink', 'blue', 'green'])

        self.assertEqual(
            r._element_tags,
            [
                None,
                ['red'],
                ['pink', 'blue', 'green']
            ]
        )

    def test_auto_tags(self):
        """Use auto text context manager."""
        r = report.Report()

        r._add_element(__class__.Foo('no tag'))
        with r.auto_tags('pink'):
            r._add_element(__class__.Foo('one tag'))
            r._add_element(__class__.Foo('three tag'))

        self.assertEqual(
            r._element_tags,
            [
                None,
                ['pink'],
                ['pink']
            ]
        )

    def test_taged_elements(self):
        """Specific element types with tags"""
        r = report.Report()

        r.title('', tags='foo')
        r.subtitle('', tags='foo')
        r.paragraph('', tags='foo')
        r.image(b'', tags='foo')
        r.image_dict({'foo': b''}, tags='foo')
        r.table(pandas.DataFrame(), tags='foo')
        r.table_dict({'foo': pandas.DataFrame()}, tags='foo')
        r.page_break(tags='foo')
        r.page_break_landscape(tags='foo')  # add 2 elements
        r.page_break_portrait(tags='foo')  # add 2 elements
        r.toggle_orientation(tags='foo')  # only 1 element
        r.list_of_figures(tags='foo')  # 2 elements
        r.list_of_tables(tags='foo')  # 2 elements
        r.table_of_contents(tags='foo')  # 2 elements

        self.assertEqual(len(r._element_tags), 19)
        tags_flattened = [tag for et in r._element_tags for tag in et]
        self.assertEqual(len(set(tags_flattened)), 1)
        self.assertEqual(r._element_tags[0][0], 'foo')

    def test_filtered_elements(self):
        """Filter elements."""
        r = report.Report()

        r._add_element(__class__.Foo('1'))
        r._add_element(__class__.Foo('2'), tags=['short', 'notfull'])
        r._add_element(__class__.Foo('3'), tags='short')
        r._add_element(__class__.Foo('4'))

        # no filter
        for element, expected_val in zip(r.get_filtered_elements(),
                                         [1, 2, 3, 4]):
            self.assertEqual(element.bar, str(expected_val))

        # short
        fe = zip(r.get_filtered_elements(tags_allowed='short'), [2, 3])
        for element, expected_val in fe:
            self.assertEqual(element.bar, str(expected_val))

        # short, exclude notfull
        fe = zip(r.get_filtered_elements(tags_allowed='short',
                                         tags_excluded='notfull'),
                 [3])
        for element, expected_val in fe:
            self.assertEqual(element.bar, str(expected_val))

        # exclude short
        fe = zip(r.get_filtered_elements(tags_excluded='short'), [1, 4])
        for element, expected_val in fe:
            self.assertEqual(element.bar, str(expected_val))


class Sections(unittest.TestCase):
    """Testing report sections"""
    # pylint: disable=protected-access,missing-function-docstring
    def test_filtered_elements(self):
        """Chld reports are flattened to one list."""

        sut = report.Report('Title')
        sut.heading('Heading 1')
        sut.paragraph('Garak')

        section = sut.section('Heading 2')

        sut.heading('Heading 3')
        sut.paragraph('Quark')

        section.paragraph('Schaftbolzen')

        expected = [
            report.Report.Heading('Title', 0),
            report.Report.Heading('Heading 1', 1),
            report.Report.Paragraph('Garak', None),
            report.Report.Heading('Heading 2', 1),
            report.Report.Paragraph('Schaftbolzen', None),
            report.Report.Heading('Heading 3', 1),
            report.Report.Paragraph('Quark', None),
        ]

        for e, exp in zip(sut.get_filtered_elements(), expected):
            self.assertEqual(e, exp)
