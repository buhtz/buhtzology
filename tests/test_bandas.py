"""Testing behavior of bandas module"""
import unittest
import pathlib
import io
import zipfile
import pandas
import pyfakefs.fake_filesystem_unittest as pyfakefs_ut
from buhtzology import bandas
from . import helper


class BandasGenerally(unittest.TestCase):
    """General tests about bandas module not fitting into other categories"""

    def test_pandas_version(self):
        """Minimum 1.2.5 as pandas version.

        Pandas API is not stable between minor versions. There is no guarantee
        that the productive 'bandas' would work with pandas older then 1.2.5.
        """
        self.assertTrue(pandas.__version__ >= '1.2.5')


class HandleZipFilesFS(pyfakefs_ut.TestCase):
    """Test all relevant parts of `bandas` about handling zip files."""

    ZIP_FILENAME = 'foo.zip'

    def setUp(self):
        helper.pyfakefs_lxml_workaround()
        # cls.setUpClassPyfakefs(allow_root_user=False)
        self.setUpPyfakefs(allow_root_user=False)

        # generate zip file in a fake filesystem
        zp = __class__.ZIP_FILENAME

        csv_content = '\n'.join([
            'Foo;Bar',
            '1;A',
            '2;B'
        ])

        entries_fp = [
            pathlib.Path('data.csv'),
            pathlib.Path('foo') / 'bar' / 'DATA.csv',
            pathlib.Path('other') / 'GoodWill.csv'
        ]

        with zipfile.ZipFile(zp, 'w') as zip_handle:
            # CSV files
            for fp in entries_fp:
                with zip_handle.open(str(fp), 'w') as entry_handle:
                    entry_handle.write(csv_content.encode())

            # Excel file
            fp = pathlib.Path('excel.xlsx')
            with zip_handle.open(str(fp), 'w') as entry_handle:
                df = pandas.DataFrame({'Foo': range(2)})
                bio = io.BytesIO()
                df.to_excel(bio, index=False)
                bio.seek(0)
                entry_handle.write(bio.read())

    def test_validate_csv(self):
        """Validate CSV from zip file."""

        # no exception
        zp = zipfile.Path(__class__.ZIP_FILENAME, 'data.csv')
        result = bandas.validate_csv(zp, columns=2)

        self.assertTrue(result)

    def test_read_and_validate_csv(self):
        """Read and validate CSV from zip file."""

        zp = zipfile.Path(__class__.ZIP_FILENAME, 'data.csv')

        result = bandas.read_and_validate_csv(
            zp,
            specs_and_rules={'Foo': 'int', 'Bar': 'str'})

        self.assertEqual(result.shape, (2, 2))

    def test_read_and_validate_excel(self):
        """Read and validate Excel from a zip file."""

        result = bandas.read_and_validate_excel(
            file_path=zipfile.Path(__class__.ZIP_FILENAME, 'excel.xlsx'),
            specs_and_rules={'Foo': 'int'}
        )

        self.assertEqual(result.columns, ['Foo'])
        self.assertEqual(result.Foo.to_list(), [0, 1])
