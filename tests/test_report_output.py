"""Testing report module"""
import unittest
from unittest import mock
import pathlib
import io
import re
import locale
import pandas
import pyfakefs.fake_filesystem_unittest as pyfakefs_ut
import PIL
import PIL.Image
import docx
import docx.document
import docx.shared
from docx.enum.style import WD_STYLE_TYPE
from buhtzology import report
from . import helper


class GenericDocOutputGenerally(unittest.TestCase):
    """Test base class for report documents"""

    # pylint: disable=protected-access,missing-function-docstring

    def test_init_default(self):
        """Default values."""
        p = pathlib.Path('foo.bar')
        gd = report._GenericDocumentOutput(p)
        self.assertEqual(gd.file_path, p)
        self.assertEqual(gd.tags_allowed, None)
        self.assertEqual(gd.tags_excluded, None)

    def test_init_tags(self):
        """Tag arguments."""
        p = pathlib.Path('foo.bar')
        gd = report._GenericDocumentOutput(p, 'one', 'two')
        self.assertEqual(gd.tags_allowed, ['one'])
        self.assertEqual(gd.tags_excluded, ['two'])

        gd = report._GenericDocumentOutput(p, ['three'], ['four', 'five'])
        self.assertEqual(gd.tags_allowed, ['three'])
        self.assertEqual(gd.tags_excluded, ['four', 'five'])

    @mock.patch('subprocess.run')
    def test_show(self, mock_run):
        """Open document."""
        p = pathlib.Path('foo.bar')
        gd = report._GenericDocumentOutput(p)

        gd.show()

        # called once?
        mock_run.assert_called_once()

        # correct filename used?
        args = mock_run.call_args_list[0][0]
        self.assertEqual(args[0][-1], p)


class TestOutputGeneration(pyfakefs_ut.TestCase):
    """Generate output formats (e.g. docx file) of a report.

    The behavior to test is the generation process itself which should not
    raise an error. The resulting file of the process is not tested in detail.
    """
    @classmethod
    def setUpClass(cls):
        cls.setUpClassPyfakefs()

    @staticmethod
    def _create_report():
        img = io.BytesIO()
        PIL.Image.new('RGB', (10, 10), color=(255, 255, 255)) \
                 .save(img, format='PNG')
        img = img.getvalue()

        r = report.Report(helper.rand_string())
        r.heading('Überschrift')
        r.table_of_contents()
        r.page_break_landscape()
        r.list_of_figures()
        r.list_of_tables()
        r.page_break_portrait()
        sec = r.section('part')
        r.paragraph(helper.rand_string())
        r.table(pandas.DataFrame({
            'Foo': range(5),
            'Bar': list('ABCDE'),
            'Moon': [helper.rand_string() for _ in range(5)]
        }))
        r.page_break()
        r.toggle_orientation()
        r.image(img)
        r.image(img, 0.5, 'caption')
        sec.paragraph(helper.rand_string())

        return r

    def test_docx(self):
        """Docx generation"""

        # add the default docx template form docx package
        # pylint: disable-next=protected-access
        self.fs.add_real_file(docx.api._default_docx_path())

        # nothing is raised
        sut = report.DocxDocument(pathlib.Path('test_docx.docx'))
        # sut.save(r)
        sut.save(self._create_report())

    def test_xlsx(self):
        """Xlsx generation"""

        sut = report.XlsxDocument(pathlib.Path('test_xlsx.xlsx'))

        with self.assertWarns(Warning):
            sut.save(self._create_report())

    def test_markdown(self):
        """Markdown generation"""

        sut = report.MarkdownDocument(pathlib.Path('test_md.md'))
        with self.assertWarns(Warning):
            sut.save(self._create_report())


class Markdown(unittest.TestCase):
    """Markdown class"""

    def test_init(self):
        """Init instance"""

        p = pathlib.Path('foobar.md')
        md = report.MarkdownDocument(p)

        self.assertEqual(md.file_path, p)


class MarkdownFS(pyfakefs_ut.TestCase):
    """Markdown output with extern images"""
    @classmethod
    def setUpClass(cls):
        cls.setUpClassPyfakefs()

        img = PIL.Image.new('RGB', (10, 10), color=(255, 255, 255))
        img.save(pathlib.Path('img.png'))

    def test_image(self):
        """Markdown with image"""
        r = report.Report()
        r.image(pathlib.Path('img.png'))

        p = pathlib.Path('r.md')
        md = report.MarkdownDocument(p)
        md.save(r)

        # the image link with random png file name
        # e.g. ![r_images/filename.png](r_images/filename.png)
        rx = re.compile(r'\!\[r_images[\\|/].*\]\(r_images[\\|/].*.png\)')
        with p.open('r', encoding='utf-8') as handle:
            content = handle.read()
            self.assertTrue(rx.match(content))

    def test_image_with_caption(self):
        """Image with capiton"""
        r = report.Report()
        r.image(pathlib.Path('img.png'), caption='I come from Cyberspace')

        p = pathlib.Path('r.md')
        md = report.MarkdownDocument(p)
        md.save(r)

        with p.open('r', encoding='utf-8') as handle:
            sut = handle.read()

        expect = (
            '![I come from Cyberspace]'
            f'(r_images{pathlib.os.sep}IcomefromCyberspace.png)\n'
            '\n'
            '<sub>I come from Cyberspace</sub>\n'
            '\n'
        )

        self.assertEqual(expect, sut)

    def test_scaled_image_with_caption(self):
        """Scaled image with caption"""

        r = report.Report()
        r.image(
            pathlib.Path('img.png'),
            caption='giants of flesh and steel',
            scale_factor=.1
        )

        p = pathlib.Path('r.md')
        md = report.MarkdownDocument(p)
        md.save(r)

        with p.open('r', encoding='utf-8') as handle:
            sut = handle.read()

        expect = (
            f'<a href="r_images{pathlib.os.sep}'
            'giantsoffleshandsteel.png">'
            '<img alt="giants of flesh and steel" '
            f'src="r_images{pathlib.os.sep}'
            'giantsoffleshandsteel.png" '
            'width="10%" />'
            '</a>\n'
            '\n'
            '<sub>giants of flesh and steel</sub>\n'
            '\n'
        )

        self.assertEqual(expect, sut)

    def test_implemented(self):
        """Implemented elements except image"""
        self.maxDiff = None

        r = report.Report('title', 'subtitle')
        r.heading('heading 1')
        r.heading('heading 4', level=4)
        r.paragraph('foobar')
        r.table(pandas.DataFrame({'A': [1, 2]}))

        p = pathlib.Path('rimplemented.md')
        report.MarkdownDocument(p).save(r)

        expected = [
            '# title',
            '<sub>subtitle</sub>',
            '',
            '## heading 1',
            '##### heading 4',
            'foobar',
            '',
            '|    |   A |',
            '|---:|----:|',
            '|  0 |   1 |',
            '|  1 |   2 |',
            ''
        ]

        with p.open('r', encoding='utf-8') as handle:
            self.assertEqual(
                handle.read(),
                '\n'.join(expected)
            )

    def test_not_implemented(self):
        """Ignored elements"""
        r = report.Report()
        r.page_break()

        p = pathlib.Path('rignored.md')

        with self.assertWarns(Warning):
            report.MarkdownDocument(p).save(r)

        with p.open('r', encoding='utf-8') as handle:
            self.assertEqual(handle.read(), '')


class Xlsx(unittest.TestCase):
    """Excel class"""

    def test_init(self):
        """Init instance"""

        p = pathlib.Path('foobar.xlsx')
        excel = report.XlsxDocument(p)

        self.assertEqual(excel.file_path, p)


class XlsxFS(pyfakefs_ut.TestCase):
    """Markdown output with extern images"""

    # pylint: disable=missing-function-docstring

    def setUp(self):
        helper.pyfakefs_lxml_workaround()
        self.setUpPyfakefs()

    def test_lazy(self):
        r = report.Report()
        r.table(pandas.DataFrame())
        r.heading('foobar')

        p = pathlib.Path('foobar.xlsx')
        xlsx = report.XlsxDocument(p)

        xlsx.save(r)

        self.assertTrue(p.exists())


class Docx(unittest.TestCase):
    """Rundimentary tests for the DocxDocument class.

    Parts that are related to python-docx and its XML content are not tested.
    """

    # pylint: disable=protected-access,missing-function-docstring

    def test_init_default(self):
        p = pathlib.Path('foobar.docx')
        doc = report.DocxDocument(p)

        self.assertEqual(doc._doc, None)
        self.assertEqual(doc._margins, (25, 25, 20, 25))  # "narrow"
        self.assertEqual(doc._autoupdate_fields, True)
        self.assertEqual(doc._header_text, None)
        self.assertEqual(doc._footer_text, None)
        self.assertEqual(doc.tags_allowed, None)
        self.assertEqual(doc.tags_excluded, None)
        self.assertEqual(doc._page_numbering, None)
        self.assertEqual(doc._spell_and_grammar_checking, False)
        self.assertEqual(doc._language, None)
        self.assertEqual(doc._captions_position_table, 1)
        self.assertEqual(doc._captions_position_figure, 1)

    def test_init_modify(self):
        p = pathlib.Path('foobar.docx')
        doc = report.DocxDocument(
            p,
            margins='moderate',
            autoupdate_fields=False,
            tags_allowed='allow',
            tags_excluded='exclude',
            text_header='header',
            text_footer='footer',
            spell_and_grammar_checking=True,
            language='de-DE',
            captions_position_table=0,
            captions_position_figure=0
        )

        self.assertEqual(doc._doc, None)
        self.assertEqual(doc._margins, (12.7, 12.7, 12.7, 12.7))  # moderate
        self.assertEqual(doc._autoupdate_fields, False)
        self.assertEqual(doc._header_text, 'header')
        self.assertEqual(doc._footer_text, 'footer')
        self.assertEqual(doc.tags_allowed, ['allow'])
        self.assertEqual(doc.tags_excluded, ['exclude'])
        self.assertEqual(doc._page_numbering, None)
        self.assertEqual(doc._spell_and_grammar_checking, True)
        self.assertEqual(doc._language, 'de-DE')
        self.assertEqual(doc._captions_position_table, 0)
        self.assertEqual(doc._captions_position_figure, 0)

    def test_page_numbering(self):
        p = pathlib.Path('foobar.docx')
        doc = report.DocxDocument(p)

        self.assertEqual(doc._page_numbering, None)

        doc.page_numbering(
            1,
            prefix='prefix',
            with_pagenum=False,
            infix='infix',
            suffix='suffix')

        self.assertEqual(
            doc._page_numbering,
            {
                'pos': 1,
                'prefix': 'prefix',
                'with_pagenum': False,
                'infix': 'infix',
                'suffix': 'suffix'
            }
        )

    def test_init_docx_document_instance(self):
        p = pathlib.Path('foobar.docx')
        doc = report.DocxDocument(p)

        self.assertEqual(doc._doc, None)

        doc._init_docx_document_instance()

        self.assertIsInstance(doc._doc, docx.document.Document)
        self.assertEqual(len(doc._doc.sections), 1)

    def test_format_numbers_in_cells(self):

        # Use systems current locale for numeric values.
        locale.setlocale(locale.LC_NUMERIC, '')

        # Override that parameters to make sure the locale behave like
        # "German".
        locale._override_localeconv['decimal_point'] = ','
        locale._override_localeconv['thousands_sep'] = '.'

        # Test again and expect numbers in "German" format
        self.assertEqual(
            '1.234,5',
            locale.format_string('%.1f', 1234.5, grouping=True)
        )

        # Now do the real tests
        test_data = [
            # value, decimal_places, expect
            (12, 0, '12'),
            (123456, 0, '123.456'),
            (12345, 0, '12.345'),
            (12.345, 2, '12,35'),
            (12.984, 2, '12,98'),
            (12345678.98765, 3, '12.345.678,988'),
            ((1.2398, 4.564), 2, ('1,24', '4,56')),
            ([1.2398, 4.564], 2, ['1,24', '4,56']),
        ]

        for val, dp, expect in test_data:
            self.assertEqual(
                expect,
                report.DocxDocument._cell_value_to_formated_string(
                    cell_value=val,
                    decimal_places=dp
                )
            )

    def test_none_str_lables(self):
        """Row and column labels not strings.

        Sometimes you have years as integers as label for a row or column."""

        # |          |   (2000, 5) |   (2000, 8) |
        # |:---------|------------:|------------:|
        # | (500, 4) |           1 |           4 |
        # | (500, 8) |           2 |           5 |
        # | (700, 2) |           3 |           6 |
        tab = pandas.DataFrame(
            {
                'idxa': [500, 500, 700],
                'idxb': [4, 8, 2],
                (2000, 5): [1, 2, 3],
                (2000, 8): [4, 5, 6]
            }
        )
        tab = tab.set_index(['idxa', 'idxb'])
        doc = report.DocxDocument(pathlib.Path('no.docx'))
        doc._init_docx_document_instance()
        doc.dataframe_to_table(tab, caption='', note='', autofit=True)

    def test_add_new_style(self):
        """Add a new style"""
        p = pathlib.Path('foobar.docx')
        doc = report.DocxDocument(p)

        self.assertEqual(doc._doc, None)

        doc.add_paragraph_style(
            name='Foobar',
            based_on='Normal',
            font_name='MyFont',
            font_pt=8
        )

        self.assertEqual(len(doc._additional_styles), 1)

        self.assertEqual(
            doc._additional_styles[0],
            {
                'type': WD_STYLE_TYPE.PARAGRAPH,
                'name': 'Foobar',
                'based_on': 'Normal',
                'font_name': 'MyFont',
                'font_pt': 8
            }
        )

    def test_add_style_with_colors(self):
        """Add a new style with colors"""
        p = pathlib.Path('foobar.docx')
        doc = report.DocxDocument(p)

        doc.add_paragraph_style(
            name='Rainbow',
            font_color='blue',
            font_bgcolor='yellow',
        )

        self.assertEqual(len(doc._additional_styles), 1)

        self.assertEqual(
            doc._additional_styles[0],
            {
                'type': WD_STYLE_TYPE.PARAGRAPH,
                'font_color': 'blue',
                'font_bgcolor': 'yellow',
                'name': 'Rainbow',
            }
        )

    @mock.patch('docx.document.Document.save')
    def test_use_new_style(self, patch_save):  # pylint: disable=W0613
        """Use new style in docx document instance"""
        p = pathlib.Path('use_new_style.docx')
        doc = report.DocxDocument(p)

        self.assertEqual(doc._doc, None)

        doc.add_paragraph_style(
            name='MyStyle',
            font_name='Schön',
            font_pt=17
        )

        r = report.Report()
        r.paragraph('Hello World', style='MyStyle')

        doc.save(r)

        # It is not a dict
        self.assertIn('MyStyle', (style.name for style in doc._doc.styles))

        style_in_document = doc._doc.styles['MyStyle']
        style_in_paragraph = doc._doc.paragraphs[-1].style

        for style in [style_in_document, style_in_paragraph]:
            self.assertEqual(style.name, 'MyStyle')
            self.assertEqual(style.font.name, 'Schön')
            self.assertEqual(style.font.size, docx.shared.Pt(17))
