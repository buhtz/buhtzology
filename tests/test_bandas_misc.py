"""Testing behavior of bandas module"""
import unittest
import io
import zipfile
import requests
import pandas
from buhtzology import bandas


class ReorderColumns(unittest.TestCase):
    """Tests about reorder_columns()"""

    # pylint: disable=missing-function-docstring

    def test_simple(self):
        """Simple re-orderings."""
        df = pandas.DataFrame([{col: 0 for col in list('ABCDE')}])
        self.assertEqual(list(df.columns), list('ABCDE'))

        df = bandas.reorder_columns(df, ['D'], 'B')
        self.assertEqual(list(df.columns), list('ABDCE'))

        # column from first half in the second half
        df = pandas.DataFrame([{col: 0 for col in list('ABCDEFGHIJ')}])
        df = bandas.reorder_columns(df, ['C'], 'G')
        self.assertEqual(list(df.columns), list('ABDEFGCHIJ'))

    def test_string_only(self):
        """Second argument is a string not a one entry list."""
        df = pandas.DataFrame([{
            col: 0 for col in ['Foo', 'Bar', 'Apple', 'Bob', 'Chuck']
        }])

        df = bandas.reorder_columns(df, 'Bob', 'Bar')
        self.assertEqual(
            list(df.columns),
            ['Foo', 'Bar', 'Bob', 'Apple', 'Chuck']
        )

    def test_duplicates(self):
        """Duplicated columns names."""
        df = pandas.DataFrame([{col: 0 for col in list('ABCDEF')}])
        df.columns = list('ABBDEF')  # B is duplicate column name

        with self.assertRaises(AttributeError):
            bandas.reorder_columns(df, ['D'], 'A')

        df = pandas.DataFrame([{col: 0 for col in list('ABCDEF')}])
        with self.assertRaises(AttributeError):
            bandas.reorder_columns(df, ['D', 'F', 'D'], 'A')

    def test_missing_columns(self):
        # unexisting move columns
        df = pandas.DataFrame([{col: 0 for col in list('ABCDEF')}])
        with self.assertRaises(KeyError):
            bandas.reorder_columns(df, ['X'], 'C')

        # unexiting behind column
        df = pandas.DataFrame([{col: 0 for col in list('ABCDEF')}])
        with self.assertRaises(ValueError):
            bandas.reorder_columns(df, ['C'], 'X')

    def test_first_column(self):
        df = pandas.DataFrame([{col: 0 for col in list('ABCDEF')}])
        df = bandas.reorder_columns(df, ['E'])
        self.assertEqual(list(df.columns), list('EABCDF'))


class CatsAddMissing(unittest.TestCase):
    """Tests about add_missing_category()"""

    # pylint: disable=missing-function-docstring

    def test_add_missing_default(self):
        cat = pandas.CategoricalDtype(list('ABC'), ordered=True)

        sut = pandas.Series([], dtype=cat)

        # pre-check
        self.assertTrue(sut.cat.ordered)
        self.assertEqual(sut.cat.categories.tolist(), ['A', 'B', 'C'])

        sut = bandas.add_missing_category(sut, '(n.a.)')

        self.assertTrue(sut.cat.ordered)
        self.assertEqual(sut.cat.categories.tolist(),
                         ['A', 'B', 'C', '(n.a.)'])

    def test_add_missing_front(self):
        cat = pandas.CategoricalDtype(list('ABC'), ordered=True)

        sut = pandas.Series([], dtype=cat)

        # pre-check
        self.assertTrue(sut.cat.ordered)
        self.assertEqual(sut.cat.categories.tolist(), ['A', 'B', 'C'])

        sut = bandas.add_missing_category(sut, '(n.a.)', insert_at_end=False)

        self.assertTrue(sut.cat.ordered)
        self.assertEqual(sut.cat.categories.tolist(),
                         ['(n.a.)', 'A', 'B', 'C'])


class ICD(unittest.TestCase):
    """ICD code handling"""

    # pylint: disable=protected-access

    @staticmethod
    def _generate_test_zip():
        """
        """
        chapters = '01;Kapitel A\n02;Kapitel B\n'
        blocks = 'A00;A09;01;Block A\nA01;A19;01;Block B\n'
        codes = (
            '3;N;X;01;A00;A00.-;A00;A00;Cholera;Cholera;;;V;V;1-002;2-001;'
            '3-003;4-002;001;9;9;9999;9999;9;J;J;J;J\n'
            '4;T;X;01;A00;A00.0;A00.0;A000;Cholera durch Vibrio cholerae O:1'
            ', Biovar cholerae;Cholera;Cholera durch Vibrio cholerae O:1'
            ', Biovar cholerae;;P;P;1-002;2-001;3-003;4-002;001;9;9;9999;'
            '9999;9;J;J;J;J\n')

        return ICD._build_zip_buffer(chapters, blocks, codes)

    @staticmethod
    def _generate_test_zip_for_catalog():
        """
        """
        # 3, 4, 5, 9
        codes = (
            ';;;01;A00;A00.-;;;;Code Foo;;;;;;;;;;;;;;;;;;\n'
            ';;;02;D07;D07.3;;;;Code Bar;;;;;;;;;;;;;;;;;;\n'
        )
        chapters = '01;Chapter Foo\n02;Chapter Bar\n'
        blocks = 'A00;A09;01;Block Foo\nD01;D19;02;Block Bar\n'

        return ICD._build_zip_buffer(chapters, blocks, codes)

    @staticmethod
    def _build_zip_buffer(chapters, blocks, codes):
        # credits: https://stackoverflow.com/a/44946732/4865723
        zip_buffer = io.BytesIO()
        with zipfile.ZipFile(
                file=zip_buffer,
                mode='a',
                compression=zipfile.ZIP_DEFLATED,
                allowZip64=False) as zip_handle:
            base_path = 'Klassifikationsdateien/icd10gm2021syst_{}.txt'
            zip_handle.writestr(base_path.format('kapitel'), chapters)
            zip_handle.writestr(base_path.format('gruppen'), blocks)
            zip_handle.writestr(base_path.format('kodes'), codes)

        return zip_buffer

    # # pylint: disable-next=missing-function-docstring
    # def test_icd10gm_as_dataframe(self):
    #     fn = self._generate_test_zip()

    #     chapters, blocks, codes = bandas._get_original_icd10gm(fn)

    #     self.assertEqual(list(chapters.columns), ['CODE', 'TEXT', 'TYP'])
    #     self.assertTrue(chapters.TYP.eq('CHAPTER').all())
    #     self.assertEqual(chapters.CODE.tolist(), ['01', '02'])
    #     self.assertEqual(chapters.TEXT.tolist(), ['Kapitel A', 'Kapitel B'])

    #     self.assertEqual(
    #         list(blocks.columns),
    #         ['START', 'END', 'CHAPTER', 'TEXT', 'TYP', 'CODE'])
    #     self.assertTrue(blocks.TYP.eq('BLOCK').all())
    #     self.assertEqual(blocks.END.tolist(), ['A09', 'A19'])
    #     self.assertEqual(blocks.CODE.tolist(), ['A00-A09', 'A01-A19'])
    #     self.assertEqual(blocks.TEXT.tolist(), ['Block A', 'Block B'])

    #     self.assertEqual(
    #         list(codes.columns),
    #         ['CHAPTER', 'BLOCKS3', 'ICD', 'TEXT', 'TYP'])
    #     self.assertTrue(codes.TYP.eq('CODE').all())
    #     self.assertTrue(codes.CHAPTER.eq('01').all())
    #     self.assertTrue(codes.TEXT.eq('Cholera').all())
    #     self.assertEqual(codes.ICD.tolist(), ['A00.-', 'A00.0'])

    def test_file_not_found(self):
        """File not found."""
        with self.assertRaises(FileNotFoundError):
            bandas.create_icd_catalog('unexisting.file')

    def test_url_not_found(self):
        """URL not found."""
        with self.assertRaises(requests.exceptions.RequestException):
            bandas.create_icd_catalog('https://foobar_')

    def test_create_cataloge(self):
        """Catalog creation."""
        icd = bandas.create_icd_catalog(
            self._generate_test_zip_for_catalog())

        self.assertEqual(icd.shape, (6, 6))
        self.assertEqual(
            list(icd.columns),
            ['CODE', 'TYP', 'CHAPTER', 'BLOCK', 'TEXT', 'CODE_TEXT'])
        self.assertEqual(
            icd.loc[icd.TYP.eq('CHAPTER'), 'CODE_TEXT'].tolist(),
            ['01 Chapter Foo', '02 Chapter Bar'])
        self.assertEqual(
            icd.loc[icd.TYP.eq('BLOCK'), 'CODE_TEXT'].tolist(),
            ['A00-A09 Block Foo', 'D01-D19 Block Bar'])
        self.assertEqual(
            icd.loc[icd.TYP.eq('CODE'), 'CODE_TEXT'].tolist(),
            ['A00.- Code Foo', 'D07.3 Code Bar'])
        self.assertEqual(
            sorted(icd.BLOCK.unique()),
            ['', 'A00-A09', 'D01-D19'])
