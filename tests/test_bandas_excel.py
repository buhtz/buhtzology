"""Test about read and validate data files"""
import pathlib
import pandas
from pandas.testing import assert_frame_equal
import pyfakefs.fake_filesystem_unittest as pyfakefs_ut
from buhtzology import bandas
from . import helper


class ReadAndValidateExcelFS(pyfakefs_ut.TestCase):
    """Using a fake filesystem instead of mocking."""

    @classmethod
    def setUpClass(cls):
        helper.pyfakefs_lxml_workaround()

    def setUp(self):
        self.setUpPyfakefs(allow_root_user=False)

    def test_simple(self):
        """Simple excel."""
        excel_path = pathlib.Path('foobar.xlsx')
        df_init = pandas.DataFrame({'FOO': range(3), 'BAR': list('ABC')})
        df_init.to_excel(excel_path, index=False)

        self.assertTrue(excel_path.exists())

        df = bandas.read_and_validate_excel(
            file_path=excel_path,
            specs_and_rules={'FOO': 'int', 'BAR': 'str'})

        self.assertEqual(df.shape, (3, 2))
        self.assertEqual(list(df.columns), ['FOO', 'BAR'])
        assert_frame_equal(df_init, df, check_dtype=False)

    def test_no_header(self):
        """Excel without header."""
        excel_path = pathlib.Path('foobar.xlsx')
        df_init = pandas.DataFrame({'FOO': range(3), 'BAR': list('ABC')})
        df_init.to_excel(excel_path, header=False, index=False)

        self.assertTrue(excel_path.exists())

        df = bandas.read_and_validate_excel(
            file_path=excel_path,
            specs_and_rules={'FOO': 'int', 'BAR': 'str'},
            no_header_line=True)

        self.assertEqual(df.shape, (3, 2))
        self.assertEqual(list(df.columns), ['FOO', 'BAR'])
        assert_frame_equal(df_init, df)

    def test_no_header_ignore_column_by_index(self):
        """Ignore column in Excel without header."""
        excel_path = pathlib.Path('foobar.xlsx')
        df_init = pandas.DataFrame({'FOO': range(3), 'BAR': list('ABC')})
        df_init.to_excel(excel_path, header=False, index=False)

        self.assertTrue(excel_path.exists())

        df = bandas.read_and_validate_excel(
            file_path=excel_path,
            specs_and_rules={0: None, 'BAR': 'str'},
            no_header_line=True)

        self.assertEqual(df.shape, (3, 1))
        self.assertEqual(list(df.columns), ['BAR'])

    def test_kwargs_skiprows_excel(self):
        """Use skiprows argument."""

        excel_path = pathlib.Path('foobar.xlsx')
        df_init = pandas.DataFrame(
            {'FOO': range(1, 11), 'BAR': list('ABCDEFGHIJ')})
        df_init.to_excel(excel_path, index=False)

        df = bandas.read_and_validate_excel(
            file_path=excel_path,
            specs_and_rules={'FOO': 'int', 'BAR': 'str'},
            skiprows=[2, 3, 4, 8])

        self.assertEqual(df.shape, (6, 2))
        self.assertEqual(list(df.FOO), [1, 5, 6, 7, 9, 10])

    def test_read_none_existing_column(self):
        """Specify more columns as exist."""

        # create CSV file
        excel_path = pathlib.Path('foobar.xlsx')
        df_init = pandas.DataFrame(
            {
                'foo': range(3),
                'bar': range(3),
            }
        )
        df_init.to_excel(excel_path, index=False)

        specs = {
            'foo': 'str',
            'bar': 'str',
            'dontexist': 'str'
        }

        with self.assertRaises(ValueError) as cm:
            bandas.read_and_validate_excel(
                file_path=excel_path,
                specs_and_rules=specs)

        self.assertEqual(
            str(cm.exception),
            "Number of columns mismatch!\n"
            "Expected headers: ['foo', 'bar', 'dontexist']\n"
            "But read        : ['foo', 'bar']")

    def test_ignore_none_existing_column(self):
        """Specify more columns as exist."""

        self.maxDiff = None

        # create Excel file
        excel_path = pathlib.Path('foobar.xlsx')
        df_init = pandas.DataFrame(
            {
                'foo': range(3),
                'bar': range(3),
            }
        )
        df_init.to_excel(excel_path, index=False)

        specs = {
            'foo': 'str',
            'bar': 'str',
            'dontexist': None
        }

        with self.assertRaises(Exception) as cm:
            bandas.read_and_validate_excel(
                file_path=excel_path,
                specs_and_rules=specs)

        self.assertEqual(
            str(cm.exception),
            "Number of columns mismatch!\n"
            "Expected headers: ['foo', 'bar', 'dontexist']\n"
            "But read        : ['foo', 'bar']")

    def test_ignore_but_rules_ok(self):
        """Ignored column with rules."""

        # create Excel file
        excel_path = pathlib.Path('foobar.xlsx')
        df_init = pandas.DataFrame(
            {
                'foo': [1, 2, 3],
                'ignoreme': ['A', 'A', 'A'],
                'bar': [1, 2, 3],
            }
        )
        df_init.to_excel(excel_path, index=False)

        specs = {
            'foo': int,
            'ignoreme': (None, None, {'val': 'A'}),
            'bar': int,
        }

        # no exceptions
        res = bandas.read_and_validate_excel(
            file_path=excel_path,
            specs_and_rules=specs
        )

        expect = pandas.DataFrame({
            'foo': [1, 2, 3],
            'bar': [1, 2, 3]
        })

        assert_frame_equal(res, expect, check_dtype=False)

    def test_ignore_but_rules_violated(self):
        """Ignored column with rules."""

        # create Excel file
        excel_path = pathlib.Path('foobar.xlsx')
        df_init = pandas.DataFrame(
            {
                'foo': range(3),
                'ignoreme': ['A', 'A', 'B'],
                'bar': range(3),
            }
        )
        df_init.to_excel(excel_path, index=False)

        specs = {
            'foo': 'str',
            'ignoreme': (None, None, {'val': 'A'}),
            'bar': 'str',
        }

        with self.assertRaises(ValueError):
            bandas.read_and_validate_excel(
                file_path=excel_path,
                specs_and_rules=specs
            )

    def test_unique_rule_ok(self):
        """Unique columns"""
        # create Excel file
        excel_path = pathlib.Path('unique_rule.xlsx')
        df_init = pandas.DataFrame(
            {
                'foo': [1, 6],
                '': [2, 7],
                'bar': [3, 8],
            }
        )
        df_init.to_excel(excel_path, index=False)

        # assert: nothing raised
        bandas.read_and_validate_excel(
            file_path=excel_path,
            specs_and_rules={
                # regular column
                'foo': (int, None, {'unique': True}),
                # unnamed column
                'Unnamed: 1': (int, None, {'unique': True}),
                # ignored column
                'bar': (None, None, {'unique': True}),
            }
        )

    def test_unique_rule_violated(self):
        """Column is not unique but should be"""
        # create Excel file
        excel_path = pathlib.Path('unique_rule.xlsx')
        df_init = pandas.DataFrame({'foo': [1, 1, 6]})
        df_init.to_excel(excel_path, index=False)

        with self.assertRaises(ValueError):
            bandas.read_and_validate_excel(
                file_path=excel_path,
                specs_and_rules={
                    'foo': (int, None, {'unique': True}),
                }
            )

    def test_len_rule_ok(self):
        """Len rule ok"""
        # create Excel file
        excel_path = pathlib.Path('unique_rule.xlsx')
        df_init = pandas.DataFrame({'foo': [1, 62]})
        df_init.to_excel(excel_path, index=False)

        bandas.read_and_validate_excel(
            file_path=excel_path,
            specs_and_rules={
                'foo': (int, None, {'len': [1, 2]}),
            }
        )

    def test_len_rule_violated(self):
        """Len rule violated"""
        # create Excel file
        excel_path = pathlib.Path('unique_rule.xlsx')
        df_init = pandas.DataFrame({'foo': [1, 62]})
        df_init.to_excel(excel_path, index=False)

        with self.assertRaises(ValueError):
            bandas.read_and_validate_excel(
                file_path=excel_path,
                specs_and_rules={
                    'foo': (int, None, {'len': [1]}),
                }
            )
