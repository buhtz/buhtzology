"""Test about read and validate data files"""
# pylint: disable=too-many-lines
import unittest
from unittest import mock
import inspect
import pathlib
import io
import zipfile
import numpy
import pandas
from pandas.testing import assert_frame_equal
import pyfakefs.fake_filesystem_unittest as pyfakefs_ut
from buhtzology import bandas
from . import helper


def _get_WRONG_file_content():  # pylint: disable=invalid-name
    """Get content of the WRONG file determining the filename by its
    prefix."""

    fp = pathlib.Path.cwd().glob(f'{bandas.FILENAME_PREFIX_WRONG}*')
    fp = list(fp)

    if len(fp) != 1:
        raise ValueError(f'To many WRONG files found. {fp}')

    with fp[0].open('r') as handle:
        return handle.read()


class ValidateCsv(unittest.TestCase):
    """Validating CSV files"""

    # pylint: disable=unused-argument,protected-access

    def test_validate_in_memory_csv_file(self):
        """Simple validation"""
        sut = io.StringIO(inspect.cleandoc("""
                Foo;Bar
                1;2
                2;1
        """))

        rc = bandas.validate_csv(
            sut,
            columns=2)

        self.assertTrue(rc)

    def test_file_path_wrong_csv(self):
        """Construct file path for WRONG fileh."""

        # simple file
        test = pathlib.Path.cwd() / 'data.csv'
        expect = pathlib.Path.cwd() / '_WRONG_data.csv'

        self.assertEqual(
            bandas._construct_file_path_wrong(test),
            expect)

        # with folders
        test = pathlib.Path.cwd() / 'foo' / 'bar' / 'data.csv'
        expect = pathlib.Path.cwd() / 'foo' / 'bar' / '_WRONG_data.csv'

        self.assertEqual(
            bandas._construct_file_path_wrong(test),
            expect)

    @pyfakefs_ut.patchfs
    def test_file_path_wrong_zip(self, fake_fs):
        """Construct file path wrong from zip file."""

        zp = pathlib.Path.cwd() / 'foo.zip'

        # generate zip file in a fake filesystem
        with zipfile.ZipFile(zp, 'w') as zip_handle:
            for fp in [
                    pathlib.Path('data.csv'),
                    pathlib.Path('foo') / 'bar' / 'DATA.csv'
            ]:
                with zip_handle.open(str(fp), 'w') as entry_handle:
                    entry_handle.write(b'foo\nbar')

        # file in root of zip
        test = zipfile.Path(zp, 'data.csv')
        expect = pathlib.Path.cwd() / '_WRONG_foo.zip.data.csv'

        self.assertEqual(
            bandas._construct_file_path_wrong(test),
            expect)

        # file in subfolder of the zip
        test = zipfile.Path(zp, str(pathlib.Path('foo') / 'bar' / 'data.csv'))
        expect = pathlib.Path.cwd() / '_WRONG_foo.zip.foo_bar_data.csv'

        self.assertEqual(
            bandas._construct_file_path_wrong(test),
            expect)

    def test_csv_line_fit_field_length_rules(self):
        """Field lengths rules for one line."""

        # 3 lines fitting all rules
        test_data = [
            ['1', '2', '-9'],
            ['20', '1', '0'],
            ['001', '', '1'],
        ]
        for line in test_data:
            with self.assertRaises(AssertionError):  # workaround
                with self.assertLogs() as logs:
                    result = bandas._csv_line_fit_field_length_rules(
                        line=line,
                        rules={
                            0: [1, 2, 3],
                            1: [0, 1],
                            2: [1, 2]
                        },
                        columns=list('ABC'))
                    self.assertTrue(result)

        base_log_str \
            = 'Error at field index {} (column "{}")! Field rules are {}.'

        # length 3 in column 0 not allowed
        with self.assertLogs() as logs:
            result = bandas._csv_line_fit_field_length_rules(
                line=['001', '', '1'],
                rules={
                    0: [1, 2],
                    1: [0, 1],
                    2: [1, 2]
                },
                columns=list('ABC'))
            self.assertFalse(result)
            self.assertTrue(logs.output[0].endswith(
                base_log_str.format(0, 'A', '[1, 2]')))

        # length 1 in column 1 not allowed
        test_data = [
            ['1', '2', '-9'],
            ['20', '1', '0'],
        ]
        for line in test_data:
            with self.assertLogs() as logs:
                result = bandas._csv_line_fit_field_length_rules(
                    line=line,
                    rules={
                        0: [2, 1],
                        1: [0],
                        2: [1, 2]
                    },
                    columns=list('ABC'))
                self.assertFalse(result, line)
                self.assertTrue(logs.output[0].endswith(
                    base_log_str.format(1, 'B', '[0]')))

        with self.assertLogs() as logs:
            # length 1 in column 1 not allowed
            result = bandas._csv_line_fit_field_length_rules(
                line=['1', '2', '3'],
                rules={1: [0]},
                columns=list('ABC'))
            self.assertFalse(result)

            # length 3 in column 0 not allowed
            # length 0 in column 1 not allowed
            result = bandas._csv_line_fit_field_length_rules(
                line=['001', '', '1'],
                rules={
                    0: [1, 2],
                    1: [1],
                    2: [1, 2]
                },
                columns=list('ABC'))
            self.assertFalse(result)

            # length 3 in column 0 not allowed
            # length 1 in column 2 not allowed
            test_data = [
                ['20', '1', '0'],
                ['001', '', '1'],
            ]
            for line in test_data:
                result = bandas._csv_line_fit_field_length_rules(
                    line=line,
                    rules={
                        0: [1, 2],
                        1: [0, 1],
                        2: [2]
                    },
                    columns=list('ABC'))
                self.assertFalse(result)

            self.assertEqual(len(logs.output), 6)

    def test_length_rules_correct(self):
        """Field lengths correct validated?"""

        # number of fields in header and data rows are fitting
        csv_content = io.StringIO(inspect.cleandoc(
            """
                A;B;C
                1;2;-9
                20;1;0
                001;;1
            """
        ))

        result = bandas.validate_csv(
            file_path=csv_content,
            columns=['A', 'B', 'C'],
            field_length_rules={
                0: [1, 2, 3],
                1: [0, 1],
                2: [1, 2]
            }
        )
        self.assertTrue(result)

    @pyfakefs_ut.patchfs
    def test_length_rules_invalid(self, fake_fs):
        """Field lengths not correct"""

        # number of fields in header and data rows are fitting
        csv_content = io.StringIO(inspect.cleandoc(
            """
                A;B;C
                1;2;-9
                20;1;0
                001;;1
            """
        ))

        # fields violating the rules
        with self.assertRaises(ValueError):
            with self.assertLogs():
                bandas.validate_csv(
                    file_path=csv_content,
                    columns=['A', 'B', 'C'],
                    field_length_rules={
                        0: [1, 2],  # 3 not in
                        1: [0, 1],
                        2: [1, 2]
                    }
                )

        self.assertEqual(
            'A;B;C\n001;;1\n',
            _get_WRONG_file_content())

        csv_content.seek(0)

        with self.assertRaises(ValueError):
            with self.assertLogs():
                bandas.validate_csv(
                    file_path=csv_content,
                    columns=['A', 'B', 'C'],
                    field_length_rules={
                        0: [1, 2, 3],
                        1: [0],  # 1 not int
                        2: [1, 2]
                    }
                )

        self.assertEqual(
            'A;B;C\n1;2;-9\n20;1;0\n',
            _get_WRONG_file_content())

        csv_content.seek(0)

        with self.assertRaises(ValueError):
            with self.assertLogs():
                bandas.validate_csv(
                    file_path=csv_content,
                    columns=['A', 'B', 'C'],
                    field_length_rules={
                        0: [1, 2, 3],
                        1: [1],  # 0 not in
                        2: [1, 2]
                    }
                )
        self.assertEqual(
            'A;B;C\n001;;1\n',
            _get_WRONG_file_content())

        csv_content.seek(0)

        with self.assertRaises(ValueError):
            with self.assertLogs():
                bandas.validate_csv(
                    file_path=csv_content,
                    columns=['A', 'B', 'C'],
                    field_length_rules={
                        0: [1, 2, 3],
                        1: [0, 1],
                        2: [2]  # 1 not int
                    }
                )
        self.assertEqual(
            'A;B;C\n20;1;0\n001;;1\n',
            _get_WRONG_file_content())

    # https://stackoverflow.com/a/70953541/4865723
    @mock.patch('pathlib.Path.unlink', autospec=True)
    def test_unlink_previous_wrong(self, mock_unlink):
        """Delete *wrong.csv files from previous runs."""
        csv_content = io.StringIO('A;B\n1;2')

        bandas.validate_csv(csv_content, columns=['A', 'B'])
        mock_unlink.assert_called_once_with(
            pathlib.Path.cwd() / '_WRONG_in-memory-buffer', missing_ok=True)

    def test_header_valid(self):
        """Valid header specs."""

        # number of fields in header and data rows are fitting
        csv_content = io.StringIO('A;B\n1;2\n2;1\n')

        result = bandas.validate_csv(
            file_path=csv_content, columns=['A', 'B'])
        self.assertTrue(result)

    def test_header_invalid_names(self):
        """Header names not valid."""

        csv_content = io.StringIO('A;B\n1;2\n2;1\n')

        with self.assertRaises(ValueError):
            bandas.validate_csv(
                file_path=csv_content, columns=['A', 'b'])

    def test_header_to_many_data_fields(self):
        """Two header-fields but more data fields."""
        csv_content = io.StringIO(inspect.cleandoc(
            """
                A;B
                1;2;3
                2;1
            """))

        with self.assertRaises(ValueError):
            bandas.validate_csv(file_path=csv_content, columns=['A', 'B'])

    @pyfakefs_ut.patchfs
    def test_header_unspecified(self, fake_fs):
        """Headers present but unspecified.

        No header line cause the first line treated as values.
        """
        csv_content = io.StringIO(inspect.cleandoc("""
            Foo;Bar
            1;2
            2;1
        """))

        with self.assertLogs() as logs:

            # execute
            with self.assertRaises(ValueError) as cm:
                bandas.validate_csv(
                    csv_content,
                    columns=2,
                    field_length_rules={0: 1, 1: 1})

            # test exception message
            self.assertEqual(
                'Found 1 invalid line(s).',
                str(cm.exception)
            )

            # log messages
            full_log_output = '\n'.join(logs.output)
            lines_expect = [
                'Error at field index 0! Field rules are [1].',
                'Error at field index 1! Field rules are [1].',
                "ERROR:buhtzology.bandas:\n1:['Foo', 'Bar']",
            ]
            for line in lines_expect:
                self.assertIn(line, full_log_output, line)

        self.assertTrue(_get_WRONG_file_content(), 'Foo;Bar\n')

    def test_with_headers(self):
        """ Two header-fields but less data fields."""
        csv_content = io.StringIO(inspect.cleandoc(
            """
                A;B
                1
                2;1
            """))

        with self.assertRaises(ValueError):
            # specific headers
            bandas.validate_csv(file_path=csv_content, columns=['A', 'B'])

    @pyfakefs_ut.patchfs
    def test_no_headers(self, fake_fs):
        """No headers line."""

        # number of fields in header and data rows are fitting
        csv_content = io.StringIO(inspect.cleandoc(
            """
                1;2
                2;1
            """))

        # no header set
        result = bandas.validate_csv(
            file_path=csv_content,
            columns=2
        )
        self.assertTrue(result)

        with self.assertRaises(ValueError):
            bandas.validate_csv(csv_content, columns=3)

        with self.assertRaises(AttributeError):
            bandas.validate_csv(csv_content, columns=None)

        with self.assertRaises(TypeError):
            # pylint: disable-next=no-value-for-parameter,missing-kwoa
            bandas.validate_csv(csv_content)

    def test_delimiter_default_semicolon(self):
        """Default delimiter is semicolon.
        """
        # number of fields in header and data rows are fitting
        csv_content = io.StringIO(inspect.cleandoc("""
            A;B
            1;2
        """))

        result = bandas.validate_csv(
            file_path=csv_content, columns=['A', 'B'])
        self.assertTrue(result)

    def test_delimiter_specific_comma(self):
        """Set comma as specific delimiter.
        """

        # number of fields in header and data rows are fitting
        csv_content = io.StringIO(inspect.cleandoc("""
            A,B
            1,2
        """))

        result = bandas.validate_csv(
            file_path=csv_content,
            columns=['A', 'B'],
            delimiter=','
        )
        self.assertTrue(result)

    def test_bad_lines_message(self):
        """Report bad lines."""

        # number of fields in header and data rows are fitting
        # opener = mock.mock_open(
        csv_content = io.StringIO(inspect.cleandoc("""
            A;B
            b;w;x
            1;2
            \x1a
        """))

        # bandas.validate_csv(file_path=csv_content, columns=['A', 'B'])
        # execute
        with self.assertLogs() as logs:
            with self.assertRaises(ValueError) as cm:
                bandas.validate_csv(file_path=csv_content, columns=['A', 'B'])

        # message of the exception
        self.assertEqual(
            str(cm.exception),
            'Found 2 invalid line(s).'
        )

        # log output length
        self.assertEqual(len(logs.output), 2)

        self.assertEqual(
            logs.output[1],
            "ERROR:buhtzology.bandas:\n2:['b', 'w', 'x']\n4:['\\x1a']"
        )

    def test_header_with_unnamed_columns(self):
        """Some columns are not named"""

        csv_content = io.StringIO(
            'foo;;bar;;end\n'
            '1;2;3;4;5\n'
            '6;7;8;9;10')

        result = bandas.validate_csv(
            file_path=csv_content,
            columns=['foo', '', 'bar', '', 'end'])

        self.assertTrue(result)


class ValidateCsvKwargs(unittest.TestCase):
    """kwargs argument while validating CSV"""

    def test_kwargs_nrows_success(self):
        """Read only nrows of lines."""

        # number of fields in header and data rows are fitting
        csv_content = io.StringIO(inspect.cleandoc("""
            A,B
            1,2
            invalid1
            invalid2
        """))

        result = bandas.validate_csv(
            file_path=csv_content,
            columns=['A', 'B'],
            delimiter=',',
            nrows=2)

        self.assertTrue(result)

    def test_kwargs_nrows_error(self):
        """Read only nrows of lines."""
        fp = pathlib.Path('foo.csv')

        # number of fields in header and data rows are fitting
        opener = mock.mock_open(
            read_data=inspect.cleandoc("""
                A,B
                1,2
                invalid1
                invalid2
            """))
        # to check what was written
        writer = helper.MockWriter()
        opener.return_value.write = writer.write

        with mock.patch('pathlib.Path.open', opener):
            with self.assertRaises(ValueError):
                bandas.validate_csv(
                    file_path=fp,
                    columns=['A', 'B'],
                    delimiter=',',
                    nrows=3)

    def test_kwargs_skiprows_success(self):
        """Read only nrows of lines."""

        # number of fields in header and data rows are fitting
        csv_content = io.StringIO(inspect.cleandoc(
            """
                invalid1
                A,B
                1,2
            """))

        result = bandas.validate_csv(
            file_path=csv_content,
            columns=['A', 'B'],
            delimiter=',',
            skiprows=1)

        self.assertTrue(result)

    def test_kwargs_skiprows_error(self):
        """Read only nrows of lines."""
        fp = pathlib.Path('foo.csv')

        # number of fields in header and data rows are fitting
        opener = mock.mock_open(
            read_data=inspect.cleandoc("""
                invalid1
                invalid2
                A,B
                1,2
            """))
        # to check what was written
        writer = helper.MockWriter()
        opener.return_value.write = writer.write

        with mock.patch('pathlib.Path.open', opener):
            with self.assertRaises(ValueError):
                bandas.validate_csv(
                    file_path=fp,
                    columns=['A', 'B'],
                    delimiter=',',
                    skiprows=1)

    def test_kwargs_skipfooter_success(self):
        """Skip invalid line at the end."""

        # number of fields in header and data rows are fitting
        csv_content = io.StringIO(inspect.cleandoc(
            """
                A,B
                1,2
                invalid1
            """
        ))

        result = bandas.validate_csv(
            file_path=csv_content,
            columns=['A', 'B'],
            delimiter=',',
            skipfooter=1)

        self.assertTrue(result)

    def test_kwargs_skipfooter_error(self):
        """Skip only one of two invalid lines at the end."""
        fp = pathlib.Path('foo.csv')

        # number of fields in header and data rows are fitting
        opener = mock.mock_open(
            read_data=inspect.cleandoc("""
                A,B
                1,2
                invalid1
                invalid2
            """))
        # to check what was written
        writer = helper.MockWriter()
        opener.return_value.write = writer.write

        with mock.patch('pathlib.Path.open', opener):
            with self.assertRaises(ValueError):
                bandas.validate_csv(
                    file_path=fp,
                    columns=['A', 'B'],
                    delimiter=',',
                    skipfooter=1)


class ReadAndValidateCsv(unittest.TestCase):
    """
    The 'bandas.read_and_validate_csv()' is a wrapper around
    'bandas.validate_csv()' and 'bandas.validate_dataframe()' which each
    have their own TestCase ('TestValidateCsv' and 'TestValidateDataFrame').

     - datentypen
     - missing values recogniten
     - len rules (is validate_csv() called?)
     - value rules

    TODO
        - {'col': ('dtype', -9, {'len': [], 'val': []}}

    """

    # pylint: disable=unused-argument,too-many-public-methods

    def test_on_bad_lines_default(self):
        """Default value of on_bad_lines should fit to pandas"""

        pandas_default = inspect.signature(pandas.read_csv) \
            .parameters['on_bad_lines'].default

        sut = inspect.signature(bandas.read_and_validate_csv) \
            .parameters['on_bad_lines'].default

        self.assertEqual(pandas_default, sut)

    def test_valid_specs(self):
        """Specs and rules used by pandas.read_csv().

        Test if the ``specs_and_rules`` resulting in the correct formatted
        data frame returned by  `pandas.read_csv()`.
        Arguments not checked but the resulting data frame.
        """

        # is read by `pandas.read_csv()`
        file_path = io.StringIO(
            inspect.cleandoc("""
                A;b;_C_;ä
                1;2;3;foobar
            """))

        specs_and_rules = {
            'A': 'Int16',
            'b': 'Int32',
            '_C_': 'float',
            'ä': 'string',
        }
        # no exceptions
        res = bandas.read_and_validate_csv(
            file_path=file_path,
            specs_and_rules=specs_and_rules)

        self.assertEqual(len(res), 1)
        self.assertEqual(list(res.columns), ['A', 'b', '_C_', 'ä'])
        self.assertEqual(res['A'].dtype.name, 'Int16')
        self.assertEqual(res['b'].dtype.name, 'Int32')
        self.assertEqual(res['_C_'].dtype.name, 'float64')
        self.assertEqual(res['ä'].dtype.name, 'string')

    @mock.patch('buhtzology.bandas.validate_csv')
    @mock.patch('pandas.read_csv')
    def test_valid_specs_ignore_column(self, mock_read_csv, mock_vcsv):
        """Headers to read.

        Test if correct arguments are given to `validate_csv()` and
        `pandas.read_csv()`.
        """
        mock_vcsv.return_value = True
        file_path = io.StringIO(
            inspect.cleandoc("""
                A;b;_C_;ä
                1;2;3;foobar
            """))

        specs_and_rules = {
            'A': 'Int16',
            'b': None,
            '_C_': 'float',
            'ä': 'string',
        }

        mock_read_csv.return_value = pandas.DataFrame(
            {
                'A': [1],
                'b': [0],
                '_C_': [3],
                'ä': ['foobar']
            }
        )
        # no exceptions
        bandas.read_and_validate_csv(
            file_path=file_path, specs_and_rules=specs_and_rules)

        # test arguments of `validate_csv()`
        mock_vcsv.assert_called_once()
        kwargs = mock_vcsv.call_args_list[0][1]
        self.assertEqual(kwargs['columns'], ['A', 'b', '_C_', 'ä'])

        # test arguments of `pandas.read_csv()`
        mock_read_csv.assert_called_once()
        kwargs = mock_read_csv.call_args_list[0][1]
        self.assertEqual(kwargs['header'], 0)

    @mock.patch('buhtzology.bandas.validate_csv')
    @mock.patch('pandas.read_csv')
    def test_field_length_rules(self, mock_read_csv, mock_vcsv):
        """Field length rules arguments.

        If `validate_csv()` was called with correct field length rules.
        """
        mock_vcsv.return_value = True

        specs_and_rules = {
            'quartal': ('str', None, {'len': 5}),
            'anonym': ('str', None, {'len': 7}),
            'lanr_anom': None,
            'pzn': 'str',
            'anzahl': 'str',
            'einloesedat': ('str', None, {'len': 8}),
            'brutto': 'str',
            'netto': 'str'
        }

        mock_read_csv.return_value = pandas.DataFrame(
            {
                'quartal': [],
                'anonym': [],
                'lanr_anom': [],
                'pzn': [],
                'anzahl': [],
                'einloesedat': [],
                'brutto': [],
                'netto': [],
            }
        )
        # no exceptions
        bandas.read_and_validate_csv(
            file_path=io.StringIO(''), specs_and_rules=specs_and_rules)

        mock_vcsv.assert_called_once()
        kwargs = mock_vcsv.call_args_list[0][1]
        self.assertEqual(kwargs['field_length_rules'],
                         {0: [5], 1: [7], 5: [8]})

    @mock.patch('buhtzology.bandas.validate_csv')
    @mock.patch('pandas.read_csv')
    def test_csv_columns(self, mock_read_csv, mock_vcsv):
        """CSV columns arguments.

        If `validate_csv()` was called with correct columns argument.
        """
        mock_vcsv.return_value = True

        specs_and_rules = {
            'quartal': ('str', None, {'len': 5}),
            'anonym': ('str', None, {'len': 7}),
            'lanr_anom': None,
            'pzn': 'str',
            'anzahl': 'str',
            'einloesedat': ('str', None, {'len': 8}),
            'brutto': 'str',
            'netto': 'str'
        }

        mock_read_csv.return_value = pandas.DataFrame(
            {
                'quartal': [],
                'anonym': [],
                'lanr_anom': [],
                'pzn': [],
                'anzahl': [],
                'einloesedat': [],
                'brutto': [],
                'netto': [],
            }
        )
        # no exceptions
        bandas.read_and_validate_csv(
            file_path=io.StringIO(''), specs_and_rules=specs_and_rules)

        mock_vcsv.assert_called_once()
        kwargs = mock_vcsv.call_args_list[0][1]
        self.assertEqual(kwargs['columns'],
                         ['quartal', 'anonym', 'lanr_anom', 'pzn', 'anzahl',
                          'einloesedat', 'brutto', 'netto'])

    @mock.patch('buhtzology.bandas.validate_dataframe')
    @mock.patch('buhtzology.bandas.validate_csv')
    @mock.patch('pandas.read_csv')
    def test_csv_without_header(self, mock_read_csv, mock_vcsv, mock_vdf):
        """CSV columns arguments when no header present.

        If `validate_csv()` and `pandas.read_csv()` was called with correct
        columns related arguments.
        """
        mock_vcsv.return_value = True

        specs_and_rules = {
            'A': 'str',
            'b': 'str',
            '_C_': None,
            'ä': 'str',
        }

        # no exceptions
        bandas.read_and_validate_csv(
            file_path=io.StringIO(''),
            no_header_line=True,
            specs_and_rules=specs_and_rules)

        # test arguments of validate_csv()
        mock_vcsv.assert_called_once()
        args, kwargs = mock_vcsv.call_args_list[0]
        self.assertEqual(args, ())
        self.assertEqual(kwargs['columns'], 4)

        # test arguments of pandas.read_csv()
        mock_read_csv.assert_called_once()
        args, kwargs = mock_read_csv.call_args_list[0]
        self.assertEqual(args, ())
        self.assertEqual(kwargs['header'], None)
        self.assertEqual(kwargs['names'], ['A', 'b', '_C_', 'ä'])

    @mock.patch('buhtzology.bandas.validate_csv')
    def test_validate_csv_raises(self, mock_vcsv):
        """Exception from validate_csv().

        See docstring of test_validate_dataframe_raises()."""
        e = helper.rand_string()
        mock_vcsv.side_effect = ValueError(e)
        with self.assertRaisesRegex(ValueError, e):
            bandas.read_and_validate_csv(
                pathlib.Path('foo.csv'), {'A': 'Int16'})

    def test_no_specs_and_rules(self):
        """Missing specs_and_rules parameter."""
        with self.assertRaises(AttributeError):
            bandas.read_and_validate_csv(
                pathlib.Path('foo.csv'), specs_and_rules=None)

    @mock.patch('buhtzology.bandas.validate_csv')
    @mock.patch('pandas.read_csv')
    def test_delimiter(self, mock_read_csv, mock_vcsv):
        """Delimiter."""
        mock_vcsv.return_value = True

        specs_and_rules = {
            'quartal': ('str', None, {'len': 5}),
            'anonym': ('str', None, {'len': 7}),
            'lanr_anom': None,
            'pzn': 'str',
            'anzahl': 'str',
            'einloesedat': ('str', None, {'len': 8}),
            'brutto': 'str',
            'netto': 'str'
        }

        mock_read_csv.return_value = pandas.DataFrame(
            {
                'quartal': [],
                'anonym': [],
                'lanr_anom': [],
                'pzn': [],
                'anzahl': [],
                'einloesedat': [],
                'brutto': [],
                'netto': [],
            }
        )
        # no exceptions
        bandas.read_and_validate_csv(
            file_path=io.StringIO(''),
            specs_and_rules=specs_and_rules,
            delimiter=','
        )

        # how was validate_csv() called?
        mock_vcsv.assert_called_once()
        kwargs = mock_vcsv.call_args_list[0][1]
        self.assertEqual(kwargs['columns'],
                         ['quartal', 'anonym', 'lanr_anom', 'pzn', 'anzahl',
                          'einloesedat', 'brutto', 'netto'])
        self.assertEqual(kwargs['delimiter'], ',')

        # how was read_csv() called?
        mock_read_csv.assert_called_once()
        kwargs = mock_read_csv.call_args_list[0][1]
        self.assertEqual(kwargs['sep'], ',')

    def test_considere_length_of_missings(self):
        """Consider field length of missing values as length rules.

        The field length rules shouldn't apply to the missing values.
        """
        # create CSV file
        csv_content = io.StringIO('foo\n123\n12345\n-9\n123456\n')

        # no exceptions
        res = bandas.read_and_validate_csv(
            file_path=csv_content,
            specs_and_rules={
                'foo': ('str', [-9], {'len': [3, 5, 6]}),
            }
        )

        self.assertEqual(res.shape, (4, 1))

    def test_ignore_but_rules(self):
        """Ignored column with rules."""
        csv_content = io.StringIO('foo;len;val\n1;1;1\n2;23;2')

        # no exceptions
        res = bandas.read_and_validate_csv(
            file_path=csv_content,
            specs_and_rules={
                'foo': 'str',
                'len': (None, None, {'len': [1, 2]}),
                'val': (None, None, {'val': [1, 2]})
            }
        )

        expect = pandas.DataFrame({'foo': ['1', '2']})

        assert_frame_equal(res, expect, check_dtype=False)

    @pyfakefs_ut.patchfs
    def test_ignore_but_len_rule_violated(self, fake_fs):
        """Ignored column with len rule violated."""

        csv_content = io.StringIO('foo;len;val\n1;1;1\n2;23;2')

        with self.assertRaises(ValueError) as cm:
            bandas.read_and_validate_csv(
                file_path=csv_content,
                specs_and_rules={
                    'foo': 'str',
                    'len': (None, None, {'len': [2]}),
                    'val': None
                }
            )

        self.assertEqual(str(cm.exception), 'Found 1 invalid line(s).')
        self.assertEqual(_get_WRONG_file_content(), 'foo;len;val\n1;1;1\n')

    def test_ignore_but_value_rule_violated(self):
        """Ignored column with value rule violated.

        Remember: No WRONG file because value rules are checked with pandas.
        """

        csv_content = io.StringIO('foo;len;val\n1;1;1\n2;23;2')

        with self.assertRaises(ValueError) as cm:
            bandas.read_and_validate_csv(
                file_path=csv_content,
                specs_and_rules={
                    'foo': 'str',
                    'len': None,
                    'val': (None, None, {'val': [2]})
                }
            )

        self.assertTrue('do not match the value rules' in str(cm.exception))

    def test_read_none_existing_column(self):
        """Specify more columns as exist."""

        specs = {
            'foo': 'str',
            'bar': 'str',
            'dontexist': 'str'
        }
        self._generic_to_much_columns(specs, "['foo', 'bar', 'dontexist']")

    def test_ignore_none_existing_column(self):
        """Specify more columns as exist."""

        specs = {
            'foo': 'str',
            'bar': 'str',
            'dontexist': None
        }
        self._generic_to_much_columns(specs, "['foo', 'bar', 'dontexist']")

    def test_unspecified_column(self):
        """Forget a column."""

        specs = {
            'bar': 'str',
        }
        self._generic_to_much_columns(specs, "['bar']")

    def _generic_to_much_columns(self, specs, expect_string):

        csv_content = io.StringIO('foo;bar\n2;1\n4;3\6;5\n')

        with self.assertRaises(ValueError) as cm:
            bandas.read_and_validate_csv(
                file_path=csv_content,
                specs_and_rules=specs)

        self.assertEqual(
            str(cm.exception),
            "Header of the CSV file do not match with "
            "expected headers!\n"
            f"Expected: {expect_string}\n"
            "But read: ['foo', 'bar']"
        )

    def test_kwargs_nrows(self):
        """Try nrows argument."""

        csv_content = io.StringIO(inspect.cleandoc(
            """
                FOO;BAR
                1;A
                2;B
                3;C
                4;D
                5;E
                6;F
                7;G
                8;H
                9;I
                10;J
            """))

        df = bandas.read_and_validate_csv(
            file_path=csv_content,
            specs_and_rules={'FOO': 'int', 'BAR': 'str'},
            nrows=5)

        self.assertEqual(df.shape, (5, 2))
        self.assertEqual(list(df.FOO), list(range(1, 6)))

    @pyfakefs_ut.patchfs
    def test_kwargs_skipfooter(self, fake_fs):
        """Use skipfooter argument."""

        csv_content = io.StringIO(inspect.cleandoc(
            """
                A;B;C
                1;2;3
                4;5;6
                ----- some invalid line ---
                and one more
            """))

        # Without skipping the rows at the end
        with self.assertRaises(ValueError):
            bandas.read_and_validate_csv(
                file_path=csv_content,
                specs_and_rules={'A': int, 'B': int, 'C': int})

        csv_content.seek(0)

        # Skipping the invalid lines at the end
        df = bandas.read_and_validate_csv(
            file_path=csv_content,
            specs_and_rules={'A': int, 'B': int, 'C': int},
            skipfooter=2
        )

        self.assertEqual(df.shape, (2, 3))
        self.assertEqual(list(df.columns), list('ABC'))

    @pyfakefs_ut.patchfs
    def test_kwargs_skiprows_no_header(self, fake_fs):
        """Use skiprows argument."""

        csv_content = io.StringIO(inspect.cleandoc(
            """
                ----- some invalid line ---
                and one more
                1;2;3
                4;5;6
            """
        ))

        # Without skipping the rows
        with self.assertRaises(ValueError):
            bandas.read_and_validate_csv(
                file_path=csv_content,
                specs_and_rules={'A': int, 'B': int, 'C': int},
                no_header_line=True)

        csv_content.seek(0)

        # With skipping the invalid lines in the beginning
        df = bandas.read_and_validate_csv(
            file_path=csv_content,
            specs_and_rules={'A': int, 'B': int, 'C': int},
            no_header_line=True,
            skiprows=2)

        self.assertEqual(df.shape, (2, 3))
        self.assertEqual(list(df.columns), list('ABC'))

    @pyfakefs_ut.patchfs
    def test_kwargs_skiprows_with_header(self, fake_fs):
        """Use skiprows argument."""

        csv_content = io.StringIO(inspect.cleandoc(
            """
                ----- some invalid line ---
                and one more
                A;B;C
                1;2;3
                4;5;6
            """
        ))

        # Without skipping the rows
        with self.assertRaises(ValueError):
            bandas.read_and_validate_csv(
                file_path=csv_content,
                specs_and_rules={'A': int, 'B': int, 'C': int})

        csv_content.seek(0)

        # With skipping the invalid lines in the beginning
        df = bandas.read_and_validate_csv(
            file_path=csv_content,
            specs_and_rules={'A': int, 'B': int, 'C': int},
            skiprows=2)

        self.assertEqual(df.shape, (2, 3))
        self.assertEqual(list(df.columns), list('ABC'))

    def test_missings_is_empty_string(self):
        """Empty in string columns is missing/na."""

        # create CSV file
        csv_content = io.StringIO('foo;bar;three\n1;;a\n2;val;b\n3;;c')

        # no exceptions
        res = bandas.read_and_validate_csv(
            file_path=csv_content,
            specs_and_rules={
                'foo': 'int',
                'bar': 'str',
                'three': 'str'
            }
        )

        # Column "bar" has 2 nan/missing values
        self.assertEqual(
            res.bar.value_counts(dropna=False)[numpy.nan],
            2
        )

    def test_unnamed_columns(self):
        """Single columns are without names but others are"""
        # 2nd and 4th columns has no name
        csv_content = io.StringIO(
            'foo;;bar;;end\n'
            '1;2;3;4;5\n'
            '6;7;8;9;10')

        df = bandas.read_and_validate_csv(
            file_path=csv_content,
            specs_and_rules={
                'foo': int,
                -1: int,
                'bar': int,
                -2: int,
                'end': int,
            }
        )

        self.assertEqual(
            df.columns.to_list(),
            ['foo', 'Unnamed: 1', 'bar', 'Unnamed: 3', 'end'])

    def test_value_rules_for_unnamed_columns(self):
        """Value rules for unnamed columns"""
        csv_content = io.StringIO(
            'foo;;bar;;end\n'
            '1;2;3;4;5\n'
            '6;7;8;9;10')

        # assert: nothing raised
        bandas.read_and_validate_csv(
            file_path=csv_content,
            specs_and_rules={
                'foo': (int, None, {'val': [1, 6]}),
                -1: int,
                'bar': int,
                -2: (int, None, {'val': [4, 9]}),
                'end': int,
            }
        )

        # violate a rule
        with self.assertRaises(ValueError):
            bandas.read_and_validate_csv(
                file_path=csv_content,
                specs_and_rules={
                    'foo': (int, None, {'val': [1, 6]}),
                    -1: int,
                    'bar': int,
                    -2: (int, None, {'val': [4]}),
                    'end': int,
                }
            )

    def test_unique_rules_valid(self):
        """Unique columns"""
        csv_content = io.StringIO(
            'foo;;bar\n'
            '1;2;3\n'
            '6;7;8')

        # assert: nothing raised
        bandas.read_and_validate_csv(
            file_path=csv_content,
            specs_and_rules={
                # regular column
                'foo': (int, None, {'unique': True}),
                # unnamed column
                -1: (int, None, {'unique': True}),
                # ignored column
                'bar': (None, None, {'unique': True}),
            }
        )

    def test_unique_rules_invalid(self):
        """Unique columns"""
        csv_content = io.StringIO(
            'foo;;bar\n'
            '1;2;3\n'
            '1;2;3')

        with self.assertRaises(ValueError) as cm:
            bandas.read_and_validate_csv(
                file_path=csv_content,
                specs_and_rules={
                    # regular column
                    'foo': (int, None, {'unique': True}),
                    # unnamed column
                    -1: (int, None, {'unique': True}),
                    # ignored column
                    'bar': (None, None, {'unique': True}),
                }
            )

        self.assertIn('foo', str(cm.exception))
        self.assertIn('Unnamed: 1', str(cm.exception))
        self.assertIn('bar', str(cm.exception))
