"""
    Helper functions and data for unit tests.

    Some functions helping with repetitive task while running unittests.
"""
import random
import string


def pyfakefs_lxml_workaround():
    """
    We need to make sure that "openpyxl" (used by "pandas" for reading
    and writing Excel files) does not use the "lxml" package. Because
    that package does not work together with "pyfakefs".
    See: https://codeberg.org/buhtz/buhtzology/issues/28
    See: https://github.com/jmcgeheeiv/pyfakefs/issues/713
    See: https://foss.heptapod.net/openpyxl/openpyxl/-/issues/1886
    """

    # pylint: disable=import-outside-toplevel

    import os

    os.environ['OPENPYXL_LXML'] = 'False'

    # reload pandas (including openpyxl)
    import importlib
    import sys

    importlib.reload(sys.modules['pandas'])


def rand_string(max_length=25, min_length=1):
    """Create a string with random uppercase characters and digits. Default
    length is 25.

    Args:
        max_length (int): Max length of the string (default: 25).
        min_length (int): Min string length (default: 1)

    Returns:
        (string): The created random string.
    """
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _
                   in range(random.randint(min_length, max_length)))


# def pep8_conform(filename_or_class):
#     """
#     """
#     if type(filename_or_class) is str:
#         # It is a filename.
#         filename = filename_or_class
#     else:
#         # It is a class. Get its filename.
#         filename = inspect.getfile(filename_or_class)

#     # E266 to many leading '#' ignored because '##' is used in doxygen to
#     # describe class/object attributes.
#     pep = pycodestyle.Checker(filename, ignore=['E266', 'W503'])

#     return pep.check_all() == 0


class MockWriter:
    """Collect all written data.

    https://stackoverflow.com/a/70868549/4865723
    """

    # pylint: disable=missing-function-docstring

    def __init__(self):
        self.contents = ''

    def write(self, data):
        self.contents += data

    def pop_contents(self):
        to_pop = self.contents
        self.contents = ''
        return to_pop
