# Changelog

All notable changes to this project will be documented in this file.
It tries to follow [Common Changelog](https://common-changelog.org) and
[Keep a Changelog](https://keepachangelog.com). The project also
adheres to [Semantic Versioning](https://semver.org).

## Unreleased

### Infrastructure
- The project now follows the OneFlow branching model
- Package metadata is centralized and redundance eliminated ([#49](https://codeberg.org/buhtz/buhtzology/issues/49))
- Tests for PEP8 & Co centralized and additionally using "ruff" ([#49](https://codeberg.org/buhtz/buhtzology/issues/49))

### Breaking change
- Stausberg: "disease of appendix" excluded by default as an artifact

### Fixed
- analy: Thousand and decimal delemiters ([#110](https://codeberg.org/buhtz/buhtzology/issues/110))
- bandas: Loading data frames from `DataContainer` is significant faster
- bandas: Ignore Excel column but check its rule ([#124](https://codeberg.org/buhtz/buhtzology/issues/124))
- bandas: Guaranteed order of data frame names in `DataContainer`
- bandas: Report convert Series into DataFrames but error about it
- bandas: Detect column type modifications in container
- bandas: Handling of missing values in `cut_bins()` ([#56](https://codeberg.org/buhtz/buhtzology/issues/56))
- bandas: Correct use of `kwargs` in `cut_bins_via_interval_count()` ([#116](https://codeberg.org/buhtz/buhtzology/issues/116))
- bandas: Remove unused labels in `cut_bins()`
- bandas: Misuse of `DataFrameGroupBy.sum()` ([#59](https://codeberg.org/buhtz/buhtzology/issues/59))
- bandas: Validate number of provided labels when cutting bins via list
  of intervals ([#63](https://codeberg.org/buhtz/buhtzology/issues/63))
- bandas: Validate CSV take skiprows, skipfooter and nrows into account ([#101](https://codeberg.org/buhtz/buhtzology/issues/101))
- Minor fixes ([#50](https://codeberg.org/buhtz/buhtzology/issues/50), [#61](https://codeberg.org/buhtz/buhtzology/issues/61), [#75](https://codeberg.org/buhtz/buhtzology/issues/75))

### Added
- Use config file `buhtzology.toml` if present ([#127](https://codeberg.org/buhtz/buhtzology/issues/127))
- analy: Sort columns by default in analy.aggregate()
- bandas: New rule "unique" to validate read data ([#119](https://codeberg.org/buhtz/buhtzology/issues/119))
- bandas: Describe `DataContainer` with markdown tables
- buhtzology: Break paragraphs via line breaks
- report: Docx with user defined paragraph styles
- report: Docx reports use thousend separators and decimal delimiters based
  on `locale` ([#58](https://codeberg.org/buhtz/buhtzology/issues/58))
- report: Reports with sections ([#66](https://codeberg.org/buhtz/buhtzology/issues/66))
- report: Temporary (not persistent) heading levels ([#85](https://codeberg.org/buhtz/buhtzology/issues/85))
- report: Customizable depth of TOC ([#54](https://codeberg.org/buhtz/buhtzology/issues/54))
- bandas: `on_bad_lines` callback/behavior while reading csv files
- bandas: Delete DataFrame's from DataContainers ([#76](https://codeberg.org/buhtz/buhtzology/issues/76))
- bandas: Optionally remove unused labels in cut_bins() ([#60](https://codeberg.org/buhtz/buhtzology/issues/60))
- [ ] bandas: cut_bins() throws an Exception if not all values are covered
  by bins/intervals ([#55](https://codeberg.org/buhtz/buhtzology/issues/55))
- bandas: Add category label for missing values to categories ([#81](https://codeberg.org/buhtz/buhtzology/issues/81))
- bandas: Use in-memory CSV files ([#90](https://codeberg.org/buhtz/buhtzology/issues/90))
- bandas: Store data container with modified permanent names ([#79](https://codeberg.org/buhtz/buhtzology/issues/79))
- bandas: Load data container complete via `load_all()` ([#78](https://codeberg.org/buhtz/buhtzology/issues/78))
- bandas: `DataContainer` support folder mode ([#77](https://codeberg.org/buhtz/buhtzology/issues/77))
- bandas: Reuse and copy dataframe files on `DataContainer.store()` with
  unmodified dataframes ([#87](https://codeberg.org/buhtz/buhtzology/issues/87), [#88](https://codeberg.org/buhtz/buhtzology/issues/88))
- bandas: `DataContainer.keep_row_count()` monitors row count.
- analy: Added module analy ([#82](https://codeberg.org/buhtz/buhtzology/issues/82), [#98](https://codeberg.org/buhtz/buhtzology/issues/98))
- analy: Summarize implementation (related to [#102](https://codeberg.org/buhtz/buhtzology/issues/102))
- bandas: Container with status indicators while load/save ([#121](https://codeberg.org/buhtz/buhtzology/issues/121))
- report: Warn about long tables (+200 rows) in DocxDocuments
- bandas: Batch reading and validating of data files ([#99](https://codeberg.org/buhtz/buhtzology/issues/99))

## [0.1.1] - 2023-04-19
Hotfix release because of an invalid `pyproject.toml`.

### Fixed
- Name of author variable in `pyproject.toml`
- Branch name computation in `hey_doc.py`

## [0.1.0] - 2023-01-01
### Changed
- Moved project setup from setup.cfg to pyproject.toml.
- Refactor reading and validation of CSV files.
- Improve documentation.
- Logging handlers can be deactivated ([#47](https://codeberg.org/buhtz/buhtzology/issues/47)).
### Added
- Generate format independent reports and export as `docx`, `xlsx` and `md`.
- Use multiprocessing for tasks on dataframes.
- Read and validate Excel files (#c9ead5590f).
- Data container for `pandas.DataFrames`.
- Validate ignored columns ([#38](https://codeberg.org/buhtz/buhtzology/issues/38)).
- Cut values in bins and label them.
- Setup logging (#d9bf995f59).
- Read and validate CSV/Excel files as entries in ZIP files via `zipfile.Path` ([#32](https://codeberg.org/buhtz/buhtzology/issues/32)).
- Stausberg comorbidity score.
- Add more documentation.
- Read the Docs setup.
### Removed
- Generated HTML doc files which are now at ReadTheDocs.org.
- Deprecated code (`_oldbuhtology.py`).
### Fixed
- PyFakeFS based unittest with Excel files ([#28](https://codeberg.org/buhtz/buhtzology/issues/28), [PyFakeFS #713](https://codeberg.org/buhtz/buhtzology/issues/713)).
- Stausberg calculation ([#43](https://codeberg.org/buhtz/buhtzology/issues/43), [#39](https://codeberg.org/buhtz/buhtzology/issues/39), [#40](https://codeberg.org/buhtz/buhtzology/issues/40)).
- Handling missing values by KHQ module with newer pandas version (#c1bea4a65d).

## [0.0.1beta1] - 2022-07-17
A beta release.

### Changed
- Use src-Layout for git repository.

### Added
- Read and validate CSV files.
- PyDoctor documentation.

### Removed
- Doxygen documentation.

### Fixed
- Multiple fixes of code and documentation.

## [Initial commit] - 2022-01-24
At this date was the first initial commit. Not a release.

[0.1.1]: https://codeberg.org/buhtz/buhtzology/releases/tag/v0.1.1
[0.1.0]: https://codeberg.org/buhtz/buhtzology/releases/tag/v0.1.0
[0.0.1beta1]: https://codeberg.org/buhtz/buhtzology/releases/tag/v0.0.1beta1
[Initial commit]: https://codeberg.org/buhtz/buhtzology/commit/8d1a80031ff467ae7b2b1985edaee97693dcf749
