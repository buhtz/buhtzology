#!/usr/bin/env python3
"""Generate the documentation with pydoctor.

The applications metadata is extracted from the project itself and given to
pydoctor via commandline arguments. The script assume a project folder in
src-layout and need to run in the docs folder.

Some Examples
=============

See this `restructuredext_demo project
<https://github.com/twisted/pydoctor/blob/-/docs/restructuredtext_demo/__init__.py>`_.

Code in an example box
----------------------

Example:

    .. python::

        # Data for the container organized in a dict.
        dataframes = {
            'Foo': pandas.DataFrame(),
            'Bar': pandas.DataFrame()
        }


Inline markup
-------------
You can write ``verbatim`` (inline code) and also `automatic link` it. Text
can be *italic* or **bold**.

Boxes (Admonitions)
-------------------

.. warning :: One line warning.

.. warning ::

    This is a multi line warning.
    With more than onen line.

.. note ::

   You can use note, tip, hint, important, warning, attention, caution,
   danger, error.

   Also possible is ``.. raw:: html`` or ``.. admonition:: Purple``.

"""

import sys
import os
import pathlib
import importlib
import logging
try:
    import tomllib
except ImportError:
    # before Python 3.11
    import tomli as tomllib

import pydoctor
import pydoctor.driver

DOC_FOLDER = 'docs'
DOC_FORMAT = 'google'

# See: https://github.com/twisted/pydoctor/issues/763
INTERSPHINX = {
    'python': 'https://docs.python.org/3/objects.inv',
    'pandas': 'https://pandas.pydata.org/docs/objects.inv',
    'seaborn': 'https://seaborn.pydata.org/objects.inv',
    'numpy': 'https://numpy.org/doc/stable/objects.inv',
    'matplotlib': 'https://matplotlib.org/stable/objects.inv',
    'PIL': 'https://pillow.readthedocs.io/en/stable/objects.inv',
    'openpyxl': 'https://openpyxl.readthedocs.io/en/stable/objects.inv',
    'docx': 'https://python-docx.readthedocs.io/en/stable/objects.inv',
    'webcolors': 'https://webcolors.readthedocs.io/en/stable/objects.inv',
}

logging.getLogger().setLevel(logging.INFO)


def current_git_branch(pkg_path: pathlib.Path) -> str:
    """Return the current branch name.

    A special case is when that script runs on a Read The Docs instance. In
    that case the branch name is extracted from environment variables.

    Credits: https://stackoverflow.com/a/51224861/4865723

    For possible improvement see a similar function in project "Back In Time"
    in the file "common/diagnostics.py".
    """
    if 'READTHEDOCS' in os.environ:
        if os.environ['READTHEDOCS_VERSION_TYPE'] == 'branch':
            return os.environ['READTHEDOCS_VERSION_NAME']  # real branch name

    # Regular
    HEAD_path = pkg_path / '.git' / 'HEAD'

    # branch name
    with HEAD_path.open('r') as handle:
        content = handle.read()

        if content.startswith('ref: refs/heads/'):
            return content.replace('ref: refs/heads/', '')

        return content.split('/')[-1].strip()


# Make sure the script runs inside the docs folder.
cwd = pathlib.Path.cwd()
if cwd.name != DOC_FOLDER:
    raise RuntimeError(
        f'The script need to run inside the {DOC_FOLDER} folder!')

# Get name of project and name of package
prj_path = cwd.parent
pkg_name = prj_path / 'src'
pkg_name = list(filter(lambda p: 'egg-info' not in str(p), pkg_name.glob('*')))
if len(pkg_name) != 1:
    raise ValueError(f'Something unexpected in src-folder. {pkg_name}')
pkg_path = pkg_name[0]
pkg_name = pkg_path.name
prj_name = cwd.parent.name

# Read pyproject.toml as dict
with (prj_path / 'pyproject.toml').open('rb') as handle:
    project_toml = tomllib.load(handle)

# get more project meta-data
pkg = importlib.import_module(pkg_name)

# URLs
website = project_toml['project']['urls']['homepage']

# viewsource-base based on current branch
viewsource_base = '{}/src/branch/{}'.format(
    project_toml['project']['urls']['repository'],
    current_git_branch(prj_path)
)

# Build commandline arguments
args = [
    f'--project-name={pkg.__name__} ({pkg.__version__})',
    # f'@ {pkg.__version_date__})',
    f'--project-version={pkg.__version__}',
    f'--project-url={website}',
    f'--html-viewsource-base={viewsource_base}',
    f'--docformat={DOC_FORMAT}',
    '--make-html',
    '--process-types',
    '--warnings-as-errors',
    '--project-base-dir=..',
    # '-v', '-v', '-v',
    '--html-output=html']

for ext_api in INTERSPHINX:
    args.append(f'--intersphinx={INTERSPHINX[ext_api]}')

args.append(f'{pkg_path}')

logging.info('Run pydoctor with arguments:\n\t{}'.format('\n\t'.join(args)))

# Run pydoctor
rc = pydoctor.driver.main(args)

if rc == 0:
    logging.info('Success.')
else:
    logging.error('Something went wrong. Check the output.')

sys.exit(rc)
